#MousePlanner API

##Introduction
MousePlanner is a tool for planning your trip to Walt Disney World, it will help you through a wizard to define your vacation details and all you need to plan to enjoy your visit. MousePlanner will generate the best schedule based on your vacation dates, settings, and interests so you will have a general idea of the activities you can do and when to do them.

MousePlanner tool consist in two parts, the front-end which is built as an AngularJS application and the back-end which is a web service (Laravel API application).
The Angular app shows the user the planning workflow that includes the dashboard, wizard and the mouse plan (schedule), and the API stores the user data within the database, and also generates and send a schedule for a user. We need to protect our API so only authorized applications is the only application sending requests to the API, besides the security concerns, the API needs to know the authenticated user to read and write data for that particular user. That way we are going to solve these two issues is by using token authentication.

MousePlanner API is built using Laravel 5.3 which offers for authentication the library [Laravel Passport](https://laravel.com/docs/5.3/passport). Since we are working with a first-party application, we are using Password grant access.


API documentation: https://mouseplanner.docs.apiary.io/#reference

###Laravel
Laravel is a PHP framework to build APIs and full web applications. It's branded as "the PHP framework for web artisans". Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

###Laravel Passport
APIs typically use tokens to authenticate users and do not maintain session state between requests.
OAuth2 is a security framework that controls access to protected areas of an application, and it's mainly used to control how different clients consume an API ensuring they have the proper permissions to access the requested resources.

Laravel Passport is a full OAuth2 server implementation; it was built to make it easy to apply authentication over an API for laravel-based web applications.

###Client
When Laravel Passport is installed a password client is created, the client has a client ID and client secret. These values will be used when requesting access tokens from other applications.

###Scope
It's a permission to access certain data, or perform a certain action.
MousePlanner users can read and post their own data.
Site admin can read all data.
Passport::tokensCan([
    'site-admin' => 'Read all plans and Configuration',
    'work-plan' => 'CRUD in user plan',
]);

###Grant
It's the method used to get an access token, in MousePlanner case, we will be using Password grant access.
In order to get an access token, we must send a POST request (/oauth/token) with the user credentials and MousePlannerAPI will respond with the token, that will be used in the rest of the requests.

###Access token
That's the token a client app needs to communicate with the server (API). MousePlanner API will generate and send access tokens to the client when requested.

##Authorizing MousePlanner

###Requesting Tokens
When the users begin to use MousePlanner app they will be requested to provide a username/email and password.
With that data in place, the app will call to the API in order to get an access token, which will allow the client to consume the rest of the [endpoints](http://docs.mouseplanner.apiary.io/#)


To get the access token, the client makes a POST request to `/oath/token` with following form-data params:

```json
{
  "grant_type": "password",
  "client_id": "the one created by Passport",
  "client_secret": "the one created by Passport",
  "username": "from users table",
  "password": "from users table",
  "scope": "*"
}
```

The answer is going to be a JSON object with the following keys:
```json
{
    "token_type": "Bearer",
    "expires_in": 3155673600,
    "access_token": "eyJ0eXAiOiJKV1QiL....",
    "refresh_token": "XslU/K6lFZShiGxF1dPyC4ztIXBx9W1g..."
}
```
The client will store both tokens - access_token and refresh_token - in a cookie.

###Protecting Routes
This is the most important step in the authentication solution, it is basically the purpose of getting the access token, to protect the endpoints.
Passport includes an authentication guard that validates access tokens on incoming requests.
Every route requires a valid access token; therefore, the guard middleware needs to be added in the constructor of the route controller:
```
public function __construct(){
  $this->middleware('auth:api');
}
```

####Consuming endpoints
The client requests need to provide its access token as part of the header:

```javascript
$response = $client->request('GET', '/api/user', [
  'headers' => [
    'Accept' => 'application/json',
    'Authorization' => 'Bearer '.$accessToken,
  ],
]);
```

##Server Configuration
We need php7, git, nginx and mysql. Follow these instructions:
[Laravel deploy](https://asked.io/how-to-install-php-7-x--nginx-1-9-x---laravel-5-x).

After installing Laravel, you may need to configure some permissions. Directories within the storage and the bootstrap/cache directories should be writable by your web server or Laravel will not run. Before changing the file permissions, execute this command git config core.fileMode false so you don't get git issues, that way git will ignore the file permissions.

Copy the .envexample file to .env
At APP_ENV write the name of the environment you are setting (Ex. staging, production, local).
Execute php artisan key:generate to set APP_KEY if is not already set already.
Set at JSON_OUTPUT the path where you have the scraper json files. It will depend on where you are executing the scraper service, which is needed for mouseplanner: [scraper](https://bitbucket.org/bixal/scraper/overview).

Execute composer install so you are able to execute mouseplanner API.

##Passport
Execute the following command to install Passport:
`php artisan passport:install`

##Database
Create a database named mouseplanner
```
mysql -udbuser -p
create database mouseplanner;
grant usage on *.* to newuser@localhost identified by 'password';
grant all privileges on mouseplanner.* to newuser@localhost;
flush privileges;
\q
```

Be sure to note the chosen password and edit the .env file of your project, changing the values for the database details:
```
DB_HOST=localhost
DB_DATABASE=mouseplanner
DB_USERNAME=newuser
DB_PASSWORD=password
```

A quick way to check that the database is correctly set up is to run these scripts, from the project's root:

`php artisan migrate`

If the migration is successful, you're all set!

The last step will be seed the db
`php artisan db:seed`

If you need to clean your db
`php artisan migrate:refresh --seed`

### Database Note:
Since MousePlannerAPI is using SQLite for the unit test, make sure your migration files works in that environment.
For example, do not add not null fields without a default value.
Also, when [dropping multiple columns](https://laravel.com/docs/5.3/migrations#dropping-columns) add them in a array or in separate migrations.

##Unit Test
Laravel has support for testing with **PHPUnit** included out of the box and a `phpunit.xml` file is already setup for MousePlannerAPI.
If you are using Homestead VM execute `phpunit` and the tests will executed.
Otherwise execute it like `vendor/bin/phpunit`

If you see this error:
`[Illuminate\Database\QueryException] could not find driver (SQL: select * from sqlite_master where type = 'table ' and name = migrations)`

Install `php7.1-sqlite3`, if you are in Ubuntu run this command:
`sudo apt-get install php7.1-sqlite3`

To add a new test execute:
`php artisan make:test UserTest`

For more information about testing in Laravel check this link [testing](https://laravel.com/docs/5.3/testing).
