<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Mousage Routes
|--------------------------------------------------------------------------|
*/

Route::get('vacations/{vacation}', 'MousageController@indexVacationMousages');

Route::get('mouseplans/{mouseplan}', 'MousageController@indexMousePlanMousages');

Route::get('days/{day}', 'MousageController@indexDayMousages');

Route::get('blocks/{block}', 'MousageController@indexBlockMousages');
