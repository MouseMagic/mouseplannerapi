<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => ['role:admin']], function() {
    Route::get('users',  'UserController@index');
});

Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::post('users/register/', 'Auth\RegisterController@register');
Route::get('users/register/verify/{confirmation_code}', 'Auth\RegisterController@confirm_registration');
Route::post('users/logout', 'UserController@logout');

Route::post('auth/google', 'Auth\SocialiteController@authGoogleUser');
Route::post('auth/facebook', 'Auth\SocialiteController@authFacebookUser');
Route::get('auth/{provider}/client-id', 'Auth\SocialiteController@clientId');
Route::post('stripe/web-hooks', 'StripeController@webHooksHandler')->middleware('stripe.webhook.verify');

Route::group(['middleware' => ['role:admin|client|agent']], function() {

    Route::get('users/me/', 'UserController@showLoggedUser');

    Route::resource('users', 'UserController', ['except' => [
        'create', 'edit', 'index'
    ]]);

    Route::resource('vacations', 'VacationController', ['except' => [
        'create', 'edit'
    ]]);

    Route::resource('trips', 'TripController', ['except' => [
        'create', 'edit'
    ]]);

    Route::resource('mouseplans', 'MousePlanController', ['except' => [
        'create', 'edit', 'destroy'
    ]]);

    Route::resource('block_types', 'BlockTypeController', ['only' => [
        'index', 'show'
    ]]);

    Route::resource('checklists', 'ChecklistController', ['only' => [
        'index', 'update', 'destroy'
    ]]);

    Route::resource('travelers', 'TravelerController', ['except' => [
        'index', 'create', 'edit'
    ]]);

    Route::resource('experiences', 'ExperienceController', ['except' => [
        'create', 'edit'
    ]]);

    Route::resource('notes', 'NoteController', ['except' => [
        'index', 'create', 'edit'
    ]]);

    Route::resource('fastpass_reservations', 'FastPassController', ['except' => [
        'index', 'create', 'edit', 'show'
    ]]);

    Route::resource('resorts', 'ResortController', ['only' => ['index']]);

    Route::resource('celebrations', 'CelebrationController', ['only' => ['index']]);

    Route::resource('prefix_titles', 'PrefixTitleController', ['only' => ['index']]);

    Route::resource('traveler_heights', 'TravelerHeightController', ['only' => ['index']]);

    Route::resource('accessibilities', 'AccessibilityController', ['only' => ['index']]);

    Route::resource('booked_places', 'BookedPlaceController', ['only' => ['index']]);

    Route::resource('dining_plans', 'DiningPlansController', ['only' => ['index']]);

    Route::resource('parks_visit_interests', 'ParksVisitInterestController', ['only' => ['index']]);

    Route::resource('water_park_interests', 'WaterParkInterestController', ['only' => ['index']]);

    Route::resource('orlando_hotels', 'OrlandoHotelController', ['only' => ['index']]);

    Route::resource('thrill_levels', 'ThrillLevelController', ['only' => ['index']]);

    Route::resource('paces', 'PaceController', ['only' => ['index']]);

    Route::resource('budgets', 'BudgetController', ['only' => ['index']]);

    Route::resource('time_flexibilities', 'TimeFlexibilityController', ['only' => ['index']]);

    Route::resource('naps', 'NapController', ['only' => ['index']]);

    Route::resource('interests', 'InterestController', ['only' => ['index']]);

    Route::resource('characters', 'CharacterController', ['only' => ['index']]);

    Route::resource('dining_preferences', 'DiningPreferenceController', ['only' => ['index']]);

    Route::resource('parks', 'ParkController', ['only' => ['index']]);

    Route::resource('custom_facets', 'CustomFacetController', ['except' => [
        'create', 'edit'
    ]]);

    Route::resource('experience_exceptions', 'ExperienceExceptionController', ['except' => [
        'create', 'edit'
    ]]);

    Route::resource('parks_exceptions', 'ParksExceptionController', ['except' => [
        'create', 'edit'
    ]]);

    Route::resource('blocks', 'BlockController', ['except' => [
        'index', 'create', 'edit'
    ]]);

    Route::get('dashboard/trips/user', 'DashboardController@indexTrips');

    Route::get('dashboard/trips', 'DashboardController@indexUserVacations');

    Route::get('dashboard/trips/{trip}/plans', 'DashboardController@indexPlans');

    Route::post('dashboard/vacations/{vacation}/duplicate', 'DashboardController@duplicatePlan');

    Route::delete('dashboard/vacations/{vacation}/delete', 'DashboardController@deletePlan');

    Route::get('wizard/vacations/{vacation}/mouseplans', 'WizardController@showPlan');

    Route::get('wizard/vacations/{vacation}/parks', 'WizardController@indexParks');

    Route::post('wizard/vacations/{vacation}/mouseplans', 'WizardController@storePlan');

    Route::get('wizard/vacations/{vacation}/travelers', 'WizardController@indexVacationTravelers');

    Route::post('wizard/vacations/{vacation}/travelers', 'WizardController@storeTraveler');

    Route::post('wizard/vacations/{vacation}/wishlist', 'WizardController@setDefaultWishlist');

    Route::post('wizard/vacations/{vacation}/parks/{park}/wishlist', 'WizardController@setParkDefaultWishlist');

    Route::get('wizard/wishlist', 'WizardController@getDefaultWishlist');

    Route::get('wizard/vacations/{vacation}/experiences', 'WizardController@getExperiencesByDates');

    Route::get('wizard/default_experiences', 'WizardController@getDefaultExperiences');

    Route::get('restaurants/parks/{park}', 'RestaurantController@indexParkRestaurants');

    Route::get('restaurants/resorts/{resort}', 'RestaurantController@indexResortRestaurants');

    Route::get('parks/{park}/vacation/{vacation}/schedule', 'ParksInfoController@showParkHours');

    Route::post('mousages/{mousage}/checklist', 'MousageController@convertToChecklistItem');

    Route::put('mousages/{mousage}', 'MousageController@update');

    Route::get('fastpass_reservations/days/{day}', 'FastPassController@indexDayFastPass');

    Route::get('accessibilities/map_values', 'AccessibilityController@indexMappedValues');

    Route::get('accessibilities/map_values/{access_type}', 'AccessibilityController@showMappedValue');

    Route::get('ages/map_values', 'AgeController@indexMappedValues');

    Route::get('ages/map_values/{age}', 'AgeController@showMappedValue');

    Route::get('characters/map_values', 'CharacterController@indexMappedValues');

    Route::get('characters/map_values/{character}', 'CharacterController@showMappedValue');

    Route::get('traveler_heights/map_values', 'TravelerHeightController@indexMappedValues');

    Route::get('traveler_heights/map_values/{height}', 'TravelerHeightController@showMappedValue');

    Route::get('thrill_levels/map_values', 'ThrillLevelController@indexMappedValues');

    Route::get('thrill_levels/map_values/{thrill_level}', 'ThrillLevelController@showMappedValue');

    Route::get('interests/map_values', 'InterestController@indexMappedValues');

    Route::get('interests/map_values/{interest}', 'InterestController@showMappedValue');

    Route::get('parks/map_values', 'ParkController@indexMappedValues');

    Route::get('parks/map_values/{park}', 'ParkController@showMappedValue');

    Route::get('stripe/api-key', 'StripeController@getApiKey');
    Route::get('stripe/plans', 'StripeController@plans');
    Route::get('stripe/coupon/{coupon}', 'StripeController@couponDetails');

    Route::post('stripe/subscribe', 'StripeController@subscribe');

});



