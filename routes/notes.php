<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Note Routes
|--------------------------------------------------------------------------|
*/

Route::get('days/{day}', 'NoteController@indexDayNotes');

Route::get('blocks/{block}', 'NoteController@indexBlockNotes');
