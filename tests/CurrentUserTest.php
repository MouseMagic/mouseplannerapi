<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CurrentUserTest extends TestCase
{
  /**
   * Get current authenticated user.
   *
   * @return void
   */
  public function testCurrentUser() {
    $user = factory(App\User::class)->make([
      'id' => 1,
    ]);

    $this->actingAs($user, 'api')
         ->json('GET', '/api/users/me')
         ->seeJsonStructure([
           'id',
           'firstname',
           'lastname',
           'email',
           'confirmed',
         ]);
  }
}
