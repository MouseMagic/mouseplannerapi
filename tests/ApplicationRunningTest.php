<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ApplicationRunningTest extends TestCase {
  /**
   * A basic functional test example.
   *
   * @return void
   */
  public function testRoot() {
      $this->visit('/')
           ->see('Laravel Mouseplanner API');
  }
}
