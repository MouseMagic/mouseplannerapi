<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class GetVacationsTest extends TestCase
{
  use DatabaseMigrations;
  /**
   * Get current user's list of vacations.
   *
   * @return void
   */
  public function testGetUserVacations() {
    $user = factory(App\User::class)->create();

    $trip = factory(App\Models\Trip::class)->create([
      'user_id' => $user->id,
    ]);

    $vacations = factory(App\Models\Vacation::class, 3)->create([
      'trip_id' => $trip->id,
    ]);

    $this->actingAs($user, 'api')
         ->json('GET', '/api/vacations/')
         ->seeJsonStructure([
           '*' => [
             'id',
             'name',
             'trip_id',
             'user_id',
           ]
         ]);
  }

  /**
   * Get vacation 1 with all its attributes.
   *
   * @return void
   */
  public function testGetVacation() {
    $this->withoutMiddleware();
    $user = factory(App\User::class)->create([
      'id' => 1,
    ]);

    $trip = factory(App\Models\Trip::class)->create([
      'user_id' => $user->id,
    ]);

    $vacation = factory(App\Models\Vacation::class)->create([
      'trip_id' => $trip->id,
    ]);

    $this->actingAs($user, 'api')
        ->json('GET', '/api/vacations/1')
        ->seeJsonStructure([
          'startTravelDate',
          'endTravelDate',
          'departureDate',
          'arrivalDate',
          'beginDayTime',
          'endDayTime',
          'celebration_types',
          'travelers',
          'interest_types',
          'characters',
          'dining_preferences',
          'parks',
          'experiences',
          'mouse_plan'
        ]);
  }
}
