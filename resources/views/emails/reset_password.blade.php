<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>MousePlanner</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <style type="text/css">
            th{padding:0;}
            a{
                outline:none;
                text-decoration:underline;
            }
            a:hover{text-decoration:none !important;}
            a[x-apple-data-detectors]{color:inherit !important; text-decoration:none !important;}
            .active:hover{opacity:0.8;}
            .active{
                -webkit-transition:all 0.3s ease;
                -moz-transition:all 0.3s ease;
                -ms-transition:all 0.3s ease;
                transition:all 0.3s ease;
            }
            a img{border:none;}
            table td{mso-line-height-rule:exactly; border-collapse:collapse;}
            .ExternalClass, .ExternalClass a, .ExternalClass span, .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div{line-height:inherit;}
            .address span{color:inherit !important; border-style:none !important;}
            @media only screen and (max-width:900px) {
                /* default style */
                table[class="flexible"]{width:100% !important;}
                table[class="table-center"]{float:none !important; margin:0 auto !important; width:auto !important;}
                *[class="hide"]{display:none !important; width:0 !important; height:0 !important; padding:0 !important; font-size:0 !important; line-height:0 !important;}
                span[class="db"]{display:block !important;}
                td[class~="aligncenter"]{text-align:center !important;}
                td[class~="img-flex"] img{width:100% !important; height:auto !important;}
                tr[class="table-holder"]{display:table !important; width:100% !important;}
                th[class="thead"]{display:table-header-group !important; width:100% !important;}
                th[class="tfoot"]{display:table-footer-group !important; width:100% !important;}
                th[class~="flex"]{display:block !important; width:100% !important;}
                td[class="indent-null"]{padding:0 !important;}
            }
            @media only screen and (max-width:600px) {
                strong[class="title-mobile"]{font-size: 26px !important;}
                td[class="indent-mobile"]{font-size: 18px}
                td[class="txt-copyright"]{font-size: 12px !important; line-height: 1.25 !important; padding:0 20px 15px !important;}
            }
        </style>
    </head>
    <body style="margin:0;padding:0;-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; background: #ffffff url({{ $message->embed(public_path('images/email/bg-page.gif')) }}) repeat-x;" bgcolor="#89f0f5">
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background: #89f0f5 url({{ $message->embed(public_path('images/email/bg-page.gif')) }}) repeat-x;" bgcolor="#ffffff">
                    <tr>
                        <td valign="top" align="center" style="padding: 5% 20px 21px;">
                            <table class="flexible" width="420" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="min-width:320px; max-width: 420px;margin: 0 auto; text-align: center; -webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 15px;">
                                <tr>
                                    <td style="padding:31px 15px 20px;text-align: center;"><a href="{{ config('app.frontend_url') }}"><img border="0" src="{{ $message->embed(public_path('images/email/MousePlanner_logo.png')) }}" width="153" height="87" alt="Mouse Planner" style="vertical-align:top;" /></a></td>
                                </tr>
                                <tr>
                                    <td class="indent-mobile" style="padding:6% 20px 0;font:20px/1.3 Helvetica, sans-serif; color:#323232;">
                                        <strong class="title-mobile" style="font-size: 30px; line-height: 1.2">Password Reset</strong><br><br>
                                        You told us you'd like to<br>reset your password.<br><br>
                                        <a href="{{ $reset_link }}" style="text-decoration:none; color: #323232;">Click the link below to reset it.</a><br><br>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 0 0 33px; text-align: center;">
                                        <table width="235" cellpadding="0" cellspacing="0" border="0" style="margin: 0 auto">
                                            <tr>
                                                <td align="center" bgcolor="#32c373" style="padding:13px 25px 14px; font:22px Helvetica, sans-serif; color:#ffffff;-moz-border-radius:8px; -webkit-border-radius: 8px; border-radius: 8px;">
                                                    <a href="{{ $reset_link }}" style="color:#fff; text-decoration:none;">Reset Password</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="txt-copyright" style="padding:9px 20px 15px; font:14px/22px Lato, Helvetica, sans-serif; color:#b0afaf; text-align: center">
                            Copyright @2018 MousePlanner. All rights reserved.<br>
                            You are receiving this email because you opted in via our website.
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <!-- fix for gmail start -->
        <tr>
            <td class="hide">
                <table class="flexible" width="420" cellpadding="0" cellspacing="0" style="width:420px !important;">
                    <tr><td style="min-width:420px; font-size:0; line-height:0;">&nbsp;</td></tr>
                </table>
            </td>
        </tr>
        <tr><td style="line-height:0;"><div style="display:none; white-space:nowrap; font:15px/1px courier;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div></td></tr>
        <!-- fix for gmail end -->
    </table>
    </body>
    </html>