<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>MousePlanner</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <style type="text/css">
            th{padding:0;}
            a{
                outline:none;
                text-decoration:underline;
            }
            a:hover{text-decoration:none !important;}
            a[x-apple-data-detectors]{color:inherit !important; text-decoration:none !important;}
            .active:hover{opacity:0.8;}
            .active{
                -webkit-transition:all 0.3s ease;
                -moz-transition:all 0.3s ease;
                -ms-transition:all 0.3s ease;
                transition:all 0.3s ease;
            }
            a img{border:none;}
            table td{mso-line-height-rule:exactly; border-collapse:collapse;}
            .ExternalClass, .ExternalClass a, .ExternalClass span, .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div{line-height:inherit;}
            .address span{color:inherit !important; border-style:none !important;}
            @media only screen and (max-width:800px) {
                /* default style */
                table[class="flexible"]{width:100% !important;}
                table[class="table-center"]{float:none !important; margin:0 auto !important; width:auto !important;}
                *[class="hide"]{display:none !important; width:0 !important; height:0 !important; padding:0 !important; font-size:0 !important; line-height:0 !important;}
                span[class="db"]{display:block !important;}
                td[class~="aligncenter"]{text-align:center !important;}
                td[class~="img-flex"] img{width:100% !important; height:auto !important;}
                tr[class="table-holder"]{display:table !important; width:100% !important;}
                th[class="thead"]{display:table-header-group !important; width:100% !important;}
                th[class="tfoot"]{display:table-footer-group !important; width:100% !important;}
                th[class~="flex"]{display:block !important; width:100% !important;}
                td[class="indent-null"]{padding:0 !important;}
                td[class="indent-mobile"]{padding: 11% 20px 21px !important;}
                td[class="bg-col"] {width:0  !important}
                td[class="bg-col-2"]{width:20px !important}
                td[class="bg-col"] img,
                td[class="bg-col-2"] img{display: none !important}
                td[class="col-mobile"]{width: 100% !important;}
            }
            @media only screen and (max-width:600px) {
                strong[class="title-mobile"]{font-size: 26px !important;}
                td[class="txt-copyright"]{font-size: 12px !important; line-height: 1.25 !important; padding:20px 20px 15px !important;}
            }
        </style>
    </head>
    <body style="margin:0;padding:0;-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; background: #ffffff url({{ $message->embed(public_path('images/email/bg-page.gif')) }}) repeat-x;" bgcolor="#89f0f5">
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background: #89f0f5 url({{ $message->embed(public_path('images/email/bg-page.gif')) }}) repeat-x;" bgcolor="#ffffff">
                    <tr>
                        <td class="indent-mobile" style="padding: 100px 20px 21px; text-align: center">
                            <table class="flexible" width="100%" cellpadding="0" cellspacing="0" border="0" style="min-width:320px; max-width: 1150px;margin: 0 auto; text-align: center; -webkit-border-radius: 15px;-moz-border-radius: 15px;border-radius: 15px;">
                                <tr>
                                    <td class="bg-col" width="178"><img border="0" src="{{ $message->embed(public_path('images/email/magic-wand-1-part.png')) }}" width="178" height="530" alt="Mouse Planner" style="vertical-align:bottom;" /></td>
                                    <td valign="top" class="col-mobile">
                                        <table width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="-webkit-border-radius: 15px;-moz-border-radius: 15px;border-radius: 15px;">
                                            <tr>
                                                <td class="bg-col-2" width="175"><img border="0" src="{{ $message->embed(public_path('images/email/magic-wand-2-part.png')) }}" width="175" height="441" alt="Mouse Planner" style="vertical-align:top;" /></td>
                                                <td>
                                                    <table width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                                                        <tr>
                                                            <td style="padding:25px 15px 20px;text-align: center;"><a href="{{ config('app.frontend_url') }}"><img border="0" src="{{ $message->embed(public_path('images/email/MousePlanner_logo.png')) }}" width="153" height="87" alt="Mouse Planner" style="vertical-align:top;" /></a></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding:3.5% 0 0.5%;font:20px/1.3 Helvetica, sans-serif; color:#323232; text-align: center;">
                                                                <strong class="title-mobile" style="font-size: 30px; line-height: 1.2">Ready to start planning?</strong><br><br>
                                                                You're minutes away from your custom Disney vacation plan. Click <a href="{{ $confirmation_url }}" style="text-decoration:none; color: #32c373;">here</a> or on the button below to confirm your email and let's get started!<br><br>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 0 0 33px; text-align: center;">
                                                                <table width="235" cellpadding="0" cellspacing="0" border="0" style="margin: 0 auto">
                                                                    <tr>
                                                                        <td align="center" bgcolor="#32c373" style="padding:13px 25px 14px; font:22px Helvetica, sans-serif; color:#ffffff;-moz-border-radius:8px; -webkit-border-radius: 8px; border-radius: 8px;">
                                                                            <a href="{{ $confirmation_url }}" style="color:#fff; text-decoration:none;">Confirm Email</a>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td class="bg-col-2" width="175" style="padding: 50px 0 0;">&nbsp;</td>
                                            </tr>
                                        </table>
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="txt-copyright" style="padding:28px 20px 15px; font:14px/22px Lato, Helvetica, sans-serif; color:#b0afaf; text-align: center">
                                                    Copyright @2018 MousePlanner. All rights reserved.<br>
                                                    You are receiving this email because you opted in via our website.
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="bg-col bg-col-mob" width="178" style="padding: 50px 0 0;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- fix for gmail start -->
                    <tr>
                        <td class="hide">
                            <table class="flexible" width="980" cellpadding="0" cellspacing="0" style="width:980px !important;">
                                <tr><td style="min-width:980px; font-size:0; line-height:0;">&nbsp;</td></tr>
                            </table>
                        </td>
                    </tr>
                    <tr><td style="line-height:0;"><div style="display:none; white-space:nowrap; font:15px/1px courier;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div></td></tr>
                    <!-- fix for gmail end -->
                </table>
            </td>
        </tr>
    </table>
    </body>
    </html>