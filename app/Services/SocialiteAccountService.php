<?php

namespace App\Services;
use App\Models\SocialiteAccount;
use App\User;
use Laravel\Socialite\Contracts\User as ProviderUser;
use Stripe\Stripe;

class SocialiteAccountService
{


    public function createOrGetUser(ProviderUser $providerUser, $provider, $email = null)
    {
        $user = null;
        $account = SocialiteAccount::where('provider', $provider)
            ->where('provider_user_id', $providerUser->getId())
            ->first();

        if ($account) {
            $user = $account->user;
        }

        if (!$user) {
            $userEmail = $providerUser->getEmail() ? $providerUser->getEmail() : $email;
            $userEmail = trim($userEmail);

            if (!$userEmail) {
                throw new \Exception('Please provide your email.', 91);
            }

            $fullname = trim($providerUser->getName());

            if (!$account ) {
                $account = new SocialiteAccount([
                    'fullname' => $fullname,
                    'provider_user_id' => $providerUser->getId(),
                    'provider' => $provider
                ]);
            }


            $user = User::whereEmail($userEmail)->first();

            if (!$user) {
                $firstname = '';
                $lastname = '';

                $nameComponents = explode(' ', $fullname);

                if (isset($nameComponents[0])) {
                    $firstname = trim($nameComponents[0]);
                    unset($nameComponents[0]);
                }

                if (count($nameComponents) > 0) {
                    $lastname = implode(' ', $nameComponents);
                } else {
                    $lastname = $firstname;
                }

                $user = User::create([
                    'email' => $userEmail,
                    'firstname' => $firstname,
                    'lastname' => $lastname,
                    'password' => bcrypt(rand(1,10000)),
                    'confirmed' => 1,
                ]);

                /* Add role for user*/
                $user->attachRole(\App\Models\Role::where('name','client')->first());
            }

            $account->user()->associate($user);
            $account->save();
        }

        if (!$user->stripe_id) {
            // Create Strice customer
            $service = new StripeService();
            $user = $service->createCustomer($user);
        }

        return $user;
    }
}