<?php

namespace App\Services;
use App\Models\Subscription;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Stripe\Stripe;
use Stripe\Subscription as StripeSubscription;

class StripeService
{

    const TRIAL_PLAN_ID = 'trial_mp_plan';

    public function __construct()
    {
        Stripe::setApiKey(config('services.stripe.secret'));
    }

    /**
     * Create Stripe user
     *
     * @param \App\User $user
     * @return \App\User
     */
    public function createCustomer(User $user)
    {
        $options = [
            'description' => sprintf('%s %s', $user->firstname, $user->lastname),
            'email' => $user->email,
        ];

        $user->createAsStripeCustomer(null, $options);

        // Create trial subscription
        $subscriptionBuilder = $user->newSubscription('MousePlanner Trial', self::TRIAL_PLAN_ID);
        $subscriptionBuilder->create();

        return $user;
    }

    public function updateSubscription(Request $request)
    {
        $data = $request->input('data', []);
        $success = false;

        // Cancel subscription
        if (isset($data['object']) && isset($data['object']['status']) && isset($data['object']['plan'])) {
            if ($data['object']['status'] != StripeSubscription::STATUS_TRIALING
                && $data['object']['plan']['id'] == self::TRIAL_PLAN_ID) {

                $id = $data['object']['id'];
                Subscription::where('stripe_id', $id)->update(['ends_at' => Carbon::now()]);

                $stripeSubscription = StripeSubscription::retrieve($id);

                $stripeSubscription->cancel();
            }

        }

        return $success;
    }

    public function suspendCustomer(Request $request)
    {
        $data = $request->input('data', []);
        $success = false;

        if (isset($data['object']) && isset($data['object']['id'])) {
            $subscription = Subscription::where('stripe_id', $data['object']['id'])
                ->first();

            if ($subscription) {
                $subscription->ends_at = Carbon::now();
                $subscription->update();

                $success = true;
            }
        }

        return $success;
    }

    public function cancelSubscription($id) {
        $stripeSubscription = StripeSubscription::retrieve($id);

        return $stripeSubscription->cancel();
    }

}