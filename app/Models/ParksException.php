<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParksException extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'park_id'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];

    /**
     * The park that belong to the mapped park.
     */
    public function park() {
        return $this->belongsTo('App\Models\Park');
    }
}
