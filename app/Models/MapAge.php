<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MapAge extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'min',
        'max',
        'disneyAgeId',
        'disneyAgeValue',
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];
}
