<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Character extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'value',
        'name',
      ];
      /**
       * The attributes that should be visible in arrays.
       *
       * @var array
       */
      protected $visible = ['id'];

    /**
     * The vacations that belong to the favorite_characters_type.
     */
    public function vacations()
    {
        return $this->belongsToMany('App\Models\Vacation')->withTimestamps();
    }
    /**
    * Get the map_character for the character.
    */
    public function map_character() {
      return $this->hasOne('App\Models\MapCharacter');
    }
}
