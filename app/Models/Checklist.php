<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Checklist extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'user_id', 'mousage_type_id', 'text', 'done'
    ];
    /**
     * The attributes that should be hidden in arrays.
     *
     * @var array
     */
    protected $hidden = ['user_id', 'mousage_type_id', 'mousage_type', 'created_at'];
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['milestone', 'type'];

    public function getChecklistIdAttribute() {
        return $this->id;
    }

    public function getMilestoneAttribute() {
        return $this->mousage_type->milestone;
    }

    public function getTypeAttribute() {
        return $this->mousage_type->key;
    }

    /**
     * Get the user that owns the checklist.
     */
    public function user() {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the mousage_type that owns the checklist.
     */
    public function mousage_type() {
        return $this->belongsTo('App\Models\MousageType');
    }
}
