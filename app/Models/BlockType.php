<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlockType extends Model
{
    /**
  	 * The database table used by the model.
  	 *
  	 * @var string
  	 */
    protected $table = "block_types";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'subtype', 'cssClass', 'image', 'in_plan', 'association_id'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];
}
