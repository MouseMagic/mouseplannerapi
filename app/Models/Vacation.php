<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

use App\Utils\DateUtils;

class Vacation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'trip_id',
        'name',
        'stepNumber',
        'highestStepCompleted',
        'startTravelDate',
        'endTravelDate',
        'isFlying',
        'departureDate',
        'arrivalDate',
        'booked_place_type_id',
        'resort_id',
        'haveDisneyPackage',
        'visitDays',
        'interest_visit_park_type_id',
        'interest_water_park_type_id',
        'dining_plan_type_id',
        'hotelName',
        'streetAddress',
        'zipCode',
        'haveCar',
        'havePurchasedParkTicket_id',
        'phone',
        'thrill_level_type_id',
        'pace_type_id',
        'budget_type_id',
        'beginDayTime',
        'endDayTime',
        'time_flexibility_type_id',
        'nap_type_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at', 'trip'];

    public function parks(){
      return $this->belongsToMany('App\Models\Park', 'vacation_park')->withTimestamps()->withPivot('hearts');
    }

    /**
     * Get the MousePlan record associated with the vacation.
     */
    public function mouse_plan()
    {
        return $this->hasOne('App\Models\MousePlan');
    }

    /**
     * The favorite_characters_types that belong to the vacation.
     */
    public function characters()
    {
        return $this->belongsToMany('App\Models\Character')->withTimestamps();
    }

    /**
     * The dining_preference_types that belong to the vacation.
     */
    public function dining_preferences()
    {
        return $this->belongsToMany('App\Models\DiningPreferenceType', 'dining_vacation')
                    ->withTimestamps();
    }

    /**
     * The celebration_types that belong to the vacation.
     */
    public function celebration_types()
    {
        return $this->belongsToMany('App\Models\CelebrationType', 'celebration_vacation')
                    ->withTimestamps();
    }

    /**
     * The interest_types that belong to the vacation.
     */
    public function interest_types()
    {
        return $this->belongsToMany('App\Models\InterestType')->withTimestamps();
    }

    /**
     * Get the resort that owns the vacation.
     */
    public function resort()
    {
        return $this->belongsTo('App\Models\Resort');
    }

    /**
     * Get the trip that owns the vacation.
     */
    public function trip()
    {
        return $this->belongsTo('App\Models\Trip');
    }

    /**
     * Get the trhill level that owns the vacation.
     */
    public function thrill_level_type()
    {
        return $this->belongsTo('App\Models\ThrillLevelType');
    }

    /**
     * The travelers for the vacation.
     */
    public function travelers()
    {
        return $this->hasMany('App\Models\Traveler');
    }

    public function experiences() {
        return $this->belongsToMany('App\Models\Experience', 'vacation_experiences')->withTimestamps()->withPivot('hearts');
    }

    /**
     * Get the booked place type that owns the vacation.
     */
    public function booked_place_type()
    {
        return $this->belongsTo('App\Models\BookedPlaceType');
    }

    /**
     * Get the interest visit park type that owns the vacation.
     */
    public function interest_visit_park_type()
    {
        return $this->belongsTo('App\Models\InterestVisitParkType');
    }

    /**
     * Get the interest water park type that owns the vacation.
     */
    public function interest_water_park_type()
    {
        return $this->belongsTo('App\Models\InterestWaterParkType');
    }

    /**
     * Get the dining plan type that owns the vacation.
     */
    public function dining_plan_type()
    {
        return $this->belongsTo('App\Models\DiningPlanType');
    }

    /**
     * Get the pace type that owns the vacation.
     */
    public function pace_type()
    {
        return $this->belongsTo('App\Models\PaceType');
    }

    /**
     * Get the budget type that owns the vacation.
     */
    public function budget_type()
    {
        return $this->belongsTo('App\Models\BudgetType');
    }

    /**
     * Get the time_flexibility type that owns the vacation.
     */
    public function time_flexibility_type()
    {
        return $this->belongsTo('App\Models\TimeFlexibilityType');
    }

    /**
     * Get the nap type that owns the vacation.
     */
    public function nap_type()
    {
        return $this->belongsTo('App\Models\NapType');
    }
    /**
     * Get the mousages for the vacation.
     */
    public function mousages() {
        return $this->hasMany('App\Models\Mousage');
    }
    /**
     * The accessibility_types that belong to the vacations.
     */
    public function accessibility_types()
    {
        return $this->belongsToMany('App\Models\AccessibilityType')->withTimestamps();
    }
}
