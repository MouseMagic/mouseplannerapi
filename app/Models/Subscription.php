<?php

namespace App\Models;

use App\Services\StripeService;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'stripe_id',
        'stripe_plan',
        'quantity',
        'trial_ends_at',
        'ends_at'
    ];

    public static function boot() {
        parent::boot();

        static::deleted(function($subscription)
        {
            $service = new StripeService();

            if (Carbon::now()->lt($subscription->ends_at)) {
                $service->cancelSubscription($subscription->stripe_id);
            }
        });
    }
}
