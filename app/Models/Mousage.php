<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mousage extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'vacation_id',
        'mousage_type_id',
        'mousageable_id',
        'mousageable_type',
        'read',
        'text'
    ];
    /**
     * The attributes that should be hidden in arrays.
     *
     * @var array
     */
    protected $hidden = ['mousage_type_id', 'mousage_type'];
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
     protected $appends = ['milestone', 'is_checklist_task', 'type'];

    public function getMilestoneAttribute() {
        return $this->mousage_type->milestone;
    }

    public function getisChecklistTaskAttribute() {
        return $this->mousage_type->isChecklistTask;
    }

    public function getTypeAttribute() {
        return $this->mousage_type->key;
    }

    /**
     * Get all of the owning mousageable models.
     */
    public function mousageable() {
        return $this->morphTo();
    }
    /**
     * Get the mousage_type that owns the mousage.
     */
    public function mousage_type() {
      return $this->belongsTo('App\Models\MousageType');
    }
    /**
     * Get the vacation that owns the mousage.
     */
    public function vacation() {
      return $this->belongsTo('App\Models\Vacation');
    }
}
