<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Day extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'mouse_plan_id', 'date', 'day_number'
    ];
    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = ['id', 'date', 'blocks', 'day_number'];

    /**
    * All of the relationships to be touched.
    *
    * @var array
    */
    protected $touches = ['mouse_plan'];

    /**
     * Get the mouse_plan that owns the day.
     */
    public function mouse_plan()
    {
        return $this->belongsTo('App\Models\MousePlan');
    }

    /**
    * Get the blocks for the day.
    */
    public function blocks(){
      return $this->hasMany('App\Models\Block');
    }
    /**
    * Get the fastpass reservations for the block.
    */
    public function fastpass_reservations() {
      return $this->hasMany('App\Models\FastpassReservation');
    }
    /**
     * Get all of the mousages for the block.
     */
     public function mousages() {
         return $this->morphMany('App\Models\Mousage', 'mousageable');
     }
     /**
      * Get all of the notes for the day.
      */
     public function notes() {
         return $this->morphMany('App\Models\Note', 'noteable');
     }
}
