<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CelebrationType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'value',
        'name',
        'img'
      ];
      /**
       * The attributes that should be visible in arrays.
       *
       * @var array
       */
      protected $visible = ['id'];

    /**
     * The vacations that belong to the celebration_vacation_type.
     */
    public function vacations()
    {
        return $this->belongsToMany('App\Models\Vacation', 'celebration_vacation')
                    ->withTimestamps();
    }
}
