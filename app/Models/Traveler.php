<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Traveler extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'vacation_id',
        'prefix_title_type_id',
        'firstname',
        'lastname',
        'monthOfBirth',
        'yearOfBirth',
        'traveler_height_id'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at', 'pivot', 'user_id'];
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
     protected $appends = ['accessibility_types'];

    public function getAccessibilityTypesAttribute() {
        return $this->accessibility_types()->get();
    }

    /**
     * The vacation that that owns the traveler.
     */
    public function vacation()
    {
        return $this->belongsTo('App\Models\Vacation');
    }

    /**
     * Get the prefix title that owns the traveler.
     */
    public function prefix_title_type(){
      return $this->belongsTo('App\Models\PrefixTitleType');
    }

    /**
     * Get the traveler height that owns the traveler.
     */
    public function traveler_height(){
      return $this->belongsTo('App\Models\TravelerHeights');
    }

    /**
     * The accessibility_types that belong to the traveler.
     */
    public function accessibility_types()
    {
        return $this->belongsToMany('App\Models\AccessibilityType')->withTimestamps();
    }
}
