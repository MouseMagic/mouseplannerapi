<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DiningPreferenceType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'value',
        'name',
      ];
      /**
       * The attributes that should be visible in arrays.
       *
       * @var array
       */
      protected $visible = ['id'];

    /**
     * The vacations that belong to the favorite_characters_type.
     */
    public function vacations()
    {
        return $this->belongsToMany('App\Models\Vacation', 'dining_vacation')
                    ->withTimestamps();
    }
}
