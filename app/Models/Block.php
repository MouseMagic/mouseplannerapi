<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'day_id',
        'block_type_id',
        'name',
        'location',
        'startTime',
        'endTime',
        'parent_id',
        'association_id',
        'park_number',
    ];
    /**
     * The attributes that should be hidden in arrays.
     *
     * @var array
     */
    protected $hidden = ['day', 'created_at', 'updated_at'];

    /**
     * Get the block_type that owns the block.
     */
    public function block_type(){
      return $this->belongsTo('App\Models\BlockType');
    }

    /**
    * All of the relationships to be touched.
    *
    * @var array
    */
    protected $touches = ['day'];

    /**
     * Get the day that owns the block.
     */
    public function day(){
      return $this->belongsTo('App\Models\Day');
    }
    /**
     * Get all of the mousages for the block.
     */
    public function mousages() {
        return $this->morphMany('App\Models\Mousage', 'mousageable');
    }
    /**
     * Get all of the notes for the block.
     */
    public function notes() {
        return $this->morphMany('App\Models\Note', 'noteable');
    }
}
