<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MapAccessibilityType extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'id',
      'disneyAccessibilityTypeId',
      'disneyAccessibilityTypeValue',
      'accessibility_type_id',
  ];
  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = ['created_at', 'updated_at'];

  /**
   * The accessibility_type that belong to the mapped accessibility type.
   */
  public function accessibility_type() {
      return $this->belongsTo('App\Models\AccessibilityType');
  }
}
