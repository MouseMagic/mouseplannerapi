<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MapCharacter extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'character_id',
        'disneyFavoriteCharactersTypeId',
        'disneyFavoriteCharactersTypeValue',
        'weight',
      ];
      /**
       * The attributes that should be hidden for arrays.
       *
       * @var array
       */
      protected $hidden = ['created_at', 'updated_at'];

    /**
     * The character that belong to the favorite_characters_type.
     */
    public function character() {
        return $this->belongsTo('App\Models\Character');
    }
}
