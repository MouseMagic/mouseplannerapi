<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookedPlaceType extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = "booked_place_types";

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'id','value','name','img'
  ];
  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = ['value', 'created_at', 'updated_at'];

  /**
   * Get the vacations for the booked place type.
   */
  public function vacations()
  {
      return $this->hasMany('App\Models\Vacation');
  }
}
