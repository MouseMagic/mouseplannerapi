<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MapThrillLevelType extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'id',
      'thrill_level_type_id',
      'disneyThrillLevelTypeId',
      'disneyThrillLevelTypeValue'
  ];
  /**
   * The attributes that should be hidden in arrays.
   *
   * @var array
   */
  protected $hidden = ['updated_at', 'created_at'];

  /**
   * The thrill_level_type that belong to the mapped thrill level.
   */
  public function thrill_level_type() {
      return $this->belongsTo('App\Models\ThrillLevelType');
  }
}
