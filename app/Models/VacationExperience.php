<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VacationExperience extends Model
{
    /**
  	 * The database table used by the model.
  	 *
  	 * @var string
  	 */
    protected $table = "vacation_experiences";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'vacation_id',
        'experience_id',
        'hearts'
    ];

}
