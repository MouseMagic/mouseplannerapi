<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MapInterestType extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'id',
      'interest_type_id',
      'disneyInterestTypeId',
      'disneyInterestTypeValue',
      'weight'
  ];
  /**
   * The attributes that should be hidden in arrays.
   *
   * @var array
   */
  protected $hidden = ['updated_at', 'created_at'];

  /**
   * The interest_type that belong to the mapped interest type.
   */
  public function interest_type() {
      return $this->belongsTo('App\Models\InterestType');
  }
}
