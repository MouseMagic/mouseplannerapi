<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'restaurantable_id',
        'restaurantable_type',
        'location',
        'cuisine',
        'service'
    ];
    /**
     * The attributes that should be hidden in arrays.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];

    /**
     * Get all of the owning restaurantable models.
     */
    public function restaurantable() {
        return $this->morphTo();
    }
}
