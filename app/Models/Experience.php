<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    /**
  	 * The database table used by the model.
  	 *
  	 * @var string
  	 */
    protected $table = "experiences";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'external_id',
        'park_id',
        'external_name',
        'external_description',
        'custom_description',
        'external_facets'
    ];
    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = ['external_id', 'park_id', 'hearts'];
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
     protected $appends = ['hearts'];

    public function getHeartsAttribute() {
        if($this->pivot) {
            return $this->pivot->hearts;
        }
        return null;
    }

    /**
     * Get the fastpass_reservation record associated with the experience.
     */
    public function fastpass_reservation() {
        return $this->hasOne('App\Models\FastpassReservation');
    }
    /**
     * The vacations that belong to the experience.
     */
    public function vacations() {
        return $this->belongsToMany('App\Models\Vacation', 'vacation_experiences')->withTimestamps()->withPivot('hearts');
    }
    /**
     * Get the park that owns the experience.
     */
    public function park() {
        return $this->belongsTo('App\Models\Park');
    }
}
