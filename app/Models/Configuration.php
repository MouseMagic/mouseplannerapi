<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
    /**
  	 * The database table used by the model.
  	 *
  	 * @var string
  	 */
    protected $table = "configuration";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'category', 'key', 'value'
    ];
    /**
     * Remove time columns automatically managed by Eloquent.
     *
     * @var array
     */
    public $timestamps = false;
}
