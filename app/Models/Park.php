<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Park extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'value',
        'name',
        'description',
        'linkto',
        'initials'
    ];
    /**
     * The attributes that should be visible for arrays.
     *
     * @var array
     */
    protected $visible = ['id', 'hearts'];
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
     protected $appends = ['hearts'];

    public function getHeartsAttribute() {
        if($this->pivot) {
            return $this->pivot->hearts;
        }
        return null;
    }

    /**
     * The vacations that belong to the parks.
     */
    public function vacations()
    {
        return $this->belongsToMany('App\Models\Vacation', 'vacation_park')->withTimestamps()->withPivot('hearts');
    }

    /**
     * Get all of the restaurants for the park.
     */
     public function restaurants() {
         return $this->morphMany('App\Models\Restaurant', 'restaurantable');
     }
     /**
      * Get the experiences record associated with the park.
      */
     public function experiences() {
         return $this->hasMany('App\Models\Experience');
     }
    /**
    * Get the map_park for the park.
    */
    public function map_park() {
      return $this->hasMany('App\Models\MapPark');
    }
   /**
   * Get the park exception for the park.
   */
   public function park_exception() {
     return $this->hasMany('App\Models\ParksException');
   }
}
