<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MousePlan extends Model
{
  /**
  * The database table used by the model.
  *
  * @var string
  */
  protected $table = "mouse_plans";
  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
  protected $fillable = [
    'id',
    'vacation_id',
    'tripScore',
    'dashboardImg',
  ];
  /**
   * The attributes that should be visible in arrays.
   *
   * @var array
   */
  protected $visible = ['id', 'vacation_id','mouse_plan_id','tripScore','days'];

  /**
  * All of the relationships to be touched.
  *
  * @var array
  */
  protected $touches = ['vacation'];

  /**
  * Get the days for the mouse plan.
  */
  public function days(){
    return $this->hasMany('App\Models\Day')->orderBy('day_number');
  }

  /**
  * Get the vacation that owns the mouse_plan.
  */
  public function vacation()
  {
    return $this->belongsTo('App\Models\Vacation');
  }

  /**
  * Get the checklists for the mouse plan.
  */
  public function checklists()
  {
    return $this->hasMany('App\Models\Checklist');
  }
  /**
   * Get all of the mousages for the mouseplan.
   */
   public function mousages() {
       return $this->morphMany('App\Models\Mousage', 'mousageable');
   }
}
