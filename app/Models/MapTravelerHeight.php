<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MapTravelerHeight extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'id',
      'traveler_height_id',
      'disneyHeightId',
      'disneyHeightValue'
  ];
  /**
   * The attributes that should be hidden in arrays.
   *
   * @var array
   */
  protected $hidden = ['updated_at', 'created_at'];

  /**
   * The traveler_height that belong to the mapped traveler height.
   */
  public function traveler_height() {
      return $this->belongsTo('App\Models\TravelerHeight');
  }
}
