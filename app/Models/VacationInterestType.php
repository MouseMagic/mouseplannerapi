<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VacationInterestType extends Model
{
    /**
  	 * The database table used by the model.
  	 *
  	 * @var string
  	 */
    protected $table = "interest_type_vacation";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'vacation_id',
        'interest_type_id'
    ];
}
