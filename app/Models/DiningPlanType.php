<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DiningPlanType extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = "dining_plan_types";

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'id','value','name','description','linto','img'
  ];
  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = ['value', 'created_at', 'updated_at'];
  
  /**
   * Get the vacations for the dining plan type.
   */
  public function vacations()
  {
      return $this->hasMany('App\Models\Vacation');
  }
}
