<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MapPark extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'park_id',
        'disneyParkInterestId',
        'disneyParkInterestValue',
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['updated_at', 'created_at'];
    /**
     * The park that belong to the mapped park.
     */
    public function park() {
        return $this->belongsTo('App\Models\Park');
    }
}
