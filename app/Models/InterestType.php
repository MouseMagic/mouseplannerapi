<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InterestType extends Model
{
    /**
  	 * The database table used by the model.
  	 *
  	 * @var string
  	 */
    protected $table = "interest_types";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'value',
        'name'
    ];
    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = ['id'];

    /**
     * The vacations that belong to the interest_types.
     */
    public function vacations()
    {
        return $this->belongsToMany('App\Models\Vacation')->withTimestamps();
    }
    /**
    * Get the map_interest_types for the interest_type.
    */
    public function map_interest_types() {
      return $this->hasMany('App\Models\MapInterestType');
    }
}
