<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GeneralOperatingHour extends Model
{
    /**
  	 * The database table used by the model.
  	 *
  	 * @var string
  	 */
    protected $table = "general_operating_hours";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'weekYear',
        'dayWeek',
        'park',
        'openTime',
        'closeTime',
        'crowd_level_id'
    ];

    /**
     * Get the crowd level that owns the general day.
     */
    public function crowd_level()
    {
        return $this->belongsTo('App\Models\CrowdLevel');
    }
}
