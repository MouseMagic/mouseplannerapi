<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VacationPark extends Model
{
    /**
  	 * The database table used by the model.
  	 *
  	 * @var string
  	 */
    protected $table = "vacation_park";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'vacation_id',
        'park_id',
        'hearts'
    ];

}
