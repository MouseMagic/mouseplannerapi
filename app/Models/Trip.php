<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = "trips";
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'id',
      'user_id',
      'name',
      'firstTrip',
      'tripCount',
      'lastVisit',
      'training_completed',
  ];
  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = ['created_at','user'];

  public function vacation(){
    return $this->hasMany('App\Models\Vacation');
  }

  /**
   * Get the user that owns the trip.
   */
  public function user()
  {
      return $this->belongsTo('App\User');
  }

  /**
  * Get all of the mouseplans for the trip.
  */
  public function mouse_plans()
  {
      return $this->hasManyThrough('App\Models\MousePlan', 'App\Models\Vacation');
  }
}
