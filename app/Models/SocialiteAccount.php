<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SocialiteAccount extends Model
{
    protected $fillable = [
        'user_id',
        'fullname',
        'provider_user_id',
        'provider'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
