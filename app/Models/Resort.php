<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Resort extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'value',
        'name',
        'resort_area'
    ];
    /**
     * The attributes that should be hidden in arrays.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];

    /**
     * Get the vacations for the resort.
     */
    public function vacations()
    {
        return $this->hasMany('App\Models\Vacation');
    }

    /**
     * Get all of the restaurants for the resort.
     */
     public function restaurants() {
         return $this->morphMany('App\Models\Restaurant', 'restaurantable');
     }
}
