<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ThrillLevelType extends Model
{
    /**
  	 * The database table used by the model.
  	 *
  	 * @var string
  	 */
    protected $table = "thrill_level_types";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'value',
        'name',
        'description',
        'img'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['value', 'created_at', 'updated_at'];

    /**
     * Get the vacations for the thrill level.
     */
    public function vacations()
    {
        return $this->hasMany('App\Models\Vacation');
    }
    /**
    * Get the map_thrill_level_types for the thrill_level_types.
    */
    public function map_thrill_level_types() {
      return $this->hasMany('App\Models\MapThrillLevelType');
    }
}
