<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccessibilityType extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'id',
      'value',
      'name',
      'description',
      'img'
  ];
  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $visible = ['id'];

  /**
   * The travelers that belong to the accessibility_types.
   */
  public function travelers()
  {
      return $this->belongsToMany('App\Models\Traveler')->withTimestamps();
  }
    /**
     * The vacations that belong to the accessibility_types.
     */
    public function vacations()
    {
        return $this->belongsToMany('App\Models\Vacation')->withTimestamps();
    }
  /**
  * Get the map_accessibility_types for the accessibility_type.
  */
  public function map_accessibility_type() {
    return $this->hasOne('App\Models\MapAccessibilityType');
  }
}
