<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FastpassReservation extends Model {
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
       'id',
       'day_id',
       'block_id',
       'experience_id',
       'reservation_time'
   ];
   /**
    * The attributes that should be hidden in arrays.
    *
    * @var array
    */
   protected $hidden = ['created_at', 'updated_at', 'experience', 'day', 'block'];
   /**
    * The accessors to append to the model's array form.
    *
    * @var array
    */
    protected $appends = ['external_id'];

   public function getExternalIdAttribute() {
       if($this->experience) {
           return $this->experience->external_id;
       }
       return null;
   }

   /**
    * Get the day that owns the fastpass reservation.
    */
   public function day(){
     return $this->belongsTo('App\Models\Day');
   }

   /**
    * Get the block that owns the fastpass reservation.
    */
   public function block(){
     return $this->belongsTo('App\Models\Block');
   }

   /**
   * All of the relationships to be touched.
   *
   * @var array
   */
   protected $touches = ['day', 'block'];

   /**
    * Get the experience that owns the reservation.
    */
   public function experience(){
     return $this->belongsTo('App\Models\Experience');
   }
}
