<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrefixTitleType extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'id',
      'value',
      'name'
  ];
  /**
   * The attributes that should be hidden in arrays.
   *
   * @var array
   */
  protected $hidden = ['value','updated_at', 'created_at'];

  /**
  * Get the travelers for the prefix type.
  */
  public function travelers(){
    return $this->hasMany('App\Models\Traveler');
  }
}
