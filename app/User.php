<?php

namespace App;

use App\Models\Subscription;
use App\Services\StripeService;
use App\Notifications\CustomResetPasswordNotification;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;
use League\OAuth2\Server\Exception\OAuthServerException;
use Stripe\Subscription as StripeSubscription;
use Stripe\Stripe;
use Zizaco\Entrust\Traits\EntrustUserTrait;


class User extends Authenticatable
{
    use Billable,HasApiTokens,Notifiable, EntrustUserTrait;

    protected $subscribed = null;
    protected $subscribed_before = null;
    protected $is_on_trial = null;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = "users";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'email', 'password','confirmation_code','confirmed',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'trial_ends_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
         'password', 'remember_token', 'created_at',
    ];


    public function trips(){
        return $this->hasMany('App\Models\Trip');
    }

    public function subscriptions()
    {
        return $this->hasMany('\App\Models\Subscription');
    }
    public function checklists(){
        return $this->hasMany('App\Models\Checklist');
    }
    /**
     * Get all of the vacations for the user.
     */
    public function vacations()
    {
        return $this->hasManyThrough('App\Models\Vacation', 'App\Models\Trip');
    }

    public function validateForPassportPasswordGrant($password)
    {
        if (!$this->confirmed) {
            $e = new OAuthServerException('Account is not confirmed', 0, 'not_confirmed', 401);
            throw $e;

            return false;
        }

        return Hash::check($password, $this->password);
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new CustomResetPasswordNotification($token));
    }

    public function calculate_is_subscribed($skipTrial = true, $subscription = false)
    {
        $q = Subscription::where(function($query) {
            $query->whereNull('ends_at')
                ->orWhere('ends_at', '>', Carbon::now());
        })
            ->where('user_id', $this->id);

        if ($subscription) {
            $q->where('name', $subscription);
        }

        if ($skipTrial) {
            $q->where('stripe_plan', '<>', StripeService::TRIAL_PLAN_ID);
        }

        $result = (bool)$q->count();

        return $result;
    }

    public function calculate_is_on_trial()
    {
        $q = Subscription::where(function($query) {
            $query->whereNull('ends_at')
                ->orWhere('ends_at', '>', Carbon::now());
        })
            ->where('user_id', $this->id)
            ->where('stripe_plan', StripeService::TRIAL_PLAN_ID);


        $result = (bool)$q->count();

        return $result;
    }

    public function check_trial_and_cancel()
    {
        $subscriptions = Subscription::where(function($query) {
            $query->whereNull('ends_at')
                ->orWhere('ends_at', '>', Carbon::now());
        })
            ->where('user_id', $this->id)
            ->where('stripe_plan', StripeService::TRIAL_PLAN_ID)
            ->get()
            ->all();

        foreach ($subscriptions as $subscription) {
            $subscription->update(['ends_at' => Carbon::now()]);

            try {
                Stripe::setApiKey(config('services.stripe.secret'));
                $stripeSubscription = StripeSubscription::retrieve($subscription->stripe_id);
                $stripeSubscription->cancel();
            } catch (\Exception $ex) { }

        }
    }

    public function getIsSubscribedAttribute()
    {
        if ($this->subscribed === null && $this->token()) {
            $session_data = $this->token()->session_data;
            $session_data = \GuzzleHttp\json_decode($session_data, true);

            $this->subscribed = isset($session_data['is_subscribed']) && $session_data['is_subscribed'] ? true : false;
        }

        return $this->subscribed;
    }

    public function getWereSubscribedBeforeAttribute() {
        if ($this->subscribed_before === null && $this->token()) {
            $session_data = $this->token()->session_data;
            $session_data = \GuzzleHttp\json_decode($session_data, true);

            $this->subscribed_before = isset($session_data['were_subscribed_before']) && $session_data['were_subscribed_before'] ? true : false;
        }

        return $this->subscribed_before;
    }

    public function getIsOnTrialAttribute()
    {
        if ($this->is_on_trial === null) {
            $session_data = $this->token()->session_data;
            $session_data = \GuzzleHttp\json_decode($session_data, true);

            $this->is_on_trial = isset($session_data['is_on_trial']) && $session_data['is_on_trial'] ? true : false;
        }

        return $this->is_on_trial;
    }

}
