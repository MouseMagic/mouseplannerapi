<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\OrlandoHotel;

class OrlandoHotelController extends Controller
{
    public function __construct(){
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return OrlandoHotel::all();
    }
}
