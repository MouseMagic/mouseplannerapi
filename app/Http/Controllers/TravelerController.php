<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Repositories\TravelerRepository;
use App\Models\Traveler;
use Auth;

class TravelerController extends Controller
{
    private $TravelerRepository;

    public function __construct(TravelerRepository $travelerRepository) {
      $this->middleware('auth:api');
      $this->TravelerRepository = $travelerRepository;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
      $attributes = $request->all();
      if (count($attributes) == 0) {
          return response()->json(['attributes' => $attributes], 400);
      }
      Log::debug("Creating traveler.", $attributes);
      $traveler = $this->TravelerRepository->create($attributes);
      if ($traveler == null) {
        return response()->json(['attributes' => $attributes], 400);
      }
      return response()->json($traveler, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  Traveler $traveler
     * @return \Illuminate\Http\Response
     */
    public function show(Traveler $traveler) {
        return $traveler;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Traveler $traveler
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Traveler $traveler) {
        $attributes = $request->all();
        if (count($attributes) == 0) {
            return response()->json(['attributes' => $attributes], 400);
        }
        $traveler_id = $traveler->id;
        Log::debug("Updating traveler.",[
            'traveler_id' => $traveler_id,
            'attributes to update' => $attributes
        ]);
        $this->TravelerRepository->update($traveler, $attributes);
        return response()->json(['traveler uptaded' => $traveler_id], 201);
    }

    /**
     * Remove the specified traveler from vacation.
     *
     * @param  Traveler $traveler
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Traveler $traveler) {
        $this->TravelerRepository->destroy($traveler);
        return response()->json(['traveler detach'], 204);
    }
}
