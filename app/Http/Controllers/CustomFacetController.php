<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Models\CustomFacet;

class CustomFacetController extends Controller
{
    public function __construct(){
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the mapped values between
     * Disney data and API data.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return CustomFacet::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $custom_facet = new CustomFacet;
        $custom_facet->disneyId = $request->disneyId;
        $custom_facet->type = $request->type;
        $custom_facet->value = $request->value;
        $custom_facet->save();
        Log::info("New custom facet record created.", ['facet_id' => $custom_facet->id]);
        return response()->json($custom_facet, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  CustomFacet $custom_facet
     * @return \Illuminate\Http\Response
     */
    public function show(CustomFacet $custom_facet) {
        return $custom_facet;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  CustomFacet $custom_facet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CustomFacet $custom_facet) {
        $attributes = $request->all();
        Log::debug("Updating facet.",
            ['facet_id' => $custom_facet->id, 'attributes to update' => $attributes]);

        $custom_facet->update($attributes);
        return response()->json(['custom facet uptaded' => $custom_facet->id], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  CustomFacet $custom_facet
     * @return \Illuminate\Http\Response
     */
    public function destroy(CustomFacet $custom_facet) {
        $custom_facet->delete();
        return response()->json(['facet'], 204);
    }
}
