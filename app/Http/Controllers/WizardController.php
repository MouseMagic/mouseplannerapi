<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Repositories\MousePlanRepository;
use App\Repositories\ConfigRepository;
use App\Repositories\VacationRepository;
use App\Repositories\DayRepository;
use App\Repositories\BlockRepository;
use App\Repositories\ChecklistRepository;
use App\Repositories\TravelerRepository;
use App\Models\Vacation;
use App\Models\MousePlan;
use App\Models\Day;
use App\Models\Block;
use App\Models\Park;
use App\Models\Traveler;
use App\Utils\NameNormalizer;
use App\Utils\DateUtils;
use App\Utils\BlocksUtils;
use App\Utils\Mousages;
use App\Utils\DisneyData;
use App\Verifiers\DiningParentsVerifier;
use App\Exceptions\MousePlannerException;

class WizardController extends Controller
{
    private $MousePlanRepository;
    private $Config;
    private $VacationRepository;
    private $DayRepository;
    private $BlockRepository;
    private $ChecklistRepository;
    private $TravelerRepository;

    public function __construct(
      MousePlanRepository $mousePlanRepository,
      ConfigRepository $config,
      VacationRepository $vacationRepository,
      DayRepository $dayRepository,
      BlockRepository $blockRepository,
      ChecklistRepository $checklistRepository,
      TravelerRepository $travelerRepository)
    {
      $this->middleware('auth:api');
      $this->MousePlanRepository = $mousePlanRepository;
      $this->Config = $config;
      $this->VacationRepository = $vacationRepository;
      $this->DayRepository = $dayRepository;
      $this->BlockRepository = $blockRepository;
      $this->ChecklistRepository = $checklistRepository;
      $this->TravelerRepository = $travelerRepository;
    }

    /**
     * Display a listing of the parks with their scores.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexParks(Vacation $vacation)
    {
        $scores = $this->VacationRepository->getScoreByPark($vacation);
        $parks =  array();
        foreach ($scores as $park => $score) {
            $park_db = Park::where('initials', $park)->first();
            array_push($parks, array('id' => $park_db['id'], 'score' => $score));
        }
        $answer =  array('vacation_id' => $vacation->id, 'parks' => $parks);
        return $answer;
    }
    /**
     * Display a listing of the mousages of the Block.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexVacationTravelers(Vacation $vacation) {
        return $vacation->travelers;
    }

    /**
     * Store a newly created mouseplan in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storePlan(Vacation $vacation, Request $request)
    {
        if ($vacation->mouse_plan == null) {
            $daysToDisney = DateUtils::calcuateDaysDifference($vacation->startTravelDate);
            Log::debug("Days to go to Disney retrieved.",
                ['vacation_id' => $vacation->id, 'days' => $daysToDisney]);
            // Step 1 of the Schedule Logic document.
            $this->ChecklistRepository->createDefaultList(
                $vacation->booked_place_type_id < 3,
                $daysToDisney
            );
        }
        // Create or get mouseplan.
        // Step 2 of the Schedule Logic document.
        $mouseplan = MousePlan::firstOrCreate(['vacation_id' => $vacation->id]);
        $vacation->load('mouse_plan');
        Log::debug("Mouseplan for existing vacation created/retrieved.",
            ['vacation_id' => $vacation->id, 'mouse_plan_id' => $mouseplan->id]);
        try {
          $this->buildMousePlan($vacation, $mouseplan);
        } catch (MousePlannerException $mpe) {
          Log::critical("MousePlan wasn't created", [$mpe->getMessage()]);
          $abortMessage = "MousePlan couldn't be created due to exception: '{$mpe->getMessage()}'";
          abort($mpe->status(), $abortMessage);
        } catch (\Exception $e) {
          Log::critical("MousePlan wasn't created", ['vacation_id'=>$vacation->id, 'exception'=>$e->__toString()]);
          $abortMessage = "MousePlan couldn't be created for vacation:{$vacation->id} due to unexpected exception: '{$e->getMessage()}'";
          abort(500, $abortMessage);
        }
        $mouseplanAnswer = $this->MousePlanRepository->getMousePlan($mouseplan);
        Log::info("New mouseplan record created.", $mouseplanAnswer->toArray());
        return response()->json($mouseplanAnswer, 201);
    }

    private function buildMousePlan(Vacation $vacation, MousePlan $mouseplan) {
      // Validates if plan can be built
      $this->validateVacation($vacation);
      $this->cleanMousages($vacation);
      // Create day models with park-day dates for the new mouseplan
      $dates = $this->VacationRepository->createDayArray($vacation);
      $this->DayRepository->storeDayArray($dates,$mouseplan);
      // Compute the maximum number of parks on a day.
      // Step 3 of the Schedule Logic document.
      $parksPerDayKey = $this->VacationRepository->getParksPerDayKey($vacation);
      $parksPerDay = $this->Config->getValue('blocks', $parksPerDayKey);
      Log::debug("Max parks per day calculated.",
      ['vacation_id' => $vacation->id, 'max_parks_day' => $parksPerDay]);
      // Compute the score of every park based on user interests.
      // Step 4 of the Schedule Logic document.
      $parksScore = $this->VacationRepository->getScoreByPark($vacation);
      // $this->validateScore($vacation, $parksScore);
      // Build a park matrix with operating hours, emh, crowd levels info.
      // Step 7 of the Schedule Logic document.
      $parkInfo = $this->BlockRepository
                       ->getParkInfoCollection($vacation, $parksScore, $dates, $parksPerDay);
      // Create blocks for each day based on MousePlanner rules
      $blockCollection = $this->BlockRepository->createBlockArrays(
          $vacation,
          $parksScore,
          $parkInfo,
          $parksPerDay
      );
      $touringHours = 0;
      $this->BlockRepository->storeBlocks($blockCollection);
      $this->BlockRepository->verifyBlocks(
          $blockCollection,
          $vacation,
          $parksScore,
          $parkInfo,
          $touringHours,
          $parksPerDay,
          array(8,9,10)
      );
      //TODO join verifiers and save new blocks on the fly
      $this->BlockRepository->storeBlocks($blockCollection);
      $this->BlockRepository->verifyBlocks(
          $blockCollection,
          $vacation,
          $parksScore,
          $parkInfo,
          $touringHours,
          $parksPerDay,
          array(1,2,3,4,5,6,7,10)
      );

      if ($blockCollection && !$blockCollection->isEmpty()) {
        $mouseplan->load('days.blocks');
        $mouseplan->tripScore = $this->MousePlanRepository->calculateTripScore(
            $vacation,
            $touringHours,
            $blockCollection
        );
        $mouseplan->save();
      } else {
        $mouseplan->tripScore = 0;
        $mouseplan->save();
      }
    }

    private function validateVacation($vacation) {
      $visitDays = $vacation->visitDays;
      if($visitDays < 1) throw new MousePlannerException($vacation, "There is less than 1 visit day.");
      $blocksUtils = BlocksUtils::getInstance($vacation);
      if($blocksUtils->travelDays() < $visitDays) {
        Log::info("Travel days are fewer than visit days, errors may happen.", ['vacation'=>$vacation->id]);
      }
      if($blocksUtils->travelDays() < $blocksUtils->schedulableDaysCount()) {
        Log::info("Travel days are fewer than schedulable days, errors may happen.", ['vacation'=>$vacation->id]);
      }
    }

    private function validateScore(Vacation $vacation, $parksScore) {
      arsort($parksScore);
      $highestScoredPark = key($parksScore);
      $vacationParks = $vacation->parks;
      $parksHearts = array();
      foreach ($vacationParks as $park) {
        $parksHearts[$park->initials] = $park->hearts;
      }
      //user didn’t rate a park, but their interests indicate that they should spend time there
      if(!key_exists($highestScoredPark, $parksHearts)) {
        $parkName = Park::where('initials', $highestScoredPark)->first()->name;
        Mousages::setLowRankParkMousage($vacation, $parkName);
      }
    }

    private function cleanMousages(Vacation $vacation) {
      $mousages = $vacation->mousages;
      foreach ($mousages as $mousage) {
        $mousage->delete();
      }
    }

    /**
     * Store a newly created traveler in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeTraveler(Vacation $vacation, Request $request) {
        $attributes = $request->all();
        if (count($attributes) == 0) {
            return response()->json(['attributes' => $attributes], 400);
        }
        $attributes['vacation_id'] = $vacation->id;
        // Create traveler
        Log::debug("Creating traveler.", $attributes);
        $traveler = $this->TravelerRepository->create($attributes);
        if ($traveler == null) {
          return response()->json(['attributes' => $attributes], 400);
        }
        Log::info("New traveler record created.",
            ['vacation_id' => $vacation->id, 'traveler_id' => $traveler->id]);
        return response()->json($traveler, 201);
    }
    /**
     * Display a mouseplan.
     *
     * @param  Vacation $vacation
     * @return \Illuminate\Http\Response
     */
    public function showPlan(Vacation $vacation)
    {
        $mouseplanModel = $vacation->mouse_plan;
        return $this->MousePlanRepository->getMousePlan($mouseplanModel);
    }

    /**
     * Set default wishlist
     *
     * @param  Vacation $vacation
     * @return \Illuminate\Http\Response
     */
    public function setDefaultWishlist(Vacation $vacation) {
      $experiences = array();
      $wishlist = $this->Config->getCategory('wishlist');
      foreach ($wishlist as $experience) {
        $experiences[] = [
          'external_id' => $experience->key,
          'hearts' => $experience->value
        ];
      }
      $attributes = ['experiences' => $experiences];
      $this->VacationRepository->syncParksExperiences($vacation, $attributes);
      Log::info("Default wishlist experiences set", ['vacation_id' => $vacation->id]);
      return response()->json($this->VacationRepository->show($vacation), 201);
    }

    /**
     * Set default wishlist
     *
     * @param  Vacation $vacation
     * @return \Illuminate\Http\Response
     */
    public function setParkDefaultWishlist(Vacation $vacation, Park $park) {
      $experiences = array();
      $wishlist = $this->Config->getCategory('wishlist');
      foreach ($wishlist as $experience) {
        $experiences[] = [
          'external_id' => $experience->key,
          'hearts' => $experience->value
        ];
      }
      $attributes = ['experiences' => $experiences];
      $this->VacationRepository->syncParkExperiences($vacation, $park, $attributes);
      Log::info("Default wishlist experiences set", ['vacation_id' => $vacation->id, 'park_id' => $park->id]);
      return response()->json($this->VacationRepository->show($vacation), 201);
    }

    /**
     * Get default wishlist
     *
     * @return \Illuminate\Http\Response
     */
    public function getDefaultWishlist() {
      $experiences = array();
      $wishlist = $this->Config->getCategory('wishlist');
      foreach ($wishlist as $experience) {
        $experiences[] = [
          'external_id' => $experience->key,
          'hearts' => $experience->value
        ];
      }
      return response()->json($experiences, 200);
    }

    /**
     * Get available experiences on the vacation dates
     *
     * @param  Vacation $vacation
     * @return \Illuminate\Http\Response
     */
    public function getExperiencesByDates(Vacation $vacation) {
      $disney = DisneyData::getInstance();
      $experiences = $disney->getAvailableExperiences($vacation->startTravelDate, $vacation->endTravelDate);
      Log::info('Available experiences retrieved for vacation', [
        'vacation_id' => $vacation->id,
        'startTravelDate' => $vacation->startTravelDate,
        'endTravelDate' => $vacation->endTravelDate,
        'experiences count' => count($experiences),
      ]);
      return response()->json($experiences, 200);
    }

    /**
     * Get default list of experience options
     *
     * @return \Illuminate\Http\Response
     */
    public function getDefaultExperiences() {
      $disney = DisneyData::getInstance();
      $experiences = $disney->getExperienceDefaultsIds();
      Log::info('Default list of experience options retrieved.');
      return response()->json($experiences, 200);
    }
}
