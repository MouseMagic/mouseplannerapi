<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Character;
use App\Models\MapCharacter;

class CharacterController extends Controller
{
    public function __construct(){
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return Character::all()->makeVisible('name');
    }
    /**
     * Display a listing of the mapped values between
     * Disney data and API data.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexMappedValues() {
        return MapCharacter::all();
    }
    /**
     * Display a listing of the mapped value between
     * Disney data and API data.
     *
     * @return \Illuminate\Http\Response
     */
    public function showMappedValue(Character $character) {
        return $character->map_character;
    }
}
