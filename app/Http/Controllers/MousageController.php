<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Models\Vacation;
use App\Models\MousePlan;
use App\Models\Day;
use App\Models\Block;
use App\Models\Mousage;
use App\Repositories\ChecklistRepository;

class MousageController extends Controller {
    private $ChecklistRepository;

    public function __construct(ChecklistRepository $checklistRepository){
        $this->middleware('auth:api');
        $this->ChecklistRepository = $checklistRepository;
    }
    /**
     * Display a listing of the mousages of the Vacation.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexVacationMousages(Vacation $vacation) {
        return $vacation->mousages;
    }
    /**
     * Display a listing of the mousages of the MousePlan.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexMousePlanMousages(MousePlan $mouseplan) {
        return $mouseplan->mousages;
    }
    /**
     * Display a listing of the mousages of the Day.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexDayMousages(Day $day) {
        return $day->mousages;
    }
    /**
     * Display a listing of the mousages of the Block.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexBlockMousages(Block $block) {
        return $block->mousages;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Mousage $mousage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mousage $mousage) {
        if (!$request->has('read'))  {
            return response()->json(['error' => "Field 'read' not found."], 400);
        }
        $attributes = $request->only('read');
        $attributes['read'] = filter_var($attributes['read'], FILTER_VALIDATE_BOOLEAN);
        Log::debug("Updating mousage.",
            ['mousage_id' => $mousage->id, 'attributes to update' => $attributes]);

        $mousage->update($attributes);
        return response()->json(['mousage uptaded' => $mousage->id], 201);
    }

    public function convertToChecklistItem(Mousage $mousage) {
        $text = $mousage->text;
        $type = $mousage->mousage_type_id;
        if($mousage->is_checklist_task) {
        	$checklist = $this->ChecklistRepository->upsert($type, $text);
        	$mousage->delete();
        	return response()->json($checklist->toArray(), 201);
        }
        return response()->json(['error' => 'Mousage is not checklist task.'], 400);
    }

}
