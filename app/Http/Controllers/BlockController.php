<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Models\Block;
use App\Repositories\BlockRepository;

class BlockController extends Controller
{

  private $BlockRepository;

  public function __construct(BlockRepository $blockRepository) {
    $this->middleware('auth:api');
    $this->BlockRepository = $blockRepository;
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request) {
    $attributes = $request->all();
    Log::debug("Creating block.",
      ['attributes' => $attributes]);
    $answer = $this->BlockRepository->create($attributes, true);
    return response()->json($answer, 201);
  }

  /**
   * Display the specified resource.
   *
   * @param  Block $block
   * @return \Illuminate\Http\Response
   */
  public function show(Block $block) {
      return $this->BlockRepository->get($block);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  Block $block
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Block $block) {
    $attributes = $request->all();
    Log::debug("Updating block.",
        ['vacation_id' => $block->day->mouse_plan->vacation->id, 'block_id' => $block->id, 'attributes to update' => $attributes]);

    $answer = $this->BlockRepository->updateModel($block, $attributes, true);
    return response()->json($answer, 201);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  Block $block
   * @return \Illuminate\Http\Response
   */
  public function destroy(Block $block) {
    $answer = $this->BlockRepository->deleteModel($block);
    return response()->json($answer, 201);
  }
}
