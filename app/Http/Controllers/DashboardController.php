<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule as ValidationRules;

use App\Models\Trip;
use App\Models\Vacation;
use App\Repositories\TripRepository;
use App\Repositories\VacationRepository;
use App\Repositories\MousePlanRepository;
use App\Repositories\TravelerRepository;
use App\Utils\DateUtils;
use Illuminate\Support\Facades\Validator;
use Sabberworm\CSS\Rule\Rule;
use Illuminate\Database\Eloquent\Collection;

class DashboardController extends Controller
{
    private $TripRepository;
    private $VacationRepository;
    private $TravelerRepository;

    public function __construct(
      TripRepository $tripRepository,
      VacationRepository $vacationRepository,
      TravelerRepository $travelerRepository
    )
    {
      $this->middleware('auth:api');
      $this->TripRepository = $tripRepository;
      $this->VacationRepository = $vacationRepository;
      $this->TravelerRepository = $travelerRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Display a listing of the trips that belongs to a specific user.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexTrips()
    {
        return $this->TripRepository->getAllTripsForAuthUser();
    }


    public function indexUserVacations(Request $request)
    {
        $validator = Validator::make($request->only('email'), [
            'email' => 'required|string|email|max:255',
        ]);
            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            }else{
                $user = User::where('email', $request->email)->first();
                if($user){
                    return $user->vacations()->get();
                }else{
                    return response()->json(['noUser' => 'User does not exists'], 400);
                }
            }
    }



    /**
     * Display a listing of the mouseplans that belongs to a specific trip.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexPlans(Trip $trip)
    {
        $answer = array();
        foreach ($trip->vacation as $vacation) {
            $plan = $this->getPlanSummary($vacation);
            array_push($answer, $plan);
        }
        return $answer;
    }

    private function newDuplicatedName($name) {
      $prefix = 'Copy of ';

      // "Copy of Fun Vacation (2017/06/01 13:03:25)"
      $position = stripos($name, $prefix);
      $actualName = $name;
      if ($position !== false) {
        $actualName = substr($name, $position + 8);
        if(!$actualName) {
          $actualName = "";
        }
      }
      $position = stripos($actualName, '(20');
      if ($position !== false) {
        $actualName = substr($actualName, 0, $position - 1);
        if(!$actualName) {
          $actualName = "";
        }
      }
      // Remove the overflow, a name cannot be greater than 40 chars
      $dateSize = 22;
      $complementaryNameSize = strlen($prefix) + $dateSize;
      $maxNameSize = 40;
      if(strlen($actualName) + $complementaryNameSize > $maxNameSize) {
        $overflow = (strlen($actualName) + $complementaryNameSize) - $maxNameSize;
        $actualName = substr($actualName, 0, $overflow * -1);
      }
      // Build final new  name
      $date = DateUtils::currentDate();
      return "{$prefix}{$actualName} ({$date})";
    }
    /**
     * Duplicate a vacation Model with all its relationships
     *
     * @return \Illuminate\Http\Response
     */
    public function duplicatePlan(Vacation $vacation)
    {
        // Duplicate vacation row
        $newVacation = $vacation->replicate();
        $newVacation->name = $this->newDuplicatedName($vacation->name);
        $newVacation->stepNumber = 0;
        $newVacation->save();
        Log::debug("vacation duplicated", ['vacation_id' => $newVacation->id]);

       $oldMousePlan = $vacation->mouse_plan;
        if ($oldMousePlan != null) {
             //Duplicate mouse plan row
            $newMousePlan = $oldMousePlan->replicate();
            $newMousePlan->vacation()->associate($newVacation);
            $newMousePlan->save();
            Log::debug("mouseplan duplicated", ['vacation_id' => $newVacation->id,
                'mouse_plan_id' => $newMousePlan->id]);
             //Duplicate days and blocks into new MousePlan
            MousePlanRepository::duplicateDays($newMousePlan, $oldMousePlan->days);
        }
        // Duplicate travelers
        $travelers = $vacation->travelers->all();

        foreach ($travelers as $traveler) {
            $newTraveler = $this->TravelerRepository->duplicate($newVacation, $traveler);
        }
        // $newVacation->travelers()->saveMany($travelers);
        Log::debug("travelers duplicated", ['vacation_id' => $newVacation->id]);
        // Attach characters
        $characters = $vacation->characters;
        foreach ($characters as $character) {
            $newVacation->characters()->attach($character->id, ['user_id' => $vacation->trip->user_id]);
        }
        Log::debug("characters duplicated", ['vacation_id' => $newVacation->id]);
        // Attach interest_types
        $interest_types = $vacation->interest_types;
        foreach ($interest_types as $interest_type) {
            $newVacation->interest_types()->attach($interest_type->id, ['user_id' => $vacation->trip->user_id]);
        }
        Log::debug("interest types duplicated", ['vacation_id' => $newVacation->id]);
        // Attach dining_preferences
        $dining_preferences = $vacation->dining_preferences;
        foreach ($dining_preferences as $dining_preference) {
            $newVacation->dining_preferences()->attach($dining_preference->id);
        }
        Log::debug("dining preferences duplicated", ['vacation_id' => $newVacation->id]);
        // Attach celebration_types
        $celebration_types = $vacation->celebration_types;
        foreach ($celebration_types as $celebration_type) {
            $newVacation->celebration_types()->attach($celebration_type->id);
        }
        Log::debug("celebration typess duplicated", ['vacation_id' => $newVacation->id]);
        // Duplicate vacation parks
        $parks = $vacation->parks;
        if ($parks != null) {
            foreach ($parks as $oldPark) {
                $newVacation->parks()->attach($oldPark->id, ['hearts' => $oldPark->hearts]);
            }
        }
        Log::debug("vacations parks duplicated", ['vacation_id' => $newVacation->id]);
        // Duplicate experiences
        $experiences = $vacation->experiences;
        if($experiences != null) {
            foreach ($experiences as $oldExperience) {
                $newVacation->experiences()->attach($oldExperience->id, ['hearts' => $oldExperience->hearts]);
            }
        }
        Log::debug("vacations experiences duplicated", ['vacation_id' => $newVacation->id]);

        $newVacation->firstname = Auth::user()->firstname;
        $newVacation->lastname = Auth::user()->lastname;
        $newVacation->email = Auth::user()->email;
      return  $this->VacationRepository->buildVacationAnswer($newVacation);

    }

    /**
     * Delete a vacation Model with all its relationships
     *
     * @return \Illuminate\Http\Response
     */
    public function deletePlan(Vacation $vacation)
    {
        if(Auth::user()->hasRole('admin') || Auth::user()->vacations->find($vacation->id)){
            $vacation_id = $vacation->id;
            $this->VacationRepository->delete($vacation);
            return response()->json(['vacation_id' => $vacation_id], 204);
        }
        return response()->json(['Access Denied'], 403);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getPlanSummary($vacation) {
        $mouseplan = $vacation->mouse_plan;
        if ($vacation->mouse_plan != null) {
            $mouseplanData = $vacation->mouse_plan->makeVisible('dashboardImg')->toArray();
        } else {
            $mouseplanData = array(
                'vacation_id' => $vacation->id,
                'tripScore' => 0,
                'dashboardImg' => '',
            );
        }
        $vacationData = array(
            'name' => $vacation->name,
            'from' => DateUtils::getTimeWithFormat($vacation->startTravelDate),
            'to' => DateUtils::getTimeWithFormat($vacation->endTravelDate),
            'trip_id' => $vacation->trip->id,
            'updated_at' => DateUtils::getTimeWithFormat($vacation->updated_at),
        );
        return array_merge($mouseplanData,$vacationData);
    }
}
