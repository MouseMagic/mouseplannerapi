<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\AccessibilityType;
use App\Models\MapAccessibilityType;

class AccessibilityController extends Controller
{
    public function __construct(){
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return AccessibilityType::all()->makeVisible('name')->makeVisible('description')->makeVisible('img');
    }
    /**
     * Display a listing of the mapped values between
     * Disney data and API data.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexMappedValues() {
        return MapAccessibilityType::all();
    }
    /**
     * Display a listing of the mapped value between
     * Disney data and API data.
     *
     * @return \Illuminate\Http\Response
     */
    public function showMappedValue(AccessibilityType $access_type) {
        return $access_type->map_accessibility_type;
    }
}
