<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Park;
use App\Models\MapPark;

class ParkController extends Controller
{
    public function __construct(){
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return Park::all()->makeHidden('hearts')->makeVisible('name')->makeVisible('description')->makeVisible('linkto')->makeVisible('initials');
    }
    /**
     * Display a listing of the mapped values between
     * Disney data and API data.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexMappedValues() {
        return MapPark::all();
    }
    /**
     * Display a listing of the mapped value between
     * Disney data and API data.
     *
     * @param Park $park
     * @return \Illuminate\Http\Response
     */
    public function showMappedValue(Park $park) {
        return $park->map_park;
    }
}
