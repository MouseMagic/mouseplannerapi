<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;


use App\Repositories\ConfigRepository;
use App\Repositories\MousePlanRepository;
use App\Repositories\VacationRepository;
use App\Repositories\BlockRepository;
use App\Repositories\DayRepository;
use App\Models\MousePlan;

class MousePlanController extends Controller
{
  private $MousePlanRepository;
  private $DayRepository;
  private $Vacation;
  private $Config;

  public function __construct(MousePlanRepository $MousePlanRepository,
                              ConfigRepository $config,
                              VacationRepository $vacation,
                              BlockRepository $blockRepository,
                              DayRepository $dayRepository){
    $this->middleware('auth:api');
    $this->MousePlanRepository = $MousePlanRepository;
    $this->Vacation = $vacation;
    $this->Config = $config;
    $this->BlockRepository = $blockRepository;
    $this->DayRepository = $dayRepository;
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return $this->MousePlanRepository->getAll();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  MousePlan $mouseplan
     * @return \Illuminate\Http\Response
     */
    public function show(MousePlan $mouseplan) {
        return $this->MousePlanRepository->getMousePlan($mouseplan);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  MousePlan $mouseplan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MousePlan $mouseplan) {
        $vacation = $mouseplan->vacation;
        $days = $request->input('days');
        if ($days == null) {
            abort(500, 'Invalid json.');
        }
        $this->DayRepository->updateDayArray($days, $mouseplan);
        $this->verifyMouseplan($mouseplan);
        Log::info("MousePlan record updated.", [
            'vacation_id' => $vacation->id,
            'mouseplan_id' => $mouseplan->id
        ]);
        $mouseplanAnswer = $this->MousePlanRepository->getMousePlan($mouseplan);
        Log::debug("Mouseplan updated.", $mouseplanAnswer->toArray());
        return response()->json($mouseplanAnswer, 201);
    }

    private function verifyMouseplan(MousePlan $mouseplan) {
        list($dates, $blockCollection) = $this->init($mouseplan);
        $scoreParks = $this->Vacation->getScoreByPark($mouseplan->vacation);
        $parksPerDayKey = $this->Vacation->getParksPerDayKey($mouseplan->vacation);
        $parksPerDay = $this->Config->getValue('blocks', $parksPerDayKey);

        $parkInfo = $this->BlockRepository->getParkInfoCollection($mouseplan->vacation, $scoreParks, $dates, $parksPerDay);
        $touringHours = 0;
        $this->BlockRepository->verifyBlocks(
            $blockCollection,
            $mouseplan->vacation,
            $scoreParks,
            $parkInfo,
            $touringHours,
            $parksPerDay,
            array(4,5,7)
        );
        $this->BlockRepository->storeNewBlocks($blockCollection);
        $mouseplan->tripScore = $this->MousePlanRepository->calculateTripScore(
          $mouseplan->vacation,
          $touringHours,
          $blockCollection
        );
        $mouseplan->save();
    }

    private function init(MousePlan &$mouseplan) {
        $dates = array();
        $blockCollection = new \Illuminate\Database\Eloquent\Collection;
        foreach ($mouseplan->days as $day) {
            $dates[] = $day->date;
            foreach ($day->blocks as $block) {
                $blockCollection->push($block);
            }
        }
        return array($dates, $blockCollection);
    }
}
