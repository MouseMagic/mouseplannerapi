<?php

namespace App\Http\Controllers;

use Dotenv\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Repositories\VacationRepository;
use App\Models\Vacation;
use App\Models\Trip;
use App\Exceptions\MousePlannerException;

class VacationController extends Controller
{
  private $VacationRepository;

  public function __construct(VacationRepository $VacationRepository){
    $this->middleware('auth:api');
    $this->VacationRepository = $VacationRepository;
  }

  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Request $request)
  {
      return $this->VacationRepository->getAll($request->sort, $request->search);
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    //
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
      $trip = Trip::find($request->trip_id);
      if ($trip == null) abort(500, "Trip not found [{$request->trip_id}]");
      Log::debug("Trip found.", ['trip_id' => $request->trip_id]);

      $vacation = new Vacation;
      if($request->has('name')) {
        $vacation->name = $request->name;
      } else {
        $vacation->name = $trip->name;
      }
      $vacation->trip()->associate($trip);
      $vacation->save();
      Log::info("New vacation record created.", ['vacation_id' => $vacation->id]);
      return response()->json($vacation, 201);
  }

  /**
  * Display the specified resource.
  *
  * @param  Vacation $vacation
  * @return \Illuminate\Http\Response
  */
  public function show(Vacation $vacation) {
    return $this->VacationRepository->show($vacation);
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id) {

  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  Vacation $vacation
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, Vacation $vacation) {
      $attributes = $request->all();
      Log::debug("Updating vacation.",
          ['vacation_id' => $vacation->id, 'attributes to update' => $attributes]);
      try {
        $this->VacationRepository->update($vacation, $attributes);
      } catch (MousePlannerException $mpe) {
        Log::critical("Vacation wasn't updated", [$mpe->getMessage()]);
        $abortMessage = "Vacation couldn't be updated due to exception: '{$mpe->getMessage()}'";
        abort($mpe->status(), $abortMessage);
      } catch (\Exception $e) {
        Log::critical("Vacation wasn't updated", ['vacation_id'=>$vacation->id, 'exception'=>$e->__toString()]);
        $abortMessage = "Vacation couldn't be updated:{$vacation->id} due to unexpected exception: '{$e->getMessage()}'";
        abort(500, $abortMessage);
      }
      return response()->json(['vacation uptaded' => $vacation->id], 201);
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    //
  }
}
