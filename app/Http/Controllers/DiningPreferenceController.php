<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\DiningPreferenceType;

class DiningPreferenceController extends Controller
{
    public function __construct(){
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return DiningPreferenceType::all()->makeVisible('name');
    }
}
