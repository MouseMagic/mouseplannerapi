<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Repositories\TripRepository;
use App\Repositories\MousePlanRepository;
use App\Models\Trip;
use App\Models\Vacation;
use App\User;
use Auth;

class TripController extends Controller
{
  private $TripRepository;

  public function __construct(TripRepository $TripRepository){
    $this->middleware('auth:api');
    $this->TripRepository = $TripRepository;
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return $this->TripRepository->getAllTripsForAuthUser();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        if ($user == null) abort(401, "User not logged in.");

        $trip = new Trip;
        $trip->user()->associate($user);
        $trip->name = $request->name;
        $trip->training_completed = true;
        
        $tripCount = $request->tripCount;
        $trip->tripCount = $tripCount;
        $trip->firstTrip = $tripCount == 0;

        if ($request->has('lastVisit')) {
            $trip->lastVisit = $request->lastVisit;
        }
        $trip->save();
        Log::debug("New trip record created.", ['trip_id' => $trip->id, 'tripName' => $trip->name]);

        $vacation = new Vacation;
        $vacation->name = $request->name;
        $vacation->trip()->associate($trip);
        $vacation->save();
        $tripAnswer = array_merge($trip->toArray(), array('vacation_id' => $vacation->id));

        return response()->json($tripAnswer, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  Trip $trip
     * @return \Illuminate\Http\Response
     */
    public function show(Trip $trip) {
        return $trip;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Trip $trip)
    {
        $trip->update($request->all());

        return response()->json($trip, 204);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Trip $trip
     * @return \Illuminate\Http\Response
     */
    public function destroy(Trip $trip) {
      $trip_id = $trip->id;
      $this->TripRepository->delete($trip);
      return response()->json(['trip_id' => $trip_id], 204);
    }
}
