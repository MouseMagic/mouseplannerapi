<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Socialite;
use App\Services\SocialiteAccountService;
//use Stripe\Stripe;
use Illuminate\Support\Arr;

class SocialiteController extends Controller
{

    public function clientId($provider)
    {
        return config("services.{$provider}.client_id");
    }

    public function authGoogleUser(Request $request, SocialiteAccountService $service)
    {
        $provider = 'google';
        $token = $request->input('token');
        $accessToken = '';

        $client = new \Google_Client(['cliend_id' => config('services.facebook.client_id')]);
        $payload = $client->verifyIdToken($token);

        if ($payload && isset($payload['sub'])) {
            // Temporary fix
            $socialUser = (new \Laravel\Socialite\Two\User())->setRaw($payload)->map([
                'id' => $payload['sub'], 'nickname' => Arr::get($payload, 'name'), 'name' => $payload['name'],
                'email' => $payload['email'], 'avatar' => Arr::get($payload, 'picture'),
            ]);

            $user = $service->createOrGetUser($socialUser, $provider);
            $accessToken = $user->createToken('')->accessToken;
        }

        return response()->json([
            'access_token' => $accessToken,
        ]);
    }

    public function authFacebookUser(Request $request, SocialiteAccountService $service)
    {
        $provider = 'facebook';
        $accessToken = $request->input('token');
        $email = $request->input('email');

        try {
            $socialiteUser = Socialite::with($provider)->scopes(['email'])->userFromToken($accessToken);

            $user = $service->createOrGetUser($socialiteUser, $provider, $email);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage(), 'code' => $ex->getCode()], 400);
        }

        return response()->json([
            'access_token' => $user->createToken('')->accessToken,
        ]);
    }

}
