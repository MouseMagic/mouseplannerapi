<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response|\Illuminate\Contracts\Routing\ResponseFactory
     */
    public function reset(Request $request)
    {
        $success = true;
        $statusCode = 200;
        $messages = [];

        $validator = validator($request->only('password','password_confirmation','token'), $this->rules());

        if ($validator->fails()) {
            $success = false;
            $statusCode = 422;
            $validation_errors = [];
            foreach ($validator->errors()->messages() as $msg) {
                $messages = array_merge($validation_errors, $msg);
            }
        } else {
            $item = DB::table(config('auth.passwords.users.table'))->where('token', $request->input('token'))->first();

            $email = $item ? $item->email : '';
            $request->merge(['email' => $email]);

            $response = $this->broker()->reset(
                $this->credentials($request), function ($user, $password) {
                    $this->resetPassword($user, $password);
                }
            );

            if ($response != Password::PASSWORD_RESET) {
                $success = false;
                $statusCode = 400;
            }

            $messages[] = trans($response);
        }

        return response()->json([
            'success' => $success,
            'messages' => $messages,
        ], $statusCode);
    }

    /**
     * Get the password reset validation rules.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'token' => 'required',
            'password' => 'required|string|confirmed|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$/',
        ];
    }
}
