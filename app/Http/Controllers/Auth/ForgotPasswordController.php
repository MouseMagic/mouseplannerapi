<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response|\Illuminate\Contracts\Routing\ResponseFactory
     */
    public function sendResetLinkEmail(Request $request)
    {
        $success = true;
        $statusCode = 200;
        $messages = [];

        $validator = Validator::make($request->all(), ['email' => 'required|email']);
        if ($validator->fails()) {
            $success = false;
            $statusCode = 422;
            $validation_errors = [];
            foreach ($validator->errors()->messages() as $msg) {
                $messages = array_merge($validation_errors, $msg);
            }
        } else {
            $response = $this->broker()->sendResetLink(
                $request->only('email')
            );

            if ($response != Password::RESET_LINK_SENT) {
                $success = false;
                $statusCode = 400;
            }

            $messages[] = trans($response);
        }

        return response()->json([
            'success' => $success,
            'messages' => $messages,
        ], $statusCode);
    }

}
