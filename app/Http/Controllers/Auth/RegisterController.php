<?php

namespace App\Http\Controllers\Auth;

use App\Services\StripeService;
use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use Stripe\Stripe;
use Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    protected  $user;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
//        $this->middleware('guest');
        $this->user = $user;
    }

    /**
     * Register new user and log in
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function register(Request $request)
    {
        $v = validator($request->only('email', 'firstname', 'password','password_confirmation'), [
            'email' => 'required|string|email|max:255|unique:users',
            'firstname' => 'required|string|max:255',
            'password' => 'required|string|confirmed|min:8|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$/',
        ]);

        if ($v->fails()) {
            return response()->json($v->errors(), 400);
        }
        $data = request()->only('email', 'password', 'firstname');
        $data = array_map('trim', $data);

        $confirmationCode = hash_hmac('sha256', str_random(40), config('app.key'));

        $names = explode(' ', $data['firstname']);
        //Remove the fist element of the array (first name), and set everything else as last name
        $firstname = array_shift($names);
        $lastname = implode(' ', $names);

        $user = $this->user->create([
            'email' => $data['email'],
            'firstname' => $firstname,
            'lastname' => $lastname,
            'password' => bcrypt($data['password']),
            'confirmation_code' => $confirmationCode,
        ]);
        $user->attachRole(\App\Models\Role::where('name','client')->first());

        $email_data = [
            'confirmation_url' => config('app.frontend_url') . '/#!/registration/verify/' . $confirmationCode,
            'username' => sprintf('%s %s', $user->firstname, $user->lastname),
        ];

        Mail::queue('emails.user_verify', $email_data, function($m) use ($user) {
            // For test
            $m->from(config('mail.from.address'),
                config('mail.from.name'));
            $m->to($user->email, '')
                ->subject('Thanks for signing up!');
        });

        return response()->json([
            'message' => 'Thanks for signing up! Please check your email.',
        ]);
    }

    public function confirm_registration(Request $request, StripeService $service, $confirmation_code)
    {
        if(!$confirmation_code) {
            return response()->json(['message' => 'Confirmation code is not defined.'], 400);
        }

        $user = User::whereConfirmationCode($confirmation_code)->first();

        if (!$user) {
            return response()->json(['message' => 'User is not exists.'], 400);
        }

        $user->confirmed = 1;
        $user->confirmation_code = null;
        $user->update();

        try {
            // Create Strice customer
            $service->createCustomer($user);
        } catch (\Exception $ex) {
            return response()->json(['message' => $ex->getMessage()], 400);
        }

        return response()->json(['message' => 'Confirmed']);
    }
}
