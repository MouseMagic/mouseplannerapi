<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\ThrillLevelType;
use App\Models\MapThrillLevelType;

class ThrillLevelController extends Controller
{
    public function __construct(){
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return ThrillLevelType::all();
    }
    /**
     * Display a listing of the mapped values between
     * Disney data and API data.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexMappedValues() {
        return MapThrillLevelType::all();
    }
    /**
     * Display a listing of the mapped value between
     * Disney data and API data.
     *
     * @param MapThrillLevelType $thrill_level
     * @return \Illuminate\Http\Response
     */
    public function showMappedValue(ThrillLevelType $thrill_level) {
        return $thrill_level->map_thrill_level_types;
    }
}
