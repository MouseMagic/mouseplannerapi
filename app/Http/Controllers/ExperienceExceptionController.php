<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Models\ExperienceException;

class ExperienceExceptionController extends Controller
{
    public function __construct(){
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the mapped values between
     * Disney data and API data.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return ExperienceException::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $experience_exception = new ExperienceException;
        $experience_exception->disneyId = $request->disneyId;
        $experience_exception->startMonth = $request->startMonth;
        $experience_exception->startDay = $request->startDay;
        $experience_exception->endMonth = $request->endMonth;
        $experience_exception->endDay = $request->endDay;
        $experience_exception->save();
        Log::info("New experience exception record created.", ['experience_exception_id' => $experience_exception->id]);
        return response()->json($experience_exception, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  ExperienceException $experience_exception
     * @return \Illuminate\Http\Response
     */
    public function show(ExperienceException $experience_exception) {
        return $experience_exception;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  ExperienceException $experience_exception
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ExperienceException $experience_exception) {
        $attributes = $request->all();
        Log::debug("Updating exception.",
            ['experience_exception_id' => $experience_exception->id, 'attributes to update' => $attributes]);

        $experience_exception->update($attributes);
        return response()->json(['experience exception uptaded' => $experience_exception->id], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  ExperienceException $experience_exception
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExperienceException $experience_exception) {
        $experience_exception->delete();
        return response()->json(['exception'], 204);
    }
}
