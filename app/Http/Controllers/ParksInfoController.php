<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Utils\DisneyData;
use App\Models\Park;
use App\Models\Vacation;
use App\Repositories\VacationRepository;

class ParksInfoController extends Controller {
    private $Vacation;

    public function __construct(VacationRepository $vacation) {
      $this->middleware('auth:api');
      $this->Vacation = $vacation;
    }

    /**
     * Display the specified resource.
     *
     * @param  Park $park
     * @param  Vacation $vacation
     * @return \Illuminate\Http\Response
     */
    public function showParkHours(Park $park, Vacation $vacation) {
        $disney = DisneyData::getInstance();
        $dates = $this->Vacation->createDayArray($vacation);
        $park_details = $disney->getParkDetails($park->initials, $dates);
        Log::debug('Getting park schedules / details', ['vacation_id'=>$vacation->id, 'park'=>$park->name, 'details'=>$park_details]);
        return response()->json($park_details, 200);
    }
}
