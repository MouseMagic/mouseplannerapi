<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\TravelerHeight;
use App\Models\MapTravelerHeight;

class TravelerHeightController extends Controller
{
    public function __construct(){
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return TravelerHeight::all();
    }
    /**
     * Display a listing of the mapped values between
     * Disney data and API data.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexMappedValues() {
        return MapTravelerHeight::all();
    }
    /**
     * Display a listing of the mapped value between
     * Disney data and API data.
     *
     * @param TravelerHeight $height
     * @return \Illuminate\Http\Response
     */
    public function showMappedValue(TravelerHeight $height) {
        return $height->map_traveler_height;
    }
}
