<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Models\MapAge;

class AgeController extends Controller
{
    public function __construct(){
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the mapped values between
     * Disney data and API data.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexMappedValues() {
        return MapAge::all();
    }
    /**
     * Display a listing of the mapped value between
     * Disney data and API data.
     *
     * @return \Illuminate\Http\Response
     */
    public function showMappedValue($age) {
        $allAges = MapAge::all();
        $ages = collect([]);
        foreach ($allAges as $a) {
            if ($age >= $a->min && $age <= $a->max) {
                $ages->push($a);
            }
        }
        return $ages;
    }
}
