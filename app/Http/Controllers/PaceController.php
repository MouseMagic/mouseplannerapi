<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\PaceType;

class PaceController extends Controller
{
    public function __construct(){
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return PaceType::all();
    }
}
