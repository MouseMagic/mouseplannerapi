<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Repositories\ExperienceRepository;
use App\Models\Experience;

class ExperienceController extends Controller {
  private $ExperienceRepository;

  public function __construct(ExperienceRepository $experienceRepository){
    $this->middleware('auth:api');
    $this->ExperienceRepository = $experienceRepository;
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return $this->ExperienceRepository->getAll();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $attributes = $request->all();
        Log::debug("Creating experience.",['attributes to store' => $attributes]);
        $experience = $this->ExperienceRepository->create($attributes);
        Log::info("New experience record created.", ['experience_id' => $experience->id]);
        return response()->json(
          $experience
          ->makeHidden('hearts')
          ->makeVisible('id')
          ->makeVisible('external_name')
          ->makeVisible('external_description')
          ->makeVisible('external_facets'), 201
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  VacationExperience $experience
     * @return \Illuminate\Http\Response
     */
    public function show(Experience $experience) {
        return $experience;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Experience $experience
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Experience $experience) {
        $attributes = $request->all();
        Log::debug("Updating experience.",
            ['experience_id' => $experience->id, 'attributes to update' => $attributes]);
        $this->ExperienceRepository->update($experience, $attributes);
        Log::info("New experience record created.", [$experience->id]);
        return response()->json($experience->id, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Experience $experience
     * @return \Illuminate\Http\Response
     */
    public function destroy(Experience $experience) {
        $this->ExperienceRepository->delete($experience);
        return response()->json(['experience'], 204);
    }
}
