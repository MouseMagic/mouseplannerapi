<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Resort;

class ResortController extends Controller
{
    public function __construct(){
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return Resort::all()->sortBy('name')->values();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) { }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Resort $resort
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Resort $resort) { }
}
