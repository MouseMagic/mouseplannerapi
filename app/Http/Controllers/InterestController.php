<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\InterestType;
use App\Models\MapInterestType;

class InterestController extends Controller
{
    public function __construct(){
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return InterestType::all()->makeVisible('name');
    }
    /**
     * Display a listing of the mapped values between
     * Disney data and API data.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexMappedValues() {
        return MapInterestType::all();
    }
    /**
     * Display a listing of the mapped value between
     * Disney data and API data.
     *
     * @param InterestType $interest
     * @return \Illuminate\Http\Response
     */
    public function showMappedValue(InterestType $interest) {
        return $interest->map_interest_types;
    }
}
