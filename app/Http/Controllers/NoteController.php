<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Models\Day;
use App\Models\Block;
use App\Models\Note;

class NoteController extends Controller
{
    public function __construct(){
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the notes of the Day.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexDayNotes(Day $day) {
        return $day->notes;
    }
    /**
     * Display a listing of the notes of the Block.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexBlockNotes(Block $block) {
        return $block->notes;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $note = new Note;
        $note->noteable_id = $request->noteable_id;
        $note->noteable_type = $request->noteable_type;
        $note->text = $request->text;
        $note->save();
        Log::info("New note record created.", ['note_id' => $note->id]);
        return response()->json($note, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  Note $note
     * @return \Illuminate\Http\Response
     */
    public function show(Note $note) {
        return $note;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Note $note
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Note $note) {
        $attributes = $request->all();
        Log::debug("Updating note.",
            ['note_id' => $note->id, 'attributes to update' => $attributes]);

        $note->update($attributes);
        return response()->json(['note uptaded' => $note->id], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Note $note
     * @return \Illuminate\Http\Response
     */
    public function destroy(Note $note) {
        $note->delete();
        return response()->json(['note'], 204);
    }
}
