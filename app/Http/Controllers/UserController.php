<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

use App\User;
use App\Models\Trip;
use Auth;
use Illuminate\Support\Facades\Session;

class UserController extends Controller {

    public function __construct(){
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        // TODO return users handled by logged in agent only
        if(Auth::user()->hasRole('admin')){
            return User::all();
        }
        return response()->json(['Access Denied'], 403);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $user = new User;
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->role_id = $request->role_id;
        $user->save();
        Log::info("New user record created.", ['user_id' => $user->id]);
        return response()->json($user, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user) {
        // TODO display only if it belongs to the agent loggedin
        if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('agent') ) {
            return $user;
        }
        return response()->json(['Access Denied'], 403);
    }
    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoggedUser() {
        $user = Auth::user();

        $user->is_subscribed = $user->isSubscribed;
        $user->were_subscribed_before = $user->wereSubscribedBefore;
        $user->is_on_trial = $user->isOnTrial;
        $user->role = $user->roles->first()->name;
        $user->role_id = $user->roles->first()->id;

//        if ($user && !$user->confirmed) {
//            return response()->json(['error' => 'Not confirmed'], 400);
//        }

        return $user;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user) {
        // TODO display only if it belongs to the agent loggedin
        // TODO rehash password
        $attributes = $request->all();
        Log::debug("Updating user.",
            ['user_id' => $user->id, 'attributes to update' => $attributes]);

        $user->update($attributes);
        return response()->json(['user uptaded' => $user->id], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user) {
        if(Auth::user()->hasRole('admin') ) {
            $user->delete();
            return response()->json(['user'], 204);
        }
        return response()->json(['Access Denied'], 403);
    }

    public function logout(Request $request)
    {
        if (!Auth::guard('api')->check()) {
            return response()->json([
                'message' => 'No active user session was found',
                'success' => false,
            ], 400);
        }

        $request->user('api')->token()->revoke();

        Session::flush();
        Session::regenerate();

        return response()->json([
            'message' => 'User was logged out',
            'success' => true
        ]);
    }


}
