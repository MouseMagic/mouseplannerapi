<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Models\FastpassReservation;
use App\Models\Experience;
use App\Models\Day;
use App\Models\Park;
use App\Utils\DateUtils;

class FastPassController extends Controller {
    public function __construct(){
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexDayFastPass(Day $day) {
        foreach ($day->fastpass_reservations as $fp) {
            $fp->reservation_time = DateUtils::getTimeWithFormat($fp->reservation_time);
        }
        return $day->fastpass_reservations->sortBy('reservation_time')->values();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $fastpass = new FastpassReservation;
        $day = Day::find($request->day_id);
        $experience = $this->getExperience($request);
        $reservation_time = DateUtils::getTimeWithFormat($request->reservation_time, 'Y-m-d H:i:s');

        $fastpass = FastpassReservation::updateOrCreate(
            ['day_id' => $day->id, 'experience_id' => $experience->id, 'block_id'=> $request->block_id],
            ['reservation_time' => $reservation_time]
        );
        Log::info("New fastpass record created/updated.", ['fastpass_id' => $fastpass->id]);
        $fastpass->reservation_time = DateUtils::getTimeWithFormat($fastpass->reservation_time);
        return response()->json($fastpass, 201);
    }

    private function getExperience(Request $request) {
      if ($request->has('experience_id')) {
        return Experience::findOrFail($request->experience_id);
      }
        $experience = Experience::firstOrCreate([
            'external_id' => $request->external_id
        ]);
        Log::debug('Experience created or retrieved', ['id'=>$experience->id, 'external_id'=>$experience->external_id]);
        return $experience;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  FastpassReservation $fastpass_reservation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FastpassReservation $fastpass_reservation) {
        $attributes = $request->all();
        if(isset($attributes['reservation_time'])) {
            $date = DateUtils::getTimeWithFormat($attributes['reservation_time'], 'Y-m-d H:i:s');
            $attributes['reservation_time'] = $date;
        }
        $vacation = $fastpass_reservation->day->mouse_plan->vacation;
        Log::debug("Updating fastpass reservation.",
            ['vacation_id' => $vacation->id, 'fastpass_id' => $fastpass_reservation->id, 'attributes to update' => $attributes]);

        $fastpass_reservation->update($attributes);
        return response()->json(['fastpass_id' => $fastpass_reservation->id], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  FastpassReservation $fastpass_reservation
     * @return \Illuminate\Http\Response
     */
    public function destroy(FastpassReservation $fastpass_reservation) {
        $fastpass_reservation->delete();
        return response()->json(['fastpass'], 204);
    }
}
