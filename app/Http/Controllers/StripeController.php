<?php

namespace App\Http\Controllers;

use App\Services\StripeService;
use Illuminate\Database\Connection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Stripe\Coupon;
use Stripe\Plan;
use Stripe\Stripe;
use Stripe\Subscription;

class StripeController extends Controller
{

    public function __construct() {
        Stripe::setApiKey(config('services.stripe.secret'));
        $this->middleware('auth:api', ['except' => ['webHooksHandler']]);
    }

    public function getApiKey()
    {
        return config('services.stripe.key');
    }

    /**
     * Return Stripe plans list for general subscription
     *
     * @return \Illuminate\Http\Response
     */
    public function plans()
    {
        $plans_list = [];
        $result = Plan::all();

        if (isset($result['data']) && count($result['data'])) {
            $plans_list = array_filter($result['data'], function($item) {
                return isset($item['metadata']['default_plan']) && (bool)$item['metadata']['default_plan'];
            });
        }

        return response()->json(array_values($plans_list));
    }


    /**
     * Return Stripe coupon object
     *
     * @param  string $coupon
     * @return \Illuminate\Http\Response
     */
    public function couponDetails($coupon)
    {
        $status = 200;
        $result = null;
        $message = '';

        try {
            $result = Coupon::retrieve($coupon);
        } catch (\Exception $ex) {
            $message = $ex->getMessage();
            $status = $ex->getHttpStatus();
        }

        return response()->json(['coupon' => $result, 'message' => $message], $status);
    }

    /**
     * Handle Stripe API events
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Services\StripeService $service
     * @return \Illuminate\Http\Response
     *
     */
    public function webHooksHandler(Request $request, StripeService $service)
    {
        $result = false;

        switch ($request->input('type')) {
            case 'customer.subscription.deleted':
                $result = $service->suspendCustomer($request);

                break;
            case 'customer.subscription.updated':
                $result = $service->updateSubscription($request);

                break;
        }



        return response()->json(['request' => $request->all(), 'success' => $result]);
    }

    /**
     * Create new subscription for customer
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     */
    public function subscribe(Request $request)
    {
        $status = 200;
        $message = '';
        $userData = [];

        $cardToken = $request->input('card_token');
        $planId = $request->input('plan_id');
        $coupon = $request->input('coupon_id');

        $user = Auth::user();
        $sessionData = \GuzzleHttp\json_decode($user->token()->session_data, true);

        try {
            $userData = $user->toArray();

            if (isset($sessionData['subscribe_action_in_progress'])) {
                throw new \Exception('You can not subscribe right now. Please try later.');
            } else {
                $sessionData['subscribe_action_in_progress'] = true;
                $user->token()->update(['session_data' => json_encode($sessionData)]);
            }

            if ($user->calculate_is_subscribed()) {
                throw new \Exception('You already are subscribed');
            }

            $userData['were_subscribed_before'] = $user->wereSubscribedBefore;
            $userData['is_subscribed'] = false;

            $plan = Plan::retrieve($planId);

            $user->updateCard($cardToken);

            if ($coupon) {
                $user->applyCoupon($coupon);
            }

            $subscriptionBuilder = $user->newSubscription($plan['name'], $planId)
                ->skipTrial();

            $newSubscription = $subscriptionBuilder->create();

            /* Cancel the subscription at the end of the current billing period */
            Subscription::update(
                $newSubscription->getAttribute('stripe_id'),
                [
                    'cancel_at_period_end' => true,
                ]
            );
            
            $user->check_trial_and_cancel();

            $sessionData['is_subscribed'] = true;
            $sessionData['is_on_trial'] = false;

            $userData['is_subscribed'] = true;
            $userData['is_on_trial'] = false;
        } catch (\Exception $ex) {
            $message = $ex->getMessage();
            $status = method_exists($ex, 'getHttpStatus') && $ex->getHttpStatus() ? $ex->getHttpStatus() : 409;
        }

        unset($sessionData['subscribe_action_in_progress']);
        $user->token()->update(['session_data' => json_encode($sessionData)]);

        return response()->json(['message' => $message, 'user' => $userData], $status);
    }

}
