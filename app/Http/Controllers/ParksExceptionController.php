<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Models\ParksException;

class ParksExceptionController extends Controller
{

    public function __construct() {
      $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return ParksException::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $park = new ParksException;
        $park->park_id = $request->park_id;
        $park->save();
        Log::info("New park record created.", ['park_id' => $park->id]);
        return response()->json($park, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  ParksException $parks_exception
     * @return \Illuminate\Http\Response
     */
    public function show(ParksException $parks_exception) {
        return $parks_exception;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  ParksException $parks_exception
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ParksException $parks_exception) {
        $attributes = $request->all();
        Log::debug("Updating park exception.",
            ['parks_exception_id' => $parks_exception->id, 'attributes to update' => $attributes]);

        $parks_exception->update($attributes);
        return response()->json(['park exception uptaded' => $parks_exception->id], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  ParksException $parks_exception
     * @return \Illuminate\Http\Response
     */
    public function destroy(ParksException $parks_exception) {
        $parks_exception->delete();
        return response()->json(['park exception'], 204);
    }
}
