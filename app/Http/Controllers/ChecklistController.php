<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Repositories\ChecklistRepository;
use App\Models\Checklist;
use App\Models\Mousage;

class ChecklistController extends Controller
{
    private $ChecklistRepository;

    public function __construct(ChecklistRepository $checklistRepository){
        $this->middleware('auth:api');
        $this->ChecklistRepository = $checklistRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return $this->ChecklistRepository->getAll();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Checklist $checklist) {
        if (!$request->has('done'))  {
            return response()->json(['error' => "Field 'done' not found."], 400);
        }
        $attributes = $request->only('done');
        $attributes['done'] = filter_var($attributes['done'], FILTER_VALIDATE_BOOLEAN);
        Log::debug("Updating checklist.",
            ['checklist_id' => $checklist->id, 'attributes to update' => $attributes]);

        $checklist->update($attributes);
        return response()->json(['checklist uptaded' => $checklist->id], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Checklist $checklist) {
        $checklist->delete();
        return response()->json(['checklist'], 204);
    }
}
