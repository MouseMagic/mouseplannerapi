<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Models\Park;
use App\Models\Resort;

class RestaurantController extends Controller {
    public function __construct(){
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the restaurants of the Park.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexParkRestaurants(Park $park) {
        return $park->restaurants;
    }
    /**
     * Display a listing of the restaurants of the Resort.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexResortRestaurants(Resort $resort) {
        return $resort->restaurants->transform(function ($restaurant) use ($resort) {
            $restaurant->location = $resort->name;
            return $restaurant;
        });
    }
}
