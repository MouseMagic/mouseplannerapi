<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class VerifyStripeWebHook
{

    /**
     * Handle an incoming request from stripe.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        // You can find your endpoint's secret in your webhook settings
        $endpoint_secret = getenv('STRIPE_WEBHOOK_SECRET');

        $payload = $request->getContent();
        $sig_header = $request->server('HTTP_STRIPE_SIGNATURE');

        try {
            \Stripe\Webhook::constructEvent(
                $payload, $sig_header, $endpoint_secret
            );
        } catch(\UnexpectedValueException $e) {
            // Invalid payload
            return response('Invalid payload.', 400);
        } catch(\Stripe\Error\SignatureVerification $e) {
            return response('Invalid signature.', 401);
        }

        return $next($request);
    }

}
