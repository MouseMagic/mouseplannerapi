<?php

namespace App\Http\Responses;

use League\OAuth2\Server\ResponseTypes\BearerTokenResponse;
use League\OAuth2\Server\Entities\AccessTokenEntityInterface;
use App\User;

class UserIdBearerTokenResponse extends BearerTokenResponse {
    protected function getExtraParams(AccessTokenEntityInterface $accessToken) {
    	$user = User::find($this->accessToken->getUserIdentifier());
        return [
            'user_id' => $user->id
        ];
    }
}
