<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Log;

use App\Models\MousePlan;
use App\Utils\DateUtils;
use App\Utils\BlocksUtils;
use App\Repositories\BlockRepository;

class MousePlanRepository implements BaseRepository {

  private $model;

  public function __construct(MousePlan $model){
    $this->model = $model;
  }

  public function getAll(){
    return $this->model->all();
  }

  public function getById($id){
    return $this->model->find($id);
  }

  public function create(array $attributes){
    return $this->model->create($attributes);
  }

  public function update($id, array $attributes){
    $modelUpdate = $this->model->findOrFail($id);
    $modelUpdate->update($attributes);
    return $modelUpdate;
  }

  public function delete($id){
    $this->model->getById($id)->delete();
    return true;
  }

  public function getMousePlan($mouseplan) {
      if ($mouseplan == null) {
          return response()->json(['error' => 'MousePlan has not been generated.'], 400);
      }
    $answer = $mouseplan->load('days.blocks');
    foreach ($answer->days as $day) {
        $day->date = DateUtils::getTimeWithFormat($day->date);
        foreach ($day->blocks as $block) {
            $block->startTime = DateUtils::getTimeWithFormat($block->startTime);
            $block->endTime = DateUtils::getTimeWithFormat($block->endTime);
        }
    }
    return $answer;
  }

  public static function duplicateDays(MousePlan $newMousePlan, $oldDays) {
      $oldToNewBlockIds = [];
      foreach ($oldDays as $oldDay) {
          $newDay = $oldDay->replicate(); 
          $newDay->mouse_plan()->associate($newMousePlan);
          $newDay->save();
          $oldBlocks = $oldDay->blocks;;
          foreach ($oldBlocks as $oldBlock) {
              $newBlock = $oldBlock->replicate();
              $newBlock->save();
              $oldToNewBlockIds[$oldBlock->id] = $newBlock->id;
              if($newBlock['parent_id']) $newBlock['parent_id'] = $oldToNewBlockIds[$newBlock['parent_id']];
              $newBlock->day()->associate($newDay);
              $newBlock->update();
          }
      }
  }

  public static function deleteDays(MousePlan $mouseplan) {
      $days = $mouseplan->days;
      foreach ($days as $day) {
          $blocks = $day->blocks;
          foreach ($blocks as $block) {
              $block->delete();
          }
          $day->delete();
      }
  }

  public static function calculateTripScore($vacation, $touringHours, &$blockCollection) {
      $park_block_types = BlocksUtils::parksInPlanBlockTypeIds();
      $parksBlocks = $blockCollection->whereIn('block_type_id', $park_block_types);
      $actualParkTime = $parksBlocks->sum(function ($block) {
          return DateUtils::getTimeDifference($block->startTime, $block->endTime);
      });
      $tripScore = round($actualParkTime/$touringHours,2)*100;
      if($tripScore < 0) {
        Log::error("TripScore is below zero.", ['vacation_id'=>$vacation->id, 'actual parks time'=>$actualParkTime, 'touringHours' => $touringHours]);
        $tripScore = 0;
      } else if ($tripScore > 100) {
        Log::error("TripScore is above 100.", ['vacation_id'=>$vacation->id, 'actual parks time'=>$actualParkTime, 'touringHours' => $touringHours]);
        $tripScore = 100;
      } else {
        Log::debug("TripScore.", ['vacation_id'=>$vacation->id, 'actual parks time'=>$actualParkTime, 'touringHours' => $touringHours, 'score' => $tripScore]);
      }
      return $tripScore;
  }
}
