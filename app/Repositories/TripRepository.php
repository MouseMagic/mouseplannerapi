<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Log;

use App\Models\Trip;
use App\Repositories\VacationRepository;
use Auth;

class TripRepository
{

    private $model;
    private $VacationRepository;

    public function __construct(Trip $model, VacationRepository $vacationRepository)
    {
        $this->model = $model;
        $this->VacationRepository = $vacationRepository;
    }

    public function getAll()
    {
        return $this->model->all();
    }


    public function getAllTripsForAuthUser()
    {
        $user = Auth::user();
        if ($user->hasRole('admin')) {
            return $this->getAllTripsForAllUsers();
        } else {
            return $user->trips()->get();
        }
    }

    public function getAllTripsForAllUsers()
    {
        $trips = new \Illuminate\Database\Eloquent\Collection;
        $users = \App\User::all();
        foreach ($users as $user) {
            if ($user->calculate_is_subscribed() || $user->calculate_is_on_trial() ) {
                $trips->push($user->trips()->get());
            }
        }
        return $trips;
    }

    public function getById($id)
    {
        return $this->model->find($id);
    }

    public function create(array $attributes)
    {
        return $this->model->create($attributes);
    }

    public function update($id, array $attributes)
    {
        $modelUpdate = $this->model->findOrFail($id);
        $modelUpdate->update($attributes);
        return $modelUpdate;
    }

    public function delete(Trip &$trip)
    {
        $vacations = $trip->vacation;
        $trip_id = $trip->id;
        foreach ($vacations as $vacation) {
            $this->VacationRepository->delete($vacation);
        }
        $trip->delete();
        Log::debug("trip deleted.", ['trip_id' => $trip_id]);
        return true;
    }
}
