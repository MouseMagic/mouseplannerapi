<?php

namespace App\Repositories;

use App\Models\GeneralOperatingHour;
use App\Utils\DateUtils;

trait GeneralOperatingHoursRepository {

  public function getGeneralData($weekYear, $dayWeek) {
      $generalData = GeneralOperatingHour::where([
          ['weekYear', $weekYear],
          ['dayWeek', $dayWeek],
      ])->get();
      return $generalData;
  }

}
