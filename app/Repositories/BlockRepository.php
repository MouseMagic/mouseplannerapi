<?php

namespace App\Repositories;

use ReflectionClass;
use ReflectionException;

use Illuminate\Support\Facades\Log;

use App\Models\Block;
use App\Models\BlockType;
use App\Models\Day;
use App\Models\MousePlan;
use App\Models\Vacation;
use App\Utils\DateUtils;
use App\Utils\BlocksUtils;
use App\Utils\DisneyData;
use App\Repositories\GeneralOperatingHoursRepository;
use App\Repositories\ConfigRepository;
use App\Repositories\MousePlanRepository;

class BlockRepository implements BaseRepository {
    use GeneralOperatingHoursRepository;

  private $model;
  private $Config;

  public function __construct(Block $model,
                              ConfigRepository $config) {
    $this->model = $model;
    $this->Config = $config;
  }

  public function getAll(){
    return $this->model->all();
  }

  public function getById($id){
    return $this->model->findById($id);
  }

  public function get(Block $block){
    return $this->buildBlockAnswer($block);
  }

  public function create(array $attributes, $verify = false) {
      if(isset($attributes['startTime'])) {
          $date = $this->formatTime($attributes['startTime'], 'Y-m-d H:i:s');
          $attributes['startTime'] = $date;
      }
      if(isset($attributes['endTime'])) {
          $date = $this->formatTime($attributes['endTime'], 'Y-m-d H:i:s');
          $attributes['endTime'] = $date;
      }
      if(key_exists('name', $attributes) && $attributes['name'] == null) {
          $attributes['name'] = "";
      }
      if($verify) {
        $block_type_id = $attributes['block_type_id'];
        $block_type = BlockType::find($block_type_id);
        $block_type_name = $block_type->name;
        $day = Day::find($attributes['day_id']);
        $mouseplan = $day->mouse_plan;
        if($block_type_name == 'park') {
          $oldParkTime = $this->actualParkTime($mouseplan);
          $oldTripScore = $mouseplan->tripScore;
          $oldTouringHours = $this->touringHours($oldParkTime, $oldTripScore);
        } else {
          $oldTouringHours = null;
        }
        $block = $this->model->create($attributes);
        $mouseplan->load('days.blocks');
        $this->verify($block, $mouseplan, $block_type_name, $oldTouringHours);
        $this->updateTripScore($mouseplan, $oldTouringHours);
        return $this->buildBlockAnswer($block, $mouseplan->tripScore);
      }
      $block = $this->model->create($attributes);
      return $this->buildBlockAnswer($block);
  }

  public function update($id, array $attributes, $verify = false) {
    $modelUpdate = $this->model->findOrFail($id);
    if(isset($attributes['startTime'])) {
        $date = $this->formatTime($attributes['startTime'], 'Y-m-d H:i:s');
        $attributes['startTime'] = $date;
    }
    if(isset($attributes['endTime'])) {
        $date = $this->formatTime($attributes['endTime'], 'Y-m-d H:i:s');
        $attributes['endTime'] = $date;
    }
    $modelUpdate->update($attributes);
    return $modelUpdate;
  }

  public function updateModel(Block &$block, array $attributes, $verify = false) {
    if(isset($attributes['startTime'])) {
        $date = $this->formatTime($attributes['startTime'], 'Y-m-d H:i:s');
        $attributes['startTime'] = $date;
    }
    if(isset($attributes['endTime'])) {
        $date = $this->formatTime($attributes['endTime'], 'Y-m-d H:i:s');
        $attributes['endTime'] = $date;
    }
    if(key_exists('name', $attributes) && $attributes['name'] == null) {
        $attributes['name'] = "";
    }
    if($verify) {
      $block_type_id = $block->block_type_id;
      $block_type = BlockType::find($block_type_id);
      $block_type_name = $block_type->name;
      $mouseplan = $block->day->mouse_plan;
      if($block_type_name == 'park') {
        $oldParkTime = $this->actualParkTime($mouseplan);
        $oldTripScore = $mouseplan->tripScore;
        $oldTouringHours = $this->touringHours($oldParkTime, $oldTripScore);
      } else {
        $oldTouringHours = null;
      }
      $block->update($attributes);
      $mouseplan->load('days.blocks');
      $this->verify($block, $mouseplan, $block_type_name, $oldTouringHours);
      $this->updateTripScore($mouseplan, $oldTouringHours);
      return $this->buildBlockAnswer($block, $mouseplan->tripScore);
    }
    $block->update($attributes);
  }

  public function delete($id){
    $this->model->getById($id)->delete();
    return true;
  }

  public function deleteModel(Block &$block) {
    $block_type_id = $block->block_type_id;
    $block_type = BlockType::find($block_type_id);
    $block_type_name = $block_type->name;
    $mouseplan = $block->day->mouse_plan;
    if($block_type_name == 'park') {
      $oldParkTime = $this->actualParkTime($mouseplan);
      $oldTripScore = $mouseplan->tripScore;
      $oldTouringHours = $this->touringHours($oldParkTime, $oldTripScore);
    } else {
      $oldTouringHours = null;
    }
    $block->delete();
    $mouseplan->load('days.blocks');
    $this->updateTripScore($mouseplan, $oldTouringHours);
    $answer = [
      'block' => null,
      'tripScore' => $mouseplan->tripScore,
    ];
    return $answer;
  }

  public function destroy(array $ids) {
    $this->model->destroy($ids);
    return true;
  }

  private function verify(&$block, &$mouseplan, $block_type_name, &$touringHours) {
    $day = $block->day;
    $blocks = $day->blocks;
    $filters = $this->blockTypeFilters($block_type_name);
    if($filters == null) return;

    $this->verifyBlocks($blocks,
                        $mouseplan->vacation,
                        null,
                        null,
                        $touringHours,
                        0,
                        $filters);
    // Refresh from DB
    $block = Block::find($block->id);
  }

  private function blockTypeFilters($block_type_name) {
    switch ($block_type_name) {
      case 'park':
        $filters = array(1,2,3,4,5,6,7,8);
        break;
      default:
        $filters = null;
        break;
    }
    return $filters;
  }

  private function scoreParks($vacation, $block_type_name) {
    if ($block_type_name == 'park') {
      $vacationRepository = new VacationRepository($vacation, $this->Config);
      return $vacationRepository->getScoreByPark($vacation);
    }
    return null;
  }

  private function updateTripScore(&$mouseplan, $touringHours) {
    if(isset($touringHours) && $touringHours > 0) {
      $vacation = $mouseplan->vacation;
      $actualParkTime = $this->actualParkTime($mouseplan);
      $tripScore = round($actualParkTime/$touringHours,2)*100;
      if($tripScore < 0) {
        Log::error("TripScore is below zero.", ['vacation_id'=>$vacation->id, 'actual parks time'=>$actualParkTime, 'touringHours' => $touringHours]);
        $tripScore = 0;
      } else if ($tripScore > 100) {
        Log::error("TripScore is above 100.", ['vacation_id'=>$vacation->id, 'actual parks time'=>$actualParkTime, 'touringHours' => $touringHours]);
        $tripScore = 100;
      } else {
        Log::debug("TripScore.", ['vacation_id'=>$vacation->id, 'actual parks time'=>$actualParkTime, 'touringHours' => $touringHours, 'score' => $tripScore]);
      }
      $mouseplan->tripScore = $tripScore;
      $mouseplan->save();
    }
  }

  private function touringHours($actualParkTime, $tripScore) {
    return round($actualParkTime/$tripScore,2)*100;
  }

  private function actualParkTime(&$mouseplan) {
    $blockCollection = new \Illuminate\Database\Eloquent\Collection;
    foreach ($mouseplan->days as $day) {
        foreach ($day->blocks as $block) {
            $blockCollection->push($block);
        }
    }
    $park_block_types = BlocksUtils::parksInPlanBlockTypeIds();
    $parksBlocks = $blockCollection->whereIn('block_type_id', $park_block_types);
    $actualParkTime = $parksBlocks->sum(function ($block) {
        return DateUtils::getTimeDifference($block->startTime, $block->endTime);
    });
    return $actualParkTime;
  }

  private function buildBlockAnswer(Block $block, $tripScore = null) {
      $blockAnswer = $block;
      //change dates format
      $blockAnswer->startTime = $this->formatTime($blockAnswer->startTime);
      $blockAnswer->endTime = $this->formatTime($blockAnswer->endTime);
      if(isset($tripScore)) {
        $blockAnswer = [
          'block' => $blockAnswer,
          'tripScore' => $tripScore,
        ];
      }
      return $blockAnswer;
  }

  public function storeNewBlocks($blocks) {
      foreach ($blocks as $key => $block) {
          if (isset($block->id)) {
              $blocks->forget($key);
          }
      }
      $this->storeBlocks($blocks);
  }

  private function formatTime($time, $format=null) {
      if ($format == null) {
          return DateUtils::getTimeWithFormat($time);
      }
      return DateUtils::getTimeWithFormat($time, $format);
  }
  
  /*
   * Step 7 of the Schedule Logic document.
   * */
  public function getParkInfoCollection(Vacation $vacation, $scoreParks, $dates, $parksPerDay) {
      $lastDate = $dates[count($dates)-1];
      $dates[] = DateUtils::getFutureTime($lastDate, 'P1D', 'Y-m-d');
      $datesInfo = DateUtils::getDateGenericInfo($dates);
      Log::debug("Dates info generated", ['vacation_id'=>$vacation->id, 'dates'=>$datesInfo]);
      $disney = DisneyData::getInstance();
      $blocksUtils = BlocksUtils::getInstance($vacation);
      $emhable = $blocksUtils->emhable($parksPerDay);
      $parkInfo = collect([]);
      foreach ($datesInfo as $dayInfo) {
          $date = $dayInfo['date'];
          $default = $this->getGeneralData($dayInfo['week_year'], $dayInfo['day_week']);
          foreach ($scoreParks as $park => $score) {
              $parkDefault = $default->where('park',$park)->first();
              // Step 6 of the Schedule Logic document.
              $emh = $this->getEMH($disney, $park, $date);
              $validEmh = $this->validEMH($date, $emh, $emhable, $blocksUtils);
              $emhInfo = ['emh'=>$emh, 'validEmh'=>$validEmh];
              $openHour = $this->getParkOpenTime($disney, $emhInfo, $park, $date, $parkDefault->openTime);
              $closeHour = $this->getParkCloseTime($disney, $emhInfo, $park, $date, $parkDefault->closeTime);
              $parkInfo->push([
                  'date' => $date,
                  'park' => $park,
                  'open_hour' => $openHour,
                  'close_hour' => $closeHour,
                  'valid_emh' => $validEmh,
                  'emh_start' => $emh ? $emh['startTime'] : null,
                  'emh_end' => $emh ? $emh['endTime'] : null,
                  'crowd_level' => $disney->getCrowdLevel($park, $date, 0)
              ]);
          }
      }
      Log::debug("Park Info table generated.", ['vacation_id'=>$vacation->id, 'info'=>$parkInfo]);
      return $parkInfo;
  }

  private function getParkCloseTime($disney, $emhInfo, $park, $date, $default) {
    if($emhInfo['validEmh']) {
      $emh = $emhInfo['emh'];
      $pm = $emh && DateUtils::getTimeWithFormat($emh['startTime'], 'a') == 'pm';
      if($pm) return $emh['endTime'];
    }
    return $disney->getCloseHour($park, $date, $default);
  }

  private function getParkOpenTime($disney, $emhInfo, $park, $date, $default) {
    if($emhInfo['validEmh']) {
      $emh = $emhInfo['emh'];
      $am = $emh && DateUtils::getTimeWithFormat($emh['startTime'], 'a') == 'am';
      if($am) return $emh['startTime'];
    }
    return $disney->getOpenHour($park, $date, $default);
  }

  private function getEMH($disney, $emhHost, $date) {
    // Step 6 of the Schedule Logic document.
    return $disney->getExtraMagicHoursDisneyData($emhHost, $date);
  }

  private function validEMH($date, $emh, $emhable, $blocksUtils) {
    $emhable = $emh && $emhable;
    if($emhable && $blocksUtils->verifyEMH($date, $emh)) {
      return true;
    }
    return false;
  }

  public function createBlockArrays($vacation, $scoreParks, $parkInfo, $parksPerDay) {
      $mouseplan = $vacation->mouse_plan;
      $days = $mouseplan->days;
      if ($days->isEmpty()) return new \Illuminate\Database\Eloquent\Collection;
      $availableParksCollection = null;
      $mouseplanCollection = null;
      $daysAtPlan = null;
      $filters = $this->Config->getCategory('filters');
      foreach ($filters as $filter) {
          $filterName = "App\\Filters\\{$filter->key}Filter";
          try {
              $filterReflectionClass = new ReflectionClass($filterName);
          } catch (ReflectionException $re){
              Log::error("Filter is not implemented.", ['filter' => $filterName]);
              continue;
          }
          Log::info("Applying filter", ['filter' => $filterName]);
          $filterInstance = $filterReflectionClass->newInstance();
          $filterInstance->setParkInfo($parkInfo);
          $filterInstance->setParksPerDay($parksPerDay);
          $filterInstance->setScoreParks($scoreParks);
          $filterInstance->setVacationModel($vacation);
          if($filterInstance->validateInitialData()) {
              list(
                $availableParksCollection,
                $mouseplanCollection,
                $daysAtPlan) =
                $filterInstance->filter(
                  $availableParksCollection,
                  $mouseplanCollection,
                  $daysAtPlan
                );
              abort_if($availableParksCollection == null, 500);
              abort_if($mouseplanCollection == null, 500);
              Log::debug('days at plan', $daysAtPlan);
          }
      }
      Log::info("Block collection built", ["total blocks" => $mouseplanCollection->count()]);
      return $mouseplanCollection;
  }

  public function verifyBlocks(&$blockCollection,
                               &$vacation,
                               $scoreParks,
                               $parkInfo,
                               &$touringHours,
                               $parksPerDay,
                               array $filters) {
      $verifiers = $this->Config->getCategory('verifiers');
      foreach ($verifiers as $verifier) {
          if (in_array($verifier->value, $filters)) continue;
          $verifierName = "App\\Verifiers\\{$verifier->key}Verifier";
          try {
              $verifierReflectionClass = new ReflectionClass($verifierName);
          } catch (ReflectionException $re){
              Log::error("Verifier is not implemented.", ['verifier' => $verifierName]);
              continue;
          }
          Log::info("Applying verifier", ['vacation_id' => $vacation->id, 'verifier' => $verifierName]);
          $verifierInstance = $verifierReflectionClass->newInstance();
          $verifierInstance->setParkInfo($parkInfo);
          $verifierInstance->setVacationModel($vacation);
          $verifierInstance->setScoreParks($scoreParks);
          $verifierInstance->setParksPerDay($parksPerDay);
          if($verifierInstance->validateInitialData()) {
              $touringHours += $verifierInstance->verify($blockCollection);
          }
      }
      Log::info("Block collection verified.", ["total blocks" => $blockCollection->count()]);
  }

  public function storeBlocks(&$blockCollection) {
      $sortedBlocks = $blockCollection->sortBy('startTime');
      foreach ($sortedBlocks as $block) {
          $block->save();
      }
  }

    public function updateBlocksArray($blocks, Day $day) {
        $mouseplan = $day->mouse_plan;
        $vacation = $mouseplan->vacation;
        $oldBlocks = $day->blocks;
        $newBlocks = array();
        foreach ($blocks as $block) {
            $attributes = $block;
            $blockDB = null;
            if(key_exists('id', $attributes)) {
                $blockDB = Block::find($attributes['id']);
            }
            unset($attributes['id']);
            if ($blockDB != null) {
              $this->updateModel($blockDB, $attributes);
              Log::debug("Existing block udpated.",
              ['vacation_id'=>$vacation->id, 'mouse_plan_id' => $mouseplan->id,'day_id'=>$day->id, 'block_id' => $blockDB->id]);
            } else {
                $blockDB = $this->create($attributes);
                Log::debug("Existing block created.",
                ['vacation_id'=>$vacation->id, 'mouse_plan_id' => $mouseplan->id,'day_id'=>$day->id, 'block_id' => $blockDB->id]);
            }
            array_push($newBlocks, $blockDB);
        }
        $removedBlocks = array_udiff($oldBlocks->all(), $newBlocks, array(__CLASS__, "compareBlocksIds"));
        foreach ($removedBlocks as $removed) {
            $removed->delete();
        }
    }

    private function compareBlocksIds(Block $oldBlock, Block $newBlock) {
        return $oldBlock->id - $newBlock->id;
    }

  //Block types helpers
  public static function getAllBlockTypes(){
    return BlockType::all();
  }

  public static function getAllParkTypeBlocks() {
      return BlockType::where('name','park')
        ->select('id','subtype', 'in_plan')
        ->get();
  }
}
