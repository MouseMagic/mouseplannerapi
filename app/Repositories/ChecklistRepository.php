<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Log;

use App\Models\Checklist;
use App\Models\MousageType;
use Auth;

class ChecklistRepository implements BaseRepository {

  private $model;

  public function __construct(Checklist $model){
    $this->model = $model;
  }

  public function getAll() {
      return Auth::user()->checklists;
  }

  public function getById($id){
    return $this->model->findById($id);
  }

  public function create(array $attributes){
    return $this->model->create($attributes);
  }

  public function createDefaultList($disneyResort, $daysToDisney) {
    $types = MousageType::where('key', 'like', 'default%')->get();
    foreach ($types as $type) {
        if ($type->key == 'default_fastpass_60' && !$disneyResort) continue;
        if ($type->key == 'default_fastpass_30' && $disneyResort) continue;
        if ($type->milestone > $daysToDisney) continue;
        $this->upsert($type->id, $type->value);
    }
  }

  public function update($id, array $attributes){
    $modelUpdate = $this->model->findOrFail($id);
    $modelUpdate->update($attributes);
    return $modelUpdate;
  }

  public function upsert($typeId, $text) {
      return Checklist::updateOrCreate(
          ['user_id' => Auth::user()->id, 'mousage_type_id' => $typeId],
          ['text' => $text, 'done' => false]
      );
  }

  public function delete($id){
    $this->model->getById($id)->delete();
    return true;
  }
}
