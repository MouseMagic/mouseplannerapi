<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Log;

use App\Models\Experience;
use App\Models\Vacation;

class ExperienceRepository {

  private $model;

  public function __construct(Experience $model) {
    $this->model = $model;
  }

  public function getAll() {
    return $this->model->all()
      ->makeHidden('hearts')
      ->makeVisible('id')
      ->makeVisible('external_name');
  }

  public function getById(int $id) {
    return $this->model->find($id);
  }

  public function create(array $attributes) {
    $experience = new Experience;
    if(key_exists('external_id', $attributes)) $experience->external_id = $attributes['external_id'];
    if(key_exists('park_id', $attributes)) $experience->park_id = $attributes['park_id'];
    if(key_exists('external_name', $attributes)) $experience->external_name = $attributes['external_name'];
    if(key_exists('external_description', $attributes)) $experience->external_description = json_encode($attributes['external_description']);
    if(key_exists('external_facets', $attributes)) $experience->external_facets = json_encode($attributes['external_facets']);
    $experience->save();
    return $experience;
  }

  public function update(Experience $experience, array $attributes) {
      $experience->update($attributes);
      return $experience;
  }

  public function delete(Experience $experience) {
    $experience->delete();
    return true;
  }
}
