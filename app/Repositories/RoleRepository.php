<?php

namespace App\Repositories;

use App\Models\Role;

class RoleRepository implements BaseRepository {

  private $model;

  public function __construct(Role $model){
    $this->model = $model;
  }

  public function getAll(){
    return $this->model->all();
  }

  public function getById($id){
    return $this->model->findById($id);
  }

  public function create(array $attributes){
    return $this->model->create($attributes);
  }
}
