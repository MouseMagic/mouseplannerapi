<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Log;

use App\Models\Traveler;
use App\Models\Vacation;
use Auth;

class TravelerRepository {

  private $model;

  public function __construct(Traveler $model) {
    $this->model = $model;
  }

  public function create(array $attributes) {
    if (!isset($attributes['vacation_id'])) {
      return null;
    }
    $traveler = Traveler::create($attributes);
    if (isset($attributes['accessibility_types'])) {
        $accessibility_ids = $attributes['accessibility_types'];
        $ids = array();
        foreach ($accessibility_ids as $id) {
            array_push($ids, $id['id']);
        }
        $traveler->accessibility_types()->sync($ids);
    }
    return $traveler;
  }

  public function update($modelUpdate, array $attributes) {
    $modelUpdate->update($attributes);
    if (isset($attributes['accessibility_types'])) {
        $accessibility_ids = $attributes['accessibility_types'];
        $ids = array();
        foreach ($accessibility_ids as $id) {
            array_push($ids, $id['id']);
        }
        $modelUpdate->accessibility_types()->sync($ids);
    } else {
        $modelUpdate->accessibility_types()->detach();
    }
    return $modelUpdate;
  }

  public function destroy(Traveler $traveler) {
      $traveler->delete();
  }

  public function duplicate(Vacation $vacation, Traveler $oldTraveler) {
    $newTraveler = $oldTraveler->replicate();
    $vacation->travelers()->save($newTraveler);
    if ($oldTraveler->accessibility_types) {
        $ids = $oldTraveler
                ->accessibility_types
                ->keyBy('id')
                ->keys()
                ->all();
        $newTraveler->accessibility_types()->sync($ids);
    }
    return $newTraveler;
  }
}
