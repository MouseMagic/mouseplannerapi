<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Models\Configuration;

class ConfigRepository implements BaseRepository {

  private $model;

  public function __construct(Configuration $model){
    $this->model = $model;
  }

  public function getAll(){
    return $this->model->all();
  }

  public function getById($id){
    return $this->model->findById($id);
  }

  public function create(array $attributes){
    return $this->model->create($attributes);
  }

  public function update($id, array $attributes){
    $modelUpdate = $this->model->findOrFail($id);
    $modelUpdate->update($attributes);
    return $modelUpdate;
  }

  public function delete($id){
    $this->model->getById($id)->delete();
    return true;
  }

  public function getParkHeartsPoints($initialPark, $hearts){
    $category = 'hearts';
    $nameNumbers = [0 => 'zero', 1 => 'one', 2 => 'two', 3 => 'three'];
    $key = $initialPark . '_park_hearts_' . $nameNumbers[$hearts];
    return $this->getPoints($category, $key);
  }

  public function getExperienceHeartsPoints($initialPark, $hearts){
    $category = 'hearts';
    $nameNumbers = [0 => 'zero', 1 => 'one', 2 => 'two', 3 => 'three'];
    $key = $initialPark . '_experience_hearts_' . $nameNumbers[$hearts];
    return $this->getPoints($category, $key);
  }

  public function getInterestsPoints($initialPark, $aliasInterest){
    $category = 'interests';
    $key = $initialPark . '_' . $aliasInterest;
    return $this->getPoints($category, $key);
  }

  public function getThrillLevelPoints($initialPark, $thrilllName){
    $category = 'thrill_level';
    $key = $initialPark . '_' . $thrilllName;
    return $this->getPoints($category, $key);
  }

  public function getCharactersPoints($initialPark, $character){
    $category = 'characters';
    $key = $initialPark . '_' . $character;
    return $this->getPoints($category, $key);
  }

  public function getResortPoints($initialPark, $resort_area){
    $category = 'disney_resort';
    $key = $initialPark . '_' . $resort_area;
    return $this->getPoints($category, $key);
  }

  public function getTravelPartyPoints($initialPark, $oldKey){
    $category = 'travel_party';
    $key = $initialPark . '_' . $oldKey;
    return $this->getPoints($category, $key);
  }

  private function getPoints($category, $key){
    try {
        $score = $this->getValue($category, $key);
    } catch(ModelNotFoundException $e) {
        $score  = 0;
    }
    return $score;
  }

  public function getValue($category, $key) {
      return $this->model
              ->where('category', $category)
              ->where('key', $key)
              ->firstOrFail()
              ->value;
  }

  public function getCategory($category) {
      return $this->model
              ->where('category', $category)
              ->orderBy('value')
              ->get();
  }
}
