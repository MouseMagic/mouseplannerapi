<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Log;

use App\Models\Vacation;
use App\Models\Experience;
use App\Models\Park;
use App\Models\Resort;
use App\Models\Traveler;
use App\Utils\NameNormalizer;
use App\Utils\DateUtils;
use App\Utils\BlocksUtils;
use App\Utils\Mousages;
use Auth;
use App;
use App\Exceptions\MousePlannerException;
use Stripe\Collection;
use Carbon\Carbon;
use App\Models\Subscription;

use DateTime;


class VacationRepository
{

    private $model;
    private $Config;

    public function __construct(Vacation $model, ConfigRepository $config)
    {
        $this->model = $model;
        $this->Config = $config;
    }

    public function getAll($sort, $search)
    {
        $answer = new \Illuminate\Database\Eloquent\Collection;
        $AuthUser = Auth::user();

        if ($AuthUser->hasRole('admin')) {
            $queryUsers = App\User::whereHas('subscriptions', function($query){
                $query->whereNull('ends_at')
                    ->orWhere('ends_at', '>', date("Y-m-d H:i:s"));
            })->has('vacations')->with(['vacations' => function ($query) use ($sort) {
                if (!empty($sort)) {
                    $query->orderBy($sort)->get();
                }
            }]);

            if (!empty($search)) {
                $queryUsers->where('email', 'like', '%' . $search . '%');
            }

            $usersWithVacations = $queryUsers->paginate(2);
            foreach ($usersWithVacations as $user) {
                    foreach ($user->vacations as $vacation) {
                        $this->buildVacationAnswer($vacation);
                    }
                    $answer->push(['user' => $user]);
            }

            return [
                'data' => $answer,
                'total' => $usersWithVacations->total(),
                'perPage' => $usersWithVacations->perPage(),
                'currentPage' => $usersWithVacations->currentPage(),
                'nextPageUrl' => $usersWithVacations->nextPageUrl(),
                'previousPageUrl' => $usersWithVacations->previousPageUrl(),
            ];

        } else {
            $user = Auth::user();
            $vacations = $user->vacations;
            $answer = new \Illuminate\Database\Eloquent\Collection;
            foreach ($vacations as $vacation) {
                $answer->push($this->buildVacationAnswer($vacation));
            }
            return [
                'data' => $answer
            ];
        }
    }

    public function show(Vacation $vacation)
    {
        return $this->buildVacationAnswer($vacation);
    }

    public function buildVacationAnswer(Vacation $vacation)
    {
        $vacationAnswer = $vacation->load(
            'celebration_types',
            'travelers.accessibility_types',
            'interest_types',
            'characters',
            'dining_preferences',
            'parks',
            'experiences',
            'mouse_plan.days.blocks'
        );
        //change dates format
        $vacationAnswer['startTravelDate'] = $vacation->startTravelDate ? $this->formatTime($vacation->startTravelDate) : null;
        $vacationAnswer['endTravelDate'] = $vacation->endTravelDate ? $this->formatTime($vacation->endTravelDate) : null;
        $vacationAnswer['departureDate'] = $vacation->departureDate ? $this->formatTime($vacation->departureDate) : null;
        $vacationAnswer['arrivalDate'] = $vacation->arrivalDate ? $this->formatTime($vacation->arrivalDate) : null;
        $vacationAnswer['beginDayTime'] = $vacation->beginDayTime ? $this->formatTime($vacation->beginDayTime) : null;
        $vacationAnswer['endDayTime'] = $vacation->endDayTime ? $this->formatTime($vacation->endDayTime) : null;
        if ($vacationAnswer->mouse_plan) {
            foreach ($vacationAnswer->mouse_plan->days as $day) {
                $day->date = $this->formatTime($day->date);
                foreach ($day->blocks as $block) {
                    $block->startTime = $this->formatTime($block->startTime);
                    $block->endTime = $this->formatTime($block->endTime);
                }
            }
        }

        return $vacationAnswer;
    }

    public function create(array $attributes)
    {
        return $this->model->create($attributes);
    }

    private function fixStartTravelDates(Vacation $vacation, array &$attributes)
    {
        switch (true) {
            case isset($attributes['startTravelDate']) && isset($attributes['arrivalDate']):
                $date = $this->formatTime($attributes['startTravelDate'], 'Y-m-d H:i:s');
                $time = $this->formatTime($attributes['arrivalDate'], 'H:i:s');
                $attributes['arrivalDate'] = DateUtils::setDateTime($date, $time);
                $attributes['startTravelDate'] = $date;
                break;
            case isset($attributes['startTravelDate']) && $vacation->arrivalDate:
                $date = $this->formatTime($attributes['startTravelDate'], 'Y-m-d H:i:s');
                $time = $this->formatTime($vacation->arrivalDate, 'H:i:s');
                $attributes['arrivalDate'] = DateUtils::setDateTime($date, $time);
                $attributes['startTravelDate'] = $date;
                break;
            case isset($attributes['startTravelDate']):
                $date = $this->formatTime($attributes['startTravelDate'], 'Y-m-d H:i:s');
                $attributes['startTravelDate'] = $date;
                break;
            case isset($attributes['arrivalDate']) && $vacation->startTravelDate:
                $date = $this->formatTime($vacation->startTravelDate, 'Y-m-d');
                $time = $this->formatTime($attributes['arrivalDate'], 'H:i:s');
                $attributes['arrivalDate'] = DateUtils::setDateTime($date, $time);
                break;
            case isset($attributes['arrivalDate']):
                $date = $this->formatTime($attributes['arrivalDate'], 'Y-m-d H:i:s');
                $attributes['arrivalDate'] = $date;
                break;
        }
    }

    private function fixEndTravelDates(Vacation $vacation, array &$attributes)
    {
        switch (true) {
            case isset($attributes['endTravelDate']) && isset($attributes['departureDate']):
                $date = $this->formatTime($attributes['endTravelDate'], 'Y-m-d H:i:s');
                $attributes['endTravelDate'] = $date;
                $time = $this->formatTime($attributes['departureDate'], 'H:i:s');
                $date = DateUtils::setDateTime($date, $time);
                $attributes['departureDate'] = $date;
                break;
            case isset($attributes['endTravelDate']) && $vacation->departureDate:
                $date = $this->formatTime($attributes['endTravelDate'], 'Y-m-d H:i:s');
                $attributes['endTravelDate'] = $date;
                $time = $this->formatTime($vacation->departureDate, 'H:i:s');
                $date = DateUtils::setDateTime($date, $time);
                $attributes['departureDate'] = $date;
                break;
            case isset($attributes['endTravelDate']):
                $date = $this->formatTime($attributes['endTravelDate'], 'Y-m-d H:i:s');
                $attributes['endTravelDate'] = $date;
                break;
            case isset($attributes['departureDate']) && $vacation->endTravelDate:
                $date = $this->formatTime($vacation->endTravelDate, 'Y-m-d');
                $time = $this->formatTime($attributes['departureDate'], 'H:i:s');
                $date = DateUtils::setDateTime($date, $time);
                $attributes['departureDate'] = $date;
                break;
            case isset($attributes['departureDate']):
                $date = $this->formatTime($attributes['departureDate'], 'Y-m-d H:i:s');
                $attributes['departureDate'] = $date;
                break;
        }
    }

    private function fixDayTimes(array &$attributes)
    {
        if (isset($attributes['beginDayTime'])) {
            $date = $this->formatTime($attributes['beginDayTime'], 'Y-m-d H:i:s');
            $attributes['beginDayTime'] = $date;
        }
        if (isset($attributes['endDayTime'])) {
            $date = $this->formatTime($attributes['endDayTime'], 'Y-m-d H:i:s');
            $attributes['endDayTime'] = $date;
        }
    }

    public function update(Vacation $modelUpdate, array $attributes)
    {
        $this->validate($modelUpdate, $attributes);
        $this->fixStartTravelDates($modelUpdate, $attributes);
        $this->fixEndTravelDates($modelUpdate, $attributes);
        $this->fixDayTimes($attributes);

        $modelUpdate->update($attributes);
        $user_id = $modelUpdate->trip->user_id;
        // Sync interest types
        if (isset($attributes['interest_types'])) {
            $interest_ids = $attributes['interest_types'];
            $ids = array();
            foreach ($interest_ids as $id) {
                array_push($ids, $id['id']);
            }
            $pivotData = array_fill(0, count($ids), ['user_id' => $user_id]);
            $syncData = array_combine($ids, $pivotData);
            $modelUpdate->interest_types()->sync($syncData);
        }
        // Sync characters
        if (isset($attributes['characters'])) {
            $character_ids = $attributes['characters'];
            $ids = array();
            foreach ($character_ids as $id) {
                array_push($ids, $id['id']);
            }
            $pivotData = array_fill(0, count($ids), ['user_id' => $user_id]);
            $syncData = array_combine($ids, $pivotData);
            $modelUpdate->characters()->sync($syncData);
        }
        // Sync dining_preferences
        if (isset($attributes['dining_preferences'])) {
            $dining_ids = $attributes['dining_preferences'];
            $ids = array();
            foreach ($dining_ids as $id) {
                array_push($ids, $id['id']);
            }
            $modelUpdate->dining_preferences()->sync($ids);
        }
        // Sync celebration_type
        if (isset($attributes['celebration_types'])) {
            $celebration_ids = $attributes['celebration_types'];
            $ids = array();
            foreach ($celebration_ids as $id) {
                array_push($ids, $id['id']);
            }
            $modelUpdate->celebration_types()->sync($ids);
        }
        // sync parks
        $this->syncParks($modelUpdate, $attributes);
        // sync experiences
        $this->syncExperiences($modelUpdate, $attributes);
        return $modelUpdate;
    }

    public function validate(Vacation $vacation, array $attributes)
    {
        if (isset($attributes['visitDays']) && $attributes['visitDays'] < 1) {
            throw new MousePlannerException($vacation, "visitDays [" . $attributes['visitDays'] . "] cannot be less than 1", 400);
        }
        if (!App::environment('local') && $vacation->mouse_plan) {
            throw new MousePlannerException($vacation, "This vacation is locked since it has a non-empty mouseplan.", 423);
        }
        if (!$this->validateTravelers($vacation, $attributes)) {
            throw new MousePlannerException($vacation, "There aren't any guests or at least one guest is not valid.", 400);
        }
    }

    public function syncParks(Vacation $vacation, array $attributes)
    {
        // Sync parks
        if (isset($attributes['parks'])) {
            $parks = $attributes['parks'];
            $ids = array();
            $hearts = array();
            foreach ($parks as $park) {
                if (!key_exists('hearts', $park)) continue;
                array_push($ids, $park['id']);
                array_push($hearts, ['hearts' => $park['hearts']]);
            }
            $syncData = array_combine($ids, $hearts);
            $vacation->parks()->sync($syncData);
        }
    }

    public function syncExperiences(Vacation $vacation, array $attributes)
    {
        // Sync experiences
        if (isset($attributes['experiences'])) {
            $experiences = $attributes['experiences'];
            $ids = array();
            $hearts = array();
            foreach ($experiences as $experience) {
                if ($experience['hearts'] == 0) continue;
                //Store the experience in case is not there, otherwise the sync will fail
                if (isset($experience['park_id'])) {
                    $db_exp = Experience::firstOrCreate([
                        'external_id' => $experience['external_id'],
                        'park_id' => $experience['park_id'],
                    ]);
                } else {
                    $db_exp = Experience::firstOrCreate([
                        'external_id' => $experience['external_id'],
                    ]);
                }
                Log::debug('Experience created or retrieved', ['id' => $db_exp->id, 'external_id' => $db_exp->external_id]);
                array_push($ids, $db_exp->id);
                array_push($hearts, ['hearts' => $experience['hearts']]);
            }
            $syncData = array_combine($ids, $hearts);
            $vacation->experiences()->sync($syncData);
        }
    }

    public function validateTravelers(Vacation $vacation, array $attributes)
    {
        if ($attributes['stepNumber'] <= 1) return true;
        if ($attributes['stepNumber'] > 1 && empty($attributes['travelers'])) return false;
        $travelers = $attributes['travelers'];
        foreach ($travelers as $traveler) {
            // If we have id, the traveler is already created
            if (isset($traveler['id'])) continue;
            // Otherwise, the request is invalid
            return false;
        }
        return true;
    }

    public function syncParksExperiences(Vacation $vacation, array $attributes)
    {
        $parks = $vacation->parks->keyBy('id')->keys();
        // Sync experiences
        $experiences = $attributes['experiences'];
        $ids = array();
        $hearts = array();
        foreach ($experiences as $experience) {
            if ($experience['hearts'] == 0) continue;
            //Store the experience in case is not there, otherwise the sync will fail
            $db_exp = Experience::firstOrCreate([
                'external_id' => $experience['external_id'],
            ]);

            if (!$parks->contains($db_exp->park_id)) continue;
            Log::debug('Experience created or retrieved', ['id' => $db_exp->id, 'external_id' => $db_exp->external_id]);
            array_push($ids, $db_exp->id);
            array_push($hearts, ['hearts' => $experience['hearts']]);
        }
        $syncData = array_combine($ids, $hearts);
        $vacation->experiences()->syncWithoutDetaching($syncData);
    }

    public function syncParkExperiences(Vacation $vacation, Park $park, array $attributes)
    {
        // Sync experiences
        $experiences = $attributes['experiences'];
        $ids = array();
        $hearts = array();
        foreach ($experiences as $experience) {
            if ($experience['hearts'] == 0) continue;
            //Store the experience in case is not there, otherwise the sync will fail
            $db_exp = Experience::firstOrCreate([
                'external_id' => $experience['external_id'],
            ]);
            if (!$db_exp->park_id || $db_exp->park_id != $park->id) continue;
            Log::debug('Experience created or retrieved', ['id' => $db_exp->id, 'external_id' => $db_exp->external_id]);
            array_push($ids, $db_exp->id);
            array_push($hearts, ['hearts' => $experience['hearts']]);
        }
        $syncData = array_combine($ids, $hearts);
        $vacation->experiences()->syncWithoutDetaching($syncData);
    }

    public function delete(Vacation &$vacation)
    {
        $vacation_id = $vacation->id;
        // Detach characters
        $vacation->characters()->detach();
        Log::debug("characters deatached.", ['vacation_id' => $vacation_id]);
        // Detach interest_types
        $vacation->interest_types()->detach();
        Log::debug("interest types detached.", ['vacation_id' => $vacation_id]);
        // Detach dining_preferences
        $vacation->dining_preferences()->detach();
        Log::debug("dining preferences detached.", ['vacation_id' => $vacation_id]);
        // Detach celebration_types
        $vacation->celebration_types()->detach();
        Log::debug("celebration types detached.", ['vacation_id' => $vacation_id]);
        // Detach vacation parks
        $vacation->parks()->detach();
        Log::debug("vacations parks detached.", ['vacation_id' => $vacation_id]);
        // Detach experiences
        $vacation->experiences()->detach();
        Log::debug("vacations experiences detached", ['vacation_id' => $vacation_id]);
        // Delete MousePlan's days and blocks
        $mousePlan = $vacation->mouse_plan;
        if ($mousePlan != null) {
            MousePlanRepository::deleteDays($mousePlan);
            // Delete mouse plan row
            $mousePlan->delete();
            Log::debug("mouseplan deleted.", ['vacation_id' => $vacation_id]);
        }
        // Delete vacation row
        $vacation->delete();
        Log::debug("vacation deleted.", ['vacation_id' => $vacation_id]);
        return true;
    }

    public function createDayArray($vacationModel)
    {
        // $limit = BlocksUtils::schedulableDaysCount($vacationModel);
        $dates = DateUtils::createRange($vacationModel->startTravelDate,
            $vacationModel->endTravelDate);
        $days  = DateUtils::calcuateDaysDifference($vacationModel->startTravelDate);

        $startTravelDate = new DateTime($vacationModel->startTravelDate);

        if ($days >= 190){
             Mousages::setAdvanceMousage($vacationModel, $startTravelDate->modify('-190 day')->format('M/d/Y'));
        }

        Log::debug("Date array built.", ['vacation_id' => $vacationModel->id,
            'day' => $dates]);
        return $dates;
    }

    private function getYoungestTraveler($vacationModel)
    {
        $youngest = $vacationModel->travelers()->orderBy('yearOfBirth', 'desc')->first();
        if ($youngest == null) {
            throw new MousePlannerException($vacationModel, "Travelers not found", 412);
        }
        return $youngest;
    }
    
    public function getThrillLevelByVacation($vacationModel)
    {
        $thrillLevel = $vacationModel->thrill_level_type;
        if ($thrillLevel == null) {
            throw new MousePlannerException($vacationModel, "Thrill Level not found", 412);
        }
        $thrillName = $thrillLevel->name;
        return NameNormalizer::scoreNormalize($thrillName);
    }

    public function getResortByVacation($vacationModel)
    {
        $isResortRequired = $vacationModel->booked_place_type_id == 2;
        $resort = $vacationModel->resort;
        if ($isResortRequired && $resort == null) {
            throw new MousePlannerException($vacationModel, "Resort not found", 412);
        }
        if (!$isResortRequired) return null;
        return $resort;
    }

    public function getScoreByPark($vacation)
    {
        $scoreParks = Park::all()->mapWithKeys(function ($park) {
            return [$park['initials'] => 0];
        })->all();

        $this->addParkHeartPoints($scoreParks, $vacation);
        $this->addExperienceHeartPoints($scoreParks, $vacation);
        $this->addInterestsPoints($scoreParks, $vacation);
        $this->addCharactersPoints($scoreParks, $vacation);
        $this->addThrillLevelPoints($scoreParks, $vacation);
        $this->addDisneyResortPoints($scoreParks, $vacation);
        $this->addTravelPartyPoints($scoreParks, $vacation);
        $scoreParks = array_filter($scoreParks);
        arsort($scoreParks);
        Log::info("Score by park calculated.", $scoreParks);
        return $scoreParks;
    }

    private function addParkHeartPoints(&$scoreParks, $vacation)
    {
        foreach ($vacation->parks as $park) {
            $parkName = $park->initials;
            Log::debug("Vacation park identified.",
                ['vacation id' => $vacation->id, 'park' => $parkName, 'hearts' => $park->hearts]);
            $score = $this->Config->getParkHeartsPoints($parkName, $park->hearts);
            $scoreParks[$parkName] = $scoreParks[$parkName] + $score;
        }
        Log::info("Parks points added to score.", $scoreParks);
    }

    private function addExperienceHeartPoints(&$scoreParks, $vacation)
    {
        $experiences = $vacation->experiences;
        foreach ($experiences as $exp) {
            Log::debug("Vacation experience identified.",
                ['vacation id' => $vacation->id, 'external_id' => $exp->external_id]);
            $parkName = $exp->park->initials;
            $score = $this->Config->getExperienceHeartsPoints($parkName, $exp->hearts);
            $scoreParks[$parkName] = $scoreParks[$parkName] + $score;
        }
        Log::info("Experiences points added to score.", $scoreParks);
    }

    private function addInterestsPoints(&$scoreParks, $vacation)
    {
        foreach ($vacation->interest_types as $interest) {
            $key = NameNormalizer::scoreNormalize($interest->name);
            Log::debug("Vacation interest identified.",
                ['vacation id' => $vacation->id, 'interest' => $key]);
            foreach ($scoreParks as $parkName => $points) {
                $score = $this->Config->getInterestsPoints($parkName, $key);
                $scoreParks[$parkName] = $points + $score;
            }
        }
        Log::info("Interests points added to score.", $scoreParks);
    }

    private function addCharactersPoints(&$scoreParks, $vacation)
    {
        foreach ($vacation->characters as $character) {
            $key = NameNormalizer::scoreNormalize($character->name);
            Log::debug("Vacation character identified.",
                ['vacation id' => $vacation->id, 'character' => $key]);
            foreach ($scoreParks as $parkName => $points) {
                $score = $this->Config->getCharactersPoints($parkName, $key);
                $scoreParks[$parkName] = $points + $score;
            }
        }
        Log::info("Characters points added to score.", $scoreParks);
    }

    private function addThrillLevelPoints(&$scoreParks, $vacation)
    {
        $thrill_level = $this->getThrillLevelByVacation($vacation);
        Log::debug("Vacation thrill level identified.",
            ['vacation id' => $vacation->id, 'thrill level' => $thrill_level]);
        foreach ($scoreParks as $parkName => $points) {
            $score = $this->Config->getThrillLevelPoints($parkName, $thrill_level);
            $scoreParks[$parkName] = $points + $score;
        }
        Log::info("Thrill Level points added to score.", $scoreParks);
    }

    private function addDisneyResortPoints(&$scoreParks, $vacation)
    {
        $resort = $this->getResortByVacation($vacation);
        if ($resort == null) return;
        Log::debug("Vacation resort identified.",
            ['vacation id' => $vacation->id, 'resort' => $resort->name, 'resort area' => $resort->resort_area]);
        foreach ($scoreParks as $parkName => $points) {
            $score = $this->Config->getResortPoints($parkName, $resort->resort_area);
            $scoreParks[$parkName] = $points + $score;
        }
        Log::info("Disney Resort points added to score.", $scoreParks);
    }

    private function getYoungestTravelerAge($vacation)
    {
        $youngest = $this->getYoungestTraveler($vacation);
        $age = date("Y") - $youngest->yearOfBirth;
        //If the current month is prior to the birth month, roll it back a year:
        if (date("m") < $youngest->monthOfBirth) $age--;
        Log::info("Youngest traveler identified.",
            ['vacation id' => $vacation->id, 'name' => $youngest->firstname, 'age' => $age]);
        return $age;
    }

    private function addTravelPartyPoints(&$scoreParks, $vacation)
    {
        $blocksUtils = BlocksUtils::getInstance($vacation);
        $age = $blocksUtils->getYoungestTravelerAge($vacation);
        switch ($age) {
            case $age > 18:
                $key = "no_kids";
                break;
            case $age > 10:
                $key = "above_10";
                break;
            case $age > 7:
                $key = "above_7";
                break;
            default:
                $key = "under_6";
        }
        foreach ($scoreParks as $parkName => $points) {
            $score = $this->Config->getTravelPartyPoints($parkName, $key);
            $scoreParks[$parkName] = $points + $score;
        }
        Log::info("Travel Party points added to score.", $scoreParks);
    }

    public function getParksPerDayKey($vacationModel)
    {
        $isParkHopper = $this->isParkHopper($vacationModel) ? 1 : 0;
        $isNapsTrue = ($vacationModel->nap_type_id < 3) ? 1 : 0;
        $pace = $vacationModel->pace_type_id;
        $blocksUtils = BlocksUtils::getInstance($vacationModel);
        if ($blocksUtils->warmMonth() && $pace > 1) $pace--;
        return "block_{$isParkHopper}_{$isNapsTrue}_{$pace}";
    }

    private function isParkHopper($vacationModel)
    {
        $value = $vacationModel->interest_visit_park_type_id;
        switch ($value) {
            case 1:
                $isParkHopper = true;
                Mousages::setParkHopperMousage($vacationModel);
                break;
            case 3:
                $resort = $this->getResortByVacation($vacationModel);
                $pace = $vacationModel->pace_type_id;
                $visitDays = $vacationModel->visitDays;
                $parksChosen = $vacationModel->parks()->count();
                $high = 3;
                $low = 1;
                if ($pace == $high ||
                    $vacationModel->budget_type_id == $high ||
                    ($resort && $resort->resort_area == 'EP') ||
                    ($pace > $low && $visitDays > $parksChosen)
                ) {
                    $isParkHopper = true;
                    Mousages::setParkHopperMousage($vacationModel);
                } else {
                    $isParkHopper = false;
                }
                break;
            default:
                $isParkHopper = false;
                break;
        }
        return $isParkHopper;
    }

    private function formatTime($time, $format = null)
    {
        if ($format == null) {
            return DateUtils::getTimeWithFormat($time);
        }
        return DateUtils::getTimeWithFormat($time, $format);
    }
}
