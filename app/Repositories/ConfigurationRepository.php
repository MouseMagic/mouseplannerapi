<?php

namespace App\Repositories;

use App\Models\Configuration;

class ConfigurationRepository implements BaseRepository {

  private $model;

  public function __construct(Configuration $model){
    $this->model = $model;
  }

  public function getAll(){
    return $this->model->all();
  }

  public function getById($id){
    return $this->model->findById($id);
  }

  public function create(array $attributes){
    return $this->model->create($attributes);
  }

  public function update($id, array $attributes){
    $modelUpdate = $this->model->findOrFail($id);
    $modelUpdate->update($attributes);
    return $modelUpdate;
  }

  public function delete($id){
    $this->model->getById($id)->delete();
    return true;
  }

  public function getByCategory($category){
    return $this->model->where('category', $category)->get();
  }
}
