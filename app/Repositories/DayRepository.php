<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Log;

use App\Utils\DateUtils;
use App\Models\MousePlan;
use App\Models\Day;
use App\Models\Block;
use App\Repositories\BlockRepository;

class DayRepository implements BaseRepository {

  private $model;
  private $BlockRepository;
  private $ConfigTZ;

  public function __construct(Day $model,
                              BlockRepository $blockRepository){
    $this->model = $model;
    $this->BlockRepository = $blockRepository;
    $this->ConfigTZ = DateUtils::getConfigTimezone();
  }

  public function getAll(){
    return $this->model->all();
  }

  public function getById($id){
    return $this->model->findById($id);
  }

  public function create(array $attributes){
    return $this->model->create($attributes);
  }

  public function update($id, array $attributes){
    $modelUpdate = $this->model->findOrFail($id);
    $modelUpdate->update($attributes);
    return $modelUpdate;
  }

  public function delete($id){
    $this->model->getById($id)->delete();
    return true;
  }

  public function storeDayArray($dates, $mouseplanModel) {
      $dayNumber = 1;
      if ($mouseplanModel->days) {
        $this->verifyPlanDays($dates, $mouseplanModel);
      } else {
        $this->createPlanDays($dates, $mouseplanModel);
      }
      $mouseplanModel->load('days');
  }

  private function verifyPlanDays($dates, MousePlan &$mouseplan) {
    $dayNumber = 1;
    $datesIndex = 0;
    $days = $mouseplan->days->sortBy('day_number');
    foreach ($days as $day) {
      if (key_exists($datesIndex, $dates)) {
        $date = $dates[$datesIndex++];
        $day->date = DateUtils::changeTimezone($date, $this->ConfigTZ, 'UTC');
        $day->day_number = $dayNumber++;
        $day->save();
        $this->cleanFastPasses($day);
        $this->BlockRepository->destroy($this->getBlocksToRemove($day));
        $day->load('blocks');
      } else {
        $day->delete();
      }
    }
    while (key_exists($datesIndex, $dates)) {
      $date = $dates[$datesIndex++];
      $fdate = DateUtils::changeTimezone($date, $this->ConfigTZ, 'UTC');
      $day = Day::create([
        'mouse_plan_id' => $mouseplan->id,
        'day_number' => $dayNumber++,
        'date' => $fdate
      ]);
    }
  }

  private function createPlanDays($dates, MousePlan &$mouseplan) {
    $dayNumber = 1;
    foreach ($dates as $date) {
        $fdate = DateUtils::changeTimezone($date, $this->ConfigTZ, 'UTC');
        $day = Day::create([
          'mouse_plan_id' => $mouseplan->id,
          'day_number' => $dayNumber++,
          'date' => $fdate
        ]);
    }
  }

  private function getBlocksToRemove(Day $day) {
      $blocksToRemove = array();
      foreach ($day->blocks as $block) {
          $blocksToRemove[] = $block->id;
      }
      return $blocksToRemove;
  }
  private function cleanFastPasses(Day $day) {
    $fastpass_reservations = $day->fastpass_reservations;
    foreach ($fastpass_reservations as $fp) {
      $fp->delete();
    }
  }

  public function updateDayArray($days, $mouseplanModel) {
      $dayNumber = 1;
      $vacation = $mouseplanModel->vacation;
      foreach ($days as $day) {
          $stdDay = (object) $day;
          $fdate = $this->formatTime($stdDay->date, 'Y-m-d H:i:s');
          // Empty days will not be saved
          if (count($stdDay->blocks) == 0) continue;
          if (isset($stdDay->id)) {
              $dayDB = $this->update($stdDay->id, [
                  'mouse_plan_id' => $mouseplanModel->id,
                  'day_number' => $dayNumber++,
                  'date' => $fdate
              ]);
              Log::debug("Existing day udpated.",
              ['vacation_id' => $vacation->id, 'mouse_plan_id' => $mouseplanModel->id,'day_id'=>$dayDB->id]);
          } else {
              $dayDB = $this->create([
                  'mouse_plan_id' => $mouseplanModel->id,
                  'day_number' => $dayNumber++,
                  'date' => $fdate
              ]);
              Log::debug("Day created.",
              ['vacation_id' => $vacation->id, 'mouse_plan_id' => $mouseplanModel->id,'day_id'=>$dayDB->id]);
          }
          $this->BlockRepository->updateBlocksArray($stdDay->blocks, $dayDB);
          $dayDB->load('blocks');
      }
      Day::where('mouse_plan_id', $mouseplanModel->id)->where('day_number', '>', count($days))->delete();
      $mouseplanModel->load('days');
  }

  private function formatTime($time, $format=null) {
      if ($format == null) {
          return DateUtils::getTimeWithFormat($time);
      }
      return DateUtils::getTimeWithFormat($time, $format);
  }
}
