<?php

namespace App\Utils;

class NameNormalizer
{

  public static function scoreNormalize($name)
  {
    $name = strtolower($name);
    $name = str_replace(' ', '_', $name);
    $name = preg_replace('/[^A-Za-z0-9\_]/', '', $name);
    return str_replace('__', '_', $name);
  }
}
