<?php

namespace App\Utils;

use Illuminate\Support\Facades\Log;

use App\Models\Mousage;
use App\Models\MousageType;
use App\Models\Vacation;
use App\Models\FastpassReservation;
use App\Models\Day;
use App\Models\Block;
use App\Utils\DateUtils;

class Mousages {

    // General Mousages
    public static function setParkHopperMousage(Vacation $vacation) {
        Mousages::setMousePlanMousage($vacation, 'parkhopper');
    }

    public static function setLowRankParkMousage(Vacation $vacation, $park) {
      Mousages::setMousePlanMousage($vacation, 'lowRankPark', array('%park' => $park));
    }

    public static function setWPPassMousage(Vacation $vacation) {
      Mousages::setMousePlanMousage($vacation, 'waterParkPass');
    }

    public static function setEpcot3Mousage(Vacation $vacation) {
      Mousages::setMousePlanMousage($vacation, 'epcot3');
    }

    public static function setMissingResortParkMousage(Vacation $vacation, $park) {
      Mousages::setMousePlanMousage($vacation, 'missingpark5', array('%park' => $park));
    }

    public static function setDisneySpaMousage(Vacation $vacation) {
      Mousages::setMousePlanMousage($vacation, 'disneySpa');
    }

    public static function setDisneyGolfMousage(Vacation $vacation) {
      Mousages::setMousePlanMousage($vacation, 'disneyGolf');
    }

    public static function setPirateLeagueMousage(Vacation $vacation) {
      Mousages::setMousePlanMousage($vacation, 'pirateLeague');
    }

    public static function setPrincessMakeoverMousage(Vacation $vacation) {
      Mousages::setMousePlanMousage($vacation, 'princessMakeover');
    }

    public static function setWildernessXplorerMousage(Vacation $vacation) {
      Mousages::setMousePlanMousage($vacation, 'wildernessXplorer');
    }

    public static function setParkHopper2Mousage(Vacation $vacation) {
      Mousages::setMousePlanMousage($vacation, 'parkhopper2');
    }

    public static function setParkHopper3Mousage(Vacation $vacation) {
      Mousages::setMousePlanMousage($vacation, 'parkhopper3');
    }

    public static function setCandlelightSEMousage(Vacation $vacation) {
      Mousages::setMousePlanMousage($vacation, 'candlelightSE');
    }

    public static function setPurchaseSEMousage(Vacation $vacation, $sename) {
      Mousages::setMousePlanMousage($vacation, 'purchaseSE', array('%sename' => $sename));
    }

    public static function setFoodSEMousage(Vacation $vacation) {
      Mousages::setMousePlanMousage($vacation, 'foodSE');
    }

    public static function setFlowerSEMousage(Vacation $vacation) {
      Mousages::setMousePlanMousage($vacation, 'flowerSE');
    }

    // Day mousages
    public static function setScheduleEarlyMousage(Vacation $vacation, Day $day, $time) {
        Mousages::setDayMousage($vacation, $day, 'schedule1', array('%time' => $time));
    }

    public static function setScheduleLateMousage(Vacation $vacation, Day $day, $park, $time, $date) {
        Mousages::setDayMousage($vacation, $day, 'schedule3', array('%park' => $park, '%time' => $time));
    }

    public static function setAdvanceMousage(Vacation $vacation, $date){
        Mousages::setMousePlanMousage($vacation, 'advance', array('%x' => $date));
    }

    public static function setOrphanFastPassMousage(Vacation $vacation, Day $day, FastpassReservation $fp) {
      $configTZ = DateUtils::getConfigTimezone();
        Mousages::setDayMousage($vacation, $day, 'orphanFP+', array(
                                  // '%experience'=> $fp->experience->external_name,
                                  '%experience'=> $fp->experience->park->name,
                                  '%reservation'=> DateUtils::changeTimezone($fp->reservation_time, 'UTC', $configTZ, 'M jS \a\t g:i A'),
                                  '%park' => $fp->experience->park->name
                                ));
    }

    public static function setEpcot2Mousage(Vacation $vacation, Day $day) {
        Mousages::setDayMousage($vacation, $day, 'epcot2');
    }

    // Setting mousages
    private static function setMousePlanMousage(Vacation $vacation, $mousageType, array $data = null) {
        $mouseplan = $vacation->mouse_plan;
        $type = MousageType::where('key', $mousageType)->first();
        $mousage = Mousage::updateOrCreate(
            ['vacation_id' => $vacation->id,
            'mousageable_id' => $mouseplan->id,
            'mousageable_type' => 'mouseplans',
            'mousage_type_id' => $type->id],
            ['text' => Mousages::getMousageText($type->value, $data),
            'read' => false]
        );
        Log::debug("Mousage added at MousePlan.",
            ['vacation_id' => $vacation->id,
             'mouse_plan_id' => $mouseplan->id,
             'mousage_id' => $mousage->id,
             'mousage_type' => $mousageType]);
    }

    private static function setDayMousage(Vacation $vacation, Day $day, $mousageType, array $data = null) {
        $type = MousageType::where('key', $mousageType)->first();
        $mousage = Mousage::updateOrCreate(
            ['vacation_id' => $vacation->id,
            'mousageable_id' => $day->id,
            'mousageable_type' => 'days',
            'mousage_type_id' => $type->id],
            ['text' => Mousages::getMousageText($type->value, $data),
            'read' => false]
        );
        Log::debug("Mousage added at Day.",
            ['vacation_id' => $vacation->id,
             'day_id' => $day->id,
             'mousage_id' => $mousage->id,
             'mousage_type' => $mousageType,
           ]);
    }

    private static function setBlockMousage(Vacation $vacation, Block $block, $mousageType, array $data = null) {
        $type = MousageType::where('key', $mousageType)->first();
        $mousage = Mousage::updateOrCreate(
            ['vacation_id' => $vacation->id,
            'mousageable_id' => $block->id,
            'mousageable_type' => 'blocks',
            'mousage_type_id' => $type->id],
            ['text' => Mousages::getMousageText($type->value, $data),
            'read' => false]
        );
        Log::debug("Mousage added at Block.",
            ['vacation_id' => $vacation->id,
             'block_id' => $block->id,
             'mousage_id' => $mousage->id,
             'mousage_type' => $mousageType,
           ]);
    }

    private static function getMousageText($text, array $data = null) {
        if (count($data)) {
            return str_replace(array_keys($data), array_values($data), $text);
        }
        return $text;
    }
}
