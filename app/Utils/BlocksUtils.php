<?php

namespace App\Utils;

use Illuminate\Support\Facades\Log;

use App\Models\Block;
use App\Models\Day;
use App\Models\Vacation;
use App\Models\Resort;
use App\Utils\DateUtils;
use App\Utils\Mousages;
use App\Repositories\BlockRepository;
use App\Exceptions\MousePlannerException;

class BlocksUtils {

    private $Vacation;
    private $TravelDays;
    private $SchedulableDaysCount;
    private $ExtraDays;
    private $SkipArrivalDay;
    private $SkipDepartureDay;
    private $ConfigTimeZone;
    private $ArrivalTimeTZ;
    private $DepartureTimeTZ;
    private $BeginDayTimeTZ;
    private $EndDayTimeTZ;
    private $YoungestAge;
    private $WarmMonth;
    private $ColdMonth;
    private $UserResortArea;
    private $WaterPark;

    // Singleton instance
    private static $_instance = null;

    private function __clone() {}

    private function __construct(Vacation $vacation) {
      $this->ConfigTimeZone = DateUtils::getConfigTimezone();
      $this->Vacation = $vacation;
      $this->travelDays();
      $this->arrivalTimeTZ();
      $this->departureTimeTZ();
      $this->skipArrivalDay();
      $this->skipDepartureDay();
      $this->schedulableDaysCount();
      $this->extraDays();
      $this->getYoungestTravelerAge();
    }

    public static function getInstance(Vacation $vacation) {
        if(!is_object(self::$_instance)) {
            self::$_instance = new BlocksUtils($vacation);
        }
        return self::$_instance;
    }

    public static function createBlock(array $attributes) {
        $block = new Block;
        $block->day()->associate($attributes['day']);
        self::addAttributes($block, $attributes);
        if ($block->name == null) $block->name = "block";
        return $block;
    }

    public static function addAttributes(Block &$block, array $attributes) {
        if(key_exists('id', $attributes)) $block->id = $attributes['id'];
        if(key_exists('type', $attributes)) $block->block_type_id = $attributes['type'];
        if(key_exists('name', $attributes)) $block->name = $attributes['name'];
        if(key_exists('location', $attributes)) $block->location = $attributes['location'];
        if(key_exists('parent_id', $attributes)) $block->parent_id = $attributes['parent_id'];
        if(key_exists('association_id', $attributes)) $block->association_id = $attributes['association_id'];
        if(key_exists('park_number', $attributes)) $block->park_number = $attributes['park_number'];

        $configTZ = DateUtils::getConfigTimezone();
        if (key_exists('startTime', $attributes)) {
            $dt = DateUtils::changeTimezone(
                $attributes['startTime'],
                $configTZ,
                'UTC'
            );
            $block->startTime = $dt;
        }
        if (key_exists('endTime', $attributes)) {
            $dt = DateUtils::changeTimezone(
                $attributes['endTime'],
                $configTZ,
                'UTC'
            );
            $block->endTime = $dt;
        }
        if ($block->id) {
          $block->save();
        }
    }

    public static function parksBlockTypeIds() {
      return BlockRepository::getAllParkTypeBlocks()->keyBy('id')->keys()->all();
    }

    public static function parksInPlanBlockTypeIds() {
      return BlockRepository::getAllParkTypeBlocks()->filter(function ($block_type) {
          return $block_type->in_plan == true;
      })->keyBy('id')->keys()->all();
    }

    public static function parksNotInPlanBlockTypeIds() {
      return BlockRepository::getAllParkTypeBlocks()->filter(function ($block_type) {
          return $block_type->in_plan == false;
      })->keyBy('id')->keys()->all();
    }

    public static function parentBlockTypeIds() {
      return BlockRepository::getAllBlockTypes()->filter(function ($block_type) {
          $parents = array('park', 'resort');
          return in_array($block_type->name, $parents);
      })->keyBy('id')->keys()->all();
    }

    public static function compare($blockA, $blockB) {
      switch (true) {
        case $blockA->day_id != $blockB->day_id:
          return false;
        case $blockA->block_type_id != $blockB->block_type_id:
          return false;
        case $blockA->park_number != $blockB->park_number:
          return false;
        case $blockA->name != $blockB->name:
          return false;
        case $blockA->location != $blockB->location:
          return false;
        case $blockA->startTime != $blockB->startTime:
          return false;
        case $blockA->endTime != $blockB->endTime:
          return false;
        case $blockA->parent_id != $blockB->parent_id:
          return false;
        case $blockA->association_id != $blockB->association_id:
          return false;
        default:
          return true;
      }
    }

    public function arrivalDay(Day $day) {
        if (DateUtils::getDaysDifference($this->Vacation->startTravelDate, $day->date) != 0) return false;
        return true;
    }

    public function departureDay(Day $day) {
        if (DateUtils::getDaysDifference($day->date, $this->Vacation->endTravelDate) != 0) return false;
        return true;
    }

    public function skipParkDay(Day $day) {
        // Do we need to skip this day due to other scheduled thing?
        if($this->arrivalDay($day)) {
          return $this->ExtraDays || $this->SkipArrivalDay;
        }
        if($this->departureDay($day)) {
          return $this->ExtraDays || $this->SkipDepartureDay;
        }
        return false;
    }

    public function travelDays() {
      if($this->TravelDays === null) {
        $startTravelDate = $this->Vacation->startTravelDate;
        $endTravelDate = $this->Vacation->endTravelDate;
        $this->TravelDays = DateUtils::getDaysDifference($startTravelDate, $endTravelDate) + 1;
      }
      return $this->TravelDays;
    }

    private function skipArrivalDay() {
      if($this->SkipArrivalDay === null) {
        $arrivalTime = DateUtils::setDateTime($this->Vacation->startTravelDate, $this->ArrivalTimeTZ);
        $limitTime = DateUtils::setDateTime($this->Vacation->startTravelDate, '21:00:00');
        $parkTime = DateUtils::getFutureTime($arrivalTime, "PT4H");
        $this->SkipArrivalDay = $parkTime >= $limitTime;
      }
      return $this->SkipArrivalDay;
    }

    private function skipDepartureDay() {
      if($this->SkipDepartureDay === null) {
        $this->SkipDepartureDay = $this->Vacation->booked_place_type_id > 2;
        $openTime = DateUtils::setDateTime($this->Vacation->endTravelDate, '09:00:00');
        $parkTime = DateUtils::getFutureTime($openTime, "PT3H");
        $limitTime = DateUtils::setDateTime($this->Vacation->endTravelDate, $this->DepartureTimeTZ);
        $this->SkipDepartureDay = $this->SkipDepartureDay || $parkTime >= $limitTime;
      }
      return $this->SkipDepartureDay;
    }

    public function extraDays() {
      if($this->ExtraDays === null) {
        $this->ExtraDays = $this->TravelDays > $this->SchedulableDaysCount;
      }
      return $this->ExtraDays;
    }

    public function schedulableDaysCount() {
      if($this->SchedulableDaysCount === null) {
        $schedulableDays = $this->Vacation->visitDays;
        // Add extra day for travel & arrival if needed
        if ($this->TravelDays > $schedulableDays && $this->SkipArrivalDay) {
            $schedulableDays += 1;
        }
        // Add extra day for travel & departure if needed
        if ($this->TravelDays > $schedulableDays && $this->SkipDepartureDay) {
            $schedulableDays += 1;
        }
        $this->SchedulableDaysCount = $schedulableDays;
      }
      return $this->SchedulableDaysCount;
    }

    public function arrivalTimeTZ() {
      if($this->ArrivalTimeTZ === null) {
        $arrivalTimeTZ = DateUtils::changeTimezone($this->Vacation->arrivalDate, 'UTC', $this->ConfigTimeZone, 'H:i:s');
        $arrivalTimeTZ = DateUtils::fixTime($arrivalTimeTZ);
        if($this->Vacation->isFlying && $this->Vacation->booked_place_type_id < 3) {
          $arrivalTimeTZ = DateUtils::getFutureTime($arrivalTimeTZ, "PT1H30M", 'H:i:s');
        }
        $this->ArrivalTimeTZ = $arrivalTimeTZ;
      }
      return $this->ArrivalTimeTZ;
    }

    public function departureTimeTZ() {
      if($this->DepartureTimeTZ === null) {
        $departureTimeTZ = DateUtils::changeTimezone($this->Vacation->departureDate, 'UTC', $this->ConfigTimeZone, 'H:i:s');
        $departureTimeTZ = DateUtils::fixTime($departureTimeTZ);
        if($this->Vacation->isFlying && $this->Vacation->booked_place_type_id < 3) {
          $departureTimeTZ = DateUtils::getPreviousTime($departureTimeTZ, "PT3H", 'H:i:s');
        }
        $this->DepartureTimeTZ = $departureTimeTZ;
      }
      return $this->DepartureTimeTZ;
    }

    public function beginDayTimeTZ($date) {
      if($this->BeginDayTimeTZ === null) {
        $this->BeginDayTimeTZ = DateUtils::changeTimezone($this->Vacation->beginDayTime, 'UTC', $this->ConfigTimeZone, 'H:i:s');
      }
      return DateUtils::setDateTime($date, $this->BeginDayTimeTZ);
    }

    public function endDayTimeTZ($date) {
      if($this->EndDayTimeTZ === null) {
        $this->EndDayTimeTZ = DateUtils::changeTimezone($this->Vacation->endDayTime, 'UTC', $this->ConfigTimeZone, 'H:i:s');
      }
      if(DateUtils::getTimeDifference('12:00:00', $this->EndDayTimeTZ) < 0) {
        $date = DateUtils::getFutureTime($date, 'P1D');
      }
      return DateUtils::setDateTime($date, $this->EndDayTimeTZ);
    }

    public function startTimeTZ(Block $block) {
      return DateUtils::changeTimezone($block->startTime, 'UTC', $this->ConfigTimeZone);
    }

    public function endTimeTZ(Block $block) {
      return DateUtils::changeTimezone($block->endTime, 'UTC', $this->ConfigTimeZone);
    }

    public function getYoungestTravelerAge() {
      if($this->YoungestAge === null) {
        $youngest = $this->getYoungestTraveler();
        $age = date("Y") - $youngest->yearOfBirth;
        //If the current month is prior to the birth month, roll it back a year:
        if(date("m") < $youngest->monthOfBirth) $age--;
        Log::info("Youngest traveler identified.",
        ['vacation id' => $this->Vacation->id, 'name' => $youngest->firstname, 'age' => $age]);
        $this->YoungestAge = $age;
      }
      return $this->YoungestAge;
    }

    private function getYoungestTraveler(){
      $youngest = $this->Vacation->travelers()->orderBy('yearOfBirth', 'desc')->first();
      if($youngest == null) {
        throw new MousePlannerException($this->Vacation, "Travelers not found", 412);
      }
      return $youngest;
    }

    public function breakfastTime() {
      return $this->BeginDayTimeTZ;
    }

    public function lunchTime() {
      return '12:00:00';
    }

    public function dinnerTime() {
      if($this->YoungestAge < 9) {
          return '17:30:00';
      }
      return '19:00:00';
    }

    public function warmMonth() {
      if($this->WarmMonth === null) {
        $vacationMonth = DateUtils::getTimeWithFormat($this->Vacation->startTravelDate, $format = 'm');
        $this->WarmMonth = in_array($vacationMonth, [6,7,8,9]);
      }
      return $this->WarmMonth;
    }

    public function coldMonth() {
      if($this->ColdMonth === null) {
        $vacationMonth = DateUtils::getTimeWithFormat($this->Vacation->startTravelDate, $format = 'm');
        $this->ColdMonth = in_array($vacationMonth, [1,2,12]);
      }
      return $this->ColdMonth;
    }

    public function verifyEMH($date, $emh) {
      $am = DateUtils::getTimeWithFormat($emh['startTime'], 'a') == 'am';
      $flexible_type = $this->Vacation->time_flexibility_type_id;
      $isEarlyTimeFlex = $flexible_type == 2 || $flexible_type == 3;
      $lateTimeFlex = $flexible_type == 2 || $flexible_type == 4;
      if ($am) {
        if($this->BeginDayTimeTZ > $emh['startTime'] && !$isEarlyTimeFlex) {
          return false;
        }
        $day = new Day;
        $day->date = $date;
        if($this->arrivalDay($day) && $this->arrivalTimeTZ() >= $emh['endTime']) {
          return false;
        }
      }
      else {
        $endDayTimeTZ = $this->endDayTimeTZ($date);
        if(DateUtils::getTimeDifference('12:00:00', $emh['endTime']) < 0) {
          $date = DateUtils::getFutureTime($date, 'P1D');
        }
        $emhDate = DateUtils::setDateTime($date, $emh['endTime']);
        if($endDayTimeTZ < $emhDate && !$lateTimeFlex) {
          return false;
        }
      }
      return true;
    }

    public function emhable($parksPerDay) {
      // User needs to be at Disney Resort to use EMH.
      $staysAtDisney = $this->Vacation->booked_place_type_id < 3;
      // User must have a 'Park Hopper' ticket to user EMH.
      $parkHopper = $parksPerDay > 1;
      return $staysAtDisney && $parkHopper;
    }

    public function userResortArea() {
      if($this->UserResortArea === null) {
        if($this->Vacation->booked_place_type_id == 2) {
          $userResort = Resort::find($this->Vacation->resort_id);
          $this->UserResortArea = $userResort->resort_area;
        } else if($this->Vacation->booked_place_type_id == 1) {
          $userResort = Resort::where('name', 'Disney\'s Art of Animation Resort')->first();
          $this->UserResortArea = $userResort->resort_area;
        } else {
          $this->UserResortArea = '';
        }
        Log::debug('User resort area', ['vacation_id'=>$this->Vacation->id, 'resort area'=>$this->UserResortArea]);
        if($this->UserResortArea == 'EP' && $this->Vacation->resort->name !== 'Disney\'s Caribbean Beach Resort') {
          Mousages::setEpcot3Mousage($this->Vacation);
        }
      }
      return $this->UserResortArea;
    }

    public function isWaterPark() {
      if($this->WaterPark === null) {
        // If they have selected Water Park Interest = Yes
        // but travel is between December and February, don't schedule water park
        $isWaterPark = $this->Vacation->interest_water_park_type_id == 1;
        $isWaterPark = $isWaterPark && !$this->coldMonth();
        //If travel is between June and September, and they are staying more than 5 days,
        //and they have selected Water Park Interest = Please recommend, then schedule Water Park
        $recommendWaterPark = $this->Vacation->interest_water_park_type_id == 3;
        $recommendWaterPark = $recommendWaterPark && $this->warmMonth() && $this->TravelDays > 5;
        $this->WaterPark = $isWaterPark || $recommendWaterPark;
        if(!$this->ExtraDays) $this->WaterPark = false;
      }
      return $this->WaterPark;
    }

    private function getParksPerDayKey($vacationModel) {
          $isParkHopper = $this->isParkHopper($vacationModel)? 1:0;
          $isNapsTrue = ($vacationModel->nap_type_id < 3)? 1:0;
          $pace = $vacationModel->pace_type_id;
          $blocksUtils = BlocksUtils::getInstance($vacationModel);
          if($blocksUtils->warmMonth() && $pace > 1) $pace--;
          return "block_{$isParkHopper}_{$isNapsTrue}_{$pace}";
      }

      private function isParkHopper($vacationModel) {
          $value = $vacationModel->interest_visit_park_type_id;
          switch ($value) {
              case 1:
                  $isParkHopper = true;
                  Mousages::setParkHopperMousage($vacationModel);
                  break;
              case 3:
                  $resort = $this->getResortByVacation($vacationModel);
                  $pace = $vacationModel->pace_type_id;
                  $visitDays = $vacationModel->visitDays;
                  $parksChosen = $vacationModel->parks()->count();
                  $high = 3;
                  $low = 1;
                  if($pace == $high ||
                    $vacationModel->budget_type_id == $high ||
                    ($resort && $resort->resort_area == 'EP') ||
                    ($pace > $low && $visitDays > $parksChosen)) {
                        $isParkHopper = true;
                        Mousages::setParkHopperMousage($vacationModel);
                  } else {
                    $isParkHopper = false;
                  }
                  break;
              default:
                  $isParkHopper = false;
                  break;
          }
          return $isParkHopper;
      }
}
