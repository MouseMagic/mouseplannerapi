<?php

namespace App\Utils;

use Illuminate\Support\Facades\Log;
use App\Repositories\GeneralOperatingHoursRepository;
use App\Models\ExperienceDefault;

class DisneyData {
    use GeneralOperatingHoursRepository;
    static $SchedulesFilename = 'schedules-all.json';
    static $EventsFilename = 'events-all.json';
    static $EntertainmentsFilename = 'entertainments-all.json';

    static $ValidEntertainmentsTypes = ['Fireworks', 'Parades', 'Nighttime Spectaculars'];

    private $Parks;
    private $DisneySchedulesData;
    private $DisneyEventsData;
    private $DisneyEntertainmentsData;
    private $DisneyDataPath;

    // Singleton instance
    private static $_instance = null;

    private function __clone() {}

    private function __construct() {
        $this->Parks = array("Magic Kingdom Park" => 'MK', "Epcot" => 'EP', "Disney's Animal Kingdom Theme Park" => 'AK', "Animal Kingdom Park" => 'AK', "Disney's Hollywood Studios" => 'HS');
        $this->DisneySchedulesData = collect([]);
        $this->DisneyEventsData = collect([]);
        $this->DisneyEntertainmentsData = collect([]);
        $this->DisneyDataPath = env('JSON_OUTPUT', '../../scraper/json-output/');
    }

    public static function getInstance() {
        if(!is_object(self::$_instance)) {
            self::$_instance = new DisneyData();
        }
        return self::$_instance;
    }

    private function getOpenHourDisneyData($park, $date) {
        $open_hour = -1;
        if($this->invalidSchedules()) {
            $this->getDisneySchedulesData();
        }
        $filtered = $this->DisneySchedulesData
            ->where('date', $date)
            ->where('park', $park)
            ->where('type', 'Operating')
            ->first();
        if($filtered == null) return $open_hour;
        return $filtered['startTime'];
    }

    private function getCloseHourDisneyData($park, $date) {
        $close_hour = -1;
        if($this->invalidSchedules()) {
            $this->getDisneySchedulesData();
        }
        $filtered = $this->DisneySchedulesData
            ->where('date', $date)
            ->where('park', $park)
            ->where('type', 'Operating')
            ->first();
        if($filtered == null) return $close_hour;
        return $filtered['endTime'];
    }

    /*
     * Step 5 of the Schedule Logic document.
     * */
    public function getSpecialEventsDisneyData($park, $date) {
      if($this->invalidEvents()) {
          $this->getDisneyEvents();
      }
      return $this->DisneyEventsData
          ->where('date', $date)
          ->where('park', $park);
    }
  
    /*
     * Step 6 of the Schedule Logic document.
     * */
    public function getExtraMagicHoursDisneyData($park, $date) {
        if($this->invalidSchedules()) {
            $this->getDisneySchedulesData();
        }
        return $this->DisneySchedulesData
            ->where('type', 'Extra Magic Hours')
            ->where('date', $date)
            ->where('park', $park)
            ->first();
    }

    public function getCrowdLevelDisneyData($park, $date, $default) {
      $crowd = $default;
      $emh = $this->getExtraMagicHoursDisneyData($park, $date);
      if($emh != null) {
        $crowd = $crowd + 1;
      }
      switch ($park) {
        case 'MK':
          if(DateUtils::getStringDayofWeek($date) == 'Saturday') {
            $crowd = $crowd + 1;
          }
          break;
        case 'EP':
          if(DateUtils::getStringDayofWeek($date) == 'Friday' ||
            DateUtils::getStringDayofWeek($date) == 'Saturday') {
            $crowd = $crowd + 1;
          }
          break;
      }
      return $crowd;
    }

    public function getEventsDetails($park, $date) {
        if($this->invalidEvents()) {
            $this->getDisneyEvents();
        }
        $events = $this->DisneyEventsData
            ->where('park', $park)
            ->where('date', $date)
            ->all();
        $answer = array();
        foreach($events as $event) {
            $name = $event['name'];
            $start = DateUtils::getTimeWithFormat($event['startTime'], 'g:ia');
            $end = DateUtils::getTimeWithFormat($event['endTime'], 'g:ia');
            if($start === $end) {
                $answer[] = "{$name} {$start}";
            } else {
                $answer[] = "{$name} {$start}-{$end}";
            }

        }
        return $answer;
    }

    public function getEntertainmentsDetails($park, $date) {
        if($this->invalidEntertainments()) {
            $this->getDisneyEntertainments();
        }
        $entertainments = $this->DisneyEntertainmentsData
            ->where('park', $park)
            ->where('date', $date)
            ->all();
        $answer = array();
        foreach($entertainments as $entertainment) {
            $name = $entertainment['name'];
            $start = DateUtils::getTimeWithFormat($entertainment['startTime'], 'g:ia');
            $end = DateUtils::getTimeWithFormat($entertainment['endTime'], 'g:ia');
            if($start === $end) {
                $answer[] = "{$name} {$start}";
            } else {
                $answer[] = "{$name} {$start}-{$end}";
            }

        }
        return $answer;
    }

    private function getDisneySchedulesData() {
        $schedulesAll = $this->getContent(DisneyData::$SchedulesFilename);
        if($schedulesAll == null) return;
        $schedulesFiltered = array_filter($schedulesAll, array(__CLASS__, "facilitiesOnly"));
        $this->DisneySchedulesData = collect([]);
        foreach($schedulesFiltered as $obj) {
            $park = $this->Parks[$obj['name']];
            $schedules = $obj['schedules'];
            $this->parseFacilitySchedules($schedules, $park);
        }
        Log::info("Park schedules parsed from Disney Data.");
    }

    private function getDisneyEventSchedules() {
        $schedulesAll = $this->getContent(DisneyData::$SchedulesFilename);
        if($schedulesAll == null) return;
        $eventsWithSchedules = array_filter($schedulesAll, array(__CLASS__, "eventsOnly"));
        Log::info("Event schedules parsed from Disney Data.");
        return $eventsWithSchedules;
    }

    private function getDisneyEntertainmentSchedules() {
        $schedulesAll = $this->getContent(DisneyData::$SchedulesFilename);
        if($schedulesAll == null) return;
        $entertainmentsWithSchedules = array_filter($schedulesAll, array(__CLASS__, "entertainmentsOnly"));
        Log::info("Entertainment schedules parsed from Disney Data.");
        return $entertainmentsWithSchedules;
    }

    private function getDisneyEvents() {
        $eventsAll = $this->getContent(DisneyData::$EventsFilename);
        if($eventsAll == null) return;
        $eventsFiltered = array_filter($eventsAll, array(__CLASS__, "themeParksOnly"));
        $eventsWithSchedules = $this->getDisneyEventSchedules();
        $this->DisneyEventsData = collect([]);
        foreach($eventsFiltered as $obj) {
            $id = substr($obj['id'], 0, -17);
            $eventSchedules = $this->getSchedules($eventsWithSchedules, $id);
            if($eventSchedules == null) continue;
            $parks = $this->getParks($obj['relatedLocations']);
            $name = $obj['name'];
            $this->parseThemeParkEvents($id, $name, $parks, $eventSchedules);
        }
        Log::info("Event details parsed from Disney Data.");
    }

    private function getDisneyEntertainments() {
      $entertainmentsAll = $this->getContent(DisneyData::$EntertainmentsFilename);
      if($entertainmentsAll == null) return;
      $entertainmentsFiltered = array_filter($entertainmentsAll, array(__CLASS__, "themeParksOnly"));
      $entertainmentsFiltered = array_filter($entertainmentsFiltered, array(__CLASS__, 'relevantEntertainments'));
      $entertainmentsWithSchedules = $this->getDisneyEntertainmentSchedules();
      foreach($entertainmentsFiltered as $obj) {
          $id = substr($obj['id'], 0, -25);
          $entertainmentSchedules = $this->getSchedules($entertainmentsWithSchedules, $id);
          if($entertainmentSchedules == null) continue;
          $parks = $this->getParks($obj['relatedLocations']);
          $name = $obj['name'];
          $this->parseThemeParkEntertainments($id, $name, $parks, $entertainmentSchedules);
      }
      Log::info("Entertainments details parsed from Disney Data.");
    }

    private function getContent($path) {
        $directory = env('JSON_OUTPUT');
        try {
            $file = file_get_contents($directory.$path);
            $content = json_decode($file, true);
        } catch(ErrorException $e) {
            $content = null;
        }
        return $content;
    }

    private function facilitiesOnly($obj) {
        return
          $this->filterSchedulesByFacilityType($obj, 'Facility') &&
          array_key_exists($obj['name'], $this->Parks);
    }

    private function eventsOnly($obj) {
        return $this->filterSchedulesByFacilityType($obj, 'Event');
    }

    private function entertainmentsOnly($obj) {
        return $this->filterSchedulesByFacilityType($obj, 'Entertainment');
    }

    private function filterSchedulesByFacilityType($obj, $facilityType) {
        return
            array_key_exists('facilityType', $obj) &&
            $obj['facilityType'] == $facilityType &&
            array_key_exists('schedules', $obj);
    }

    private function themeParksOnly($obj) {
        if(!array_key_exists('relatedLocations', $obj)) return false;
        $relatedLocation = $obj['relatedLocations'];
        $primaryLocation = $relatedLocation['primaryLocations'][0];
        $links = $primaryLocation['links'];
        return array_key_exists('ancestorThemePark', $links);
    }

    private function relevantEntertainments($obj) {
        if(!key_exists('facets', $obj)) return false;
        $facets = $obj['facets'];
        if(!key_exists('entertainmentType', $facets)) return false;
        $entertainmentTypes = $facets['entertainmentType'];
        foreach ($entertainmentTypes as $entertainmentType) {
          $type = $entertainmentType['value'];
          if(in_array($type, DisneyData::$ValidEntertainmentsTypes)) {
            return true;
          }
        }
        return false;
    }

    private function invalidSchedules() {
      $firstDate = $this->DisneySchedulesData->first()['date'];
      return $this->DisneySchedulesData->isEmpty() ||
            DateUtils::getDaysDifference('now', $firstDate) > 1;
    }

    private function invalidEvents() {
        $oldDate = DateUtils::getPreviousDateFromCurrent(3);
        return $this->DisneyEventsData->isEmpty() ||
            $this->DisneyEventsData->where('date', $oldDate)->first() != null;
    }

    private function invalidEntertainments() {
        $oldDate = DateUtils::getPreviousDateFromCurrent(3);
        return $this->DisneyEntertainmentsData->isEmpty() ||
            $this->DisneyEntertainmentsData->where('date', $oldDate)->first() != null;
    }

    private function parseFacilitySchedules(array $schedules, $park) {
        foreach($schedules as $schedule) {
            $data = array(
                'park' => $park,
                'type' => $schedule['type'],
                'date' => $schedule['date'],
                'startTime' => $schedule['startTime'],
                'endTime' => $schedule['endTime']
            );
            $this->DisneySchedulesData->push($data);
        }
    }

    private function getParks(array $locations) {
        $parks = array();
        $primaryLocations = $locations['primaryLocations'];
        foreach($primaryLocations as $loc) {
            $links = $loc['links'];
            if(array_key_exists('ancestorThemePark', $links)) {
                $ancestorThemePark = $links['ancestorThemePark']['title'];
                $park = $this->Parks[$ancestorThemePark];
                if(in_array($park, $parks)) continue;
                $parks[] = $park;
            }
        }
        return $parks;
    }

    private function getSchedules(&$eventsWithSchedules, $id) {
        if(!$eventsWithSchedules) return null;
        foreach($eventsWithSchedules as $schedule) {
            if($schedule['id'] == $id) return $schedule['schedules'];
        }
        return null;
    }

    private function parseThemeParkEvents($id, $name, array $parks, array $schedules) {
        foreach($schedules as $schedule) {
            foreach($parks as $park) {
                $data = $this->buildThemeParkRecord($park, $name, $schedule);
                $this->DisneyEventsData->push($data);
            }
        }
    }

    private function parseThemeParkEntertainments($id, $name, array $parks, array $schedules) {
        foreach($schedules as $schedule) {
            foreach($parks as $park) {
                $data = $this->buildThemeParkRecord($park, $name, $schedule);
                $this->DisneyEntertainmentsData->push($data);
            }
        }
    }

    private function buildThemeParkRecord($park, $name, $schedule) {
      return array(
          'park' => $park,
          'name' => $name,
          'type' => $schedule['type'],
          'date' => $schedule['date'],
          'startTime' => $schedule['startTime'],
          'endTime' => $schedule['endTime']
        );
    }

    public function getParkDetails($park, $dates) {
        $details = array();
        foreach($dates as $date) {
            // get regular operation hours
            list($openHour, $closeHour) = $this->getParkHours($park, $date);
            // get extra operating (magic) hours
            $emhRaw = $this->getExtraMagicHoursDisneyData($park, $date);
            $emh = "";
            if($emhRaw != null) {
                // This park has Extra Magin Hours this day
                $emhStart = DateUtils::getTimeWithFormat($emhRaw['startTime'], 'A ga');
                $emhEnd = DateUtils::getTimeWithFormat($emhRaw['endTime'], 'ga');
                $emh = "Magic Hours {$emhStart} - {$emhEnd}";
            }
            // format operating hours
            $openHourFormat = DateUtils::getTimeWithFormat($openHour, 'ga');
            $closeHourFormat = DateUtils::getTimeWithFormat($closeHour, 'ga');
            // get events with schedule
            $events = $this->getEntertainmentsDetails($park, $date);
            $details[] = [
                'day' => DateUtils::getStringDayofWeek($date),
                'park_hours' => "{$openHourFormat} - {$closeHourFormat}",
                "emh" => $emh,
                "events" => $events,
            ];
        }
        $answer = array('details' => $details);
        return $answer;
    }

    public function getParkHours($park, $date) {
        if($park == 'DS') {
            return array('10:00:00', '24:00:00');
        }
        if(in_array($park, array("TL", "BB"))) {
            return array('10:00:00', '17:00:00');
        }
        $weekDay = DateUtils::getStringDayofWeek($date);
        $yearWeek = DateUtils::getWeekofYear($date);
        $default = $this->getGeneralData($yearWeek, $weekDay)
                        ->where('park',$park)
                        ->first();
        $open_hour = $this->getOpenHour($park, $date, $default->openTime);
        $close_hour = $this->getCloseHour($park, $date, $default->closeTime);
        return array($open_hour, $close_hour);
    }

    public function getOpenHour($park, $date, $default) {
        $open_hour = $this->getOpenHourDisneyData($park, $date);
        if($open_hour < 0) {
            $open_hour = $default;
        }
        return $open_hour;
    }

    public function getCloseHour($park, $date, $default) {
        $close_hour = $this->getCloseHourDisneyData($park, $date);
        if($close_hour < 0) {
            $close_hour = $default;
        }
        if($close_hour === '00:00:00') $close_hour = '24:00:00';
        return $close_hour;
    }

    public function getCrowdLevel($park, $date, $default) {
        $crowd = $this->getCrowdLevelDisneyData($park, $date, $default);
        return $crowd;
    }

    public function getAvailableExperiences($startTravelDate, $endTravelDate) {
      if($this->invalidSchedules()) {
          $this->getDisneySchedulesData();
      }
      $vacationDates = DateUtils::createRange($startTravelDate, $endTravelDate);
      $schedulesAll = $this->getContent(DisneyData::$SchedulesFilename);
      $ids = $this->getExperienceIdsOnDateRange($schedulesAll, $vacationDates);
      if(!$ids || count($ids) < 1) {
        $ids = $this->getExperienceDefaultsIds();
      }
      return $ids;
    }

    private function getExperienceIdsOnDateRange($schedulesAll, $vacationDates) {
      $ids = array();
      $types = ['Performance Time', 'Operating', 'Special Ticketed Event'];
      foreach ($schedulesAll as $obj) {
        $validObject = key_exists('id', $obj) &&
                       key_exists('facilityType', $obj) &&
                       key_exists('schedules', $obj);
        if(!$validObject) continue;
        $schedules = $obj['schedules'];
        foreach ($schedules as $schedule) {
          if(!in_array($schedule['type'], $types)) continue;
          $date = $schedule['date'];
          if(in_array($date, $vacationDates)) {
            $ids[] = $obj['id'] . ';entityType=' . $obj['facilityType'];
            continue 2;
          }
        }
      }
      return $ids;
    }

    public function getExperienceDefaultsIds() {
      return ExperienceDefault::all()->pluck('disneyId')->all();
    }
}
