<?php

namespace App\Utils;

use DateTime;
use DateInterval;
use Carbon\Carbon;

use App\Repositories\ConfigRepository;
use App\Models\Configuration;

use Illuminate\Support\Facades\Log;

class DateUtils
{
    static private $DayOfTheWeek = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');

    public static function createRange($start, $end) {
        $dates = array();
        $newDate = Carbon::parse($start)->toDateString();
        while (DateUtils::getDaysDifference($end, $newDate) < 1) {
            $dates[] = $newDate;
            $newDate = Carbon::parse($newDate)->addDay()->toDateString();
        }
        return $dates;
    }

    public static function getDateGenericInfo($dates) {
        $datesInfo = array();
        foreach ($dates as $date) {
            $datesInfo[] = [
                'date' => $date,
                'week_year' => DateUtils::getWeekofYear($date),
                'day_week' => DateUtils::getStringDayofWeek($date)];
        }
        return $datesInfo;
    }

    public static function getStringDayofWeek($date) {
        $dateTime = new DateTime($date);
        $dayInt = $dateTime->format('w');
        return self::$DayOfTheWeek[$dayInt];
    }

    public static function weekend($date) {
        $dateTime = new DateTime($date);
        $dayInt = $dateTime->format('w');
        $dayString = self::$DayOfTheWeek[$dayInt];
        return in_array($dayString, ['Friday','Saturday','Sunday']);
    }

    public static function getWeekofYear($date) {
        $dateTime = new DateTime($date);
        $weekInt = $dateTime->format('W');
        return $weekInt;
    }

    public static function currentDate($format = 'Y-m-d g:i:s') {
      $date = Carbon::now();
      return $date->format($format);
    }

    public static function calcuateDaysDifference($toDate) {
        $today = new DateTime('now');
        $to = new DateTime($toDate);
        return $today->diff($to)->format("%r%a");
    }

    public static function isAM($time) {
        $hour = idate('H', strtotime($time));
        return ($hour - 12 < 0);
    }

    public static function getTimeWithFormat($time, $format = 'Y-m-d\TH:i:s.uP') {
        return Carbon::parse($time)->format($format);
    }

    public static function changeTimezone($time, $sourceTZ, $targetTZ, $format = 'Y-m-d H:i:s') {
        $dt = new Carbon($time, $sourceTZ);
        return $dt->timezone($targetTZ)->format($format);
    }

    public static function setDateTime($date, $time) {
        $dt = Carbon::parse($date);
        list($hour, $minutes, $seconds) = explode(':', $time, 3);
        return $dt->hour($hour)->minute($minutes)->second($seconds);
    }

    public static function fixTime($time) {
      $time = Carbon::parse($time);
      $minutes = $time->minute;
      if($minutes == 0) return $time->toTimeString();
      $remainder = $minutes % 10;
      if ($remainder > 0) {
        $minutes = $minutes - $remainder;
        $time->minute = $minutes;
        $time->second = 0;
      }
      return $time->toTimeString();
    }

    public static function getConfigTimezone() {
        return 'UTC';
    }

    public static function getPreviousDateFromCurrent($days, $format = 'Y-m-d H:i:s') {
        $date = new DateTime("now");
        $date->sub(new DateInterval("P{$days}D"));
        return $date->format($format);
    }

    public static function getPreviousTime($from, $interval, $format = 'Y-m-d H:i:s') {
        $fromDate = Carbon::parse($from);
        $date = Carbon::parse($fromDate)->sub(new DateInterval($interval));
        return $date->format($format);
    }

    public static function getFutureTime($from, $interval, $format = 'Y-m-d H:i:s') {
        $fromDate = Carbon::parse($from);
        $date = Carbon::parse($fromDate)->add(new DateInterval($interval));
        return $date->format($format);
    }

    public static function getTimeDifference($fromDate, $toDate) {
        $from = Carbon::parse($fromDate);
        $to = Carbon::parse($toDate);
        return $from->diffInMinutes($to, false);
    }

    public static function getDaysDifference($fromDate, $toDate) {
        $from = Carbon::parse($fromDate)->hour(0)->minute(0)->second(0);
        $to = Carbon::parse($toDate)->hour(0)->minute(0)->second(0);
        return $from->diffInDays($to, false);
    }
}
