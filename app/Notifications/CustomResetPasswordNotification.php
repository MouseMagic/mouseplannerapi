<?php

namespace App\Notifications;

use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Notifications\Messages\MailMessage;

class CustomResetPasswordNotification extends ResetPassword
{

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $from_email = config('mail.from.address');
        $from_name = config('mail.from.name');
        $link = config('app.frontend_url') . '/#!/password/reset/' . $this->token;

        return (new MailMessage)
            ->view('emails.reset_password', ['reset_link' => $link])
            ->from($from_email, $from_name)
            ->subject(sprintf('Password Reset Instructions for %s', config('mail.from.name')));
    }

}