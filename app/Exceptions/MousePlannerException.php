<?php

namespace App\Exceptions;

use App\Models\Vacation;

class MousePlannerException extends \Exception {
	private $Status;

  public function __construct(Vacation $vacation, $message, $code = 500, Exception $previous = null) {
    $message = "vacation_id:".$vacation->id." error:".$message;
		$this->Status = $code;
    parent::__construct($message, $code, $previous);
  }

	public function status() {
		return $this->Status;
	}
}
