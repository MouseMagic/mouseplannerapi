<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        Relation::morphMap([
            'days' => \App\Models\Day::class,
            'blocks' => \App\Models\Block::class,
            'mouseplans' => \App\Models\MousePlan::class,
            'parks' => \App\Models\Park::class,
            'resorts' => \App\Models\Resort::class,
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
