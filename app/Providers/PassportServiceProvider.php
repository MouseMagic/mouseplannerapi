<?php

namespace App\Providers;

use App\TokenServer;
use League\OAuth2\Server\AuthorizationServer;
use Laravel\Passport\Bridge;
use Laravel\Passport\Passport;

class PassportServiceProvider extends \Laravel\Passport\PassportServiceProvider {

    /**
     * Make the authorization service instance.
     *
     * @return AuthorizationServer
     */
    public function makeAuthorizationServer() {
//        return new TokenServer(
//            $this->app->make(\Laravel\Passport\Bridge\ClientRepository::class),
//            $this->app->make(\Laravel\Passport\Bridge\AccessTokenRepository::class),
//            $this->app->make(\Laravel\Passport\Bridge\ScopeRepository::class),
//            'file://'.storage_path('oauth-private.key'),
//            'file://'.storage_path('oauth-public.key')
//        );
        $server = new AuthorizationServer(
            $this->app->make(Bridge\ClientRepository::class),
            $this->app->make(Bridge\AccessTokenRepository::class),
            $this->app->make(Bridge\ScopeRepository::class),
            'file://'.Passport::keyPath('oauth-private.key'),
            'file://'.Passport::keyPath('oauth-public.key')
        );

        $server->setEncryptionKey(app('encrypter')->getKey());

        return $server;

    }

}
