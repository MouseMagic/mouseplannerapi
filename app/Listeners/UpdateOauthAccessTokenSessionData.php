<?php

namespace App\Listeners;

use App\Models\Subscription;
use App\Services\StripeService;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\Events\AccessTokenCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Connection;

class UpdateOauthAccessTokenSessionData
{

    /**
     * The database connection.
     *
     * @var \Illuminate\Database\Connection
     */
    protected $database;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Connection $database)
    {
        $this->database = $database;
    }

    /**
     * Handle the event.
     *
     * @param  LaravelPassportEventsAccessTokenCreated  $event
     * @return void
     */
    public function handle(AccessTokenCreated $event)
    {
        $user = User::find($event->userId);

        $prev_subscriptions_count = Subscription::where('user_id', $user->id)
            ->where('stripe_plan', '<>', StripeService::TRIAL_PLAN_ID)
            ->where('ends_at', '<', Carbon::now())
            ->count();

        if ($user) {
            $this->database->table('oauth_access_tokens')->where('id', $event->tokenId)
                ->update(['session_data' => json_encode([
                    'is_subscribed' => $user->calculate_is_subscribed(false),
                    'is_on_trial' => $user->calculate_is_on_trial(),
                    'were_subscribed_before' => (bool)$prev_subscriptions_count,
                ])]);
        }

    }
}
