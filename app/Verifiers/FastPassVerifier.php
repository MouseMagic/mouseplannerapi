<?php

namespace App\Verifiers;

use Illuminate\Support\Facades\Log;

use App\Models\Vacation;
use App\Models\Day;
use App\Models\Block;
use App\Models\FastpassReservation;
use App\Utils\DateUtils;
use App\Utils\BlocksUtils;
use App\Utils\Mousages;

class FastPassVerifier extends Verifier {

    public function verify(&$blockCollection) {
        if(!$blockCollection->isEmpty()){
            $this->addBlockIds();
        }
        return 0;
    }

    private function addBlockIds() {
        $vacation = $this->VacationModel;
        $mouseplan = $vacation->mouse_plan;
        $days = $mouseplan->days;
        foreach ($days as $day) {
            $this->verifyDay($day);
        }
    }

    private function verifyDay(Day $day) {
      $parksBlocks = $this->findBlocksByTypes($day->blocks, $day, BlocksUtils::parksInPlanBlockTypeIds());
      $day_fastpass = $day->fastpass_reservations;
      foreach ($day_fastpass as $fp) {
          $this->addBlockId($day, $fp, $parksBlocks);
      }
    }

    private function addBlockId(Day $day, FastpassReservation $fp, $parksBlocks) {
        $fp_park_id = $fp->experience->park_id;
        if($fp_park_id == null) return;
        $time = $fp->reservation_time;
        $blocks = $this->findBlocks($parksBlocks, $day, null, $time, $time);
        if ($blocks == null) {
          Log::info("No parent block found for FastPass+ Reservation.", [
            'vacation_id' => $this->VacationModel->id,
            'fp' => $fp]);
          $this->clearFastPassReservation($day, $fp);
          return;
        }
        foreach ($blocks as $block) {
          if($block->association_id != null && $block->association_id == $fp_park_id) {
            $fp->block_id = $block->id;
            $fp->save();
            break;
          }
        }
        if ($fp->block_id == null) {
          Log::info("Parent block found, but parks didn't match for FastPass+ Reservation.", [
            'vacation_id' => $this->VacationModel->id,
            'fp' => $fp,
            'FastpassReservation Park' => $fp_park_id]);
          $this->clearFastPassReservation($day, $fp);
        }
    }

    public function clearFastPassReservation(Day $day, FastpassReservation $fp) {
      Mousages::setOrphanFastPassMousage($this->VacationModel, $day, $fp);
      $fp->block_id = null;
      $fp->save();
    }

    public function validateInitialData() {
        if(!$this->VacationModel) {
            $this->logCritical("vacationModel");
            return false;
        }
        return true;
    }

    public function logCritical($error) {
        Log::critical("Input data is not set for DiningParentsVerifier.", ["missing" => $error]);
    }
}
