<?php

namespace App\Verifiers;

use Illuminate\Support\Facades\Log;

use App\Models\Vacation;
use App\Models\Day;
use App\Models\Park;
use App\Utils\DateUtils;
use App\Utils\DisneyData;
use App\Utils\BlocksUtils;
use App\Utils\Mousages;
use App\Exceptions\MousePlannerException;

/*
 * ParkTimesVerifier
 * Step 13 of the Schedule Logic document.
 * */
class ParkTimesVerifier extends Verifier {
    private $Parks;
    private $IsEarlyTimeFlex;
    private $IsLateTimeFlex;
    // private $DisneyData;

    public function verify(&$blockCollection) {
        if($blockCollection->isEmpty()){
            return 0;
        }
        $this->DisneyData = DisneyData::getInstance();
        $this->Parks = Park::all()->keyBy('initials');
        $flexible_type = $this->VacationModel->time_flexibility_type_id;
        $this->IsEarlyTimeFlex = $flexible_type == 2 || $flexible_type == 3;
        $this->IsLateTimeFlex = $flexible_type == 2 || $flexible_type == 4;
        $this->addParksTimes($blockCollection);
        $this->verifyTimes($blockCollection);
        return $this->calculateIdealParksTime($blockCollection);
    }

    private function addParksTimes(&$blockCollection) {
        $days = $this->VacationModel->mouse_plan->days;
        $totalHoursAtPlan = $this->getHoursAtPlan($blockCollection);
        $hoursAtPlan = $totalHoursAtPlan;
        $minimumHours = 3;
        foreach ($days as $day) {
            $blocks = $this->parksBlocks($blockCollection, $day)->sortBy('park_number');
            if (count($blocks) == 0) continue;
            $date = DateUtils::getTimeWithFormat($day->date, 'Y-m-d');
            $dayInfo = $this->ParkInfo->where('date', $date);
            foreach ($blocks as $block) {
                if($block->startTime != null) {
                  continue;
                }
                $park = $this->Parks->where('id', $block->association_id)->first()->initials;
                $parkInfo = $dayInfo->where('park', $park)->first();
                if ($parkInfo == null) {
                    Log::error("Park operating hours are missing.", ['vacation_id'=>$this->VacationModel->id, 'park' => $park, 'date' => $date]);
                }
                $maximumHours = $this->maximumHours($hoursAtPlan[$park], count($blocks));
                list($start, $end) = $this->parkTimes($day, $block, $date, $park, $blocks, $parkInfo, $minimumHours, $maximumHours);
                $minutesDiff = DateUtils::getTimeDifference($start, $end);
                $hoursDiff = round($minutesDiff/60);
                $hoursAtPlan = $this->updateHoursAtPlan($hoursAtPlan, $park, $hoursDiff * -1, 3, $totalHoursAtPlan);
                $this->setDateTimeInBlock($block, $start, $end);
                Log::debug("Parks time set", [
                    'vacation_id'=>$this->VacationModel->id,
                    'day'=>$day->date,
                    'start time'=>$block->startTime,
                    'end time'=>$block->endTime,
                    'park'=>$block->name,
                    'association_id'=>$block->association_id,
                ]);
            }
        }
    }

    private function verifyTimes(&$blockCollection) {
      $days = $this->VacationModel->mouse_plan->days;
      foreach ($days as $day) {
        $planBlocks = $this->parksInPlanBlocks($blockCollection, $day)->values();
        if (count($planBlocks) == 0) continue;
        $dayBlocks = $this->parksBlocks($blockCollection, $day)->values();
        $date = DateUtils::getTimeWithFormat($day->date, 'Y-m-d');
        $parkDay = $this->ParkInfo->where('date', $date);
        foreach ($planBlocks as $block) {
          $park = $this->Parks->where('id', $block->association_id)->first()->initials;
          $parkInfo = $parkDay->where('park', $park)->first();
          $this->verifyTime($day, $block, $dayBlocks, $parkInfo, $park);
        }
      }
    }

    private function verifyTime($day, &$block, $dayBlocks, $parkInfo, $park) {
      $userBeginTime = $this->BlocksUtils->beginDayTimeTZ($day->date);
      $parkOpenHour = DateUtils::setDateTime($day->date, $parkInfo['open_hour']);

      $userEndTime = $this->BlocksUtils->endDayTimeTZ($day->date);
      $parkCloseHour = DateUtils::setDateTime($day->date, $parkInfo['close_hour']);
      if($parkInfo['close_hour'] < '12:00:00') {
        $parkCloseHour = DateUtils::getFutureTime($parkCloseHour, 'P1D');
      }
      $times = [
        'day' => $day,
        'userBeginTime' => $userBeginTime,
        'parkOpenHour' => $parkOpenHour,
        'userEndTime' => $userEndTime,
        'parkCloseHour' => $parkCloseHour,
      ];
      if($this->verifyEndTime($day, $block, $dayBlocks, $userEndTime, $parkCloseHour)) {
        $this->notifyVerification($block, $park, $times);
      }
      if($this->verifyStartTime($day, $block, $dayBlocks, $userBeginTime, $parkOpenHour)) {
        $this->notifyVerification($block, $park, $times);
      }
    }

    private function notifyVerification($block, $park, $times) {
      $this->addMousages($block, $park, $times);
      Log::debug("Parks time verified", [
          'vacation_id' => $this->VacationModel->id,
          'day' => $times['day']->date,
          'start time' => $block->startTime,
          'end time' => $block->endTime,
          'park' => $block->name,
          'association_id' => $block->association_id,
      ]);
    }

    private function verifyEndTime($day, &$block, $dayBlocks, $userEndTime, $parkCloseHour) {
      $latestEnd = $this->endDayTime($day, $userEndTime, $parkCloseHour);
      $endTimeTZ = $this->BlocksUtils->endTimeTZ($block);
      if(DateUtils::getTimeDifference($endTimeTZ, $latestEnd) > 0) {
        $end = $this->increaseEndTime($endTimeTZ, 1, 12, $dayBlocks, $latestEnd);
        BlocksUtils::addAttributes($block, ['endTime' => $end]);
        return true;
      }
      return false;
    }

    private function verifyStartTime($day, &$block, $dayBlocks, $userBeginTime, $parkOpenHour) {
      $earliestStart = $this->startDayTime($day, $userBeginTime, $parkOpenHour, $dayBlocks);
      $startTimeTZ = $this->BlocksUtils->startTimeTZ($block);
      if(DateUtils::getTimeDifference($startTimeTZ, $earliestStart) < 0) {
        $start = $this->increaseStartTime($startTimeTZ, 1, 12, $dayBlocks, $earliestStart);
        BlocksUtils::addAttributes($block, ['startTime' => $start]);
        return true;
      }
      return false;
    }

    private function setDateTimeInBlock(&$block, $startTime, $endTime) {
      $attributes = array (
        'startTime' => $startTime,
        'endTime' => $endTime,
      );
      BlocksUtils::addAttributes($block, $attributes);
    }

    private function parkTimes($day, $block, $date, $park, $dayBlocks, $parkInfo, $minimumHours, $maximumHours) {
      $userBeginTime = $this->BlocksUtils->beginDayTimeTZ($day->date);
      $parkOpenHour = DateUtils::setDateTime($day->date, $parkInfo['open_hour']);

      $userEndTime = $this->BlocksUtils->endDayTimeTZ($day->date);
      $parkCloseHour = DateUtils::setDateTime($day->date, $parkInfo['close_hour']);
      if($parkInfo['close_hour'] < '12:00:00') {
        $parkCloseHour = DateUtils::getFutureTime($parkCloseHour, 'P1D');
      }

      $start = $this->startDayTime($day, $userBeginTime, $parkOpenHour, $dayBlocks);
      $end = $latestEnd = $this->endDayTime($day, $userEndTime, $parkCloseHour);
      // $pmEmh = $parkInfo['valid_emh'] && DateUtils::getTimeWithFormat($parkInfo['emh_start'], 'a') == 'pm';
      $interval = $minimumHours;
      do {
        if($interval == 0) {
          throw new MousePlannerException($this->VacationModel, "There is not time for {$park} on {$date}, day {$day->day_number}.");
        }
        // list($start, $end) = $this->findFreeSpace($pmEmh, $start, $latestEnd, $interval, $dayBlocks);
        list($start, $end) = $this->findFreeSpace($start, $latestEnd, $interval, $dayBlocks);
        if($end > $latestEnd) $interval = $interval - 1;
      } while($end > $latestEnd);
      $end = $this->increaseEndTime($end, $interval, $maximumHours, $dayBlocks, $latestEnd);

      $times = [
        'day' => $day,
        'userBeginTime' => $userBeginTime,
        'parkOpenHour' => $parkOpenHour,
        'userEndTime' => $userEndTime,
        'parkCloseHour' => $parkCloseHour,
      ];
      $this->addMousages($block, $park, $times);
      return array($start, $end);
    }

    private function increaseEndTime($time, $interval, $maximumHours, $dayBlocks, $latestEnd) {
      while($interval < $maximumHours) {
        $increment = 1;
        $newTime = DateUtils::getFutureTime($time, "PT{$increment}H");
        if($this->findConflicts($dayBlocks, null, $time, $newTime, true)) {
          break;
        }
        if($newTime > $latestEnd) {
          break;
        }
        $time = $newTime;
        $interval = $interval + $increment;
      }
      return $time;
    }

    private function increaseStartTime($time, $interval, $maximumHours, $dayBlocks, $earliestStart) {
      while($interval < $maximumHours) {
        $increment = 1;
        $newTime = DateUtils::getPreviousTime($time, "PT{$increment}H");
        if($this->findConflicts($dayBlocks, null, $newTime, $time, true)) {
          break;
        }
        if($newTime < $earliestStart) {
          break;
        }
        $time = $newTime;
        $interval = $interval + $increment;
      }
      return $time;
    }

    private function findFreeSpace($start, $latestEnd, $intervalHours, $dayBlocks) {
      $interval = "PT{$intervalHours}H";
      list($start, $end) = $this->findRegularFreeSpace($start, $intervalHours, $dayBlocks);
      return array($start, $end);
    }

    private function findRegularFreeSpace($start, $intervalHours, $dayBlocks) {
      $interval = "PT{$intervalHours}H";
      $end = DateUtils::getFutureTime($start, $interval);
      while ($this->findConflicts($dayBlocks, null, $start, $end, true)) {
        $interval = 'PT1H';
        $start = DateUtils::getFutureTime($start, $interval);
        $end = DateUtils::getFutureTime($end, $interval);
      }
      return array($start, $end);
    }

    private function maximumHours($parkTime, $dayBlocksCount, $maxDayHours = 12) {
      $minimumHours = 3.0;
      $parkHopper = $dayBlocksCount > 1;
      $longParkTime = $parkTime >= $maxDayHours;
      if (!$parkHopper && $longParkTime) {
        return $maxDayHours;
      }
      if ($parkHopper && $longParkTime) {
        return $maxDayHours / $dayBlocksCount;
      }
      if ($dayBlocksCount == 2 && $parkTime >= 9) {
        return 9;
      }
      if ($dayBlocksCount == 3 && $parkTime >= 6) {
        return 6;
      }
      // Get the next smaller multiple of 3
      $time = $minimumHours*floor($parkTime/$minimumHours);
      if($time >= $minimumHours) return $time;
      return $parkTime;
    }

    private function updateHoursAtPlan($hoursAtPlan, $park, $value, $minimumHours, $totalHoursAtPlan) {
      $newPlan = $hoursAtPlan;
      $newValue = $hoursAtPlan[$park] + $value;
      if ($newValue < $minimumHours) $newValue = $minimumHours;
      if ($newValue > $totalHoursAtPlan[$park]) $newValue = $totalHoursAtPlan[$park];
      $newPlan[$park] = $newValue;
      return $newPlan;
    }

    private function startDayTime(Day $day, $userBeginTime, $parkOpenHour, $dayBlocks) {
      $ds = $this->findBlocksByName($dayBlocks, $day, 'DISNEY SPRINGS');
      switch (true) {
        case $ds->count() > 0:
          $time = $this->BlocksUtils->endTimeTZ($ds->first());
          break;
        case $this->BlocksUtils->arrivalDay($day):
          $time = DateUtils::setDateTime($day->date, $this->BlocksUtils->arrivalTimeTZ());
          break;
        case $parkOpenHour == null:
          $time = $userBeginTime;
          break;
        case DateUtils::getTimeDifference($parkOpenHour, $userBeginTime) <= 0:
          //initial time occurs before park opens, so we need to use park open time.
          $time = $parkOpenHour;
          break;
        case $this->IsEarlyTimeFlex:
          //initial time occurs after park opens, if flexible, let's use the park open time.
          $time = $parkOpenHour;
          break;
        default:
          $time = $userBeginTime;
          break;
      }
      return $time;
    }

    private function endDayTime(Day $day, $userEndTime, $parkCloseHour) {
      switch (true) {
        case $this->BlocksUtils->departureDay($day):
          $time = DateUtils::setDateTime($day->date, $this->BlocksUtils->departureTimeTZ());
          break;
        case $parkCloseHour == null:
          $time = $userEndTime;
          break;
        case DateUtils::getTimeDifference($parkCloseHour, $userEndTime) >= 0:
          //end time occurs after park closes, so we need to use park close time.
          $time = $parkCloseHour;
          break;
        case $this->IsLateTimeFlex:
          //end time occurs before park closes, if flexible, let's use the park close time.
          $time = $parkCloseHour;
          break;
        default:
          //end time occurs before park closes but not flexible, so we'll use it.
          $time = $userEndTime;
          break;
      }
      return $time;
    }

    private function addMousages($block, $park, array $times) {
      $startTime = $this->BlocksUtils->startTimeTZ($block);
      $endTime = $this->BlocksUtils->endTimeTZ($block);
      $day = $times['day'];

      if($startTime == $times['parkOpenHour'] && $times['userBeginTime'] > $startTime) {
        Mousages::setScheduleEarlyMousage($this->VacationModel, $day, DateUtils::getTimeWithFormat($startTime, 'g:ia'));
      }

      if($endTime == $times['parkCloseHour'] && $times['userEndTime'] < $endTime) {
        $parkName = $this->Parks->get($park)->name;
        $hour = DateUtils::getTimeWithFormat($endTime, 'g:ia');
        $dayDate = DateUtils::getTimeWithFormat($day->date, 'F jS');
        Mousages::setScheduleLateMousage($this->VacationModel, $day, $parkName, $hour, $dayDate);
      }
    }

    private function getHoursAtPlan($blockCollection) {
        $scoreSum = $this->getPlanScoreSum($blockCollection);
        $hoursAtPlan = array();
        foreach ($this->ScorePark as $park => $score) {
            if($score <= 0) {
              $hoursAtPlan[$park] = 0.0;
              continue;
            }
            $associatedPark = $this->Parks->get($park);
            $parkBlocks = $this->findBlocksByPark($blockCollection, null, $associatedPark);
            if($parkBlocks->isEmpty()) {
              $hoursAtPlan[$park] = 0.0;
              continue;
            }
            $hours = $this->getParkHours($score, $scoreSum);
            $scheduledTime = $this->getScheduledHours($parkBlocks);
            if ($scheduledTime > $hours) {
              $hoursAtPlan[$park] = 0.0;
            } else {
              $hoursAtPlan[$park] = round($hours - $scheduledTime);
            }
        }
        return $hoursAtPlan;
    }

    private function getParkHours($score, $scoreSum) {
        $totalDays = $this->VacationModel->visitDays;
        $totalTime = $totalDays * 12;
        $timePercent = round(($score/$scoreSum), 2);
        $hours = $totalTime * $timePercent;
        return $hours;
    }

    private function getScheduledHours($parkBlocks) {
        $scheduledTime = 0;
        foreach ($parkBlocks as $block) {
            if($block->startTime != null) {
                $minutes = DateUtils::getTimeDifference($block->startTime, $block->endTime);
                $scheduledTime = $scheduledTime + $minutes/60;
            }
        }
        return $scheduledTime;
    }

    private function getPlanScoreSum($blockCollection) {
        $scoreSum = 0;
        foreach ($this->ScorePark as $park => $score) {
            $associatedPark = $this->Parks->get($park);
            $parkBlocks = $this->findBlocksByPark($blockCollection, null, $associatedPark);
            if($parkBlocks->isEmpty()) continue;
            $scoreSum += $score;
        }
        if ($scoreSum == 0) {
          Log::critical("ParkTimesVerifier->getPlanScoreSum crashed and the $scoreSum is still zero");
          abort(500, "Scheduling is imposible because the parks score is zero.");
        }
        return $scoreSum;
    }

    private function calculateIdealParksTime($blockCollection) {
        $days = $this->VacationModel->mouse_plan->days;
        $initialTime = '08:00:00';
        $finalTime = '22:00:00';
        $totalParksTime = 0;
        foreach ($days as $day) {
            $blocks = $this->parksInPlanBlocks($blockCollection, $day);
            if (count($blocks) == 0) continue;
            $date = DateUtils::getTimeWithFormat($day->date, 'Y-m-d');
            // Set ideal times to calculate trip score
            $idealStart = null;
            $idealEnd = null;
            $dayParksInfo = $this->ParkInfo->where('date', $date);
            foreach ($blocks as $block) {
                $park = $this->Parks->where('id', $block->association_id)->first()->initials;
                $parkInfo = $dayParksInfo->where('park', $park)->first();
                if ($parkInfo['open_hour'] != null) {
                    $openHour = DateUtils::setDateTime($day->date, $parkInfo['open_hour']);
                    if(!$idealStart) {
                      $idealStart = $openHour;
                    } else if($idealStart > $openHour) {
                      $idealStart = $openHour;
                    }
                }
                if ($parkInfo['close_hour'] != null) {
                    if($parkInfo['close_hour'] < '12:00:00') {
                      $nextDayDate = DateUtils::getFutureTime($day->date, 'P1D');
                      $closeHour = DateUtils::setDateTime($nextDayDate, $parkInfo['close_hour']);
                    } else {
                      $closeHour = DateUtils::setDateTime($day->date, $parkInfo['close_hour']);
                    }
                    if(!$idealEnd) {
                      $idealEnd = $closeHour;
                    } else if($idealEnd < $closeHour) {
                      $idealEnd = $closeHour;
                    }
                }
            }
            if(!$idealStart) $idealStart = DateUtils::setDateTime($day->date, $initialTime);
            if(!$idealEnd) $idealEnd = DateUtils::setDateTime($day->date, $finalTime);
            $totalParksTime += DateUtils::getTimeDifference($idealStart, $idealEnd);
        }
        return $totalParksTime;
    }

    public function validateInitialData() {
        if(!$this->ParkInfo) {
            $this->logCritical("parkInfo");
            return false;
        }
        if(!$this->VacationModel) {
            $this->logCritical("vacationModel");
            return false;
        }
        if(!$this->VacationModel->mouse_plan) {
            $this->logCritical("VacationModel->mouse_plan");
            return false;
        }
        if(!$this->VacationModel->beginDayTime) {
            $this->logCritical("VacationModel->beginDayTime");
            return false;
        }
        if(!$this->VacationModel->endDayTime) {
            $this->logCritical("VacationModel->endDayTime");
            return false;
        }
        if(!$this->VacationModel->time_flexibility_type_id) {
            $this->logCritical("VacationModel->time_flexibility_type_id");
            return false;
        }
        return true;
    }

    public function logCritical($error) {
        Log::critical("Input data is not set for ParkTimesVerifier.", ["missing" => $error]);
    }
}
