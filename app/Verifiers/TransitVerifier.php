<?php

namespace App\Verifiers;

use Illuminate\Support\Facades\Log;

use App\Models\Vacation;
use App\Models\Resort;
use App\Models\Block;
use App\Models\Day;
use App\Models\Configuration;
use App\Utils\DateUtils;
use App\Utils\BlocksUtils;
use App\Utils\Mousages;
use App\Repositories\ConfigRepository;

/*
 * TransitVerifier
 * Step 16 of the Schedule Logic document.
 * */
class TransitVerifier extends Verifier {
    private $ConfigTZ;
    private $TotalTransitsTime;

    public function __construct() {
        $this->ConfigTZ = DateUtils::getConfigTimezone();
        $this->TotalTransitsTime = 0;
    }

    public function verify(&$blockCollection) {
        if($blockCollection->isEmpty()){
            return 0;
        }
        $this->addTransitBlocks($blockCollection);
        return $this->TotalTransitsTime * -1;
    }

    private function addTransitBlocks(&$blockCollection) {
        $days = $this->VacationModel->mouse_plan->days;
        foreach ($days as $day) {
            $filter = array(
                self::$BLOCK_TYPES['dining'],
                // self::$BLOCK_TYPES['custom'],
                self::$BLOCK_TYPES['arrival'],
                self::$BLOCK_TYPES['departure'],
            );
            $blocks = $this->filterBlocksByType($blockCollection, $day, $filter);
            $dayBlocks = $this->getDayTransitBlocks($day, $blocks);
            foreach ($dayBlocks as $block) {
              $blockCollection->push($block);
            }
        }
    }

    private function getDayTransitBlocks(Day $day, &$blocks) {
        $slice_size = 2;
        $transits = array();
        $transitable = $this->filterBlocksByType($blocks, $day, [self::$BLOCK_TYPES['transit']]);
        $slice_index = 0;
        while ($slice_index < $transitable->count() - 1) {
            $slice = $transitable->slice($slice_index++, $slice_size);
            $from = $slice->first();
            $to = $slice->last();
            if ($this->isTransitNeeded($blocks, $from, $to)) {
                $transitInterval = $this->getTransitTimeInverval($from, $to, $day->date);
                $times = $this->getTransitTimes($from, $to, $transitInterval);
                if ($times == null) {
                  Log::critical(
                    "Transit block can not be scheduled due to time conflict between source and target block", [
                      'vacation_id' => $this->VacationModel->id,
                      'day' => $day->date,
                      'source end time' => $from->endTime,
                      'target start time' => $to->startTime,
                      'transit time' => DateUtils::getTimeDifference($from->endTime, $to->startTime)
                    ]);
                  continue;
                }
                list($start, $end) = $times;
                $name = $this->transitName($from, $to, $transitInterval);
                $this->TotalTransitsTime += $transitInterval;
                $transits[] = $this->getTransit($start, $end, $name, $day);
            }
        }
        return $transits;
    }

    private function transitName(Block &$from, Block &$to, $transitInterval) {
      return "Transit";
    }

    private function getTransitTimes(Block $from, Block $to, $transitInterval) {
        $timeInBlocks = DateUtils::getTimeDifference($from->endTime, $to->startTime);
        if ($timeInBlocks < 0) return null;
        switch (true) {
            case $timeInBlocks >= $transitInterval:
                $start = DateUtils::getPreviousTime($to->startTime, "PT{$transitInterval}M");
                $end = $to->startTime;
                break;
            case $from->block_type_id == self::$BLOCK_TYPES['arrival']:
                $start = $from->endTime;
                $end = DateUtils::getFutureTime($start, "PT{$transitInterval}M");
                $to->startTime = $end;
                break;
            case $to->block_type_id == self::$BLOCK_TYPES['departure']:
                $start = DateUtils::getPreviousTime($to->startTime, "PT{$transitInterval}M");
                $end = $to->startTime;
                $from->endTime = $start;
                break;
            default:
                $transitInterval = $transitInterval - $timeInBlocks;
                if ($transitInterval % 2 == 0) {
                    $shiftTime = $transitInterval / 2;
                    $start = DateUtils::getPreviousTime($from->endTime, "PT{$shiftTime}M");
                    $end = DateUtils::getFutureTime($to->startTime, "PT{$shiftTime}M");
                } else {
                    $shiftTime = floor($transitInterval / 2);
                    $start = DateUtils::getPreviousTime($from->endTime, "PT{$shiftTime}M");
                    $shiftTime++;
                    $end = DateUtils::getFutureTime($to->startTime, "PT{$shiftTime}M");
                }
                $from->endTime = $start;
                $to->startTime = $end;
                break;
        }
        $this->saveTargetBlocks($from, $to);
        return array($start, $end);
    }

    private function saveTargetBlocks(Block $from, Block $to) {
        if (isset($from->id)) {
            $from->save();
        }
        if (isset($to->id)) {
            $to->save();
        }
    }

    private function isTransitNeeded(&$blockCollection, Block $from, Block $to) {
        $needed = false;
        $transits = $this->findBlocks(
            $blockCollection,
            null,
            self::$BLOCK_TYPES['transit'],
            $from->endTime,
            $to->startTime
        );
        if ($transits->isEmpty()) {
            $needed = true;
        }
        if($needed && $from->block_type_id == self::$BLOCK_TYPES['custom']) {
          if($from->name != 'Afternoon Break') {
            $needed = false;
          }
        }
        if($needed && $to->block_type_id == self::$BLOCK_TYPES['custom']) {
          if($to->name != 'Afternoon Break') {
            $needed = false;
          }
        }
        return $needed;
    }

    private function getTransitTimeInverval($fromBlock, $toBlock, $date) {
        $config = new ConfigRepository(new Configuration());
        $minSize = $config->getValue('blocks', 'minimumMinutesSize');
        $default_time = $minSize > 30? $minSize : 30;
        if($minSize > 60) {
            return $default_time;
        }
        if($fromBlock->block_type_id == self::$BLOCK_TYPES['custom'] || $toBlock->block_type_id == self::$BLOCK_TYPES['custom']) {
          return 60;
        }
        return 30;
        // if($minSize > 45) {
        //     return $default_time;
        // }
        // $resort_type = self::$BLOCK_TYPES['resort'];
        // $park_block_types = BlocksUtils::parksBlockTypeIds();
        // if(in_array($toBlock->block_type_id, $park_block_types)) {
        //     $crowd_level = $this->ParkInfo
        //                   ->where('date', DateUtils::getTimeWithFormat($date, 'Y-m-d'))
        //                   ->where('park', $toBlock->name)
        //                   ->first()['crowd_level'];
        //     if($crowd_level == 3) return 45;
        // }
        // if($minSize > 15) {
        //     return $default_time;
        // }
        // if($fromBlock->block_type_id == $resort_type && in_array($toBlock->block_type_id, $park_block_types)) {
        //     return $this->getResortParkTransitTime($fromBlock, $toBlock);
        // }
        // if($toBlock->block_type_id == $resort_type && in_array($fromBlock->block_type_id, $park_block_types)) {
        //     return $this->getResortParkTransitTime($toBlock, $fromBlock);
        // }
    }

    public function getResortParkTransitTime(&$resotBlock, &$parkBlock) {
        $time = 30;
        $resort = Resort::where('name', $resotBlock->name)->first();
        $resort_area = $resort->resort_area;
        $parkName = $parkBlock->name;
        if($parkName == $resort_area) {
            $time = 15;
        }
        return $time;
    }

    public function getTransit($start, $end, $name, &$day) {
        $start = $this->changeTimezone($start);
        $end = $this->changeTimezone($end);
        $attributes = array(
            'day' => $day,
            'name' => $name,
            'type' => self::$BLOCK_TYPES['transit'],
            'startTime' => $start,
            'endTime' => $end,
        );
        $block = BlocksUtils::createBlock($attributes);
        Log::debug("New transit block", [
            'vacation_id' => $this->VacationModel->id,
            'date' => $day->date,
            'from' => $start,
            'to' => $end,
            'name' => $name
        ]);
        return $block;
    }

    private function changeTimezone($time) {
        return DateUtils::changeTimezone($time, 'UTC', $this->ConfigTZ);
    }

    public function validateInitialData() {
        if(!$this->VacationModel) {
            $this->logCritical("vacationModel");
            return false;
        }
        if(!$this->VacationModel->mouse_plan) {
            $this->logCritical("VacationModel->mouse_plan");
            return false;
        }
        if(!$this->ParkInfo) {
            $this->logCritical("ParkInfo");
            return false;
        }
        return true;
    }

    public function logCritical($error) {
        Log::critical("Input data is not set for TransitVerifier.", ["missing" => $error]);
    }
}
