<?php

namespace App\Verifiers;

use Illuminate\Support\Facades\Log;

use App\Models\Vacation;
use App\Models\Park;
use App\Utils\BlocksUtils;

class ParkNamesVerifier extends Verifier {
    private $Parks;

    public function verify(&$blockCollection) {
        if($blockCollection->isEmpty()) {
            return 0;
        }
        $this->Parks = Park::all()->keyBy('id');
        $this->updateParkNames($blockCollection);
        return 0;
    }

    private function updateParkNames(&$blockCollection) {
      $blocks = $this->parksBlocks($blockCollection);
      foreach ($blocks as $block) {
          if(!$block->association_id) throw new MousePlannerException($this->VacationModel, "Park blocks need an association_id.", 412);
          $park = $this->Parks->get($block->association_id);
          if ($block->name == $park->name) continue;
          $attributes = array('name' => $park->name);
          BlocksUtils::addAttributes($block, $attributes);
          Log::debug("Park name set", [
              'vacation_id'=>$this->VacationModel->id,
              'day'=>$block->day->date,
              'block_id'=>$block->id,
              'park'=>$block->name,
          ]);
          $block->save();
      }
    }

    public function validateInitialData() {
        if(!$this->VacationModel->mouse_plan) {
            $this->logCritical("VacationModel->mouse_plan");
            return false;
        }
        return true;
    }

    public function logCritical($error) {
        Log::critical("Input data is not set for ParkTimesVerifier.", ["missing" => $error]);
    }
}
