<?php

namespace App\Verifiers;

use Illuminate\Support\Facades\Log;

use App\Models\Block;
use App\Models\Day;
use App\Models\Vacation;
use App\Models\Resort;
use App\Models\Park;
use App\Models\BlockType;
use App\Utils\DateUtils;
use App\Utils\BlocksUtils;

/*
 * DisneySpringsVerifier
 * Step 12 of the Schedule Logic document.
 * */
class DisneySpringsVerifier extends Verifier {

    public function verify(&$blockCollection) {
        $blockCollection = $this->verifyDS($blockCollection);
        return 0;
    }

    private function verifyDS($blockCollection) {
        $roomForDS = $this->isDisneySprings($blockCollection);
        if(!$roomForDS && !$this->isDisneySpringsResort($blockCollection)){
          return $blockCollection;
        }
        $day = $this->firstEmptyDay($blockCollection);
        if($day == null) return $blockCollection;
        $block = $this->addDSBlock($day, '11:00:00', '19:00:00');
        $blockCollection->push($block);
        Log::debug("New park block",
            ['vacation_id' => $this->VacationModel->id,
             'park' => 'DisneySprings',
             'day' => $day['date']]);
        return $blockCollection;
    }

    private function addDSBlock(Day $day, $startTime, $endTime) {
      $initials = 'DS';
      $association_id = null;
      $park = Park::where('initials', $initials)->first();
      if($park != null) {
        $association_id = $park['id'];
      }
      $name = 'DISNEY SPRINGS';
      return BlocksUtils::createBlock([
        'day' => $day,
        'type' => self::$BLOCK_TYPES[$name],
        'name' => $name,
        'startTime' => DateUtils::setDateTime($day->date, $startTime),
        'endTime' => DateUtils::setDateTime($day->date, $endTime),
        'association_id' => $association_id,
        'park_number' => 1,
      ]);
    }

    private function isDisneySprings($blockCollection) {
    /**
    * Verify if Disney Springs park should be added to the park list (just one block):
    * IF trip length is >7 days, Add Disney Springs
    * IF trip length is >5 days, AND Interests = Shopping or Food & Drink, then add Disney Springs
    **/
        $vacation = $this->VacationModel;
        $travelDays = $this->BlocksUtils->travelDays();
        $availableDays = $this->firstEmptyDay($blockCollection) != null;
        $isLongTrip = $travelDays > 7;
        $shopping = $vacation->interest_types->search('Shopping');
        $food = $vacation->interest_types->search('Food & Drink');
        $isShoppingTrip = $travelDays > 5 && ($shopping || $food);
        return $availableDays && ($isLongTrip || $isShoppingTrip);
    }

    private function firstEmptyDay($blockCollection) {
      $vacation = $this->VacationModel;
      $days = $vacation->mouse_plan->days;
      foreach ($days as $day) {
        if($this->BlocksUtils->arrivalDay($day)) continue;
        if($this->BlocksUtils->departureDay($day)) continue;
        if($this->BlocksUtils->skipParkDay($day)) continue;
        $blocks = $this->parksBlocks($blockCollection, $day);
        if($blocks->isEmpty()) return $day;
      }
      return null;
    }

    private function isDisneySpringsResort($blockCollection) {
        /**
        * If staying at DS resort and add a message to the transit block before the Disney Springs block: “Did you know that you can take a riverboat between your resort and Disney Springs?”
        **/
        $vacation = $this->VacationModel;
        if(!$this->BlocksUtils->extraDays()) return false;
        $resort = null;
        if($vacation->booked_place_type_id == 2) {
            $resort = Resort::find($vacation->resort_id);
        }
        $ds = $resort && $resort->resort_area == 'DS';
        return $ds;
    }

    public function validateInitialData() {
        if(!$this->VacationModel) {
            $this->logCritical("vacationModel");
            return false;
        }
        if(!$this->VacationModel->budget_type_id) {
            $this->logCritical("VacationModel->budget_type_id");
            return false;
        }
        if(!$this->VacationModel->booked_place_type_id > 2) {
            $this->logInfo("VacationModel->resort");
            return false;
        }
        if(!$this->VacationModel->startTravelDate) {
            $this->logCritical("VacationModel->startTravelDate");
            return false;
        }
        if(!$this->VacationModel->startTravelDate) {
            $this->logCritical("VacationModel->endTravelDate");
            return false;
        }
        if(!$this->VacationModel->interest_types) {
            $this->logCritical("VacationModel->interest_types");
            return false;
        }
        return true;
    }

    public function logInfo($data) {
        Log::info("DisneySpringsFilter does not apply for this vacation.",
          ["vacation_id" => $this->VacationModel->id, "missing" => $data]);
    }

    public function logCritical($error) {
        Log::critical("Input data is not set for DisneySpringsFilter.",
          ["vacation_id" => $this->VacationModel->id, "missing" => $error]);
    }
}
