<?php

namespace App\Verifiers;

use Illuminate\Support\Facades\Log;

use App\Models\Vacation;
use App\Models\Day;
use App\Models\Block;
use App\Models\Resort;
use App\Models\Restaurant;
use App\Utils\DateUtils;
use App\Utils\BlocksUtils;
use App\Utils\Mousages;

/*
 * DiningParentsVerifier
 * Step 18 of the Schedule Logic document.
 * */
class DiningParentsVerifier extends Verifier {

    public function verify(&$blockCollection) {
        if(!$blockCollection->isEmpty()){
            $this->addDiningParents();
        }
        return 0;
    }

    private function addDiningParents() {
        $vacation = $this->VacationModel;
        $mouseplan = $vacation->mouse_plan->load('days');
        $days = $mouseplan->days;
        foreach ($days as $day) {
            $this->verifyDay($day);
        }
    }

    private function verifyDay(Day $day) {
      $diningBlocks = $day->blocks->filter(function ($block) {
          return $block->block_type_id == self::$BLOCK_TYPES['dining'];
      });
      $parentBlocks = $day->blocks->filter(function ($block) {
          return in_array($block->block_type_id, BlocksUtils::parentBlockTypeIds());
      });
      foreach ($diningBlocks as $block) {
          $this->addParent($day, $block, $parentBlocks);
      }
    }

    private function addParent(Day $day, Block $block, &$parentBlocks) {
        $blockParent = $this->findBlockByTime($parentBlocks, $day, $block->startTime);
        if ($blockParent != null) {
            $this->parentInfo($block, $blockParent);
            if($blockParent == $parentBlocks->last() &&
                $blockParent->endTime < $block->endTime) {
                    $blockParent->endTime = $block->endTime;
                    $blockParent->save();
            }
            return;
        }
        $blockParent = $this->findBlockByTime($parentBlocks, $day, $block->endTime);
        if ($blockParent != null) {
            $this->parentInfo($block, $blockParent);
            if($blockParent == $parentBlocks->first() &&
                $blockParent->startTime > $block->startTime) {
                    $blockParent->startTime = $block->startTime;
                    $blockParent->save();
            }
            return;
        }
        $this->clearParentInfo($block);
    }

    private function parentInfo(&$diningBlock, $blockParent) {
      $diningBlock->parent_id = $blockParent->id;
      if ($blockParent->block_type_id == self::$BLOCK_TYPES['resort']) {
        $diningBlock->location = $blockParent->name;
      } else {
        $diningBlock->location = "";
      }
      $diningBlock->save();
    }

    private function clearParentInfo(&$diningBlock) {
        $diningBlock->parent_id = null;
        $diningBlock->location = "";
        $diningBlock->save();
    }

    public function validateInitialData() {
        if(!$this->VacationModel) {
            $this->logCritical("vacationModel");
            return false;
        }
        return true;
    }

    public function logCritical($error) {
        Log::critical("Input data is not set for DiningParentsVerifier.", ["missing" => $error]);
    }
}
