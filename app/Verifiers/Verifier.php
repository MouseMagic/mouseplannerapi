<?php

namespace App\Verifiers;

use Illuminate\Support\Facades\Log;

use App\Models\Vacation;
use App\Models\Day;
use App\Models\Park;
use App\Utils\DateUtils;
use App\Utils\BlocksUtils;

abstract class Verifier {

    protected $ParkInfo;
    protected $ScorePark;
    protected $VacationModel;
    protected $ParksPerDay;
    protected $BlocksUtils;

    public static $BLOCK_TYPES = array('MAGIC KINGDOM' => 1, 'ANIMAL KINGDOM' => 2, 'EPCOT' => 3, 'HOLLYWOOD STUDIOS' => 4,
                                'DISNEY SPRINGS' => 5, 'BLIZZARD BEACH' => 6, 'TYPHOON LAGOON' => 7, 'resort' => 8,
                                'dining' => 9, 'transit' => 10, 'arrival' => 11, 'departure' => 12,
                                'custom' => 13);

    abstract function verify(&$blockCollection);

    abstract function validateInitialData();

    public function setParkInfo($parkInfo) {
        $this->ParkInfo = $parkInfo;
    }

    public function setVacationModel(Vacation $vacation) {
        $this->VacationModel = $vacation;
        $this->BlocksUtils = BlocksUtils::getInstance($vacation);
    }

    public function setScoreParks($scoreParks) {
        $this->ScorePark = $scoreParks;
    }

    public function setParksPerDay($parksPerDay) {
        $this->ParksPerDay = $parksPerDay;
    }

    public function findBlockByTime($blockCollection, $day, $time, $toUTC = false) {
        $timeUTC = $time;
        if($toUTC) {
          $configTZ = DateUtils::getConfigTimezone();
          $timeUTC = DateUtils::changeTimezone($time, $configTZ, 'UTC');
        }
        $blocks = $blockCollection->where('day_id', $day->id);
        foreach ($blocks as $block) {
            if($block->startTime == $timeUTC) {
                return $block;
            }
            if($block->startTime <= $timeUTC && $block->endTime > $timeUTC) {
              return $block;
            }
        }
        return null;
    }

    public function findBlocks($blockCollection, $day, $type, $start, $end) {
        if ($day == null && $type == null) {
            Log::critical("findBlock needs day or type to filter the search.");
            return null;
        }
        if ($type == null) {
            $blocks = $blockCollection->where('day_id', $day->id)->sortBy('endTime');
        } else if ($day == null) {
            $blocks = $blockCollection->where('block_type_id', $type)->sortBy('endTime');
        } else {
            $blocks = $blockCollection->where('day_id', $day->id)->where('block_type_id', $type)->sortBy('endTime');
        }
        $blocks = $blocks->where('startTime', '>=', $start);
        $blocks = $blocks->where('endTime', '<=', $end);
        return $blocks;
    }

    public function findConflicts($blockCollection, $day, $start, $end, $toUTC = false) {
      $startUTC = $start;
      $endUTC = $end;
      if($toUTC) {
        $configTZ = DateUtils::getConfigTimezone();
        $startUTC = DateUtils::changeTimezone($start, $configTZ, 'UTC');
        $endUTC = DateUtils::changeTimezone($end, $configTZ, 'UTC');
      }
      if ($day == null) {
          $blocks = $blockCollection;
      } else {
          $blocks = $blockCollection->where('day_id', $day->id);
      }
      foreach ($blocks as $block) {
          if($block->endTime > $startUTC && $block->startTime < $endUTC) {
              return true;
          }
      }
      return false;
    }

    public function findBlocksByType($blockCollection, $day, $type) {
        $blocks = $blockCollection->where('day_id', $day->id)->where('block_type_id', $type)->sortBy('endTime');
        return $blocks;
    }

    public function findBlocksByTypes($blockCollection, $day, array $types) {
        if ($day == null) {
            $blocks = $blockCollection;
        } else {
            $blocks = $blockCollection->where('day_id', $day->id);
        }
        $blocks = $blocks->whereIn('block_type_id', $types);
        return $blocks->sortBy('endTime');
    }

    public function filterBlocksByType($blockCollection, $day, array $types) {
        if ($day == null) {
            $blocks = $blockCollection;
        } else {
            $blocks = $blockCollection->where('day_id', $day->id);
        }
        foreach ($types as $type) {
            $blocks = $blocks->where('block_type_id', '<>', $type);
        }
        return $blocks->sortBy('endTime');
    }

    public function findBlocksByName($blockCollection, $day, $name) {
        if ($day == null) {
            return $blockCollection->where('name', $name);
        }
        return $blockCollection->where('day_id', $day->id)->where('name', $name)->sortBy('endTime');
    }

    public function findBlocksByPark($blockCollection, $day, Park $park) {
        if ($day == null) {
            return $blockCollection->where('association_id', $park->id);
        }
        return $blockCollection->where('day_id', $day->id)->where('association_id', $park->id)->sortBy('endTime');
    }

    public static function parksInPlanBlocks(&$blockCollection, $day = null) {
      $park_block_types = BlocksUtils::parksInPlanBlockTypeIds();
        if ($day == null) {
            return $blockCollection->whereIn('block_type_id', $park_block_types)->sortBy('endTime');
        }
        return $blockCollection->where('day_id', $day->id)->whereIn('block_type_id', $park_block_types)->sortBy('endTime');
    }

    public static function parksBlocks($blockCollection, $day = null) {
      $park_block_types = BlocksUtils::parksBlockTypeIds();
        if ($day == null) {
            return $blockCollection->whereIn('block_type_id', $park_block_types)->sortBy('endTime');
        }
        return $blockCollection->where('day_id', $day->id)->whereIn('block_type_id', $park_block_types)->sortBy('endTime');
    }
}
