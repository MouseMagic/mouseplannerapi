<?php

namespace App\Verifiers;

use Illuminate\Support\Facades\Log;

use App\Models\Day;
use App\Models\Block;
use App\Models\Resort;
use App\Models\Park;
use App\Utils\DateUtils;
use App\Utils\DisneyData;
use App\Utils\BlocksUtils;

/*
 * ResortVerifier
 * Step 15 of the Schedule Logic document.
 * */
class ResortVerifier extends Verifier {
    private $ConfigTZ;
    private $ResortsTime;
    private $Parks;
    private $UserResortArea;
    private $DisneyData;

    public function __construct() {
        $this->ConfigTZ = DateUtils::getConfigTimezone();
        $this->ResortsTime = 0;
    }

    public function verify(&$blockCollection) {
        if($blockCollection->isEmpty()){
            return 0;
        }
        $this->DisneyData = DisneyData::getInstance();
        $this->UserResortArea = $this->BlocksUtils->userResortArea();
        $this->Parks = Park::all()->keyBy('initials');
        return $this->addResortBlocks($blockCollection) * -1;
    }

    private function addResortBlocks(&$blockCollection) {
        $resort = $this->VacationModel->resort;
        if($this->VacationModel->booked_place_type_id == 1) {
          $resort = new Resort;
        }
        $days = $this->VacationModel->mouse_plan->days;
        $resortType = $this->resortType();
        if ($resortType == 'none') return 0;

        foreach ($days as $day) {
            if (count($this->findBlocksByType($blockCollection, $day, 8)) > 0) continue;
            if($this->BlocksUtils->arrivalDay($day, $this->VacationModel)) continue;
            if($this->BlocksUtils->departureDay($day, $this->VacationModel)) continue;
            if($this->BlocksUtils->skipParkDay($day, $this->VacationModel)) continue;
            if($this->parksInPlanBlocks($blockCollection, $day)->isEmpty()) continue;
            $parksBlocks = $this->parksBlocks($blockCollection, $day);
            $interval = 2;
            // User will need longer breaks on warm months.
            if ($resortType == 'warm') $interval = 3;
            list($start, $end) = $this->resortTimes($parksBlocks, $day, $resortType, $interval);
            if (!$start || !$end) continue;
            //resolve conflicts
            list($startTime, $endTime) = $this->solveConflict(
              $blockCollection,
              $parksBlocks,
              $day,
              $start,
              $end
            );
            $this->ResortsTime += DateUtils::getTimeDifference($startTime, $endTime);
            if($this->VacationModel->booked_place_type_id < 3 && $resort) {
              $block = $this->getResortBlock($day, $resort, $startTime, $endTime);
            } else {
              $block = $this->getCustomBlock($day, $startTime, $endTime);
            }
            $blockCollection->push($block);
        }
        return $this->ResortsTime;
    }

    private function resortType() {
      $resort = $this->VacationModel->booked_place_type_id < 3;
      $type = 'none';
      switch (true) {
        case $this->BlocksUtils->warmMonth():
          $type = 'warm';
          break;
        case $this->VacationModel->nap_type_id == 1:
          $type = 'everyday';
          break;
        case $resort && $this->VacationModel->nap_type_id == 2:
          $type = 'ifPosible';
          break;
        case $resort && $this->VacationModel->visitDays > 6:
          $type = 'longVacation';
          break;
      }
      return $type;
    }

    private function resortTimes(&$parksBlocks, Day $day, $type, $interval) {
      $resortInterval = "PT{$interval}H";
      $start = null;
      $end = null;
      $resort =  false;
      switch ($type) {
        case 'everyday':
        case 'warm':
        if ($parksBlocks->count() < 3) {
          $resort =  true;
        }
          break;
        case 'ifPosible':
        case 'longVacation':
          $not_day_1 = $day->day_number > 1;
          $valid_day_number = $not_day_1 && $day->day_number % 2 == 1;
          if($valid_day_number && $parksBlocks->count() < 3) {
            $resort =  true;
          }
          break;
        default:
          $not_day_1 = $day->day_number > 1;
          $valid_day_number = $not_day_1 && $day->day_number % 2 == 1;
          if ($valid_day_number && $parksBlocks->count() == 1) {
            $resort =  true;
          }
          break;
      }
      if($parksBlocks->count() == 1 && $this->notResortForPark($parksBlocks->first(), $interval)) $resort = false;
      if($resort) {
        $start = $this->bestTime($day, $parksBlocks);
        $end = DateUtils::getFutureTime($start, $resortInterval);
      }
      return array($start, $end);
    }

    private function bestTime($day, $parksBlocks) {
      if($parksBlocks->isEmpty()) {
          $time = $this->BlocksUtils->beginDayTimeTZ($day->date);
          return DateUtils::setDateTime($day->date, $time);
      }
      $firstBlock = $parksBlocks->first();
      $multipleBlocks = $parksBlocks->count() > 1;
      if ($multipleBlocks || $this->notInPlan($firstBlock)) {
        $time = $firstBlock->endTime;
        $time = DateUtils::changeTimezone($time, 'UTC', $this->ConfigTZ);
        return $time;
      }
      $time = $this->BlocksUtils->lunchTime();
      $time = DateUtils::setDateTime($day->date, $time);
      return DateUtils::getFutureTime($time, 'PT90M');
    }

    private function notResortForPark($block, $restortTime) {
      if($this->notInPlan($block)) return false;
      $minimumParkMinutes = 7 * 60;
      $minimumParkMinutes = $minimumParkMinutes + ($restortTime * 60);
      $parkTime = DateUtils::getTimeDifference($block->startTime, $block->endTime);
      return $parkTime < $minimumParkMinutes;
    }

    private function notInPlan($firstBlock) {
      $notInPlanIds = BlocksUtils::parksNotInPlanBlockTypeIds();
      return in_array($firstBlock->block_type_id, $notInPlanIds);
    }

    private function resortConflict($parksBlocks, $day, $time) {
        if(!$parksBlocks || $parksBlocks->isEmpty()) {
            return null;
        }
        return $this->findBlockByTime($parksBlocks, $day, $time, true);
    }

    private function solveConflict(&$blockCollection, &$parksBlocks, Day $day, $startTime, $endTime) {
      switch ($parksBlocks->count()) {
        case 1:
          $block = $this->resortConflict($parksBlocks, $day, $startTime);
          if (!$block) break;
          list($startTime, $endTime) = $this->breakPark($blockCollection,$block,$startTime,$endTime);
          break;
        case 2:
          $downBlock = $parksBlocks->last();
          $conflictCollection = collect([$downBlock]);
          $conflict = $this->findConflicts($conflictCollection, $day, $startTime, $endTime, true);
          if (!$conflict) break;
          $upBlock = $parksBlocks->first();
          list($startTime, $endTime) = $this->solveDownBlock($downBlock, $upBlock, $startTime, $endTime);
          break;
        default:
          $conflict = $this->resortConflict($parksBlocks, $day, $endTime);
          if (!$conflict) break;
          BlocksUtils::addAttributes($conflict, ['startTime' => $endTime]);
          if ($conflict->id) $conflict->save();
          break;
      }
      return array($startTime, $endTime);
    }

    private function solveDownBlock(&$downBlock, &$upBlock, $startTime, $endTime) {
      $downBlockEndTime = DateUtils::changeTimezone($downBlock->endTime, 'UTC', $this->ConfigTZ);
      $remainingTime = DateUtils::getTimeDifference($endTime, $downBlockEndTime);
      if($remainingTime < 180) {
        list($startTime, $endTime) = $this->moveResortUp($startTime, $endTime, $remainingTime);
        BlocksUtils::addAttributes($upBlock, ['endTime' => $startTime]);
      }
      BlocksUtils::addAttributes($downBlock, ['startTime' => $endTime]);
      return array($startTime, $endTime);
    }

    private function moveResortUp($startTime, $endTime, $remainingTime) {
      $decreaseInterval = 180 - $remainingTime;
      $endTime = DateUtils::getPreviousTime($endTime, "PT{$decreaseInterval}M");
      $startTime = DateUtils::getPreviousTime($startTime, "PT{$decreaseInterval}M");
      return array($startTime, $endTime);
    }

    private function breakPark(&$blockCollection, Block &$block, $startTime, $endTime) {
      $blockEndTime = DateUtils::changeTimezone($block->endTime, 'UTC', $this->ConfigTZ);
      $remainingTime = DateUtils::getTimeDifference($endTime, $blockEndTime);
      if($remainingTime < 180) {
        list($startTime, $endTime) = $this->moveResortUp($startTime, $endTime, $remainingTime);
      }
      $afterBlock = $block->replicate();
      BlocksUtils::addAttributes($block, ['endTime' => $startTime]);
      BlocksUtils::addAttributes($afterBlock, ['startTime' => $endTime]);
      $blockCollection->push($afterBlock);
      return array($startTime, $endTime);
    }

    private function getResortBlock(Day $day, Resort $resort, $startTime, $endTime) {
        $resortName = $resort->name;
        if($this->VacationModel->booked_place_type_id == 1) {
          $resortName = 'Resort';
        }
        $attributes = array(
            'day' => $day,
            'name' => $resortName,
            'type' => self::$BLOCK_TYPES['resort'],
            'startTime' => $startTime,
            'endTime' => $endTime,
            'association_id' => $resort->id,
        );
        $block = BlocksUtils::createBlock($attributes);
        Log::debug("New resort break block", [
            'vacation_id' => $this->VacationModel->id,
            'day' => $day->date,
            'start' => $block->startTime,
            'end' => $block->endTime,
            'association_id' => $block->association_id,
        ]);
        return $block;
    }

    private function getCustomBlock(Day $day, $startTime, $endTime) {
        $attributes = array(
            'day' => $day,
            'name' => 'Afternoon Break',
            'type' => self::$BLOCK_TYPES['custom'],
            'startTime' => $startTime,
            'endTime' => $endTime,
        );
        $block = BlocksUtils::createBlock($attributes);
        Log::debug("New custom break block", [
            'vacation_id' => $this->VacationModel->id,
            'day' => $day->date,
            'start' => $block->startTime,
            'end' => $block->endTime,
        ]);
        return $block;
    }

    public function validateInitialData() {
        if(!$this->VacationModel) {
            $this->logCritical("vacationModel");
            return false;
        }
        if(!$this->VacationModel->mouse_plan) {
            $this->logCritical("VacationModel->mouse_plan");
            return false;
        }
        if(!$this->VacationModel->booked_place_type_id) {
            $this->logCritical("VacationModel->booked_place_type_id");
            return false;
        }
        if(!$this->VacationModel->nap_type_id) {
            $this->logCritical("VacationModel->nap_type_id");
            return false;
        }
        if(!$this->VacationModel->pace_type_id) {
            $this->logCritical("VacationModel->pace_type_id");
            return false;
        }
        if(!$this->VacationModel->startTravelDate) {
            $this->logCritical("VacationModel->startTravelDate");
            return false;
        }
        if($this->VacationModel->booked_place_type_id == 2 && !$this->VacationModel->resort) {
            $this->logCritical("VacationModel->resort");
            return false;
        }
        return true;
    }

    public function logCritical($error) {
        Log::critical("Input data is not set for ResortVerifier.", ["missing" => $error]);
    }
}
