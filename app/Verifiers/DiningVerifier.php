<?php

namespace App\Verifiers;

use Illuminate\Support\Facades\Log;

use App\Models\Vacation;
use App\Models\Day;
use App\Models\Block;
use App\Utils\DateUtils;
use App\Utils\BlocksUtils;
use App\Utils\Mousages;

/*
 * DiningVerifier
 * Step 17 of the Schedule Logic document.
 * */
class DiningVerifier extends Verifier {

    public function verify(&$blockCollection) {
        if($blockCollection->isEmpty()){
            return 0;
        }
        $this->addDiningBlocks($blockCollection);
        return 0;
    }

    private function addDiningBlocks(&$blockCollection) {
        $days = $this->VacationModel->mouse_plan->days;
        $dayMeals = array(
          'Breakfast' => $this->BlocksUtils->breakfastTime(),
          'Lunch' => $this->BlocksUtils->lunchTime(),
          'Dinner' => $this->BlocksUtils->dinnerTime()
        );
        $parentBlocksTypesIds = BlocksUtils::parksBlockTypeIds();
        foreach ($days as $day) {
            if ($this->parksBlocks($blockCollection, $day)->isEmpty()) continue;
            foreach ($dayMeals as $name => $defaultTime) {
              if ($name == 'Breakfast') {
                list($start, $end) = $this->breakfastTime(
                  $blockCollection,
                  $day,
                  $defaultTime,
                  $parentBlocksTypesIds
                );
              }
              else {
                list($start, $end) = $this->diningTimes(
                    $blockCollection,
                    $day,
                    $defaultTime,
                    $parentBlocksTypesIds
                );
              }
                if (!$start) continue;
                $block = $this->getDiningBlock($day, $name, $start, $end);
                $blockCollection->push($block);
            }
        }
    }

    private function getDiningBlock(Day $day, $name, $startTime, $endTime) {
        $attributes = array(
            'day' => $day,
            'name' => $name,
            'type' => self::$BLOCK_TYPES['dining'],
            'startTime' =>  $startTime,
            'endTime' => $endTime
        );
        $block = BlocksUtils::createBlock($attributes);
        Log::debug("New dining block", [
            'vacation_id' => $this->VacationModel->id,
            'day' => $day['date'],
            'name' => $name,
            'start' => $block->startTime,
            'end' => $block->endTime,
            'blocktype' => self::$BLOCK_TYPES['dining']
        ]);
        return $block;
    }

    private function diningTimes($blockCollection, Day $day, $time, $parentBlockTypeIds) {
      $initialStartTime = DateUtils::setDateTime($day->date, $time);
      $maxStartTime = DateUtils::getFutureTime($initialStartTime, 'PT2H10M');
      return $this->adjustDiningTimes(
          $blockCollection,
          $day,
          $time,
          $maxStartTime,
          $parentBlockTypeIds
      );
    }

    private function breakfastTime($blockCollection, Day $day, $time, $parentBlockTypeIds) {
      // Schedule breakfast 1 hour before the park opens and after the begin day time
      $firstParkBlock = $this->parksInPlanBlocks($blockCollection, $day)->first();
      if(!$firstParkBlock) return array(null, null);
      $maxStartTime = $this->BlocksUtils->startTimeTZ($firstParkBlock);
      if(DateUtils::getTimeWithFormat($maxStartTime, 'H:i:s') > '09:00:00') return array(null, null);
      list($start, $end) = $this->adjustDiningTimes(
          $blockCollection,
          $day,
          $time,
          $maxStartTime,
          $parentBlockTypeIds
      );
      $end = DateUtils::getPreviousTime($end, 'PT1M');
      return array($start, $end);
    }

    private function adjustDiningTimes($blockCollection, Day $day, $startTime, $maxStartTime, $parentBlockTypeIds) {
      $start = DateUtils::setDateTime($day->date, $startTime);
      $end = DateUtils::getFutureTime($start, 'PT1H');
      $conflictStartBlock = $this->findBlockByTime($blockCollection, $day, $start, true);
      $conflictEndBlock = $this->findBlockByTime($blockCollection, $day, $end, true);
      if($start >= $maxStartTime) return array(null, null);
      while (!$this->validParent($conflictStartBlock, $conflictEndBlock, $parentBlockTypeIds)) {
        $start = DateUtils::getFutureTime($start, 'PT5M');
        $end = DateUtils::getFutureTime($end, 'PT5M');
        $conflictStartBlock = $this->findBlockByTime($blockCollection, $day, $start, true);
        $conflictEndBlock = $this->findBlockByTime($blockCollection, $day, $end, true);
        if($start == $maxStartTime) {
          $start = null;
          $end = null;
          break;
        }
      }
      return array($start, $end);
    }

    private function validParent($conflictStartBlock, $conflictEndBlock, $parentBlockTypeIds) {
      $onValidParent = $conflictStartBlock && in_array($conflictStartBlock->block_type_id, $parentBlockTypeIds);
      $onValidParent = $onValidParent && (!$conflictEndBlock || BlocksUtils::compare($conflictEndBlock, $conflictStartBlock));
      $beforeValidParent = !$conflictStartBlock && $conflictEndBlock && in_array($conflictEndBlock->block_type_id, $parentBlockTypeIds);
      return $beforeValidParent || $onValidParent;
    }

    public function validateInitialData() {
        if(!$this->VacationModel) {
            $this->logCritical("vacationModel");
            return false;
        }
        if(!$this->VacationModel->mouse_plan) {
            $this->logCritical("VacationModel->mouse_plan");
            return false;
        }
        if(!$this->VacationModel->beginDayTime) {
            $this->logCritical("VacationModel->beginDayTime");
            return false;
        }
        return true;
    }

    public function logCritical($error) {
        Log::critical("Input data is not set for DiningVerifier.", ["missing" => $error]);
    }
}
