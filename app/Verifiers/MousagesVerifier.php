<?php

namespace App\Verifiers;

use Illuminate\Support\Facades\Log;

use App\Models\Vacation;
use App\Models\Park;
use App\Models\InterestType;
use App\Models\Day;
use App\Utils\BlocksUtils;
use App\Utils\Mousages;

class MousagesVerifier extends Verifier {
    private $Parks;

    public function verify(&$blockCollection) {
        if($blockCollection->isEmpty()) {
            return 0;
        }
        $this->Parks = Park::all()->keyBy('id');
        $this->setMousages($blockCollection);
        return 0;
    }

    private function setMousages($blockCollection) {
      $resortArea = $this->BlocksUtils->userResortArea();
      $resortPark = $this->Parks->where('initials', $resortArea)->first();
      $interests = $this->VacationModel->interest_types->keyBy('name');
      $characters = $this->VacationModel->characters->keyBy('name');
      if($this->setMissingResortPark($blockCollection, $resortPark)) {
        Mousages::setMissingResortParkMousage($this->VacationModel, $resortPark->name);
      }
      if($this->setDisneySpa($interests)) {
        Mousages::setDisneySpaMousage($this->VacationModel);
      }
      if($this->setDisneyGolf($interests)) {
        Mousages::setDisneyGolfMousage($this->VacationModel);
      }
      if($this->setPirateLeague($characters)) {
        Mousages::setPirateLeagueMousage($this->VacationModel);
      }
      if($this->setPrincessMakeover($characters)) {
        Mousages::setPrincessMakeoverMousage($this->VacationModel);
      }
      if($this->setWildernessXplorer($interests)) {
        Mousages::setWildernessXplorerMousage($this->VacationModel);
      }
      if($this->setParkHopper2()) {
        Mousages::setParkHopper2Mousage($this->VacationModel);
      }
      if($this->setParkHopper3()) {
        Mousages::setParkHopper3Mousage($this->VacationModel);
      }
    }

    private function setMissingResortPark($blockCollection, $resortPark) {
      if(!$resortPark) return false;
      return $this->findBlocksByPark($blockCollection, null, $resortPark)->isEmpty();
    }

    private function setDisneySpa($interests) {
      $relaxation = $interests->get('Relaxation') != null;
      return $relaxation && $this->VacationModel->budget_type_id == 3;
    }

    private function setDisneyGolf($interests) {
      $golf = $interests->get('Golf') != null;
      return $golf;
    }

    private function setPirateLeague($characters) {
      $pirates = $characters->get('Pirates') != null;
      return $pirates;
    }

    private function setPrincessMakeover($characters) {
      $princesses = $characters->get('Princesses') != null;
      return $princesses;
    }

    private function setWildernessXplorer($interests) {
      $wilderness = $interests->get('Animal Encounters') != null;
      $wilderness = $wilderness && $interests->get('Educational') != null;
      return $wilderness;
    }

    private function setParkHopper2() {
      return $this->ParksPerDay > 1 && $this->VacationModel->booked_place_type_id < 3;
    }

    private function setParkHopper3() {
      return $this->ParksPerDay > 1 && $this->VacationModel->booked_place_type_id == 3;
    }

    public function validateInitialData() {
        if(!$this->VacationModel->mouse_plan) {
            $this->logCritical("VacationModel->mouse_plan");
            return false;
        }
        return true;
    }

    public function logCritical($error) {
        Log::critical("Input data is not set for MousagesVerifier.", ["missing" => $error]);
    }
}
