<?php

namespace App\Verifiers;

use Illuminate\Support\Facades\Log;

use App\Models\Vacation;
use App\Models\Block;
use App\Models\Day;
use App\Utils\DateUtils;
use App\Utils\BlocksUtils;

/*
 * TravelVerifier
 * Step 14 of the Schedule Logic document.
 * */
class TravelVerifier extends Verifier {

    public function verify(&$blockCollection) {
        if($blockCollection->isEmpty()){
            return 0;
        }
        $this->addTravelBlocks($blockCollection);
        return 0;
    }

    private function addTravelBlocks(&$blockCollection) {
        $mouseplan = $this->VacationModel->mouse_plan;
        $days = $mouseplan->days;
        $this->addArrivalBlock($days->where('day_number', 1)->first(), $blockCollection);
        $this->addDepartureBlock($days->last(), $blockCollection);
    }

    private function addArrivalBlock(Day $day, &$blockCollection) {
        if (count($this->findBlocksByType($blockCollection, $day, 11)) > 0) return;
        $arrivalTime = $this->BlocksUtils->arrivalTimeTZ();
        $arrivalTime = DateUtils::setDateTime($day->date, $arrivalTime);
        $conflictBlock = $this->findBlockByTime($blockCollection, $day, $arrivalTime, true);
        if ($conflictBlock != null) {
            BlocksUtils::addAttributes($conflictBlock, ['startTime' => $arrivalTime]);
        }
        $block = $this->getTravelBlock(
            $day,
            'Travel & Arrival',
            self::$BLOCK_TYPES['arrival'],
            DateUtils::setDateTime($day->date, '00:00:00'),
            $arrivalTime
        );
        $blockCollection->push($block);
        Log::debug("New arrival block", [
            'vacation_id' => $this->VacationModel->id,
            'day' => $day->date,
            'time' => $block->endTime
        ]);
    }

    private function addDepartureBlock(Day $day, &$blockCollection) {
        if (count($this->findBlocksByType($blockCollection, $day, 12)) > 0) return;
        $mouseplan = $this->VacationModel->mouse_plan;
        $endTravelDate = $this->VacationModel->endTravelDate;
        if (DateUtils::getDaysDifference($day->date, $endTravelDate) != 0) {
            $day = Day::updateOrCreate(
                ['mouse_plan_id' => $mouseplan->id, 'day_number' => $day->day_number+1],
                ['date' => $endTravelDate]
            );
            $mouseplan->load('days');
        }
        $departureTime = $this->BlocksUtils->departureTimeTZ();
        $departureTime = DateUtils::setDateTime($day->date, $departureTime);
        $conflictBlock = $this->findBlockByTime($blockCollection, $day, $departureTime, true);
        if ($conflictBlock != null) {
            BlocksUtils::addAttributes($conflictBlock, ['endTime' => $departureTime]);
        }
        $block = $this->getTravelBlock(
            $day,
            'Departure & Travel',
            self::$BLOCK_TYPES['departure'],
            $departureTime,
            DateUtils::setDateTime($day->date, '23:59:59')
        );
        $blockCollection->push($block);
        Log::debug("New departure block", [
            'vacation_id' => $this->VacationModel->id,
            'day' => $day->date,
            'time' => $block->startTime
        ]);
    }

    private function getTravelBlock(Day $day, $name, $type, $startTime, $endTime) {
        $attributes = array(
            'day' => $day,
            'name' => $name,
            'type' => $type,
            'startTime' => $startTime,
            'endTime' => $endTime
        );
        $block = BlocksUtils::createBlock($attributes);
        return $block;
    }

    public function validateInitialData() {
        if(!$this->VacationModel) {
            $this->logCritical("vacationModel");
            return false;
        }
        if(!$this->VacationModel->mouse_plan) {
            $this->logCritical("VacationModel->mouse_plan");
            return false;
        }
        if(!$this->VacationModel->mouse_plan->days) {
            $this->logCritical("VacationModel->mouse_plan->days");
            return false;
        }
        if(!$this->VacationModel->endTravelDate) {
            $this->logCritical("VacationModel->endTravelDate");
            return false;
        }
        if(!$this->VacationModel->arrivalDate) {
            $this->logCritical("VacationModel->arrivalDate");
            return false;
        }
        if(!$this->VacationModel->departureDate) {
            $this->logCritical("VacationModel->departureDate");
            return false;
        }
        return true;
    }

    public function logCritical($error) {
        Log::critical("Input data is not set for TravelVerifier.", ["missing" => $error]);
    }
}
