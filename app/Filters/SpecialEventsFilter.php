<?php

namespace App\Filters;

use Illuminate\Support\Facades\Log;
use \Illuminate\Database\Eloquent\Collection;

use App\Utils\DateUtils;
use App\Utils\DisneyData;
use App\Utils\Mousages;

/*
 * SpecialEventsFilter
 * Step 10 of the Schedule Logic document.
 * */
class SpecialEventsFilter extends Filter {

    private $Disney;
    private $Mousages;

    protected function init() {
      $this->Disney = DisneyData::getInstance();
      $this->Mousages = [
        'candlelight' => false,
        'halloween' => false,
        'christmas' => false,
        'food' => false,
        'flower' => false,
      ];
    }

    protected  function exclude(array $args) {
      $associatedPark = $args['associatedPark']->initials;
      $date = DateUtils::getTimeWithFormat($args['day']->date, 'Y-m-d');
      $allsp = $this->Disney->getSpecialEventsDisneyData($associatedPark, $date);
      // Example of event object
      // [{"park":"MK","name":"Mickey's Not-So-Scary Halloween Party","type":"Special Ticketed Event","date":"2017-11-01","startTime":"19:00:00","endTime":"00:00:00"}]
      if($allsp->isEmpty()) return false;
      $allnames = $allsp->pluck('name');
      foreach ($allnames as $name) {
        $marathon = 'Marathon';
        $pos = strpos($name, $marathon);
        $this->setSpecialEventMousages($name);
        if ($pos !== false) {
            return true;
        }
      }
      return false;
    }

    private function setSpecialEventMousages($name) {
      // If one of these events are happening within the given day, set one mousage. Only one per event.
      if(!$this->Mousages['candlelight'] && strpos($name, 'Candlelight Processional') !== false) {
        Mousages::setCandlelightSEMousage($this->VacationModel);
        $this->Mousages['candlelight'] = true;
      }
      if(!$this->Mousages['halloween'] && strpos($name, 'Not-So-Scary Halloween Party') !== false) {
        Mousages::setPurchaseSEMousage($this->VacationModel, $name);
        $this->Mousages['halloween'] = true;
      }
      if(!$this->Mousages['christmas'] && strpos($name, 'Very Merry Christmas') !== false) {
        Mousages::setPurchaseSEMousage($this->VacationModel, $name);
        $this->Mousages['christmas'] = true;
      }
      if(!$this->Mousages['food'] && strpos($name, 'Food & Wine') !== false) {
        Mousages::setFoodSEMousage($this->VacationModel);
        $this->Mousages['food'] = true;
      }
      if(!$this->Mousages['flower'] && strpos($name, 'Flower & Garden') !== false) {
        Mousages::setFlowerSEMousage($this->VacationModel);
        $this->Mousages['flower'] = true;
      }
    }

    protected function mustDo(array $args) {
      return false;
    }

    protected function parkAttributes(array $args) {}

    protected function updateDaysAtPlan(array $args) {
      return [$args['daysAtPlan'], $args['dayTime']];
    }

    public function validateInitialData() {
        if(!$this->VacationModel) {
            $this->logCritical("vacationModel");
            return false;
        }
        if(!$this->ScorePark) {
            $this->logCritical("ScorePark");
            return false;
        }
        return true;
    }
}
