<?php

namespace App\Filters;

use Illuminate\Support\Facades\Log;
use \Illuminate\Database\Eloquent\Collection;

use App\Models\Day;
use App\Utils\DateUtils;
use App\Utils\Mousages;

/*
 * ExtraMagicHoursFilter
 * Step 9 of the Schedule Logic document.
 * */
class ExtraMagicHoursFilter extends Filter {
    private $EmhCollection;

    protected function init() {
      $this->EmhCollection = new Collection;
    }

    protected function exclude(array $args) {
      $parkInfo = $args['parkInfo'];
      // There are extra magic hours for this park but they are not valid
      if($parkInfo['emh_start'] && !$parkInfo['valid_emh']) {
        Log::debug('There are EMH for this park but they are not valid.', [
          'vacation_id' => $this->VacationModel->id,
          'park' => $parkInfo['park'],
          'emh_start' => $parkInfo['emh_start'],
          'valid_emh' => $parkInfo['valid_emh'],
        ]);
        return true;
      }
      return false;
    }

    protected function mustDo(array $args) {
      $mustDoPark = $this->mustDoPark(
                              $args['day'],
                              $args['availableParks'],
                              $args['dayInfo'],
                              $args['daysAtPlan']
                            );
      if($mustDoPark) {
        $associatedPark = $args['associatedPark'];
        if($associatedPark->initials == $mustDoPark) {
          $this->sendMousages(
            $args['day'],
            $associatedPark->initials,
            $args['parkInfo']['emh_start']
          );
          return true;
        }
      }
      return false;
    }

    private function sendMousages(Day $day, $parkName, $emhTime) {
      $ep = $parkName == 'EP';
      $epWeekend = $ep && DateUtils::weekend($day->date);
      $epWeekendNight = $epWeekend && DateUtils::getTimeWithFormat($emhTime, 'a') == 'pm';
      if($epWeekendNight) {
        Mousages::setEpcot2Mousage($this->VacationModel, $day);
      }
    }

    private function mustDoPark(Day $day, $availableParks, $dayInfo, $daysAtPlan) {
      $dayEmh = $this->EmhCollection->where('day_number', $day->day_number);
      if(!$dayEmh || $dayEmh->isEmpty()) {
        return $this->buildEmhDay($day, $availableParks, $dayInfo, $daysAtPlan);
      }
      return $dayEmh->first()['park'];
    }

    private function buildEmhDay(Day $day, $availableParks, $dayInfo, $daysAtPlan) {
      $park = $this->bestEmhPark($availableParks, $dayInfo, $daysAtPlan);
      $this->EmhCollection->push([
        'day_number' => $day->day_number,
        'park' => $park,
      ]);
      return $park;
    }

    private function bestEmhPark($parkCollection, $dayInfo, $daysAtPlan) {
      $emhs = array();
      foreach ($parkCollection as $parkBlock) {
        $associatedPark = $this->associatedPark($parkBlock);
        $parkInfo = $dayInfo->where('park', $associatedPark)->first();
        // There are extra magic hours for this park and it's valid.
        $emhs[$associatedPark] = $parkInfo['emh_start'] && $parkInfo['valid_emh'];
      }
      $emhs = array_filter($emhs);
      if (empty($emhs)) {
        // No EMH for this day.
        return null;
      }
      else if (count($emhs) == 1) {
        // Just 1 EMH for this day, return that park.
        Log::debug('Only 1 EMH available.', ['vacation_id' => $this->VacationModel->id, 'emh'=>key($emhs)]);
        return key($emhs);
      }
      else {
        Log::debug('More than 1 EMH available.', ['vacation_id' => $this->VacationModel->id, 'emhs'=>$emhs]);
        // More than 2 EMHs for this day, untie with the crowd levels.
        $parks = $this->lowestLevelParks($emhs, $dayInfo);
        if (count($parks) == 1) {
          // Return the 1 park with the lowest crowd level from the parks with EMHs.
          Log::debug('Only 1 EMH available with lowest crowd level.', ['vacation_id' => $this->VacationModel->id, 'emhs'=>key($parks)]);
          return key($parks);
        }
        Log::debug('More than 2 EMHs available with lowest crowd level.', ['vacation_id' => $this->VacationModel->id, 'emhs'=>$parks]);
        // More than 2 EMHs with the same low crowd level for this day, untie with the score.
        $highest = $this->highestScoredPark($parks, $daysAtPlan);
        Log::debug('EMH avalable with low crowd level, and highest score.', ['vacation_id' => $this->VacationModel->id, 'emh'=>$highest]);
        return $highest;
      }
    }

    private function lowestLevelParks($parkArray, $dayInfo) {
      $crowdLevel = array();
      foreach ($parkArray as $park => $emh) {
        $parkInfo = $dayInfo->where('park', $park)->first();
        $crowdLevel[$park] = $parkInfo['crowd_level'];
      }
      asort($crowdLevel);
      // Lowest crowd level.
      $lowestLevel = current($crowdLevel);
      // Collect all the parks with the lowest crowd level.
      $lowestLevelParks = array_filter($crowdLevel, function($v) use($lowestLevel) {
        return $v == $lowestLevel;
      });
      return $lowestLevelParks;
    }

    private function associatedPark($parkBlock) {
      return $this->Parks
                  ->where('id', $parkBlock->association_id)
                  ->first()
                  ->initials;
    }

    private function highestScoredPark($parkArray, $daysAtPlan) {
      $parkTimes = array();
      foreach ($parkArray as $park => $crowdLevel) {
        $parkTimes[$park] = $daysAtPlan[$park];
      }
      arsort($parkTimes);
      return key($parkTimes);
    }

    protected function parkAttributes(array $args) {
      $parkNumber = $this->parkNumber($args['parkInfo']);
      return [
        'park_number' => $parkNumber,
      ];
    }

    private function parkNumber($parkInfo) {
      $am = DateUtils::getTimeWithFormat($parkInfo['emh_start'], 'a') == 'am';
      if($am) return 1;
      return $this->ParksPerDay;
    }

    protected function updateDaysAtPlan(array $args) {
      $park = $args['associatedPark']->initials;
      $daysAtPlan = $args['daysAtPlan'];
      $parkTime = $daysAtPlan[$park];
      $emhTime = 0.25;
      $totalTime = $parkTime - $emhTime;
      if($totalTime < 0) $totalTime = 0;
      $daysAtPlan[$park] = $totalTime;
      $dayTime = $args['dayTime'] + $emhTime;
      return [$daysAtPlan, $dayTime];
    }

    public function validateInitialData() {
        if(!$this->VacationModel) {
            $this->logCritical("vacationModel");
            return false;
        }
        if(!$this->VacationModel->booked_place_type_id) {
            $this->logCritical("VacationModel->booked_place_type_id");
            return false;
        }
        if(!$this->VacationModel->startTravelDate) {
            $this->logCritical("VacationModel->startTravelDate");
            return false;
        }
        if(!$this->VacationModel->endTravelDate) {
            $this->logCritical("VacationModel->endTravelDate");
            return false;
        }
        if(!$this->VacationModel->visitDays) {
            $this->logCritical("VacationModel->visitDays");
            return false;
        }
        if(!$this->VacationModel->pace_type_id ) {
            $this->logCritical("VacationModel->pace_type_id ");
            return false;
        }
        if($this->VacationModel->booked_place_type_id == 2 && !$this->VacationModel->resort_id) {
            $this->logCritical("VacationModel->resort_id");
            return false;
        }
        if(!$this->ScorePark) {
            $this->logCritical("ScorePark");
            return false;
        }
        return true;
    }
}
