<?php

namespace App\Filters;

use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Collection;
use App\Utils\BlocksUtils;
use App\Utils\DateUtils;
use App\Models\Day;
use App\Models\Block;
use App\Models\Park;
use App\Repositories\BlockRepository;

abstract class Filter {

    protected $ParkInfo;
    protected $ParksPerDay;
    protected $ScorePark;
    protected $VacationModel;
    protected $BlockTypes;
    protected $Parks;
    protected $BlocksUtils;

    public function __construct(){
      $this->BlockTypes = BlockRepository::getAllParkTypeBlocks()->mapWithKeys(function ($item) {
          return [$item['subtype'] => $item['id']];
      });
      $this->Parks = Park::all()->keyBy('initials');
    }

    public function filter($availableParksCollection, $mouseplanCollection, $daysAtPlan) {
      $this->init();
      if(!$availableParksCollection || $availableParksCollection->isEmpty()) {
          $availableParksCollection = $this->buildAvailableParksCollection();
      }
      if(!$mouseplanCollection) {
        $mouseplanCollection = new Collection;
      }
      if(!$daysAtPlan) {
        $daysAtPlan = $this->getDaysAtPlan();
      }
      list($availableParksCollection, $mouseplanCollection, $daysAtPlan) =
        $this->applyRules($availableParksCollection, $mouseplanCollection, $daysAtPlan);
      return array($availableParksCollection, $mouseplanCollection, $daysAtPlan);
    }

    protected function buildAvailableParksCollection() {
        $blockCollection = new Collection;
        $days = $this->VacationModel->mouse_plan->days;
        $parkDays = 1;
        $waterpark = $this->BlocksUtils->isWaterPark();
        if($waterpark) {
          $parkDays = 0;
        }
        foreach ($days as $day) {
          if($parkDays > $this->VacationModel->visitDays) break;
          if($this->BlocksUtils->skipParkDay($day)) continue;
          foreach ($this->ScorePark as $park => $score) {
            $block = $this->createParkBlock($day, $park);
            $blockCollection->push($block);
          }
          if($waterpark) {
            $block = $this->createParkBlock($day, $this->getWaterPark($day));
            $blockCollection->push($block);
          }
          $parkDays = $parkDays + 1;
        }
        return $blockCollection;
    }

    private function getDaysAtPlan() {
        $totalDays = $this->VacationModel->visitDays;
        $scoreSum = array_sum($this->ScorePark);
        $daysAtPlan = [];

        foreach ($this->ScorePark as $park => $score) {
          $time = round(($score/$scoreSum)*$totalDays, 2);
          // Parks with time below 0 would only be scheduled when others are unavailable.
          $daysAtPlan[$park] = $time;
          // Always add Magic Kingdom.
          if (!isset($daysAtPlan['MK']) || $daysAtPlan['MK'] < 0.2) {
            $daysAtPlan['MK'] = 0.2;
          }
        }
        return $daysAtPlan;
    }

    protected function applyRules($availableParksCollection, $mouseplanCollection, $daysAtPlan) {
      $days = $this->VacationModel->mouse_plan->days;
      foreach ($days as $day) {
        $date = DateUtils::getTimeWithFormat($day->date, 'Y-m-d');
        Log::debug('Date', ['vacation_id' => $this->VacationModel->id, 'date'=>$date]);
        $availableParks = $this->findBlocksByDay($availableParksCollection, $day);
        if($availableParks->isEmpty()) {
          Log::debug('No available parks this day', ['vacation_id' => $this->VacationModel->id]);
          continue;
        }
        $dayInfo = $this->ParkInfo->where('date', $date);
        // exclude the following parks on this day
        $availableParksCollection = $this->excludeParks($availableParksCollection, $daysAtPlan, $day, $dayInfo);
        // schedule the following parks on this day
        list($availableParksCollection, $mouseplanCollection, $daysAtPlan) =
          $this->scheduleParks($availableParksCollection, $mouseplanCollection, $daysAtPlan, $day, $dayInfo);
      }
      return array($availableParksCollection, $mouseplanCollection, $daysAtPlan);
    }

    private function excludeParks($availableParksCollection, $daysAtPlan, $day, $dayInfo) {
      $availableParks = $this->findBlocksByDay($availableParksCollection, $day);
      foreach ($availableParks as $park) {
        $associatedPark = $this->Parks->where('id', $park->association_id)->first();
        $parkInfo = $dayInfo->where('park', $associatedPark->initials)->first();
        $args = [
          'day' => $day,
          'associatedPark' => $associatedPark,
          'daysAtPlan' => $daysAtPlan,
          'availableParks' => $availableParks,
          'parkInfo' => $parkInfo,
        ];
        // Remove the park from this day, but only if it won't be empty.
        if($this->exclude($args) && $availableParksCollection->count() > 2) {
          $availableParksCollection = $this->removePark($availableParksCollection, $park);
        }
      }
      return $availableParksCollection;
    }

    private function scheduleParks($availableParksCollection, $mouseplanCollection, $daysAtPlan, $day, $dayInfo) {
      $availableParks = $this->findBlocksByDay($availableParksCollection, $day);
      $mouseplanParks = $this->findBlocksByDay($mouseplanCollection, $day);
      $parkNumber = 1;
      $dayTime = $this->getDayTime($mouseplanParks, $day);
      $maxDayTime = 0.75;
      $dayParks = $mouseplanParks->count();
      foreach ($availableParks as $park) {
        if($dayTime > $maxDayTime || $dayParks >= $this->ParksPerDay) break;
        $associatedPark = $this->Parks->where('id', $park->association_id)->first();
        $parkInfo = $dayInfo->where('park', $associatedPark->initials)->first();
        $args = [
          'day' => $day,
          'associatedPark' => $associatedPark,
          'daysAtPlan' => $daysAtPlan,
          'parkNumber' => $parkNumber,
          'mouseplanParks' => $mouseplanParks,
          'availableParks' => $availableParks,
          'mouseplanCollection' => $mouseplanCollection,
          'dayInfo' => $dayInfo,
          'parkInfo' => $parkInfo,
          'dayTime' => $dayTime,
        ];

        if($this->mustDo($args)) {
          list($daysAtPlan, $dayTime) = $this->updateDaysAtPlan($args);
          $attributesToUpdate = $this->parkAttributes($args);
          BlocksUtils::addAttributes($park, $attributesToUpdate);
          $mouseplanCollection->push($park);
          $mouseplanParks = $this->findBlocksByDay($mouseplanCollection, $day);
          $wp = in_array($associatedPark->initials, ['BB', 'TL']);
          if($wp) {
            $availableParksCollection = $this->removeAllParks($availableParksCollection, $day);
          } else {
            $availableParksCollection = $this->removePark($availableParksCollection, $park);
          }
          $dayParks++;
          Log::debug("New park block",
              ['vacation_id' => $this->VacationModel->id,
               'park' => $park->name,
               'day' => $day->date,
               'block_type' => $park->block_type_id,
               'association_id' => $park->association_id,
               'park_number' => $park->park_number,
           ]);
        }
      }
      return array($availableParksCollection, $mouseplanCollection, $daysAtPlan);
    }

    protected function getDayTime($mouseplanParks, Day $day) {
        $arrival = 0;
        $departure = 0;
        if($this->BlocksUtils->arrivalDay($day)) {
          $arrivalTimeTZ = $this->BlocksUtils->arrivalTimeTZ();
          $parksOpenHour = '09:00:00';
          $arrivalBlockTime = DateUtils::getTimeDifference($parksOpenHour, $arrivalTimeTZ);
          if($arrivalBlockTime > 0) {
            // get portion of time out of 12hrs
            $arrivalBlockTime = $arrivalBlockTime / 60;
            $arrival = round($arrivalBlockTime / 12, 2);
          }
        }
        if($this->BlocksUtils->departureDay($day)) {
          $departureTimeTZ = $this->BlocksUtils->departureTimeTZ();
          $parksCloseHour = '21:00:00';
          $departureBlockTime = DateUtils::getTimeDifference($departureTimeTZ, $parksCloseHour);
          if($departureBlockTime > 0) {
            // get portion of time out of 12hrs
            $departureBlockTime = $departureBlockTime / 60;
            $departure = round($departureBlockTime / 12, 2);
          }
        }
        $blocksCount = $mouseplanParks->count();
        $parksTime = round($blocksCount / $this->ParksPerDay, 2);
        return $parksTime + $arrival + $departure;
    }

    protected function removePark($availableParksCollection, Block $park) {
      return $availableParksCollection->reject(function ($item) use($park) {
        return $item->day_id == $park->day_id && $item->name == $park->name;
      });
    }

    protected function removeAllParks($availableParksCollection, Day $day) {
      return $availableParksCollection->reject(function ($item) use($day) {
        return $item->day_id == $day->id;
      });
    }

    protected function createParkBlock(Day $day, $parkInitials) {
      $type = $this->BlockTypes->get($parkInitials);
      $associatedPark = $this->Parks->get($parkInitials);
      return BlocksUtils::createBlock([
        'day'=> $day,
        'type'=> $type,
        'name'=> $associatedPark->name,
        'association_id' => $associatedPark->id,
      ]);
    }

    protected function findBlocksByDay($blockCollection, Day $day) {
      return $blockCollection->filter(function ($block) use($day) {
        return $block->day_id == $day->id;
      });
    }

    private function getWaterPark(Day $day) {
        //TODO write rules to decide which water park to visit in a given day
        return 'BB';
    }

    public function setParkInfo($parkInfo) {
        $this->ParkInfo = $parkInfo;
    }

    public function setParksPerDay($parksPerDay) {
        $this->ParksPerDay = $parksPerDay;
    }

    public function setScoreParks($scoreParks) {
        $this->ScorePark = $scoreParks;
    }

    public function setVacationModel($vacation) {
        $this->VacationModel = $vacation;
        $this->BlocksUtils = BlocksUtils::getInstance($vacation);
    }

    public function logCritical($error) {
        Log::critical("Input data is not set for ".get_class($this),
          ["vacation_id" => $this->VacationModel->id, "missing" => $error]);
    }

    abstract protected function init();

    abstract protected function exclude(array $args);

    abstract protected function mustDo(array $args);

    abstract protected function updateDaysAtPlan(array $args);

    abstract protected function parkAttributes(array $args);

    abstract protected function validateInitialData();
}
