<?php

namespace App\Filters;

use App\Utils\DateUtils;
use Illuminate\Support\Facades\Log;
use \Illuminate\Database\Eloquent\Collection;

use App\Models\Day;

/*
 * TimeFilter
 * Step 10.5? of the Schedule Logic document.
 * */
class TimeFilter extends Filter {
  
  private $ClosingTimesCollection;
  private $EveningParksCollection;

  protected function init() {
    $this->ClosingTimesCollection = new Collection;
    $this->EveningParksCollection = new Collection;
  }

  /**
   * Exclude the park that was scheduled the prior day if:
   * 1. There are 2 or more parks.
   * 2. It was scheduled by this filter.
   *
   * @param array $args
   * @return bool
   */
  protected function exclude(array $args) {
    if($args['availableParks']->count() < 2) return FALSE;
    $day = $args['day'];
    if($this->ParksPerDay == 1) {
       // 2 days before
       $priorDayPark = $this->EveningParksCollection->where('day_number', $day->day_number - 2)
       ->first();
      if(!empty($priorDayPark)){
        if($priorDayPark['initials'] == $args['associatedPark']->initials)  return TRUE;  
      }
    }
    $priorDayPark = $this->EveningParksCollection->where('day_number', $day->day_number - 1) 
    ->first();
    if(!empty($priorDayPark)) {
      if($priorDayPark['initials'] == $args['associatedPark']->initials)    return TRUE;
    }
      return FALSE;
  }

  /**
   * Schedule the park that closes the latest if:
   * 1. Is before user end time.
   * 2. There is only one at the same end time.
   * 3. It is available.
   *
   * @param array $args
   * @return bool
   */
  protected function mustDo(array $args) {
    // Is there space in the evening?
    $parksThisDay = $args['mouseplanParks'];
    $lastPark = $parksThisDay->where('park_number', $this->ParksPerDay);
    if (!$lastPark->isEmpty()) return FALSE;
    
    $availableParksInfo = $this->availableParks($args['availableParks'], $args['dayInfo']);
    $parkInitials = $args['associatedPark']->initials;
    $day = $args['day'];
    $latest = $this->latest($args['day'], $availableParksInfo);
    
    if($parkInitials == $latest) {
      $eveningPark = [
        'day_number' => $day->day_number,
        'initials' => $parkInitials,
      ];
      $this->EveningParksCollection->push($eveningPark);
      return TRUE;
    }
    return FALSE;
  }

  private function availableParks($parksThisDay, $dayInfo) {
    $availableParksInitials = [];
    foreach ($parksThisDay as $parkBlock) {
      $associatedPark = $this->associatedPark($parkBlock);
      $parkInfo = $dayInfo->where('park', $associatedPark)->first();
      $availableParksInitials[$associatedPark] = $parkInfo['close_hour'];
    }
    arsort($availableParksInitials);
    return $availableParksInitials;
  }

  private function associatedPark($parkBlock) {
    return $this->Parks
      ->where('id', $parkBlock->association_id)
      ->first()
      ->initials;
  }

  /**
   * Get the park that closes the latest on a given day.
   *
   * @param Day $day
   * @param $availableParksInfo
   * @return string
   */
  protected function latest(Day $day, $availableParksInfo) {
    $dayClosingTimes = $this->ClosingTimesCollection
      ->where('day_number', $day->day_number)
      ->first();
    if(empty($dayClosingTimes)) {
      $dayClosingTimes = $this->buildDayClosingTimes($day, $availableParksInfo);
    }
    return $dayClosingTimes['latest'];
  }

  /**
   * Build collection with closing times in a given day.
   *
   * @param Day $day
   * @param array $availableParksInfo
   * @return array
   */
  private function buildDayClosingTimes(Day $day, array $availableParksInfo) {
    Log::debug("availableParksInfo", $availableParksInfo);
    $closingTimesCount = array_count_values($availableParksInfo);
    // Set latest closing time of the day.
    $latest = '';
    $latestTime = current($availableParksInfo);
    $isUniquePark = $closingTimesCount[$latestTime] == 1;
    $flexible_type = $this->VacationModel->time_flexibility_type_id;
    $isLateTimeFlex = $flexible_type == 2 || $flexible_type == 4;
    $beforeEndDayTime = DateUtils::getTimeDifference($latestTime, $this->VacationModel->endDayTime) <= 0;
    // If there is only one park with the same late hour.
    // And it's before or at the end day time.
    // Or if it's after end day time, the user is flex in the evenings.
    if ($isUniquePark && ($beforeEndDayTime || $isLateTimeFlex)) {
      $latest = key($availableParksInfo);
    }
    // Set earliest closing time of the day.
    $earliest = '';
    $earliestTime = end($availableParksInfo);
    $isUniquePark = $closingTimesCount[$earliestTime] == 1;
    $beforeEndDayTime = DateUtils::getTimeDifference($earliestTime, $this->VacationModel->endDayTime) < 0;
    // If there is only one park with the same early hour.
    // And it's before the end day time.
    // Or if it's after end day time, the user is flex in the evenings.
    if ($isUniquePark && ($beforeEndDayTime || $isLateTimeFlex)) {
      $earliest = key($availableParksInfo);
    }
    $dayClosingTimes = [
      'day_number' => $day->day_number,
      'latest' => $latest,
      'earliest' => $earliest,
    ];
    $this->ClosingTimesCollection->push($dayClosingTimes);
    return $dayClosingTimes;
  }

  protected function updateDaysAtPlan(array $args) {
    $park = $args['associatedPark']->initials;
    $daysAtPlan = $args['daysAtPlan'];
    $parkTime = $daysAtPlan[$park];
    $maxDayTime = 1;
    $halfDay = 0.5;
    $dayTime = $args['dayTime'];
    switch (TRUE) {
      case $parkTime < $maxDayTime && $dayTime == 0:
        $daysAtPlan[$park] = 0;
        $dayTime = $dayTime + $parkTime;
        break;
      case $parkTime < $maxDayTime && ($dayTime + $parkTime) <= 1:
        $daysAtPlan[$park] = 0;
        $dayTime = $dayTime + $parkTime;
        break;
      case $parkTime < $maxDayTime:
        $timeToCompleteDay = 1 - $dayTime;
        $daysAtPlan[$park] = $parkTime - $timeToCompleteDay;
        $dayTime = $maxDayTime;
        break;
      case $parkTime >= $maxDayTime && $dayTime == 0:
        $dayTime = $maxDayTime;
        $daysAtPlan[$park] = $parkTime - $maxDayTime;
        break;
      case $parkTime >= $maxDayTime && $dayTime > $halfDay:
        $dayTime = round($dayTime + $halfDay);
        $daysAtPlan[$park] = round($parkTime - $halfDay);
        break;
      default:
        $daysAtPlan[$park] = $parkTime - $dayTime;
        $dayTime = $maxDayTime;
        break;
    }
    return [$daysAtPlan, $dayTime];
  }

  protected function parkAttributes(array $args) {
    return [
      'park_number' => $this->ParksPerDay,
    ];
  }

  public function validateInitialData() {
  if(!$this->VacationModel) {
    $this->logCritical("vacationModel");
    return FALSE;
  }
  if(!$this->VacationModel->beginDayTime) {
    $this->logCritical("VacationModel->beginDayTime");
    return FALSE;
  }
  if(!$this->VacationModel->endDayTime) {
    $this->logCritical("VacationModel->endDayTime");
    return FALSE;
  }
  return TRUE;
  }

}
