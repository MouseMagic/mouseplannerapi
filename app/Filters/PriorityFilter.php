<?php

namespace App\Filters;

use Illuminate\Support\Facades\Log;
use \Illuminate\Database\Eloquent\Collection;

use App\Models\Day;
use App\Utils\DateUtils;

/*
 * SpecialEventsFilter
 * Step 11 of the Schedule Logic document.
 * */
class PriorityFilter extends Filter {

    // private static $MinimumDayTime = 0.09; // 1hour out of 12
    private $CrowdLevelCollection;

    private static $PreferenceCommute;

    protected function init() {
       $this->CrowdLevelCollection = new Collection;
       // Same-day park combination preferences.
       self::$PreferenceCommute = collect([
         'MK' => ['EP', 'HS', 'AK'],
         'AK' => ['MK', 'HS', 'EP'],
         'EP' => ['MK', 'HS', 'AK'],
         'HS' => ['EP', 'MK', 'AK'],
       ]);
    }

    protected function exclude(array $args) {
      $daysAtPlan = $args['daysAtPlan'];
      $associatedPark = $args['associatedPark'];
      if($daysAtPlan[$associatedPark->initials] < 0) {
        return true;
      }
      return false;
    }

    protected function mustDo(array $args) {
      // get list of available parks with less crowd level
      $mustDoParks = $this->mustDoParks(
                              $args['day'],
                              $args['availableParks'],
                              $args['dayInfo']
                            );
      // filter list to use unscheduled parks only
      $unscheduledParks = $this->unscheduledOnly(
                              $mustDoParks,
                              $args['mouseplanCollection'],
                              $args['daysAtPlan'],
                              $args['parkNumber']
                            );
      // only if there are unscheduled parks then use that filtered list
      if(count($unscheduledParks) > 0) $mustDoParks = $unscheduledParks;
      $associatedPark = $args['associatedPark'];
      if(count($mustDoParks) > 1) {
        if($args['mouseplanParks']->isEmpty()) {
          $highestScoredParks = $this->highestScoredParks(
                                        $mustDoParks,
                                        $args['daysAtPlan']
                                      );
          $lessScheduledPark = $this->lessScheduledPark(
                                        $highestScoredParks,
                                        $args['mouseplanCollection']
                                      );
          if ($lessScheduledPark == $associatedPark->initials) {
            Log::debug('Available parks with less crowd level, untied by score and less scheduled.', [
              'vacation_id' => $this->VacationModel->id,
              'crowd level parks' => $mustDoParks,
              'scores' => $highestScoredParks,
              'less scheduled' => $lessScheduledPark,
            ]);
            // return the park with highest score that is less scheduled
            return TRUE;
          }
          return FALSE;
        }
        $nextInPreference = $this->nextInPreference(
                                    $mustDoParks,
                                    $this->pivotPark($args['mouseplanParks'])
                                  );
        if ($nextInPreference == $associatedPark->initials) {
          Log::debug('Available parks with less crowd level, untied by commute preference.', [
            'vacation_id' => $this->VacationModel->id,
            'crowd level parks' => $mustDoParks,
            'parks' => $nextInPreference
          ]);
          // return the park that is next in commute preference.
          return TRUE;
        }
        return FALSE;
      }
      // return the park with lowest crowd level
      if ($associatedPark->initials == key($mustDoParks)) {
        Log::debug('Available parks with less crowd level, and not scheduled enough.', [
          'vacation_id' => $this->VacationModel->id,
          'parks'=>$mustDoParks
        ]);
        return TRUE;
      }
      return FALSE;
    }

    private function mustDoParks(Day $day, $availableParks, $dayInfo) {
      $dayCrowdLevel = $this->CrowdLevelCollection
                            ->where('day_number', $day->day_number);
      if(!$dayCrowdLevel || $dayCrowdLevel->isEmpty()) {
        return $this->buildCrowdLevelDay(
                                    $day,
                                    $availableParks,
                                    $dayInfo
                                  );
      }
      return $dayCrowdLevel->first()['parks'];
    }

    private function buildCrowdLevelDay(Day $day, $availableParks, $dayInfo) {
      $parks = $this->lowestLevelParks($availableParks, $dayInfo);
      $this->CrowdLevelCollection->push([
        'day_number' => $day->day_number,
        'parks' => $parks,
      ]);
      return $parks;
    }

    private function lowestLevelParks($parkCollection, $dayInfo) {
      $availableParksInitials = array();
      foreach ($parkCollection as $parkBlock) {
        $associatedPark = $this->associatedPark($parkBlock);
        $parkInfo = $dayInfo->where('park', $associatedPark)->first();
        $availableParksInitials[$associatedPark] = $parkInfo['crowd_level'];
      }
      asort($availableParksInitials);
      $lowestLevel = current($availableParksInitials);
      $lowestLevelParks = array_filter($availableParksInitials, function($v) use($lowestLevel) {
        return $v == $lowestLevel;
      });
      return $lowestLevelParks;
    }

    private function associatedPark($parkBlock) {
      return $this->Parks
                  ->where('id', $parkBlock->association_id)
                  ->first()
                  ->initials;
    }

    private function unscheduledOnly($mustDoParks, $mouseplanCollection, $daysAtPlan) {
      if($mouseplanCollection->isEmpty()) return $mustDoParks;
      // if a park hasn't been scheduled enough.
      $unscheduledParks = array_filter($mustDoParks, function($park) use($mouseplanCollection, $daysAtPlan) {
        // Get Park object.
        $associatedPark = $this->Parks->get($park);
        // Get blocks with the Park id.
        $scheduledParkBlocks = $mouseplanCollection->where('association_id', $associatedPark->id);
        return $scheduledParkBlocks->count() <= $daysAtPlan[$park];
      }, ARRAY_FILTER_USE_KEY);
      if(count($unscheduledParks) > 0) return $unscheduledParks;

      // Or if the highest scored park are not scheduled enough.
      arsort($daysAtPlan);
      $highestScore = floor(current($daysAtPlan));
      $unscheduledParks = array_filter($mustDoParks, function($park) use($daysAtPlan, $highestScore) {
        return $daysAtPlan[$park] > $highestScore;
      }, ARRAY_FILTER_USE_KEY);
      return $unscheduledParks;
    }

    private function nextInPreference($parks, $pivotPark) {
      $parks2commute = self::$PreferenceCommute->get($pivotPark, array());
      foreach($parks2commute as $commute) {
        if(key_exists($commute, $parks)) {
          return $commute;
        }
      }
      return '';
    }

    private function pivotPark($mouseplanParks) {
      $pivotPark = $mouseplanParks->last();
      if(!$pivotPark) return '';
      return $this->associatedPark($pivotPark);
    }

    private function highestScoredParks($parkArray, $daysAtPlan) {
      $parkTimes = array();
      foreach ($parkArray as $park => $crowdLevel) {
        $parkTimes[$park] = $daysAtPlan[$park];
      }
      arsort($parkTimes);
      return $parkTimes;
    }

    private function lessScheduledPark($highestScoredParks, $mouseplanCollection) {
      $countOnMouseplanParks = [];
      foreach($highestScoredParks as $park => $days) {
        // Just look up with the first two highest scored parks.
        if (count($countOnMouseplanParks) < 3) break;
        $associatedPark = $this->Parks->get($park);
        $scoredParkBlocks = $mouseplanCollection->where('association_id', $associatedPark->id);
        if(!$scoredParkBlocks->isEmpty()) {
          $countOnMouseplanParks[$park] = $scoredParkBlocks->count();
        }
        else {
          $countOnMouseplanParks[$park] = 0;
        }
      }
      if (count($countOnMouseplanParks) > 0) {
        asort($countOnMouseplanParks);
        return key($countOnMouseplanParks);
      }
      return key($highestScoredParks);
    }

    protected function parkAttributes(array $args) {
      $parkNumber = $this->parkNumber($args['mouseplanParks'], $args['parkNumber']);
      return [
        'park_number' => $parkNumber,
      ];
    }

    protected function parkNumber($mouseplanParks, $parkNumber) {
      if($mouseplanParks->isEmpty()) return 1;
      if($parkNumber > 1 || $this->ParksPerDay < 3 || $this->parkNumberUsed($mouseplanParks, 1)) {
        while($parkNumber <= $this->ParksPerDay) {
          if($this->parkNumberUsed($mouseplanParks, $parkNumber)) {
            $parkNumber++;
            if($parkNumber > $this->ParksPerDay) $parkNumber = 1;
          } else break;
        }
      } else {
        while($parkNumber > 0) {
          if($this->parkNumberUsed($mouseplanParks, $parkNumber)) {
            $parkNumber--;
          } else break;
        }
      }
      return $parkNumber;
    }

    private function parkNumberUsed($mouseplanParks, $parkNumber) {
      return $mouseplanParks->contains(function ($item) use($parkNumber) {
          return $item->park_number == $parkNumber;
      });
    }

    protected function updateDaysAtPlan(array $args) {
      $park = $args['associatedPark']->initials;
      $daysAtPlan = $args['daysAtPlan'];
      $parkTime = $daysAtPlan[$park];
      $maxDayTime = 1;
      $halfDay = 0.5;
      $dayTime = $args['dayTime'];
      switch (true) {
        case $parkTime < $maxDayTime && $dayTime == 0:
            $daysAtPlan[$park] = 0;
            $dayTime = $dayTime + $parkTime;
            break;
        case $parkTime < $maxDayTime && ($dayTime + $parkTime) <= 1:
            $daysAtPlan[$park] = 0;
            $dayTime = $dayTime + $parkTime;
            break;
        case $parkTime < $maxDayTime:
            $timeToCompleteDay = 1 - $dayTime;
            $daysAtPlan[$park] = $parkTime - $timeToCompleteDay;
            $dayTime = $maxDayTime;
            break;
        case $parkTime >= $maxDayTime && $dayTime == 0:
            $dayTime = $maxDayTime;
            $daysAtPlan[$park] = $parkTime - $maxDayTime;
            break;
        case $parkTime >= $maxDayTime && $dayTime > $halfDay:
            $dayTime = round($dayTime + $halfDay);
            $daysAtPlan[$park] = round($parkTime - $halfDay);
            break;
        default:
            $daysAtPlan[$park] = $parkTime - $dayTime;
            $dayTime = $maxDayTime;
            break;
      }
      return [$daysAtPlan, $dayTime];
    }

    public function validateInitialData() {
        if(!$this->ParkInfo) {
            $this->logCritical("parkInfo");
            return false;
        }
        if(!$this->ParksPerDay) {
            $this->logCritical("parksPerDay");
            return false;
        }
        if(!$this->ScorePark) {
            $this->logCritical("scorePark");
            return false;
        }
        if(!$this->VacationModel) {
            $this->logCritical("vacationModel");
            return false;
        }
        if(!$this->VacationModel->visitDays) {
            $this->logCritical("VacationModel->visitDays");
            return false;
        }
        return true;
    }
}
