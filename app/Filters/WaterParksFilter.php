<?php

namespace App\Filters;

use Illuminate\Support\Facades\Log;

use App\Models\Block;
use App\Models\Day;
use App\Models\Park;
use App\Repositories\BlockRepository;
use App\Utils\DateUtils;
use App\Utils\BlocksUtils;
use App\Utils\Mousages;
use App\Verifiers\Verifier;

/*
 * WaterParksFilter
 * Step 8 of the Schedule Logic document.
 * */
class WaterParksFilter extends Filter {

  private $WPDay;
  private $ScheduleWP;

  private static $WaterParks = ['BB', 'TL'];

  protected function init() {
    $this->ScheduleWP = $this->BlocksUtils->isWaterPark();
    $this->WPDay = $this->getWaterParkDay();
  }

  private function getWaterParkDay() {
      if(!$this->ScheduleWP) return null;
      $mouseplan = $this->VacationModel->mouse_plan;
      $days = $mouseplan->days;
      $daysCount = $this->BlocksUtils->schedulableDaysCount();
      $midDayNumber = round($daysCount / 2);

      if($midDayNumber == 1 || $this->VacationModel->visitDays == 1) {
          $wpDay = $days->keyBy('day_number')->get(intval(3));
          if(!$wpDay || $this->BlocksUtils->departureDay($wpDay)) {
            $wpDay = $days->keyBy('day_number')->get(intval(2));
          }
      } else {
          $wpDay = $days->keyBy('day_number')->get(intval($midDayNumber + 1));
          if(!$wpDay || $this->BlocksUtils->departureDay($wpDay)) {
            $wpDay = $days->keyBy('day_number')->get(intval($midDayNumber));
          }
      }
      return $wpDay;
  }

  protected function exclude(array $args) {
    $associatedPark = $args['associatedPark'];
    $day = $args['day'];
    if(in_array($associatedPark->initials, self::$WaterParks)) {
      if($this->ScheduleWP) {
        return $this->WPDay->id != $day->id;
      }
      return true;
    }
    return false;
  }

  protected function mustDo(array $args) {
    $associatedPark = $args['associatedPark'];
    $wp = in_array($associatedPark->initials, self::$WaterParks);
    if($wp && $this->ScheduleWP && $this->WPDay) {
      $day = $args['day'];
      if($this->WPDay->id == $day->id) {
        Mousages::setWPPassMousage($this->VacationModel);
        return true;
      }
    }
    return false;
  }

  protected function updateDaysAtPlan(array $args) {
    return [$args['daysAtPlan'], 1];
  }

  protected function parkAttributes(array $args) {
    $day = $args['day'];
    return [
      'park_number' => 1,
      'startTime' => DateUtils::setDateTime($day->date, '10:00:00'),
      'endTime' => DateUtils::setDateTime($day->date, '17:00:00'),
    ];
  }

    public function validateInitialData() {
        if(!$this->VacationModel) {
            $this->logCritical("vacationModel");
            return false;
        }
        if(!$this->VacationModel->visitDays) {
            $this->logCritical("VacationModel->visitDays");
            return false;
        }
        if(!$this->VacationModel->interest_water_park_type_id) {
            $this->logCritical("VacationModel->interest_water_park_type_id");
            return false;
        }
        if(!$this->VacationModel->startTravelDate) {
            $this->logCritical("VacationModel->startTravelDate");
            return false;
        }
        return true;
    }
}
