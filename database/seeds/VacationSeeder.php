<?php

use Illuminate\Database\Seeder;

class VacationSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    DB::table('vacations')->insert([
        'trip_id' => 1, 'name' => "Test 1", 'stepNumber' => 4, 'highestStepCompleted' => 6,'startTravelDate' => '2017-07-16 08:32:21',
        'endTravelDate' => '2017-07-18 08:32:21','isFlying' => true, 'departureDate' => '2017-07-18 08:32:21',
        'arrivalDate' => '2017-07-16 08:32:21', 'booked_place_type_id' => 2, 'resort_id' => 5,
        'haveDisneyPackage' => false, 'visitDays' => 4, 'interest_visit_park_type_id' => 2, 'interest_water_park_type_id' => 2,
        'dining_plan_type_id' => 2, 'havePurchasedParkTicket_id' => 1, 'thrill_level_type_id' => 3, 'pace_type_id' => 2,
        'budget_type_id' => 2, 'time_flexibility_type_id' => 3, 'nap_type_id' => 1, 'hotelName' => "none",
        'streetAddress' => 'none', 'zipCode' => 'none', 'haveCar' => false, 'phone' => 'none', 'beginDayTime' => '2017-07-16 08:32:21',
        'endDayTime' => '2017-07-16 22:32:21'
    ]);
  }
}
