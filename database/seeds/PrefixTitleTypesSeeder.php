<?php

use Illuminate\Database\Seeder;

class PrefixTitleTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('prefix_title_types')->insert(['value' => 1, 'name' => 'Mr.', 'is_adult'=>true]);
      DB::table('prefix_title_types')->insert(['value' => 2, 'name' => 'Ms.', 'is_adult'=>true]);
      DB::table('prefix_title_types')->insert(['value' => 3, 'name' => 'Mrs.', 'is_adult'=>true]);
      DB::table('prefix_title_types')->insert(['value' => 4, 'name' => 'Dr.', 'is_adult'=>true]);
      DB::table('prefix_title_types')->insert(['value' => 5, 'name' => 'Master', 'is_adult'=>false]);
      DB::table('prefix_title_types')->insert(['value' => 6, 'name' => 'Miss', 'is_adult'=>false]);
    }
}
