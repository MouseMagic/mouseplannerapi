<?php

use Illuminate\Database\Seeder;

use App\Models\Traveler;

class TravelerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('travelers')->delete();

      Traveler::create(array(
        'vacation_id' => 1,
        'prefix_title_type_id' => 3,
        'firstname' => 'John',
        'lastname' => 'Snow',
        'monthOfBirth' => 2,
        'yearOfBirth' => 1980,
        'traveler_height_id' => 4
      ));

      Traveler::create(array(
        'vacation_id' => 1,
        'prefix_title_type_id' => 4,
        'firstname' => 'Sansa',
        'lastname' => 'Stark',
        'monthOfBirth' => 4,
        'yearOfBirth' => 1995,
        'traveler_height_id' => 4
      ));

      Traveler::create(array(
        'vacation_id' => 1,
        'prefix_title_type_id' => 3,
        'firstname' => 'Arya',
        'lastname' => 'Stark',
        'monthOfBirth' => 5,
        'yearOfBirth' => 2005,
        'traveler_height_id' => 4
      ));
    }
}
