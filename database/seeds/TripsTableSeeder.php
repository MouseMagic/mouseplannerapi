<?php

use Illuminate\Database\Seeder;

use App\Models\Trip;

class TripsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Trip::create(array(
        'user_id' => 1,
        'name' => 'fun trip',
        'firstTrip' => true,
        'tripCount' => 0,
      ));
    }
}
