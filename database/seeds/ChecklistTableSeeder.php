<?php

use Illuminate\Database\Seeder;

use App\Models\Checklist;

class ChecklistTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('checklists')->delete();

        Checklist::create(array(
          'user_id' => 1,
          'mousage_type_id' => 8,
          'text' => "Remember to make your FastPass+ reservation for star tours at Hollywood Studios as soon as you are able to!",
          'done' => false,
        ));
    }
}
