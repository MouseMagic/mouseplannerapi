<?php

use Illuminate\Database\Seeder;

class ParksExceptionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
      DB::table('parks_exceptions')->insert(['park_id' => 5]);
      DB::table('parks_exceptions')->insert(['park_id' => 6]);
      DB::table('parks_exceptions')->insert(['park_id' => 7]);
    }
}
