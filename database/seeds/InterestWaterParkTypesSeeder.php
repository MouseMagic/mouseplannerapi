<?php

use Illuminate\Database\Seeder;

class InterestWaterParkTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('interest_water_park_types')->insert(['value' => 1, 'name' => 'Yes']);
      DB::table('interest_water_park_types')->insert(['value' => 2, 'name' => 'No']);
      DB::table('interest_water_park_types')->insert(['value' => 3, 'name' => 'Please recommend']);
    }
}
