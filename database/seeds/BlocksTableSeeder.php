<?php

use Illuminate\Database\Seeder;

use App\Models\Block;

class BlocksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('blocks')->delete();

      Block::create(array(
        'day_id' => 1,
        'block_type_id' => 11,
        'name' => 'arrival',
        'location' => '',
        'startTime' => '2017-07-16 08:32:21',
        'endTime' => '2017-07-16 11:32:21',
        'association_id' => null,
      ));

      Block::create(array(
        'day_id' => 1,
        'block_type_id' => 8,
        'name' => 'resort',
        'location' => '',
        'startTime' => '2017-07-16 11:00:00',
        'endTime' => '2017-07-16 13:00:00',
        'association_id' => 1,
      ));

      Block::create(array(
        'day_id' => 1,
        'block_type_id' => 1,
        'name' => 'MAGIC KINGDOM',
        'location' => '',
        'startTime' => '2017-07-16 13:00:00',
        'endTime' => '2017-07-16 20:00:00',
        'association_id' => 1,
        'park_number' => 1,
      ));

      Block::create(array(
        'day_id' => 1,
        'block_type_id' => 9,
        'name' => 'Dining',
        'location' => 'Magic Kingdom',
        'startTime' => '2017-07-16 13:30:00',
        'endTime' => '2017-07-16 14:30:00',
      ));

      Block::create(array(
        'day_id' => 2,
        'block_type_id' => 10,
        'name' => 'Monorail',
        'location' => '',
        'startTime' => '2017-07-17 08:00:00',
        'endTime' => '2017-07-17 08:15:00',
      ));

      Block::create(array(
        'day_id' => 2,
        'block_type_id' => 3,
        'name' => 'EPCOT',
        'location' => '',
        'startTime' => '2017-07-17 09:00:00',
        'endTime' => '2017-07-17 20:00:00',
        'association_id' => 3,
        'park_number' => 1,
      ));

      Block::create(array(
        'day_id' => 2,
        'block_type_id' => 9,
        'name' => 'EPCOT Restaurant',
        'location' => 'EPCOT Land',
        'startTime' => '2017-07-17 12:30:00',
        'endTime' => '2017-07-17 13:30:00',
      ));

      Block::create(array(
        'day_id' => 2,
        'block_type_id' => 10,
        'name' => 'MonoRail',
        'location' => '',
        'startTime' => '2017-07-17 20:00:00',
        'endTime' => '2017-07-17 20:15:00',
      ));

      Block::create(array(
        'day_id' => 2,
        'block_type_id' => 8,
        'name' => 'EPCOTcot Resort',
        'location' => '',
        'startTime' => '2017-07-17 20:15:00',
        'endTime' => '2017-07-17 23:00:00',
        'association_id' => 2,
      ));

      Block::create(array(
        'day_id' => 2,
        'block_type_id' => 9,
        'name' => 'EPCOTcot Bar',
        'location' => 'EPCOTcot Resort',
        'startTime' => '2017-07-17 21:00:00',
        'endTime' => '2017-07-17 22:00:00',
      ));

      Block::create(array(
        'day_id' => 3,
        'block_type_id' => 2,
        'name' => 'ANIMAL KINGDOM',
        'location' => '',
        'startTime' => '2017-07-18 09:00:00',
        'endTime' => '2017-07-18 22:00:00',
        'association_id' => 2,
        'park_number' => 1,
      ));
    }
}
