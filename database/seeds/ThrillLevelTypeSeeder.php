<?php

use Illuminate\Database\Seeder;

class ThrillLevelTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('thrill_level_types')->insert(['value' => 1, 'name' => 'Low', 'description' => "Nice and easy", 'img' => '']);
      DB::table('thrill_level_types')->insert(['value' => 2, 'name' => 'Medium', 'description' => "Everything except roller coasters", 'img' => '']);
      DB::table('thrill_level_types')->insert(['value' => 3, 'name' => 'High', 'description' => "The more intense the better", 'img' => '']);
    }
}
