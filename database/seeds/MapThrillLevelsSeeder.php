<?php

use Illuminate\Database\Seeder;

use App\Models\MapThrillLevelType;

class MapThrillLevelsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run() {
        DB::table('map_thrill_level_types')->delete();

        MapThrillLevelType::create(array('thrill_level_type_id' => 1, 'disneyThrillLevelTypeId' => 16669881, 'disneyThrillLevelTypeValue' => 'Slow Rides'));
        MapThrillLevelType::create(array('thrill_level_type_id' => 2, 'disneyThrillLevelTypeId' => 16669885, 'disneyThrillLevelTypeValue' => 'Big Drops'));
        MapThrillLevelType::create(array('thrill_level_type_id' => 2, 'disneyThrillLevelTypeId' => 16669892, 'disneyThrillLevelTypeValue' => 'Loud'));
        MapThrillLevelType::create(array('thrill_level_type_id' => 3, 'disneyThrillLevelTypeId' => 16669877, 'disneyThrillLevelTypeValue' => 'Thrill Ride'));
        MapThrillLevelType::create(array('thrill_level_type_id' => 3, 'disneyThrillLevelTypeId' => 16669891, 'disneyThrillLevelTypeValue' => 'Scary'));
        MapThrillLevelType::create(array('thrill_level_type_id' => 2, 'disneyThrillLevelTypeId' => 16669886, 'disneyThrillLevelTypeValue' => 'Small Drops'));
        MapThrillLevelType::create(array('thrill_level_type_id' => 2, 'disneyThrillLevelTypeId' => 16669889, 'disneyThrillLevelTypeValue' => 'Spinning'));
        MapThrillLevelType::create(array('thrill_level_type_id' => 2, 'disneyThrillLevelTypeId' => 16669890, 'disneyThrillLevelTypeValue' => 'Dark'));
    }
}
