<?php

use Illuminate\Database\Seeder;

class AccessibilityTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('accessibility_types')->insert(['value' => 1, 'name' => 'Wheelchair / ECV', 'description' => "", 'img' => 'iwheelchair']);
      DB::table('accessibility_types')->insert(['value' => 2, 'name' => 'Service Animal', 'description' => "", 'img' => 'iservice']);
      DB::table('accessibility_types')->insert(['value' => 3, 'name' => 'Hearing and Visual', 'description' => "", 'img' => 'ihearing']);
      DB::table('accessibility_types')->insert(['value' => 4, 'name' => 'Food Restrictions', 'description' => "", 'img' => 'ifood']);
    }
}
