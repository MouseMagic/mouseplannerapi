<?php

use Illuminate\Database\Seeder;

use App\Models\ExperienceDefault;

class ExperienceDefaultsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
      ExperienceDefault::firstOrCreate(array('disneyId' => '18665185;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '18665186;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010178;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '26068;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010157;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010123;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010119;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010104;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010175;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010235;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010150;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010154;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '17396838;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010164;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010228;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '266924;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '17633457;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '18372009;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '161106;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '17633419;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '18050921;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '12431;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '12432;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '209857;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010151;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010182;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '18240966;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010193;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010207;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010231;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '18240543;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010218;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '279230;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010896;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '18240816;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '17842841;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '18411723;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '18361662;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '18697650;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '15839605;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '18189394;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '18378213;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '18378182;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '136;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '18669338;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010887;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '224093;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010848;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010173;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010191;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '18269694;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010161;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010147;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010199;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '107785;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '207395;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '62992;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '20194;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010204;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '18375495;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '220239;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '15574088;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010859;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '17564219;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '15574089;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '78704;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '15695444;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '268746;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '15574090;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010192;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010176;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010208;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010129;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '16767284;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010177;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '16767276;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '16124144;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010149;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '136550;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010110;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010220;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '16767263;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010190;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80069748;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010213;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010162;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010210;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010230;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80069754;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010153;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010114;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '80010170;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '321125;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '18465067;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '127313;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '17718925;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '18672598;entityType=Entertainment'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '18904138;entityType=Attraction'));
      ExperienceDefault::firstOrCreate(array('disneyId' => '18904172;entityType=Attraction'));
    }
}
