<?php

use Illuminate\Database\Seeder;

class BookedPlaceTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('booked_place_types')->insert(['value' => 1, 'name' => "Yes, I'd like MousePlanner to book it for me", 'img' => '']);
      DB::table('booked_place_types')->insert(['value' => 2, 'name' => "Yes, I already have my Disney reservation", 'img' => '']);
      DB::table('booked_place_types')->insert(['value' => 3, 'name' => "No, I'll make other lodging arrangements", 'img' => '']);
    }
}
