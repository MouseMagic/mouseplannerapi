<?php

use Illuminate\Database\Seeder;

class ParkSeederUpgrade extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            DB::table('parks')->where('id',1)->update(['name' => 'Magic Kingdom']);
            DB::table('parks')->where('id',2)->update(['name' => 'Animal Kingdom']);
            DB::table('parks')->where('id',3)->update(['name' => 'Epcot']);
            DB::table('parks')->where('id',4)->update(['name' => 'Hollywood Studios']);
            DB::table('parks')->where('id',5)->update(['name' => 'Disney Springs']);
            DB::table('parks')->where('id',6)->update(['name' => 'Blizzard Beach']);
            DB::table('parks')->where('id',7)->update(['name' => 'Typhoon Lagoon']);
    }
}
