<?php

use Illuminate\Database\Seeder;

use App\Models\Restaurant;

class RestaurantTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('restaurants')->truncate();

        Restaurant::create(array('name' => 'Aloha Isle', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 1, 'location' => 'Adventureland'));
        Restaurant::create(array('name' => 'Auntie Gravity\'s Galactic Goodies', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 1, 'location' => 'Tomorrowland'));
        Restaurant::create(array('name' => 'Be Our Guest Restaurant', 'service' => 'table service, counter service', 'cost_id' => 2, 'cuisine' => 'American, French', 'restaurantable_type' => 'parks', 'restaurantable_id' => 1, 'location' => 'Fantasyland'));
        Restaurant::create(array('name' => 'Casey\'s Corner', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 1, 'location' => 'Main Street'));
        Restaurant::create(array('name' => 'Cheshire Café', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 1, 'location' => 'Fantasyland'));
        Restaurant::create(array('name' => 'Cinderella\'s Royal Table', 'service' => 'character dining, signature dining', 'cost_id' => 3, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 1, 'location' => 'Fantasyland'));
        Restaurant::create(array('name' => 'Columbia Harbour House', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 1, 'location' => 'Liberty Square'));
        Restaurant::create(array('name' => 'Cool Ship', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 1, 'location' => 'Tomorrowland'));
        Restaurant::create(array('name' => 'Cosmic Ray\'s Starlight Café', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 1, 'location' => 'Tomorrowland'));
        Restaurant::create(array('name' => 'The Diamond Horseshoe', 'service' => 'table service', 'cost_id' => 3, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 1, 'location' => 'Liberty Square'));
        Restaurant::create(array('name' => 'Ferrytale Wishes: A Fireworks Dessert Cruise', 'service' => 'dining event', 'cost_id' => 3, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 1, 'location' => 'Seven Seas Lagoon'));
        Restaurant::create(array('name' => 'Fireworks Dessert Party at Tomorrowland Terrace', 'service' => 'dining event', 'cost_id' => 4, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 1, 'location' => 'Tomorrowland'));
        Restaurant::create(array('name' => 'Fireworks Dessert Party with Plaza Garden Viewing', 'service' => 'dining event', 'cost_id' => 3, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 1, 'location' => 'Tomorrowland'));
        Restaurant::create(array('name' => 'The Friar\'s Nook', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 1, 'location' => 'Fantasyland'));
        Restaurant::create(array('name' => 'Gaston\'s Tavern', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 1, 'location' => 'Fantasyland'));
        Restaurant::create(array('name' => 'Golden Oak Outpost', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 1, 'location' => 'Frontierland'));
        Restaurant::create(array('name' => 'Jungle Navigation Co. Ltd. Skipper Canteen', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'Multiple Cuisines', 'restaurantable_type' => 'parks', 'restaurantable_id' => 1, 'location' => 'Adventureland'));
        Restaurant::create(array('name' => 'Liberty Square Market', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 1, 'location' => 'Liberty Square'));
        Restaurant::create(array('name' => 'Liberty Tree Tavern', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 1, 'location' => 'Liberty Square'));
        Restaurant::create(array('name' => 'The Lunching Pad', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 1, 'location' => 'Tomorrowland'));
        Restaurant::create(array('name' => 'Main Street Bakery', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 1, 'location' => 'Main Street'));
        Restaurant::create(array('name' => 'Pecos Bill Tall Tale Inn and Cafe', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'Mexican', 'restaurantable_type' => 'parks', 'restaurantable_id' => 1, 'location' => 'Frontierland'));
        Restaurant::create(array('name' => 'Pinocchio Village Haus', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 1, 'location' => 'Fantasyland'));
        Restaurant::create(array('name' => 'Plaza Ice Cream Parlor', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 1, 'location' => 'Main Street'));
        Restaurant::create(array('name' => 'The Plaza Restaurant', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 1, 'location' => 'Main Street'));
        Restaurant::create(array('name' => 'Prince Eric’s Village Market', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 1, 'location' => 'Fantasyland'));
        Restaurant::create(array('name' => 'Sleepy Hollow', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 1, 'location' => 'Liberty Square'));
        Restaurant::create(array('name' => 'Storybook Treats', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 1, 'location' => 'Fantasyland'));
        Restaurant::create(array('name' => 'Sunshine Tree Terrace', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 1, 'location' => 'Adventureland'));
        Restaurant::create(array('name' => 'Tiana\'s Riverboat Party - Ice Cream Social & Viewing Party', 'service' => 'dining event', 'cost_id' => 2, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 1, 'location' => 'Liberty Square'));
        Restaurant::create(array('name' => 'Tomorrowland Terrace Restaurant', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 1, 'location' => 'Tomorrowland'));
        Restaurant::create(array('name' => 'Tony\'s Town Square Restaurant', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'Italian', 'restaurantable_type' => 'parks', 'restaurantable_id' => 1, 'location' => 'Main Street'));
        Restaurant::create(array('name' => 'Tortuga Tavern', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 1, 'location' => 'Frontierland'));
        Restaurant::create(array('name' => 'Westward Ho', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 1, 'location' => 'Frontierland'));
        Restaurant::create(array('name' => 'Wishes Fireworks Holiday Dessert Party at Tomorrowland Terrace', 'service' => 'dining event', 'cost_id' => 4, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 1, 'location' => 'Tomorrowland'));
        Restaurant::create(array('name' => 'Akershus Royal Banquet Hall', 'service' => 'table service, character dining', 'cost_id' => 3, 'cuisine' => 'Norwegian, American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'Biergarten Restaurant', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'German', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'Block & Hans', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'Candlelight Dining Package', 'service' => 'dining event', 'cost_id' => 3, 'cuisine' => 'Multiple', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'Chefs de France', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'French', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'Coral Reef Restaurant', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'American, Seafood', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'Future World'));
        Restaurant::create(array('name' => 'Crepes des Chefs de France', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'French', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'Electric Umbrella', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'Future World'));
        Restaurant::create(array('name' => 'Fife & Drum Tavern', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'Fountain View', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'Future World'));
        Restaurant::create(array('name' => 'Funnel Cake', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'The Garden Grill', 'service' => 'table service, character dining', 'cost_id' => 2, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'Future World'));
        Restaurant::create(array('name' => 'Gelati', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'Italian', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'Holidays Around the World Marketplaces', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'Multiple', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'IllumiNations Sparkling Dessert Party', 'service' => 'dining event', 'cost_id' => 3, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'Joy of Tea', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'Chinese', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'Kabuki Cafe', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'Multiple', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'Katsura Grill', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'Asian', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'Kringla Bakeri Og Kafe', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'Norwegian', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'La Cantina de San Angel', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'Mexican', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'La Cava del Tequila', 'service' => 'lounge', 'cost_id' => 1, 'cuisine' => 'Mexican', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'La Hacienda de San Angel', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'Mexican', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'The Land Cart', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'Future World'));
        Restaurant::create(array('name' => 'L\'Artisan des Glaces', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'French, American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'Le Cellier Steakhouse', 'service' => 'signature', 'cost_id' => 3, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'Les Halles Boulangerie-Patisserie', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'French', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'Les Vins des Chefs de France', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'French', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'Liberty Inn', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'Lotus Blossom Café', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'Chinese, Asian', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'Monsieur Paul', 'service' => 'signature', 'cost_id' => 3, 'cuisine' => 'French', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'New Year\'s Eve at Epcot World ShowPlace', 'service' => 'dining event', 'cost_id' => 3, 'cuisine' => 'Varies', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'Nine Dragons Restaurant', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'Chinese, Asian', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'Popcorn in Canada', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'Promenade Refreshments', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'Refreshment Cool Post', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'Refreshment Port', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'Restaurant Marrakesh', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'Mediterranean', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'Rose & Crown Dining Room', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'British', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'Rose & Crown Pub', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'British', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'San Angel Inn Restaurante', 'service' => 'table service', 'cost_id' => 1, 'cuisine' => 'Mexican', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'Sommerfest', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'German', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'Spice Road Table', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'Mediterranean', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'Sunshine Seasons', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'Varies', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'Future World'));
        Restaurant::create(array('name' => 'Tangierine Café', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'Mediterranean', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'Taste Track', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'Future World'));
        Restaurant::create(array('name' => 'Teppan Edo', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'Japanese', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'Test Track Cool Wash', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'Future World'));
        Restaurant::create(array('name' => 'Tokyo Dining', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'Multiple', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'Tutto Gusto Wine Cellar', 'service' => 'lounge', 'cost_id' => 1, 'cuisine' => 'Italian', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'Tutto Italia Ristorante', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'Italian', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'UK Beer Cart', 'service' => 'bar', 'cost_id' => 1, 'cuisine' => 'British', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'Via Napoli Pizza Window', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'Italian', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'Via Napoli Ristorante e Pizzeria', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'Italian', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => 'Yorkshire County Fish Shop', 'service' => 'table service', 'cost_id' => 1, 'cuisine' => 'British', 'restaurantable_type' => 'parks', 'restaurantable_id' => 3, 'location' => 'World Showcase'));
        Restaurant::create(array('name' => '50\'s Prime Time Café', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 4, 'location' => 'Echo Lake'));
        Restaurant::create(array('name' => 'ABC Commissary', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 4, 'location' => 'Comissary Lane'));
        Restaurant::create(array('name' => 'Anaheim Produce', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 4, 'location' => 'Sunset Boulevard'));
        Restaurant::create(array('name' => 'Backlot Express', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 4, 'location' => 'Echo Lake'));
        Restaurant::create(array('name' => 'Catalina Eddie\'s', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 4, 'location' => 'Sunset Boulevard'));
        Restaurant::create(array('name' => 'Dining with an Imagineer', 'service' => 'dining event', 'cost_id' => 4, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 4, 'location' => 'Hollywood Boulevard'));
        Restaurant::create(array('name' => 'Fairfax Fare', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 4, 'location' => 'Sunset Boulevard'));
        Restaurant::create(array('name' => 'Fantasmic! Dinner Package', 'service' => 'dining event', 'cost_id' => 3, 'cuisine' => 'Varies', 'restaurantable_type' => 'parks', 'restaurantable_id' => 4, 'location' => 'Multiple locations'));
        Restaurant::create(array('name' => 'The Hollywood Brown Derby', 'service' => 'signature', 'cost_id' => 3, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 4, 'location' => 'Hollywood Boulevard'));
        Restaurant::create(array('name' => 'The Hollywood Brown Derby Lounge', 'service' => 'lounge', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 4, 'location' => 'Hollywood Boulevard'));
        Restaurant::create(array('name' => 'Hollywood Scoops', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 4, 'location' => 'Sunset Boulevard'));
        Restaurant::create(array('name' => 'Hollywood & Vine', 'service' => 'table service, character dining', 'cost_id' => 3, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 4, 'location' => 'Echo Lake'));
        Restaurant::create(array('name' => 'Jingle Bell, Jingle BAM! Holiday Party at Disney\'s Hollywood Studios', 'service' => 'dining event', 'cost_id' => 3, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 4, 'location' => 'Hollywood Boulevard'));
        Restaurant::create(array('name' => 'KRNR The Rock Station', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 4, 'location' => 'Sunset Boulevard'));
        Restaurant::create(array('name' => 'Mama Melrose\'s Ristorante Italiano', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'Italian', 'restaurantable_type' => 'parks', 'restaurantable_id' => 4, 'location' => 'Muppet Courtyard'));
        Restaurant::create(array('name' => 'Min and Bill\'s Dockside Diner', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 4, 'location' => 'Echo Lake'));
        Restaurant::create(array('name' => 'Oasis Canteen', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 4, 'location' => 'Echo Lake'));
        Restaurant::create(array('name' => 'PizzeRizzo', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'Italian', 'restaurantable_type' => 'parks', 'restaurantable_id' => 4, 'location' => 'Muppet Courtyard'));
        Restaurant::create(array('name' => 'Rosie\'s All-American Café', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 4, 'location' => 'Sunset Boulevard'));
        Restaurant::create(array('name' => 'Sci-Fi Dine-In Theater Restaurant', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 4, 'location' => 'Comissary Lane'));
        Restaurant::create(array('name' => 'Starring Rolls Cafe', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 4, 'location' => 'Sunset Boulevard'));
        Restaurant::create(array('name' => 'Star Wars: A Galactic Spectacular Dessert Party at Disney\'s Hollywood Studios', 'service' => 'dining event', 'cost_id' => 3, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 4, 'location' => 'Hollywood Boulevard'));
        Restaurant::create(array('name' => 'Sunshine Day Café', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 4, 'location' => 'Sunset Boulevard'));
        Restaurant::create(array('name' => 'The Trolley Car Café', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 4, 'location' => 'Hollywood Boulevard'));
        Restaurant::create(array('name' => 'Tune-In Lounge', 'service' => 'lounge', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 4, 'location' => 'Echo Lake'));
        Restaurant::create(array('name' => 'Anandapur Ice Cream Truck', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 2, 'location' => 'Asia'));
        Restaurant::create(array('name' => 'Creature Comforts', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 2, 'location' => 'Discovery Island'));
        Restaurant::create(array('name' => 'Dawa Bar', 'service' => 'bar', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 2, 'location' => 'Africa'));
        Restaurant::create(array('name' => 'Dino-Bite Snacks', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 2, 'location' => 'DinoLand'));
        Restaurant::create(array('name' => 'Eight Spoon Café', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 2, 'location' => 'Discovery Island'));
        Restaurant::create(array('name' => 'Flame Tree Barbecue', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 2, 'location' => 'Discovery Island'));
        Restaurant::create(array('name' => 'Harambe Fruit Market', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 2, 'location' => 'Africa'));
        Restaurant::create(array('name' => 'Harambe Market', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'African', 'restaurantable_type' => 'parks', 'restaurantable_id' => 2, 'location' => 'Africa'));
        Restaurant::create(array('name' => 'Isle of Java', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 2, 'location' => 'Discovery Island'));
        Restaurant::create(array('name' => 'Kusafiri Coffee Shop & Bakery', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'Multiple', 'restaurantable_type' => 'parks', 'restaurantable_id' => 2, 'location' => 'Africa'));
        Restaurant::create(array('name' => 'Mahindi', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 2, 'location' => 'Africa'));
        Restaurant::create(array('name' => 'Nomad Lounge', 'service' => 'lounge', 'cost_id' => 1, 'cuisine' => 'Multiple', 'restaurantable_type' => 'parks', 'restaurantable_id' => 2, 'location' => 'Discovery Island'));
        Restaurant::create(array('name' => 'Pizzafari', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 2, 'location' => 'Discovery Island'));
        Restaurant::create(array('name' => 'Rainforest Cafe® at Disney\'s Animal Kingdom', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 2, 'location' => 'Main Entrance'));
        Restaurant::create(array('name' => 'Restaurantosaurus', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 2, 'location' => 'DinoLand'));
        Restaurant::create(array('name' => 'The Smiling Crocodile', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 2, 'location' => 'Discovery Island'));
        Restaurant::create(array('name' => 'Tamu Tamu Refreshments', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 2, 'location' => 'Africa'));
        Restaurant::create(array('name' => 'Terra Treats', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 2, 'location' => 'Discovery Island'));
        Restaurant::create(array('name' => 'Thirsty River Bar & Trek Snacks', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 2, 'location' => 'Asia'));
        Restaurant::create(array('name' => 'Tiffins', 'service' => 'signature', 'cost_id' => 3, 'cuisine' => 'Multiple', 'restaurantable_type' => 'parks', 'restaurantable_id' => 2, 'location' => 'Discovery Island'));
        Restaurant::create(array('name' => 'Trilo-Bites', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 2, 'location' => 'DinoLand'));
        Restaurant::create(array('name' => 'Tusker House Restaurant', 'service' => 'table service, character dining', 'cost_id' => 2, 'cuisine' => 'African', 'restaurantable_type' => 'parks', 'restaurantable_id' => 2, 'location' => 'Africa'));
        Restaurant::create(array('name' => 'Warung Outpost', 'service' => 'bar', 'cost_id' => 1, 'cuisine' => 'Mexican', 'restaurantable_type' => 'parks', 'restaurantable_id' => 2, 'location' => 'Asia'));
        Restaurant::create(array('name' => 'Yak & Yeti™ Local Food Cafes', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'Asian', 'restaurantable_type' => 'parks', 'restaurantable_id' => 2, 'location' => 'Asia'));
        Restaurant::create(array('name' => 'Yak & Yeti™ Quality Beverages', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 2, 'location' => 'Asia'));
        Restaurant::create(array('name' => 'Yak & Yeti™ Restaurant', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'Multiple', 'restaurantable_type' => 'parks', 'restaurantable_id' => 2, 'location' => 'Asia'));
        Restaurant::create(array('name' => 'AMC® Disney Springs 24 Dine-In Theatres', 'service' => 'table service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'West Side'));
        Restaurant::create(array('name' => 'Amorette\'s Patisserie', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'Town Center'));
        Restaurant::create(array('name' => 'B.B. Wolf\'s Sausage Co.', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'Marketplace'));
        Restaurant::create(array('name' => 'Blaze Fast-Fire\'d Pizza', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'Town Center'));
        Restaurant::create(array('name' => 'The BOATHOUSE®: Great Food, Waterfront Dining, Dream Boats™', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'Multiple', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'The Landing'));
        Restaurant::create(array('name' => 'Bongos Cuban Café™', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'Cuban', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'West Side'));
        Restaurant::create(array('name' => 'Bongos Cuban Café™ Express', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'Cuban', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'West Side'));
        Restaurant::create(array('name' => 'Chef Art Smith\'s Homecoming', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'The Landing'));
        Restaurant::create(array('name' => 'Coca-Cola Store Rooftop Beverage Bar', 'service' => 'bar', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'West Side'));
        Restaurant::create(array('name' => 'Cookes of Dublin', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'Irish', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'The Landing'));
        Restaurant::create(array('name' => 'The Daily Poutine', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'American'));
        Restaurant::create(array('name' => 'The Dining Room at Wolfgang Puck® Grand Cafe', 'service' => 'signature', 'cost_id' => 2, 'cuisine' => 'Multiple', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'West Side'));
        Restaurant::create(array('name' => 'D-Luxe Burger', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'Town Center'));
        Restaurant::create(array('name' => 'Dockside Margaritas', 'service' => 'bar', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'Marketplace'));
        Restaurant::create(array('name' => 'Earl Of Sandwich®', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'Marketplace'));
        Restaurant::create(array('name' => 'Erin McKenna\'s Bakery NYC', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'The Landing'));
        Restaurant::create(array('name' => 'The Express at Wolfgang Puck® Grand Cafe', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'West Side'));
        Restaurant::create(array('name' => 'Florida Snow Company', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'Marketplace'));
        Restaurant::create(array('name' => 'FoodQuest', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'Disney Springs'));
        Restaurant::create(array('name' => 'Food Truck - Fantasy Fare', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'West Side'));
        Restaurant::create(array('name' => 'Food Truck - Namaste Cafe', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'Asian', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'West Side'));
        Restaurant::create(array('name' => 'Food Truck - Superstar Catering', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'West Side'));
        Restaurant::create(array('name' => 'Food Truck - World Showcase of Flavors', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'West Side'));
        Restaurant::create(array('name' => 'Frontera Cocina', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'American, Mexican', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'Town Center'));
        Restaurant::create(array('name' => 'The Front Porch', 'service' => 'lounge', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'West Side'));
        Restaurant::create(array('name' => 'Ghirardelli® Ice Cream & Chocolate Shop', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'Marketplace'));
        Restaurant::create(array('name' => 'Häagen-Dazs® at Disney Springs West Side', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'West Side'));
        Restaurant::create(array('name' => 'House of Blues Restaurant & Bar', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'West Side'));
        Restaurant::create(array('name' => 'Jock Lindsey\'s Hangar Bar', 'service' => 'table service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'The Landing'));
        Restaurant::create(array('name' => 'Joffrey\'s Handcrafted Smoothies at Disney Springs Marketplace', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'Marketplace'));
        Restaurant::create(array('name' => 'Joffrey\'s Handcrafted Smoothies at Disney Springs West Side', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'West Side'));
        Restaurant::create(array('name' => 'Lava Lounge at Rainforest Cafe®', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'Marketplace'));
        Restaurant::create(array('name' => 'MacGUFFINS', 'service' => 'lounge', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'West Side'));
        Restaurant::create(array('name' => 'Morimoto Asia', 'service' => 'signature', 'cost_id' => 2, 'cuisine' => 'Multiple', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'The Landing'));
        Restaurant::create(array('name' => 'Morimoto Asia Street Food', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'Asian', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'The Landing'));
        Restaurant::create(array('name' => 'Paradiso 37, Taste of the Americas', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'Multiple', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'The Landing'));
        Restaurant::create(array('name' => 'Planet Hollywood Observatory - Coming Early 2017', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'West Side'));
        Restaurant::create(array('name' => 'Portobello Country Italian Trattoria', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'Italian, Seafood', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'The Landing'));
        Restaurant::create(array('name' => 'Raglan Road™ Irish Pub and Restaurant', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'Irish', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'The Landing'));
        Restaurant::create(array('name' => 'Rainforest Cafe® at Disney Springs Marketplace', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'Marketplace'));
        Restaurant::create(array('name' => 'The Smokehouse', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'West Side'));
        Restaurant::create(array('name' => 'Splitsville™ Dining Room', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'West Side'));
        Restaurant::create(array('name' => 'Sprinkles', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'Town Center'));
        Restaurant::create(array('name' => 'STARBUCKS® at Disney Springs Marketplace', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'Marketplace'));
        Restaurant::create(array('name' => 'STARBUCKS® at Disney Springs West Side', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'West Side'));
        Restaurant::create(array('name' => 'STK Orlando', 'service' => 'signature', 'cost_id' => 3, 'cuisine' => 'Multiple Cuisines', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'The Landing'));
        Restaurant::create(array('name' => 'Tea Traders Café by Joffrey\'s', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'The Landing'));
        Restaurant::create(array('name' => 'T-REX ™', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'Marketplace'));
        Restaurant::create(array('name' => 'Vivoli il Gelato', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'Italian', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'The Landing'));
        Restaurant::create(array('name' => 'Wetzel\'s Pretzels at Disney Springs Marketplace', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'Marketplace'));
        Restaurant::create(array('name' => 'Wetzel\'s Pretzels at Disney Springs West Side', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'West Side'));
        Restaurant::create(array('name' => 'Wolfgang Puck® Express at Disney Springs Marketplace', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'Marketplace'));
        Restaurant::create(array('name' => 'Wolfgang Puck® Grand Cafe', 'service' => 'signature', 'cost_id' => 2, 'cuisine' => 'Multiple Cuisines', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'West Side'));
        Restaurant::create(array('name' => 'YeSake', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'Asian, Japanese', 'restaurantable_type' => 'parks', 'restaurantable_id' => 5, 'location' => 'West Side'));
        Restaurant::create(array('name' => 'AbracadaBar', 'service' => 'bar', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 13, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Afternoon Tea at Garden View Tea Room', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'British', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 15, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Ale and Compass Lounge', 'service' => 'lounge', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 12, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Ample Hills Creamery', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 13, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Artist Point', 'service' => 'signature', 'cost_id' => 3, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 17, 'location' => 'resort'));
        Restaurant::create(array('name' => 'The Artist\'s Palette', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 17, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Backstretch Pool Bar', 'service' => 'bar', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 26, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Banana Cabana Pool Bar', 'service' => 'bar', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 6, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Barefoot Pool Bar', 'service' => 'bar', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 16, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Beach Club Marketplace', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 12, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Beaches & Cream Soda Shop', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 12, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Beaches Pool Bar & Grill', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 12, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Belle Vue Lounge', 'service' => 'lounge', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 13, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Big River Grille & Brewing Works', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 13, 'location' => 'resort'));
        Restaurant::create(array('name' => 'BoardWalk Bakery', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 13, 'location' => 'resort'));
        Restaurant::create(array('name' => 'BoardWalk Joe\'s Marvelous Margaritas', 'service' => 'lounge', 'cost_id' => 1, 'cuisine' => 'American, Mexican', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 13, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Boatwright\'s Dining Hall', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 9, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Boma - Flavors of Africa', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'African', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 11, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Cabana Bar and Beach Club', 'service' => 'bar', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 29, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Cafe Rix', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 7, 'location' => 'resort'));
        Restaurant::create(array('name' => 'California Grill', 'service' => 'signature', 'cost_id' => 3, 'cuisine' => 'Multiple', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 14, 'location' => 'resort'));
        Restaurant::create(array('name' => 'California Grill Lounge', 'service' => 'lounge', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 14, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Cape May Cafe', 'service' => 'table service, character dining', 'cost_id' => 3, 'cuisine' => 'American, Seafood', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 12, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Cape Town Lounge and Wine Bar', 'service' => 'lounge', 'cost_id' => 1, 'cuisine' => 'African', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 11, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Captain\'s Grille', 'service' => 'table service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 18, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Capt. Cook\'s', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American, Seafood', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 16, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Chef Mickey\'s', 'service' => 'table service, character dining', 'cost_id' => 3, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 14, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Cítricos', 'service' => 'signature', 'cost_id' => 3, 'cuisine' => 'American, Mediterranean', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 15, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Cítricos Chef\'s Domain', 'service' => 'dining event', 'cost_id' => 4, 'cuisine' => 'American, Mediterranean', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 15, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Cítricos Dining with an Imagineer', 'service' => 'dining event', 'cost_id' => 3, 'cuisine' => 'American, Mediterranean', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 15, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Cítricos Lounge', 'service' => 'lounge', 'cost_id' => 1, 'cuisine' => 'American, Mediterranean', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 15, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Contempo Café', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 14, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Contemporary Grounds', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 14, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Courtyard Pool Bar', 'service' => 'bar', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 15, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Cove Bar', 'service' => 'bar', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 14, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Crew\'s Cup Lounge', 'service' => 'lounge', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 18, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Crockett\'s Tavern', 'service' => 'lounge', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 10, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Dine with Animal Specialist', 'service' => 'dining event', 'cost_id' => 3, 'cuisine' => 'African', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 11, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Disney\'s Perfectly Princess Tea', 'service' => 'dining event', 'cost_id' => 2, 'cuisine' => 'British', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 15, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Disney\'s Spirit of Aloha Dinner Show', 'service' => 'dinner show', 'cost_id' => 3, 'cuisine' => 'Polynesian', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 16, 'location' => 'resort'));
        Restaurant::create(array('name' => 'The Drop Off Pool Bar', 'service' => 'bar', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 3, 'location' => 'resort'));
        Restaurant::create(array('name' => 'End Zone Food Court', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 2, 'location' => 'resort'));
        Restaurant::create(array('name' => 'ESPN Club', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 13, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Everything POP Shopping & Dining', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 4, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Flying Fish', 'service' => 'signature', 'cost_id' => 3, 'cuisine' => 'American, Seafood', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 13, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Flying Fish- Chef\'s Tasting Wine Dinner', 'service' => 'dining event', 'cost_id' => 4, 'cuisine' => 'American, Seafood', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 13, 'location' => 'resort'));
        Restaurant::create(array('name' => 'The Fountain', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 29, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Fresh Mediterranean Market', 'service' => 'counter service', 'cost_id' => 2, 'cuisine' => 'American, Mediterranean', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 29, 'location' => 'Walt Disney World Dolphin'));
        Restaurant::create(array('name' => 'Funnel Cake Cart', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 13, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Garden Grove', 'service' => 'table service, character dining', 'cost_id' => 2, 'cuisine' => 'American, Seafood', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 29, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Garden View Tea Room', 'service' => 'table service', 'cost_id' => 3, 'cuisine' => 'British', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 15, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Gasparilla Island Grill', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 15, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Good\'s Food to Go', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 24, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Grand Floridian Cafe', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 15, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Grandstand Spirits', 'service' => 'bar', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 2, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Gurgling Suitcase', 'service' => 'lounge', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 24, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Highway in the Sky Dine Around', 'service' => 'dining event', 'cost_id' => 4, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 14, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Hoop-Dee-Doo Musical Revue', 'service' => 'dinner show', 'cost_id' => 3, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 10, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Hurricane Hanna\'s Waterside Bar and Grill', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 12, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Il Mulino', 'service' => 'signature', 'cost_id' => 3, 'cuisine' => 'Italian', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 29, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Il Mulino Lounge', 'service' => 'lounge', 'cost_id' => 2, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 29, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Intermission Food Court', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 1, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Java Bar', 'service' => 'bar', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 29, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Jiko - The Cooking Place', 'service' => 'signature', 'cost_id' => 3, 'cuisine' => 'African', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 11, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Jiko Wine Tasting', 'service' => 'dining event', 'cost_id' => 3, 'cuisine' => 'African', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 11, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Kimonos', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'Multiple', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 29, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Kimonos Lounge', 'service' => 'lounge', 'cost_id' => 1, 'cuisine' => 'Multiple', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 29, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Kona Cafe', 'service' => 'table service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 16, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Kona Island', 'service' => 'table service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 16, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Laguna Bar', 'service' => 'bar', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 7, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Landscape of Flavors', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 3, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Las Ventanas', 'service' => 'table service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 7, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Leaping Horse Libations', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 13, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Lobby Lounge', 'service' => 'lounge', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 29, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Luau Cove', 'service' => 'table service', 'cost_id' => 4, 'cuisine' => 'Polynesian', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 16, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Maji Pool Bar', 'service' => 'bar', 'cost_id' => 1, 'cuisine' => 'African, American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 21, 'location' => 'resort'));
        Restaurant::create(array('name' => 'The Mara', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'African, American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 11, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Mardi Grogs', 'service' => 'lounge', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 8, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Martha\'s Vineyard', 'service' => 'table service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 12, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Maya Grill', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'Multiple Cuisines', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 7, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Meadow Snack Bar', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 10, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Mickey\'s Backyard BBQ', 'service' => 'dining event', 'cost_id' => 3, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 10, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Mizner\'s Lounge', 'service' => 'lounge', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 15, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Muddy Rivers', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 9, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Narcoossee\'s', 'service' => 'signature', 'cost_id' => 3, 'cuisine' => 'American, Seafood', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 15, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Oasis Bar & Grill', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 16, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Ohana', 'service' => 'table service, character dining', 'cost_id' => 2, 'cuisine' => 'American, Polynesian', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 16, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Old Port Royale Food Court', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 6, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Olivia\'s Cafe', 'service' => 'table service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 24, 'location' => 'resort'));
        Restaurant::create(array('name' => 'On the Rocks', 'service' => 'bar', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 26, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Outer Rim', 'service' => 'lounge', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 18, 'location' => 'resort'));
        Restaurant::create(array('name' => 'The Paddock Grill', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 26, 'location' => 'resort'));
        Restaurant::create(array('name' => 'El Mercado de Coronado', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American, Mexican', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 7, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Petals Pool Bar', 'service' => 'bar', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 4, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Picabu', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 29, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Pineapple Lanai', 'service' => 'snack', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 16, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Pioneer Hall', 'service' => 'table service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 10, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Pizza Window', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 10, 'location' => 'resort'));
        Restaurant::create(array('name' => 'P & J\'s Southern Takeout', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 9, 'location' => 'resort'));
        Restaurant::create(array('name' => 'River Roost', 'service' => 'table service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 9, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Riverside Mill Food Court', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 9, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Rix Lounge', 'service' => 'lounge', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 7, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Roaring Fork', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 17, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Sanaa', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'African, Indian', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 21, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Sanaa Lounge', 'service' => 'lounge', 'cost_id' => 1, 'cuisine' => 'African, Indian', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 21, 'location' => 'resort'));
        Restaurant::create(array('name' => 'The Sand Bar', 'service' => 'bar', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 18, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Sassagoula Floatworks and Food Factory', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American, Cajun/Creole', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 8, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Scat Cat\'s Club', 'service' => 'lounge', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 8, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Shula\'s Lounge', 'service' => 'lounge', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 29, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Shula\'s Steak House', 'service' => 'table service', 'cost_id' => 3, 'cuisine' => 'Multiple', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 29, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Shutters at Old Port Royale', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 6, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Siestas Cantina', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 7, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Silver Screen Spirits Pool Bar', 'service' => 'bar', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 31, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Singing Spirits Pool Bar', 'service' => 'bar', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 1, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Splash', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 29, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Tambu Lounge', 'service' => 'lounge', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 16, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Territory Lounge', 'service' => 'lounge', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 17, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Todd English\'s bluezoo', 'service' => 'signature', 'cost_id' => 3, 'cuisine' => 'Seafood', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 29, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Todd English\'s bluezoo Lounge', 'service' => 'lounge', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 29, 'location' => 'resort'));
        Restaurant::create(array('name' => 'The To-Go Cart', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 13, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Trader Sam\'s Grog Grotto', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'Polynesian', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 16, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Trader Sam\'s Tiki Terrace', 'service' => 'lounge', 'cost_id' => 1, 'cuisine' => 'Polynesian', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 16, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Trail\'s End Restaurant', 'service' => 'table service', 'cost_id' => 2, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 10, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Trattoria al Forno', 'service' => 'table service', 'cost_id' => 1, 'cuisine' => 'Italian', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 13, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Trout Pass Pool Bar', 'service' => 'bar', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 17, 'location' => 'resort'));
        Restaurant::create(array('name' => 'The Turf Club Bar and Grill', 'service' => 'counter service', 'cost_id' => 2, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 26, 'location' => 'resort'));
        Restaurant::create(array('name' => 'The Turf Club Lounge', 'service' => 'lounge', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 26, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Turtle Shack Poolside Snacks', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 24, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Uzima Springs Pool Bar', 'service' => 'bar', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 21, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Victoria & Albert\'s', 'service' => 'signature', 'cost_id' => 4, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 15, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Victoria & Albert\'s Chef\'s Table Dinner', 'service' => 'dining event', 'cost_id' => 4, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 15, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Victoria & Albert\'s Dinner Queen Victoria Room', 'service' => 'dining event', 'cost_id' => 4, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 15, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Victoria Falls Lounge', 'service' => 'lounge', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 21, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Wanyama Safari', 'service' => 'signature', 'cost_id' => 4, 'cuisine' => 'African, American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 21, 'location' => 'resort'));
        Restaurant::create(array('name' => 'The Wave Lounge', 'service' => 'table service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 14, 'location' => 'resort'));
        Restaurant::create(array('name' => 'The Wave... of American Flavors', 'service' => 'table service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 14, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Whispering Canyon Cafe', 'service' => 'table service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 17, 'location' => 'resort'));
        Restaurant::create(array('name' => 'World Premiere Food Court', 'service' => 'counter service', 'cost_id' => 1, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 31, 'location' => 'resort'));
        Restaurant::create(array('name' => 'Yachtsman Steakhouse', 'service' => 'signature', 'cost_id' => 3, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 18, 'location' => 'resort'));
        Restaurant::create(array('name' => '1900 Park Fare', 'service' => 'table service, character dining', 'cost_id' => 3, 'cuisine' => 'American', 'restaurantable_type' => 'resorts', 'restaurantable_id' => 15, 'location' => 'resort'));

        /* new Restaurants */
        Restaurant::create(array(
            'name' => 'Toledo Tapas - Steaks and Seafood',
            'service' => 'table service',
            'cost_id' => 3,
            'cuisine' => 'Spanish, Seafood, Steakhouse',
            'restaurantable_type' => 'resorts',
            'restaurantable_id' => 7,
            'location' => 'Disney\'s Coronado Springs Resort'
        ));

        Restaurant::create(array(
            'name' => 'Topolino\'s Terrace – Flavors of the Riviera',
            'service' => 'table service',
            'cost_id' => 4,
            'cuisine' => 'Mediterranean',
            'restaurantable_type' => 'resorts',
            'restaurantable_id' => 41,
            'location' => 'Disney\'s Riviera Resort'
        ));

        Restaurant::create(array(
            'name' => 'Le Petit Café',
            'service' => 'table service',
            'cost_id' => 2,
            'cuisine' => 'Mediterranean',
            'restaurantable_type' => 'resorts',
            'restaurantable_id' => 41,
            'location' => 'Disney\'s Riviera Resort'
        ));

        Restaurant::create(array(
            'name' => 'Primo Piatto',
            'service' => 'table service',
            'cost_id' => 3,
            'cuisine' => 'Italian',
            'restaurantable_type' => 'resorts',
            'restaurantable_id' => 41,
            'location' => 'Disney\'s Riviera Resort'
        ));

        Restaurant::create(array(
            'name' => 'Capa',
            'service' => 'table service, signature',
            'cost_id' => 4,
            'cuisine' => 'Steakhouse',
            'restaurantable_type' => 'resorts',
            'restaurantable_id' => 39,
            'location' => 'Four Seasons Resort Orlando at Walt Disney World Resort'
        ));

        Restaurant::create(array(
            'name' => 'Ravello',
            'service' => 'table service, signature, character dining',
            'cost_id' => 3,
            'cuisine' => 'Italian',
            'restaurantable_type' => 'resorts',
            'restaurantable_id' => 39,
            'location' => 'Four Seasons Resort Orlando at Walt Disney World Resort'
        ));

        Restaurant::create(array(
            'name' => 'Jaleo by Jose Andres',
            'service' => 'table service, signature',
            'cost_id' => 3,
            'cuisine' => 'Spanish',
            'restaurantable_type' => 'parks',
            'restaurantable_id' => 5,
            'location' => 'Disney Springs'
        ));

        Restaurant::create(array(
            'name' => 'Il Mulino',
            'service' => 'table service, signature',
            'cost_id' => 3,
            'cuisine' => 'Italian',
            'restaurantable_type' => 'resorts',
            'restaurantable_id' => 30,
            'location' => 'Walt Disney World Swan'
        ));

        Restaurant::create(array(
            'name' => 'Kimonos',
            'service' => 'table service',
            'cost_id' => 2,
            'cuisine' => 'Japanese',
            'restaurantable_type' => 'resorts',
            'restaurantable_id' => 30,
            'location' => 'Walt Disney World Swan'
        ));

        Restaurant::create(array(
            'name' => 'Takuma-Tei',
            'service' => 'table service, signature',
            'cost_id' => 4,
            'cuisine' => 'Japanese',
            'restaurantable_type' => 'parks',
            'restaurantable_id' => 3,
            'location' => 'World Showcase'
        ));

        Restaurant::create(array(
            'name' => 'The Edison',
            'service' => 'table service',
            'cost_id' => 2,
            'cuisine' => 'American',
            'restaurantable_type' => 'parks',
            'restaurantable_id' => 5,
            'location' => 'Disney Springs'
        ));

        Restaurant::create(array(
            'name' => 'Enzo\'s Hideaway',
            'service' => 'table service',
            'cost_id' => 2,
            'cuisine' => 'Italian',
            'restaurantable_type' => 'parks',
            'restaurantable_id' => 5,
            'location' => 'Disney Springs'
        ));

        Restaurant::create(array(
            'name' => 'Garden Grove',
            'service' => 'table service',
            'cost_id' => 2,
            'cuisine' => 'American',
            'restaurantable_type' => 'resorts',
            'restaurantable_id' => 30,
            'location' => 'Walt Disney World Swan'
        ));

        Restaurant::create(array(
            'name' => 'Sebastian\'s Bistro',
            'service' => 'table service',
            'cost_id' => 2,
            'cuisine' => 'Latin, Seafood',
            'restaurantable_type' => 'resorts',
            'restaurantable_id' => 6,
            'location' => 'Disney\'s Caribbean Beach Resort'
        ));

        Restaurant::create(array(
            'name' => 'Wine Bar George - A Restaurant & Bar',
            'service' => 'table service',
            'cost_id' => 2,
            'cuisine' => 'American',
            'restaurantable_type' => 'parks',
            'restaurantable_id' => 5,
            'location' => 'Disney Springs'
        ));

        Restaurant::create(array(
            'name' => 'Docking Bay 7 Food and Cargo',
            'service' => 'table service',
            'cost_id' => 2,
            'cuisine' => 'American',
            'restaurantable_type' => 'parks',
            'restaurantable_id' => 4,
            'location' => 'Star Wars: Galaxy\'s Edge'
        ));

        Restaurant::create(array(
            'name' => 'Satu\'li Canteen',
            'service' => 'counter service',
            'cost_id' => 1,
            'cuisine' => 'American',
            'restaurantable_type' => 'parks',
            'restaurantable_id' => 2,
            'location' => 'Pandora'
        ));

        Restaurant::create(array(
            'name' => 'Woody\'s Lunch Box',
            'service' => 'counter service',
            'cost_id' => 1,
            'cuisine' => 'American',
            'restaurantable_type' => 'parks',
            'restaurantable_id' => 4,
            'location' => 'Toy Story Land'
        ));

    }
}
