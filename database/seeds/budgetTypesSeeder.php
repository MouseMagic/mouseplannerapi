<?php

use Illuminate\Database\Seeder;

class BudgetTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('budget_types')->insert(['value' => 1, 'name' => "Economy", 'description' => "", 'img' => '']);
      DB::table('budget_types')->insert(['value' => 2, 'name' => "Moderate", 'description' => "", 'img' => '']);
      DB::table('budget_types')->insert(['value' => 3, 'name' => "Deluxe", 'description' => "", 'img' => '']);
    }
}
