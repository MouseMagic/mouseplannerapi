<?php

use Illuminate\Database\Seeder;

class TravelerHeightsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('traveler_heights')->insert(['value' => 1, 'name' => 'Less than 32in (82cm)']);
      DB::table('traveler_heights')->insert(['value' => 2, 'name' => 'Between 32-48in (82-113cm)']);
      DB::table('traveler_heights')->insert(['value' => 3, 'name' => 'Between 49-60in (123cm - 152cm)']);
      DB::table('traveler_heights')->insert(['value' => 4, 'name' => 'More than 60in (153cm)']);

    }
}
