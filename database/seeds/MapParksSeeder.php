<?php

use Illuminate\Database\Seeder;

use App\Models\MapPark;

class MapParksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run() {
        DB::table('map_parks')->delete();

        MapPark::create(array('park_id' => 1, 'disneyParkInterestId' => 80007944, 'disneyParkInterestValue' => 'Magic Kingdom Park'));
        MapPark::create(array('park_id' => 2, 'disneyParkInterestId' => 80007823, 'disneyParkInterestValue' => "Disney's Animal Kingdom Theme Park"));
        MapPark::create(array('park_id' => 2, 'disneyParkInterestId' => 17511595, 'disneyParkInterestValue' => "Animal Kingdom Park"));
        MapPark::create(array('park_id' => 3, 'disneyParkInterestId' => 80007838, 'disneyParkInterestValue' => 'Epcot'));
        MapPark::create(array('park_id' => 4, 'disneyParkInterestId' => 80007998, 'disneyParkInterestValue' => "Disney's Hollywood Studios"));
        MapPark::create(array('park_id' => 5, 'disneyParkInterestId' => 1268, 'disneyParkInterestValue' => 'Disney Springs Resort Area'));
        MapPark::create(array('park_id' => 6, 'disneyParkInterestId' => 17471013, 'disneyParkInterestValue' => 'Water Parks'));
        MapPark::create(array('park_id' => 7, 'disneyParkInterestId' => 17471013, 'disneyParkInterestValue' => 'Water Parks'));
    }
}
