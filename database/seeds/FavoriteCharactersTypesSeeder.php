<?php

use Illuminate\Database\Seeder;

class CharactersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('characters')->insert(['value' => 4, 'name' => 'Mickey & Pals']);
      DB::table('characters')->insert(['value' => 1, 'name' => 'Disney Classics']);
      DB::table('characters')->insert(['value' => 3, 'name' => 'Princesses']);
      DB::table('characters')->insert(['value' => 5, 'name' => 'Pixar Pals']);
      DB::table('characters')->insert(['value' => 6, 'name' => 'Star Wars']);
      DB::table('characters')->insert(['value' => 2, 'name' => 'Disney Jr.']);
      DB::table('characters')->insert(['value' => 7, 'name' => 'Pirates']);
    }
}
