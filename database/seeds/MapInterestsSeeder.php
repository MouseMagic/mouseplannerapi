<?php

use Illuminate\Database\Seeder;

use App\Models\MapInterestType;

class MapInterestsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run() {
        DB::table('map_interest_types')->delete();

        MapInterestType::create(array('interest_type_id' => 1, 'disneyInterestTypeId' => 16669982, 'disneyInterestTypeValue' => 'Animal Encounters', 'weight' => 1));
        MapInterestType::create(array('interest_type_id' => 3, 'disneyInterestTypeId' => 16726785, 'disneyInterestTypeValue' => 'Behind the Scenes', 'weight' => 0));
        MapInterestType::create(array('interest_type_id' => 2, 'disneyInterestTypeId' => 17043507, 'disneyInterestTypeValue' => 'Stage Shows', 'weight' => 1));
        MapInterestType::create(array('interest_type_id' => 8, 'disneyInterestTypeId' => 16669882, 'disneyInterestTypeValue' => 'Water Rides', 'weight' => 0));
    }
}
