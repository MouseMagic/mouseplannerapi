<?php

use Illuminate\Database\Seeder;

class ParkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('parks')->delete();

        DB::table('parks')->insert(['id' => 1, 'value' => 1, 'name' => 'Magic Kingdom',     'description' => "Fairytale dreams come true for children of all ages at Magic Kingdom park. Delight in classic attractions, enchanting fireworks, musical parades and beloved Disney Characters across 6 whimsical lands. Zoom through space, become a swashbuckling pirate and watch fireworks light up the sky over Cinderella Castle.", 'linkto' => 'https://disneyworld.disney.go.com/destinations/magic-kingdom/', 'initials' => 'MK']);
        DB::table('parks')->insert(['id' => 2, 'value' => 3, 'name' => 'Animal Kingdom',    'description' => "Encounter exotic animals and exciting adventures at Disney's Animal Kingdom park, one of the largest animal theme parks in the world. Home to more than 1,700 animals across 250 species, the park reflects Walt Disney's dedication to conservation and is committed to animal care, education and research.", 'linkto' => 'https://disneyworld.disney.go.com/destinations/animal-kingdom/', 'initials' => 'AK']);
        DB::table('parks')->insert(['id' => 3, 'value' => 2, 'name' => 'Epcot',             'description' => "Explore exciting attractions, enchanting international pavilions, award-winning fireworks and seasonal special events. Celebrating the human spirit, Epcot has 2 distinct realms: Future World, which features technological innovations, and World Showcase, which shares with Guests the culture and cuisine of 11 countries.", 'linkto' => 'https://disneyworld.disney.go.com/destinations/epcot/', 'initials' => 'EP']);
        DB::table('parks')->insert(['id' => 4, 'value' => 5, 'name' => 'Hollywood Studious', 'description' => "Movie magic comes to life at Disney's Hollywood Studios, awash in the glitz and glamour of Hollywood's Golden Age. Step into the action with attractions based on blockbuster movies and top TV shows, and delight in exciting entertainment that puts you center stage. With the vibrancy of a bustling movie set, this park features 8 sections.", 'linkto' => 'https://disneyworld.disney.go.com/destinations/hollywood-studios/', 'initials' => 'HS']);
        DB::table('parks')->insert(['id' => 5, 'value' => 6, 'name' => 'Disney Springs',    'description' => "World-class restaurants, dazzling entertainment and unique shops line the waterfront at the Downtown Disney area, a fun-filled district that includes The Landing, West Side and Marketplace! Here, you can shop in the world's largest Disney store, dine amid prehistoric creatures, bowl a game at the 30-lane alley and more.", 'linkto' => 'https://disneyworld.disney.go.com/destinations/disney-springs/', 'initials' => 'DS']);
        DB::table('parks')->insert(['id' => 6, 'value' => 4, 'name' => 'Blizzard Beach',    'description' => "Discover frosty fun for the whole family at Disney’s Blizzard Beach water park, a one-time ski resort that has melted into a watery wonderland. Zip down the slushy slopes of Mount Gushmore on one of the world’s tallest and fastest waterslides. Float down the tranquil river and sunbathe on the white-sand beach. Children under 48 inches tall can even splash around in their own water play area with a snow-castle fountain and kid-sized waterslides.", 'linkto' => 'https://disneyworld.disney.go.com/destinations/blizzard-beach/', 'initials' => 'BB']);
        DB::table('parks')->insert(['id' => 7, 'value' => 7, 'name' => 'Typhoon Lagoon',    'description' => "Escape to Disney’s Typhoon Lagoon water park for a storm of fun in the sun! Plunge down rushing rapids, sunbathe on the sandy beach and glide down the lazy river on a relaxing raft ride. After an epic typhoon hurled surfboards into palm trees and tossed boats like toys, the Mount Mayday became a topsy-turvy oasis of water-filled adventure!.", 'linkto' => 'https://disneyworld.disney.go.com/destinations/typhoon-lagoon/', 'initials' => 'TL']);
    }
}
