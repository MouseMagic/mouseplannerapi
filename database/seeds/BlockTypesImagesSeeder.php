<?php

use Illuminate\Database\Seeder;

use App\Models\BlockType;

class BlockTypesImagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      BlockType::where('association_id', 1)->update(['menu_image' => 'magic.png']);
      BlockType::where('association_id', 2)->update(['menu_image' => 'animal-kingdom.png']);
      BlockType::where('association_id', 3)->update(['menu_image' => 'epcot.png']);
      BlockType::where('association_id', 4)->update(['menu_image' => 'Hollywood-Studios.png']);
      BlockType::where('association_id', 5)->update(['menu_image' => 'Disney-Springs.png']);
      BlockType::where('association_id', 6)->update(['menu_image' => 'Blixard-Beach.png']);
      BlockType::where('association_id', 7)->update(['menu_image' => 'Typhoon-Lagoon.png']);
    }
}
