<?php

use Illuminate\Database\Seeder;

use App\Models\BlockType;

class BlockTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('block_types')->delete();

      BlockType::create(array('name' => 'park', 'subtype' => 'MK',
        'cssClass' => 'blue', 'image' => 'imagic.png', 'in_plan' => true, 'association_id' => 1));
      BlockType::create(array('name' => 'park', 'subtype' => 'AK',
        'cssClass' => 'green', 'image' => 'ianimalkingdom.png', 'in_plan' => true, 'association_id' => 2));
      BlockType::create(array('name' => 'park', 'subtype' => 'EP',
        'cssClass' => 'purple', 'image' => 'iepcot.png', 'in_plan' => true, 'association_id' => 3));
      BlockType::create(array('name' => 'park', 'subtype' => 'HS',
        'cssClass' => 'yellow', 'image' => 'ihollywood.png', 'in_plan' => true, 'association_id' => 4));
      BlockType::create(array('name' => 'park', 'subtype' => 'DS',
        'cssClass' => 'pink', 'image' => 'isprings.png', 'association_id' => 5));
      BlockType::create(array('name' => 'park', 'subtype' => 'BB',
        'cssClass' => 'light-blue', 'image' => 'iblizzard-beach.png', 'association_id' => 6));
      BlockType::create(array('name' => 'park', 'subtype' => 'TL',
        'cssClass' => 'light-blue', 'image' => 'ityphoon.png', 'association_id' => 7));
      BlockType::create(array('name' => 'resort', 'subtype' => ''));
      BlockType::create(array('name' => 'dining', 'subtype' => ''));
      BlockType::create(array('name' => 'transit', 'subtype' => 'default'));
      BlockType::create(array('name' => 'transit', 'subtype' => 'arrival'));
      BlockType::create(array('name' => 'transit', 'subtype' => 'departure'));
      BlockType::create(array('name' => 'custom', 'subtype' => ''));
    }
}
