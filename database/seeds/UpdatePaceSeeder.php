<?php

use Illuminate\Database\Seeder;
use App\Models\PaceType;

class UpdatePaceSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        PaceType::find(1)->update(["description" => "Only 1 park a day."]);
        PaceType::find(2)->update(["description" => "Occasional downtime."]);
        PaceType::find(3)->update(["description" => "Let’s do it all!"]);
    }
}
