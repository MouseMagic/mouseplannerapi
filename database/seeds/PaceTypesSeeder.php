<?php

use Illuminate\Database\Seeder;

class PaceTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('pace_types')->insert(['value' => 1, 'name' => 'Leisurely', 'description' => 'We want to take our time. No more than one park or activity a day', 'img' => '']);
      DB::table('pace_types')->insert(['value' => 2, 'name' => 'Moderate', 'description' => 'We want to do as much as we can, but we want some downtime too.', 'img' => '']);
      DB::table('pace_types')->insert(['value' => 3, 'name' => 'Energetic', 'description' => "We'll hustle to do it all and sleep when we're back home!", 'img' => '']);
    }
}
