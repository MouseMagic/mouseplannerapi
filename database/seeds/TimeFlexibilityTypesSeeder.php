<?php

use Illuminate\Database\Seeder;

class TimeFlexibilityTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('time_flexibility_types')->insert(['value' => 1, 'name' => 'Not flexible', 'description' => '', 'img' => '']);
      DB::table('time_flexibility_types')->insert(['value' => 2, 'name' => 'Very flexible', 'description' => '', 'img' => '']);
      DB::table('time_flexibility_types')->insert(['value' => 3, 'name' => 'Flexible only in the morning', 'description' => '', 'img' => '']);
      DB::table('time_flexibility_types')->insert(['value' => 4, 'name' => 'Flexible only in the evening', 'description' => '', 'img' => '']);
    }
}
