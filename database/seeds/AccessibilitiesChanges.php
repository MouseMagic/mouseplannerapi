<?php

use Illuminate\Database\Seeder;

class AccessibilitiesChanges extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('accessibility_types')
          ->where('value', 3)
          ->update([
            'name' => 'Visual',
            'img' => 'ivisual'
          ]);

      DB::table('accessibility_types')
          ->where('value', 4)
          ->update([
            'name' => 'Hearing',
            'img' => 'ihearing'
          ]);

      DB::table('accessibility_types')
          ->insert([
            'value' => 5,
            'name' => 'Food Restrictions',
            'description' => "",
            'img' => 'ifood'
          ]);
    }
}
