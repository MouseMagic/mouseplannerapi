<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new Role();
        $admin->name = 'admin';
        $admin->display_name = 'User Administrator';
        $admin->description  = 'User is allowed to manage and edit other users';
        $admin->save();

        $agent = new Role();
        $agent->name = 'agent';
        $agent->display_name = 'User Consultant';
        $agent->save();

        $client = new Role();
        $client->name = 'client';
        $client->display_name = 'User';
        $client->save();
    }
}
