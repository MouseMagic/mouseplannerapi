<?php

use Illuminate\Database\Seeder;

class CustomFacetsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('custom_facets')->insert(['disneyId' => '17875518;entityType=tour', 'type' => 'interests', 'value' => 'Custom Facet 1']);
      DB::table('custom_facets')->insert(['disneyId' => '17875518;entityType=tour', 'type' => 'interests', 'value' => 'Custom Facet 2']);
      DB::table('custom_facets')->insert(['disneyId' => '17875518;entityType=tour', 'type' => 'interests', 'value' => 'Custom Facet 3']);
      DB::table('custom_facets')->insert(['disneyId' => '80010208;entityType=Attraction', 'type' => 'interests', 'value' => 'Disney World Classics']);
      DB::table('custom_facets')->insert(['disneyId' => '80069754;entityType=Attraction', 'type' => 'interests', 'value' => 'Disney World Classics']);
      DB::table('custom_facets')->insert(['disneyId' => '80010176;entityType=Attraction', 'type' => 'interests', 'value' => 'Disney World Classics']);
      DB::table('custom_facets')->insert(['disneyId' => '80010176;entityType=Attraction', 'type' => 'interests', 'value' => 'Disney Classics']);
      DB::table('custom_facets')->insert(['disneyId' => '80010170;entityType=Attraction', 'type' => 'interests', 'value' => 'Disney Classics']);
      DB::table('custom_facets')->insert(['disneyId' => '80010149;entityType=Attraction', 'type' => 'interests', 'value' => 'Disney World Classics']);
      DB::table('custom_facets')->insert(['disneyId' => '80010210;entityType=Attraction', 'type' => 'interests', 'value' => 'Disney Classics']);
      DB::table('custom_facets')->insert(['disneyId' => '16767284;entityType=Attraction', 'type' => 'interests', 'value' => 'Disney Classics']);
      DB::table('custom_facets')->insert(['disneyId' => '80010160;entityType=Attraction', 'type' => 'interests', 'value' => 'Disney World Classics']);
      DB::table('custom_facets')->insert(['disneyId' => '16633259;entityType=Attraction', 'type' => 'interests', 'value' => 'Disney Jr.']);
      DB::table('custom_facets')->insert(['disneyId' => '16491297;entityType=Attraction', 'type' => 'interests', 'value' => 'Mickey & Pals']);
      DB::table('custom_facets')->insert(['disneyId' => '16124144;entityType=Attraction', 'type' => 'interests', 'value' => 'Disney World Classics']);
      DB::table('custom_facets')->insert(['disneyId' => '16491299;entityType=Attraction', 'type' => 'interests', 'value' => 'Disney World Classics']);
      DB::table('custom_facets')->insert(['disneyId' => '16767276;entityType=Attraction', 'type' => 'interests', 'value' => 'Disney Classics']);
      DB::table('custom_facets')->insert(['disneyId' => '80010129;entityType=Attraction', 'type' => 'interests', 'value' => 'Disney World Classics']);
      DB::table('custom_facets')->insert(['disneyId' => '80010129;entityType=Attraction', 'type' => 'interests', 'value' => 'Disney Classics']);
      DB::table('custom_facets')->insert(['disneyId' => '18276132;entityType=Attraction', 'type' => 'interests', 'value' => 'Star Wars']);
      DB::table('custom_facets')->insert(['disneyId' => '62992;entityType=Attraction', 'type' => 'interests', 'value' => 'Disney Classics']);
      DB::table('custom_facets')->insert(['disneyId' => '80010222;entityType=Attraction', 'type' => 'interests', 'value' => 'Disney World Classics']);
      DB::table('custom_facets')->insert(['disneyId' => '18240966;entityType=Attraction', 'type' => 'interests', 'value' => 'Star Wars']);
      DB::table('custom_facets')->insert(['disneyId' => '17272158;entityType=Attraction', 'type' => 'interests', 'value' => 'Pirates']);
      DB::table('custom_facets')->insert(['disneyId' => '80010190;entityType=Attraction', 'type' => 'interests', 'value' => 'Disney World Classics']);
      DB::table('custom_facets')->insert(['disneyId' => '18375495;entityType=Attraction', 'type' => 'interests', 'value' => 'Disney Princesses']);
      DB::table('custom_facets')->insert(['disneyId' => '136550;entityType=Attraction', 'type' => 'interests', 'value' => 'Pixar Pals']);
      DB::table('custom_facets')->insert(['disneyId' => '80010153;entityType=Attraction', 'type' => 'interests', 'value' => 'Disney World Classics']);
      DB::table('custom_facets')->insert(['disneyId' => '80010213;entityType=Attraction', 'type' => 'interests', 'value' => 'Disney Classics']);
      DB::table('custom_facets')->insert(['disneyId' => '80010162;entityType=Attraction', 'type' => 'interests', 'value' => 'Disney World Classics']);
      DB::table('custom_facets')->insert(['disneyId' => '80010162;entityType=Attraction', 'type' => 'interests', 'value' => 'Disney Classics']);
      DB::table('custom_facets')->insert(['disneyId' => '209857;entityType=Attraction', 'type' => 'interests', 'value' => 'Disney World Classics']);
      DB::table('custom_facets')->insert(['disneyId' => '209857;entityType=Attraction', 'type' => 'interests', 'value' => 'Pixar Pals']);
      DB::table('custom_facets')->insert(['disneyId' => '80010110;entityType=Attraction', 'type' => 'interests', 'value' => 'Disney World Classics']);
      DB::table('custom_facets')->insert(['disneyId' => '80010232;entityType=Attraction', 'type' => 'interests', 'value' => 'Disney World Classics']);
      DB::table('custom_facets')->insert(['disneyId' => '80010114;entityType=Attraction', 'type' => 'interests', 'value' => 'Pixar Pals']);
      DB::table('custom_facets')->insert(['disneyId' => '80010177;entityType=Attraction', 'type' => 'interests', 'value' => 'Disney World Classics']);
      DB::table('custom_facets')->insert(['disneyId' => '80010177;entityType=Attraction', 'type' => 'interests', 'value' => 'Pirates']);
      DB::table('custom_facets')->insert(['disneyId' => '80010196;entityType=Attraction', 'type' => 'interests', 'value' => 'Disney World Classics']);
      DB::table('custom_facets')->insert(['disneyId' => '80010193;entityType=Attraction', 'type' => 'interests', 'value' => 'Star Wars']);
      DB::table('custom_facets')->insert(['disneyId' => '107785;entityType=Attraction', 'type' => 'interests', 'value' => 'Pixar Pals']);
      DB::table('custom_facets')->insert(['disneyId' => '16414579;entityType=Attraction', 'type' => 'interests', 'value' => 'Disney Classics']);
      DB::table('custom_facets')->insert(['disneyId' => '80069748;entityType=Attraction', 'type' => 'interests', 'value' => 'Disney World Classics']);
      DB::table('custom_facets')->insert(['disneyId' => '16767263;entityType=Attraction', 'type' => 'interests', 'value' => 'Disney Classics']);
      DB::table('custom_facets')->insert(['disneyId' => '80010199;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '80010170;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '80010149;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '80010210;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '80010164;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '80010154;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '16767284;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '20194;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '80010218;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '16633259;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '80010175;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '16491299;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '80010129;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '80010228;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '80010224;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '18276132;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '62992;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '16512939;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '80010192;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '18240966;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '80010151;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '80010190;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '80010119;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '18375495;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '136550;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '80010157;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '80010182;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '17396838;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '209857;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '80010110;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '80010232;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '80010191;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '80010177;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '80010123;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '80010193;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '26068;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '107785;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
      DB::table('custom_facets')->insert(['disneyId' => '16414579;entityType=Attraction', 'type' => 'favorites',	'value' => 'Default Wishlist']);
    }
}
