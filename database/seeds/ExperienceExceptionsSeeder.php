<?php

use Illuminate\Database\Seeder;

class ExperienceExceptionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
      DB::table('experience_exceptions')->truncate();

      DB::table('experience_exceptions')->insert(['disneyId' => '80010110;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '61265;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 9, 'endDay' => 20]);
      DB::table('experience_exceptions')->insert(['disneyId' => '61265;entityType=Entertainment', 'startMonth' => 9, 'startDay' => 31, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '17883692;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '80010103;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '80010142;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '80010159;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '80010239;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '18394963;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '80010124;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '80010131;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '2219610;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '80010200;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '16767209;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '80010214;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '5490;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '80010165;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '17883688;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '17823158;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '17883694;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '80069743;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '18377701;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '16491299;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '80010207;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '26421;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '18362817;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '17920665;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '17905380;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '17883682;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '80010140;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '17883689;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '17883684;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '17883614;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '80010137;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '80010126;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '80069745;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '80010217;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '80010135;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '18263038;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '61525;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '18299758;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '17821434;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '8403;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '18091481;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '17000640;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '18713863;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '17022320;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '8385;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '8386;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '18693117;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '16665645;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '7922;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '80010862;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '18091526;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '18091527;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '16086540;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '18693119;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '17343241;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '16086541;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '18649512;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '81786;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '254514;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '16917380;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '17575399;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '18447293;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '8336;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '141853;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '17490262;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '157989;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '80010850;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '255950;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '8067;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '18181345;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '80010864;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '16630482;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '17506082;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '18715885;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '15958368;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '8066;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '4127;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '18101943;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '8074;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '80010873;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '13507;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '17418832;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '16235871;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '18431064;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '18479931;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '18368385;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '18375653;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '8515;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '16629705;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '242220;entityType=Entertainment', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      //Aug 30th additions
      DB::table('experience_exceptions')->insert(['disneyId' => '17883691;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '160914;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '18677928;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '18276132;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '18685279;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '18685133;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '18685276;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '18478569;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '18673477;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '18478570;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '18673386;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '18673532;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '17883693;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '18672355;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '17883690;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
      DB::table('experience_exceptions')->insert(['disneyId' => '18478578;entityType=Attraction', 'startMonth' => 0, 'startDay' => 1, 'endMonth' => 11, 'endDay' => 31]);
    }
}
