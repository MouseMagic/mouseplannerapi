<?php

use Illuminate\Database\Seeder;

class MapAccessibilityTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('map_accessibility_types')->truncate();

      $wheelchairId = DB::table('accessibility_types')->where('name', 'Wheelchair / ECV')->first()->id;
      $serviceId = DB::table('accessibility_types')->where('name', 'Service Animal')->first()->id;
      $visualId = DB::table('accessibility_types')->where('name', 'Visual')->first()->id;
      $hearingId = DB::table('accessibility_types')->where('name', 'Hearing')->first()->id;
      $foodId = DB::table('accessibility_types')->where('name', 'Food Restrictions')->first()->id;

      DB::table('map_accessibility_types')->insert(['accessibility_type_id' => $wheelchairId, 'disneyAccessibilityTypeId' => 16669960, 'disneyAccessibilityTypeValue' => 'Must Be Ambulatory']);

      DB::table('map_accessibility_types')->insert(['accessibility_type_id' => $serviceId, 'disneyAccessibilityTypeId' => 16669945, 'disneyAccessibilityTypeValue' => 'Permitted with Caution']);
      DB::table('map_accessibility_types')->insert(['accessibility_type_id' => $serviceId, 'disneyAccessibilityTypeId' => 16983973, 'disneyAccessibilityTypeValue' => 'Due to the nature of the experience, service animals are not permitted on this attraction.']);
      DB::table('map_accessibility_types')->insert(['accessibility_type_id' => $serviceId, 'disneyAccessibilityTypeId' => 16669996, 'disneyAccessibilityTypeValue' => 'Not Permitted']);


      DB::table('map_accessibility_types')->insert(['accessibility_type_id' => $visualId, 'disneyAccessibilityTypeId' => 16669948, 'disneyAccessibilityTypeValue' => 'Audio Description']);

      DB::table('map_accessibility_types')->insert(['accessibility_type_id' => $hearingId, 'disneyAccessibilityTypeId' => 16669959, 'disneyAccessibilityTypeValue' => 'Assistive Listening']);
      DB::table('map_accessibility_types')->insert(['accessibility_type_id' => $hearingId, 'disneyAccessibilityTypeId' => 16669953, 'disneyAccessibilityTypeValue' => 'Handheld Captioning']);
      DB::table('map_accessibility_types')->insert(['accessibility_type_id' => $hearingId, 'disneyAccessibilityTypeId' => 16669952, 'disneyAccessibilityTypeValue' => 'Reflective Captioning']);
      DB::table('map_accessibility_types')->insert(['accessibility_type_id' => $hearingId, 'disneyAccessibilityTypeId' => 16669949, 'disneyAccessibilityTypeValue' => 'Sign Language']);
      DB::table('map_accessibility_types')->insert(['accessibility_type_id' => $hearingId, 'disneyAccessibilityTypeId' => 16669950, 'disneyAccessibilityTypeValue' => 'Video Captioning']);

    }
}
