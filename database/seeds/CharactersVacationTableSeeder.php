<?php

use Illuminate\Database\Seeder;

use App\Models\CharacterVacation;

class CharactersVacationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('character_vacation')->delete();

      CharacterVacation::create(array(
        'user_id' => 1,
        'vacation_id' => 1,
        'character_id' => 4
      ));

      CharacterVacation::create(array(
        'user_id' => 1,
        'vacation_id' => 1,
        'character_id' => 6
      ));

      CharacterVacation::create(array(
        'user_id' => 1,
        'vacation_id' => 1,
        'character_id' => 1
      ));
    }
}
