<?php

use Illuminate\Database\Seeder;

class CelebrationTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('celebration_types')->insert(['value' => 1, 'name' => "Birthday", 'img' => 'ibirthday']);
      DB::table('celebration_types')->insert(['value' => 2, 'name' => "Anniversary", 'img' => 'ianniversary']);
      DB::table('celebration_types')->insert(['value' => 3, 'name' => "Wedding", 'img' => 'iwedding']);
      DB::table('celebration_types')->insert(['value' => 4, 'name' => "Reunion", 'img' => 'ireunion']);
      DB::table('celebration_types')->insert(['value' => 5, 'name' => "1st Visit", 'img' => 'idisney']);
      DB::table('celebration_types')->insert(['value' => 6, 'name' => "Special Event", 'img' => 'ievent']);
    }
}
