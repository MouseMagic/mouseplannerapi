<?php

use Illuminate\Database\Seeder;

class DiningPreferenceTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('dining_preference_types')->insert(['value' => 1, 'name' => 'Quick Service']);
      DB::table('dining_preference_types')->insert(['value' => 2, 'name' => 'Table Service']);
      DB::table('dining_preference_types')->insert(['value' => 3, 'name' => 'Fine Dining']);
      DB::table('dining_preference_types')->insert(['value' => 4, 'name' => 'Character Dining']);
      DB::table('dining_preference_types')->insert(['value' => 5, 'name' => 'Dinner Shows']);
    }
}
