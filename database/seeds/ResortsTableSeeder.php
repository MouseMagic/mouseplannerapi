<?php

use Illuminate\Database\Seeder;

use App\Models\Resort;

class ResortsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('resorts')->delete();

      Resort::create(array('id' => 1, 'value' => '1', 'name' => 'Disney\'s All-Star Music Resort', 'resort_area' => 'AK'));
      Resort::create(array('id' => 2, 'value' => '2', 'name' => 'Disney\'s All-Star Sports Resort', 'resort_area' => 'AK'));
      Resort::create(array('id' => 3, 'value' => '3', 'name' => 'Disney\'s Art of Animation Resort', 'resort_area' => ''));
      Resort::create(array('id' => 4, 'value' => '4', 'name' => 'Disney\'s Pop Century Resort', 'resort_area' => ''));
      Resort::create(array('id' => 5, 'value' => '5', 'name' => 'The Campsites at Disney\'s Fort Wilderness Resort', 'resort_area' => 'MK'));
      Resort::create(array('id' => 6, 'value' => '6', 'name' => 'Disney\'s Caribbean Beach Resort', 'resort_area' => 'EP'));
      Resort::create(array('id' => 7, 'value' => '7', 'name' => 'Disney\'s Coronado Springs Resort', 'resort_area' => 'AK'));
      Resort::create(array('id' => 8, 'value' => '8', 'name' => 'Disney\'s Port Orleans Resort - French Quarter', 'resort_area' => 'DS'));
      Resort::create(array('id' => 9, 'value' => '9', 'name' => 'Disney\'s Port Orleans Resort - Riverside', 'resort_area' => 'DS'));
      Resort::create(array('id' => 10, 'value' => '10', 'name' => 'The Cabins at Disney\'s Fort Wilderness Resort', 'resort_area' => 'MK'));
      Resort::create(array('id' => 11, 'value' => '11', 'name' => 'Disney\'s Animal Kingdom Lodge', 'resort_area' => 'AK'));
      Resort::create(array('id' => 12, 'value' => '12', 'name' => 'Disney\'s Beach Club Resort', 'resort_area' => 'EP'));
      Resort::create(array('id' => 13, 'value' => '13', 'name' => 'Disney\'s BoardWalk Inn', 'resort_area' => 'EP'));
      Resort::create(array('id' => 14, 'value' => '14', 'name' => 'Disney\'s Contemporary Resort', 'resort_area' => 'MK'));
      Resort::create(array('id' => 15, 'value' => '15', 'name' => 'Disney\'s Grand Floridian Resort & Spa', 'resort_area' => 'MK'));
      Resort::create(array('id' => 16, 'value' => '16', 'name' => 'Disney\'s Polynesian Village Resort', 'resort_area' => 'MK'));
      Resort::create(array('id' => 17, 'value' => '17', 'name' => 'Disney\'s Wilderness Lodge', 'resort_area' => 'MK'));
      Resort::create(array('id' => 18, 'value' => '18', 'name' => 'Disney\'s Yacht Club Resort', 'resort_area' => 'EP'));
      Resort::create(array('id' => 19, 'value' => '19', 'name' => 'Bay Lake Tower at Disney\'s Contemporary Resort', 'resort_area' => 'MK'));
      Resort::create(array('id' => 20, 'value' => '20', 'name' => 'Disney\'s Animal Kingdom Villas - Jambo House', 'resort_area' => 'AK'));
      Resort::create(array('id' => 21, 'value' => '21', 'name' => 'Disney\'s Animal Kingdom Villas - Kidani Village', 'resort_area' => 'AK'));
      Resort::create(array('id' => 22, 'value' => '22', 'name' => 'Disney\'s Beach Club Villas', 'resort_area' => 'EP'));
      Resort::create(array('id' => 23, 'value' => '23', 'name' => 'Disney\'s BoardWalk Villas', 'resort_area' => 'EP'));
      Resort::create(array('id' => 24, 'value' => '24', 'name' => 'Disney\'s Old Key West Resort', 'resort_area' => 'DS'));
      Resort::create(array('id' => 25, 'value' => '25', 'name' => 'Disney\'s Polynesian Villas & Bungalows', 'resort_area' => 'MK'));
      Resort::create(array('id' => 26, 'value' => '26', 'name' => 'Disney\'s Saratoga Springs Resort & Spa', 'resort_area' => 'DS'));
      Resort::create(array('id' => 27, 'value' => '27', 'name' => 'The Villas at Disney\'s Grand Floridian Resort & Spa', 'resort_area' => 'MK'));
      Resort::create(array('id' => 28, 'value' => '28', 'name' => 'The Boulder Ridge Villas at Disney\'s Wilderness Lodge', 'resort_area' => 'MK'));
      Resort::create(array('id' => 29, 'value' => '29', 'name' => 'Walt Disney World Dolphin', 'resort_area' => 'EP'));
      Resort::create(array('id' => 30, 'value' => '30', 'name' => 'Walt Disney World Swan', 'resort_area' => 'EP'));
      Resort::create(array('id' => 31, 'value' => '31', 'name' => 'Disney\'s All-Star Movies Resort', 'resort_area' => 'AK'));

      //new Resorts
      Resort::create(array('id' => 39, 'value' => '39', 'name' => 'Four Seasons Resort Orlando at Walt Disney World Resort', 'resort_area' => ''));
      Resort::create(array('id' => 40, 'value' => '40', 'name' => 'Shades of Green at Walt Disney World', 'resort_area' => ''));
      Resort::create(array('id' => 41, 'value' => '41', 'name' => 'Disney\'s Riviera Resort', 'resort_area' => 'EP'));
      Resort::create(array('id' => 42, 'value' => '42', 'name' => 'Copper Creek Villas & Cabins at Disney’s Wilderness Lodge', 'resort_area' => 'MK'));
    }
}
