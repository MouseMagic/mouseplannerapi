<?php

use Illuminate\Database\Seeder;

use App\Models\MapTravelerHeight;
use App\Models\TravelerHeight;

class MapTravelerHeightsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run() {
        DB::table('map_traveler_heights')->delete();

        MapTravelerHeight::create(array('traveler_height_id' => 2, 'disneyHeightId' => 16669800, 'disneyHeightValue' => '35in (89cm) or taller'));
        MapTravelerHeight::create(array('traveler_height_id' => 2, 'disneyHeightId' => 16669801, 'disneyHeightValue' => '38in (97cm) or taller'));
        MapTravelerHeight::create(array('traveler_height_id' => 2, 'disneyHeightId' => 16669802, 'disneyHeightValue' => '40in (102cm) or taller'));
        MapTravelerHeight::create(array('traveler_height_id' => 2, 'disneyHeightId' => 16669803, 'disneyHeightValue' => '42in (107cm) or taller'));
        MapTravelerHeight::create(array('traveler_height_id' => 2, 'disneyHeightId' => 16669806, 'disneyHeightValue' => '44in (112cm) or taller'));
        MapTravelerHeight::create(array('traveler_height_id' => 2, 'disneyHeightId' => 16669809, 'disneyHeightValue' => '48in (122cm) or shorter'));
        MapTravelerHeight::create(array('traveler_height_id' => 3, 'disneyHeightId' => 16669810, 'disneyHeightValue' => '48in (122cm) or taller'));
        MapTravelerHeight::create(array('traveler_height_id' => 3, 'disneyHeightId' => 16669815, 'disneyHeightValue' => '60 inches (152cm) or shorter'));
        MapTravelerHeight::create(array('traveler_height_id' => 2, 'disneyHeightId' => 16669742, 'disneyHeightValue' => '32in (81cm) or taller'));
        MapTravelerHeight::create(array('traveler_height_id' => 3, 'disneyHeightId' => 16669742, 'disneyHeightValue' => '32in (81cm) or taller'));
        MapTravelerHeight::create(array('traveler_height_id' => 3, 'disneyHeightId' => 16669800, 'disneyHeightValue' => '35in (89cm) or taller'));
        MapTravelerHeight::create(array('traveler_height_id' => 3, 'disneyHeightId' => 16669801, 'disneyHeightValue' => '38in (97cm) or taller'));
        MapTravelerHeight::create(array('traveler_height_id' => 3, 'disneyHeightId' => 16669802, 'disneyHeightValue' => '40in (102cm) or taller'));
        MapTravelerHeight::create(array('traveler_height_id' => 3, 'disneyHeightId' => 16669803, 'disneyHeightValue' => '42in (107cm) or taller'));
        MapTravelerHeight::create(array('traveler_height_id' => 3, 'disneyHeightId' => 16669806, 'disneyHeightValue' => '44in (112cm) or taller'));
        MapTravelerHeight::create(array('traveler_height_id' => 1, 'disneyHeightId' => 16669809, 'disneyHeightValue' => '48in (122cm) or shorter'));
        MapTravelerHeight::create(array('traveler_height_id' => 2, 'disneyHeightId' => 16669815, 'disneyHeightValue' => '60 inches (152cm) or shorter'));
        MapTravelerHeight::create(array('traveler_height_id' => 1, 'disneyHeightId' => 16669815, 'disneyHeightValue' => '60 inches (152cm) or shorter'));
        MapTravelerHeight::create(array('traveler_height_id' => 4, 'disneyHeightId' => 16669742, 'disneyHeightValue' => '32in (81cm) or taller'));
        MapTravelerHeight::create(array('traveler_height_id' => 4, 'disneyHeightId' => 16669800, 'disneyHeightValue' => '35in (89cm) or taller'));
        MapTravelerHeight::create(array('traveler_height_id' => 4, 'disneyHeightId' => 16669801, 'disneyHeightValue' => '38in (97cm) or taller'));
        MapTravelerHeight::create(array('traveler_height_id' => 4, 'disneyHeightId' => 16669802, 'disneyHeightValue' => '40in (102cm) or taller'));
        MapTravelerHeight::create(array('traveler_height_id' => 4, 'disneyHeightId' => 16669803, 'disneyHeightValue' => '42in (107cm) or taller'));
        MapTravelerHeight::create(array('traveler_height_id' => 4, 'disneyHeightId' => 16669806, 'disneyHeightValue' => '44in (112cm) or taller'));
        MapTravelerHeight::create(array('traveler_height_id' => 4, 'disneyHeightId' => 16669810, 'disneyHeightValue' => '48in (122cm) or taller'));
        MapTravelerHeight::create(array('traveler_height_id' => 1, 'disneyHeightId' => 16669818, 'disneyHeightValue' => 'Any Height'));
        MapTravelerHeight::create(array('traveler_height_id' => 2, 'disneyHeightId' => 16669818, 'disneyHeightValue' => 'Any Height'));
        MapTravelerHeight::create(array('traveler_height_id' => 3, 'disneyHeightId' => 16669818, 'disneyHeightValue' => 'Any Height'));
        MapTravelerHeight::create(array('traveler_height_id' => 4, 'disneyHeightId' => 16669818, 'disneyHeightValue' => 'Any Height'));
    }
}
