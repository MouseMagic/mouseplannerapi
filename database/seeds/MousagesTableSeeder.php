<?php

use Illuminate\Database\Seeder;

use App\Models\Mousage;

class MousagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mousages')->delete();

        Mousage::create(array(
          'vacation_id' => 1,
          'mousage_type_id' => 1,
          'mousageable_id' => 1,
          'mousageable_type' => 'days',
          'read' => false,
          'text' => "Scheduling a couple early mornings might be a good idea given all you want to do. If you arrive at the park by 7:00am, you'll avoid the crowds.",
        ));

        Mousage::create(array(
          'vacation_id' => 1,
          'mousage_type_id' => 2,
          'mousageable_id' => 2,
          'mousageable_type' => 'days',
          'read' => false,
          'text' => "Since Epcot closes at 11:00pm we recommend to finish later on January 2nd.",
        ));

        Mousage::create(array(
          'vacation_id' => 1,
          'mousage_type_id' => 3,
          'mousageable_id' => 3,
          'mousageable_type' => 'days',
          'read' => false,
          'text' => "Epcot gets extra busy on Friday and Saturday nights so consider selecting a weeknight to enjoy an evening at Epcot."
        ));

        Mousage::create(array(
          'vacation_id' => 1,
          'mousage_type_id' => 5,
          'mousageable_id' => 1,
          'mousageable_type' => 'blocks',
          'read' => false,
          'text' => "Are you sure you don't want to go to Animal Kingdom? There are a few experiences there you might not want to miss! For exampe DINOSAUR, Expedition Everest, Kali River Rapids, and Kilimanjaro Safaris.",
        ));

        Mousage::create(array(
          'vacation_id' => 1,
          'mousage_type_id' => 6,
          'mousageable_id' => 5,
          'mousageable_type' => 'blocks',
          'read' => false,
          'text' => "The monorail is a great way to get to Magic Kingdom and Epcot, but did you know you can also take a boat to the Magic Kingdom?",
        ));

        Mousage::create(array(
          'vacation_id' => 1,
          'mousage_type_id' => 8,
          'mousageable_id' => 1,
          'mousageable_type' => 'days',
          'read' => false,
          'text' => "Remember to make your FastPass+ reservation for star tours at Hollywood Studios as soon as you are able to!",
        ));

        Mousage::create(array(
          'vacation_id' => 1,
          'mousage_type_id' => 10,
          'mousageable_id' => 1,
          'mousageable_type' => 'days',
          'read' => false,
          'text' => "Plan for the last Fantasmic show of the evening to beat the crowds and maximize your time in the park. Remember, you can also reserve FP+ if you want to reserve a seat, or purchase a Fantasmic Dinner Show Package!",
        ));

        Mousage::create(array(
          'vacation_id' => 1,
          'mousage_type_id' => 12,
          'mousageable_id' => 1,
          'mousageable_type' => 'mouseplans',
          'read' => false,
          'text' => "Crowds will be at their highest levels on this trip. See our touring strategies for maximizing your fun.",
        ));
    }
}
