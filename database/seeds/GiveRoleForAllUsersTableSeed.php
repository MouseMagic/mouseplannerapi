<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\User;
class GiveRoleForAllUsersTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        foreach ($users as $user){
            if(!$user->hasRole('client') || !$user->hasRole('admin') || !$user->hasRole('agent')){
                $user->attachRole(Role::where('name','client')->first());
            }
        }
    }
}
