<?php

use Illuminate\Database\Seeder;

use App\Models\MousageType;

class AddMoreMousagesTypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      MousageType::create(array(
        'key' => 'lowRankPark',
        'value' => "Are you sure you don't want to include %park? There are experiences there that you won't want to miss!",
        'isChecklistTask' => false,
        'milestone' => 0
      ));
    }
}
