<?php

use Illuminate\Database\Seeder;

class ModifyDiningAnswers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
      DB::table('dining_plan_types')
          ->where('value', 1)
          ->update(['name' => 'No dining plan']);

      DB::table('dining_plan_types')
          ->where('value', 2)
          ->update(['name' => '<p>Disney</p> <p>Quick Service</p> <p>Dining Plan</p>']);

      DB::table('dining_plan_types')
          ->where('value', 3)
          ->update(['name' => '<p>Disney</p> <p>Dining Plan</p>']);

      DB::table('dining_plan_types')
          ->where('value', 4)
          ->update(['name' => '<p>Disney</p> <p>Deluxe</p> <p>Dining Plan</p>']);
    }
}
