<?php

use Illuminate\Database\Seeder;

use App\Models\Day;

class DaysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('days')->delete();

      Day::create(array(
        'mouse_plan_id' => 1,
        'date' => '2017-07-16 08:32:21',
        'day_number' => 1,
      ));

      Day::create(array(
        'mouse_plan_id' => 1,
        'date' => '2017-07-17 08:32:21',
        'day_number' => 2,
      ));

      Day::create(array(
        'mouse_plan_id' => 1,
        'date' => '2017-07-18 08:32:21',
        'day_number' => 3,
      ));
    }
}
