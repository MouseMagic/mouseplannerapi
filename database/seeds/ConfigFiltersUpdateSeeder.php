<?php

use Illuminate\Database\Seeder;
use App\Models\Configuration;

class ConfigFiltersUpdateSeeder extends Seeder {

    /**
     * Add TimeFilter to schedule the parks that close late, before scheduling by Priority.
     *
     * @return void
     */
    public function run() {
      Configuration::create(['category' => 'filters', 'key' => 'Time', 'value' => 4]);
      Configuration::where('category', 'filters')->where('key', 'Priority')->update(['value' => 5]);
    }

}
