<?php

use Illuminate\Database\Seeder;
use App\Models\Configuration;

class ConfigBlockUpdateSeeder extends Seeder {

    /**
     * Change to 2 parks a day when park hopper = 1, naps = 0, and pace = 1.
     *
     * @return void
     */
    public function run() {
      Configuration::where('category', 'blocks')->where('key', 'block_1_0_1')->update(['value' => 2]);
    }

}
