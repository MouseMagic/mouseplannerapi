<?php

use Illuminate\Database\Seeder;

use App\Models\VacationPark;

class VacationParkTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('vacation_park')->delete();

      VacationPark::create(array(
        'vacation_id' => 1,
        'park_id' => 1,
        'hearts' => 0
      ));

      VacationPark::create(array(
        'vacation_id' => 1,
        'park_id' => 2,
        'hearts' => 2
      ));

      VacationPark::create(array(
        'vacation_id' => 1,
        'park_id' => 3,
        'hearts' => 1
      ));

      VacationPark::create(array(
        'vacation_id' => 1,
        'park_id' => 4,
        'hearts' => 3
      ));
    }
}
