<?php

use Illuminate\Database\Seeder;

class DiningPlanTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('dining_plan_types')->insert(['value' => 1, 'name' => 'Undecided', 'description' => "MousePlanner can help you decide.<br> <a href='https://disneyworld.disney.go.com/planning-guides/in-depth-advice/disney-dining-plan/' target='_blank'> Learn More</a>", 'linkto' => 'https://disneyworld.disney.go.com/guest-services/guests-with-disabilities/', 'img' => '']);
      DB::table('dining_plan_types')->insert(['value' => 2, 'name' => 'Disney Quick Service Dining Plan', 'description' => "Includes 2 quick service, 1 snack, and refillable mug, per guest, for each night's stay.<br>  <a href='http://disneyworld.disney.go.com/media/wdw_nextgen/CoreCatalog/WaltDisneyWorld/en_us/PDF/2016QuickService.pdf' target='_blank'> See details.</a>", 'linkto' => 'https://disneyworld.disney.go.com/guest-services/guests-with-disabilities/', 'img' => '']);
      DB::table('dining_plan_types')->insert(['value' => 3, 'name' => 'Disney Dining Plan', 'description' => "Includes 1 table service, 1 quick service, 1 snack, and refillable mug, per guest, for each night's stay.<br>  <a href='http://disneyworld.disney.go.com/media/wdw_nextgen/CoreCatalog/WaltDisneyWorld/en_us/PDF/2016Dining.pdf' target='_blank'> See details.</a>", 'linkto' => 'https://disneyworld.disney.go.com/guest-services/guests-with-disabilities/', 'img' => '']);
      DB::table('dining_plan_types')->insert(['value' => 4, 'name' => 'Disney Deluxe Dining Plan', 'description' => "Includes 3 table services, 2 snacks, and refillable mug, per guest, for each night's stay.<br> <a href='http://disneyworld.disney.go.com/media/wdw_nextgen/CoreCatalog/WaltDisneyWorld/en_us/PDF/2016DeluxeDining.pdf' target='_blank'> See details.</a>", 'linkto' => 'https://disneyworld.disney.go.com/guest-services/guests-with-disabilities/', 'img' => '']);
    }
}
