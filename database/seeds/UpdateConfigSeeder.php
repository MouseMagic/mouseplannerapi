<?php

use Illuminate\Database\Seeder;

class UpdateConfigSeeder extends Seeder
{
    /**
     * that seed updates ConfigSeeder.php
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('configuration')->where(['key' => 'block_1_0_3'])->update(['value' => 2]);
      DB::table('configuration')->where(['key' => 'block_1_1_3'])->update(['value' => 2]);        
    }
}
