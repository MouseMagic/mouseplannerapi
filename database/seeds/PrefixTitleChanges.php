<?php

use Illuminate\Database\Seeder;

class PrefixTitleChanges extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('prefix_title_types')
          ->where('value', 1)
          ->update([
            'is_adult'=>true,
          ]);

      DB::table('prefix_title_types')
          ->where('value', 2)
          ->update([
            'is_adult'=>true,
          ]);

      DB::table('prefix_title_types')
          ->where('value', 3)
          ->update([
            'is_adult'=>true,
          ]);

      DB::table('prefix_title_types')
          ->where('value', 4)
          ->update([
            'is_adult'=>true,
          ]);

      DB::table('prefix_title_types')
          ->where('value', 5)
          ->update([
            'is_adult'=>false,
          ]);

      DB::table('prefix_title_types')
          ->where('value', 6)
          ->update([
            'is_adult'=>false,
          ]);
    }
}
