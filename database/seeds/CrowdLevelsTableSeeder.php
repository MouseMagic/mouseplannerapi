<?php

use Illuminate\Database\Seeder;

use App\Models\CrowdLevel;

class CrowdLevelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('crowd_levels')->delete();

      CrowdLevel::create(array('value' => 'low'));
      CrowdLevel::create(array('value' => 'medium'));
      CrowdLevel::create(array('value' => 'high'));
    }
}
