<?php

use Illuminate\Database\Seeder;

class TravelerVacationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('traveler_vacation')->delete();

        DB::table('traveler_vacation')->insert(['vacation_id' => 1, 'traveler_id' => 1]);
        DB::table('traveler_vacation')->insert(['vacation_id' => 1, 'traveler_id' => 2]);
        DB::table('traveler_vacation')->insert(['vacation_id' => 1, 'traveler_id' => 3]);
    }
}
