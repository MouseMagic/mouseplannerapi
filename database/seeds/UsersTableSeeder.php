<?php

use Illuminate\Database\Seeder;

use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @return void
     */
    public function run()
    {
      User::create(array(
        'firstname' => 'Just_for_Test',
        'lastname' => 'That_user_does_not_work',
        'email' => 'userbot@altexsoft.com',
        'password' => Hash::make('Mouse861'),
        'confirmed' => 1
      ));
    }
}
