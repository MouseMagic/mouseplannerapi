<?php

use Illuminate\Database\Seeder;

use App\Models\VacationExperience;

class ExperiencesVacationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('vacation_experiences')->delete();

      VacationExperience::create(array(
        'vacation_id' => 1,
        'experience_id' => 1,
        'hearts' => 2,
      ));
      VacationExperience::create(array(
        'vacation_id' => 1,
        'experience_id' => 2,
        'hearts' => 2,
      ));
      VacationExperience::create(array(
        'vacation_id' => 1,
        'experience_id' => 3,
        'hearts' => 1,
      ));
      VacationExperience::create(array(
        'vacation_id' => 1,
        'experience_id' => 4,
        'hearts' => 1,
      ));
      VacationExperience::create(array(
        'vacation_id' => 1,
        'experience_id' => 5,
        'hearts' => 2,
      ));
      VacationExperience::create(array(
        'vacation_id' => 1,
        'experience_id' => 6,
        'hearts' => 3,
      ));
      VacationExperience::create(array(
        'vacation_id' => 1,
        'experience_id' => 7,
        'hearts' => 1,
      ));
      VacationExperience::create(array(
        'vacation_id' => 1,
        'experience_id' => 8,
        'hearts' => 0,
      ));
      VacationExperience::create(array(
        'vacation_id' => 1,
        'experience_id' => 9,
        'hearts' => 2,
      ));
      VacationExperience::create(array(
        'vacation_id' => 1,
        'experience_id' => 10,
        'hearts' => 3,
      ));
    }
}
