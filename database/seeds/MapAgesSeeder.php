<?php

use Illuminate\Database\Seeder;

class MapAgesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('map_ages')->insert(['min' => 0, 'max' => 1000, 'disneyAgeId' => 16669938, 'disneyAgeValue' => 'All Ages']);
      DB::table('map_ages')->insert(['min' => 0, 'max' => 5, 'disneyAgeId' => 16726942, 'disneyAgeValue' => 'Preschoolers']);
      DB::table('map_ages')->insert(['min' => 6, 'max' => 9, 'disneyAgeId' => 16669903, 'disneyAgeValue' => 'Kids']);
      DB::table('map_ages')->insert(['min' => 10, 'max' => 13, 'disneyAgeId' => 16727047, 'disneyAgeValue' => 'Tweens']);
      DB::table('map_ages')->insert(['min' => 14, 'max' => 18, 'disneyAgeId' => 16727023, 'disneyAgeValue' => 'Teens']);
      DB::table('map_ages')->insert(['min' => 19, 'max' => 1000, 'disneyAgeId' => 16726768, 'disneyAgeValue' => 'Adults']);

    }
}
