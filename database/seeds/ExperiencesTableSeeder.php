<?php

use Illuminate\Database\Seeder;

use App\Models\Experience;

class ExperiencesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      // Animal Kingdom, id=2
      Experience::firstOrCreate(array(
        'external_name' => 'Maharajah Jungle Trek',
        'park_id' => 2,
        'external_id' => '80010164;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Kali River Rapids',
        'park_id' => 2,
        'external_id' => '80010154;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Pangani Forest Exploration Trail',
        'park_id' => 2,
        'external_id' => '80010175;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Kilimanjaro Safaris',
        'park_id' => 2,
        'external_id' => '80010157;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Wilderness Explorers',
        'park_id' => 2,
        'external_id' => '17396838;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'DINOSAUR',
        'park_id' => 2,
        'external_id' => '80010123;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Expedition Everest - Legend of the Forbidden Mountain',
        'park_id' => 2,
        'external_id' => '26068;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Avatar Flight of Passage',
        'park_id' => 2,
        'external_id' => '18665186;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Na\'vi River Journey',
        'park_id' => 2,
        'external_id' => '18665185;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Conservation Station',
        'park_id' => 2,
        'external_id' => '80010119;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'TriceraTop Spin',
        'park_id' => 2,
        'external_id' => '80010228;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Rivers of Light',
        'park_id' => 2,
        'external_id' => '18372009;entityType=Entertainment'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Festival of the Lion King',
        'park_id' => 2,
        'external_id' => '12432;entityType=Entertainment'
      ));

      // Hollywood Studios, id=4
      Experience::firstOrCreate(array(
        'external_name' => 'For the First Time in Forever: A Frozen Sing-Along Celebration',
        'park_id' => 4,
        'external_id' => '17842841;entityType=Entertainment'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'March of the First Order',
        'park_id' => 4,
        'external_id' => '18411723;entityType=Entertainment'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Beauty and the Beast-Live on Stage',
        'park_id' => 4,
        'external_id' => '80010848;entityType=Entertainment'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Star Wars: A Galactic Spectacular',
        'park_id' => 4,
        'external_id' => '18378182;entityType=Entertainment'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'The Twilight Zone Tower of Terror',
        'park_id' => 4,
        'external_id' => '80010218;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Star Wars Launch Bay',
        'park_id' => 4,
        'external_id' => '18276132;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Star Wars: Path of the Jedi',
        'park_id' => 4,
        'external_id' => '18240966;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Muppet*Vision 3D',
        'park_id' => 4,
        'external_id' => '80010151;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Rock \'n\' Roller Coaster Starring Aerosmith',
        'park_id' => 4,
        'external_id' => '80010182;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Toy Story Midway Mania!',
        'park_id' => 4,
        'external_id' => '209857;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Star Tours',
        'park_id' => 4,
        'external_id' => '80010193;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Fantasmic!',
        'park_id' => 4,
        'external_id' => '80010887;entityType=Entertainment'
      ));

      // Epcot, id=3
      Experience::firstOrCreate(array(
        'external_name' => 'Test Track',
        'park_id' => 3,
        'external_id' => '80010199;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Soarin\'',
        'park_id' => 3,
        'external_id' => '20194;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Turtle Talk With Crush',
        'park_id' => 3,
        'external_id' => '62992;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Frozen Ever After',
        'park_id' => 3,
        'external_id' => '18375495;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Spaceship Earth',
        'park_id' => 3,
        'external_id' => '80010191;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'IllumiNations: Reflections of Earth',
        'park_id' => 3,
        'external_id' => '80010859;entityType=Entertainment'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'The Seas with Nemo & Friends',
        'park_id' => 3,
        'external_id' => '107785;entityType=Attraction'
      ));

      // Magic Kingdom, id=1
      Experience::firstOrCreate(array(
        'external_name' => 'Peter Pan\'s Flight',
        'park_id' => 1,
        'external_id' => '80010176;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Under the Sea ~ Journey of The Little Mermaid',
        'park_id' => 1,
        'external_id' => '16767263;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Enchanted Tales with Belle',
        'park_id' => 1,
        'external_id' => '16767276;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Haunted Mansion',
        'park_id' => 1,
        'external_id' => '80010208;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Mickey\'s PhilharMagic',
        'park_id' => 1,
        'external_id' => '80010170;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => '"it\'s a small world"',
        'park_id' => 1,
        'external_id' => '80010149;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Seven Dwarfs Mine Train',
        'park_id' => 1,
        'external_id' => '16767284;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Walt Disney World Railroad',
        'park_id' => 1,
        'external_id' => '16491299;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Dumbo the Flying Elephant',
        'park_id' => 1,
        'external_id' => '80010129;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Splash Mountain',
        'park_id' => 1,
        'external_id' => '80010192;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Space Mountain',
        'park_id' => 1,
        'external_id' => '80010190;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Monsters, Inc. Laugh Floor',
        'park_id' => 1,
        'external_id' => '136550;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Big Thunder Mountain Railroad',
        'park_id' => 1,
        'external_id' => '80010110;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Walt Disney\'s Carousel of Progress',
        'park_id' => 1,
        'external_id' => '80010232;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Pirates of the Caribbean',
        'park_id' => 1,
        'external_id' => '80010177;entityType=Attraction'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Move It! Shake It! Dance & Play It! Street Party',
        'park_id' => 1,
        'external_id' => '321125;entityType=Entertainment'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Happily Ever After',
        'park_id' => 1,
        'external_id' => '18672598;entityType=Entertainment'
      ));
      Experience::firstOrCreate(array(
        'external_name' => 'Disney Festival of Fantasy Parade',
        'park_id' => 1,
        'external_id' => '17718925;entityType=Entertainment'
      ));

      Experience::firstOrCreate(array(
        'external_name' => 'Slinky Dog Dash',
        'park_id' => 4,
        'external_id' =>  '18904138;entityType=Attraction',
      ));
    }
}
