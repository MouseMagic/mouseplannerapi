<?php

use Illuminate\Database\Seeder;

use App\Models\VacationInterestType;

class InterestsVacationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('interest_type_vacation')->delete();

      VacationInterestType::create(array(
        'user_id' => 1,
        'vacation_id' => 1,
        'interest_type_id' => 10
      ));

      VacationInterestType::create(array(
        'user_id' => 1,
        'vacation_id' => 1,
        'interest_type_id' => 12
      ));

      VacationInterestType::create(array(
        'user_id' => 1,
        'vacation_id' => 1,
        'interest_type_id' => 1
      ));

      VacationInterestType::create(array(
        'user_id' => 1,
        'vacation_id' => 1,
        'interest_type_id' => 3
      ));
    }
}
