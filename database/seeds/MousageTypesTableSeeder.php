<?php

use Illuminate\Database\Seeder;

use App\Models\MousageType;

class MousageTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Default mousages for user
        MousageType::firstOrCreate(array(
          'key' => 'default_package',
          'value' => "Purchase your Disney Vacation Package.",
          'isChecklistTask' => true,
          'milestone' => 0
        ));

        MousageType::firstOrCreate(array(
          'key' => 'default_d_experience1',
          'value' => "Go to www.startyourdisneyexperience.com and create a My Disney Experience Account if you don't already have one.",
          'isChecklistTask' => true,
          'milestone' => 0
        ));

        MousageType::firstOrCreate(array(
          'key' => 'default_d_experience2',
          'value' => "Link your resort reservation with your My Disney Experience account by entering your confirmation number.",
          'isChecklistTask' => true,
          'milestone' => 0
        ));

        MousageType::firstOrCreate(array(
          'key' => 'default_dining',
          'value' => "Make dining reservations – popular restaurants fill up fast so book ASAP as soon as it's 180 days out from check in!",
          'isChecklistTask' => true,
          'milestone' => 180
        ));

        MousageType::firstOrCreate(array(
          'key' => 'default_fastpass_60',
          'value' => "Make your FastPass+ reservations through your Disney account at exactly 7am EST 60 days from your check in date.",
          'isChecklistTask' => true,
          'milestone' => 60
        ));

        MousageType::firstOrCreate(array(
          'key' => 'default_d_experience3',
          'value' => "Download the My Disney Experience app on iOS or Android.",
          'isChecklistTask' => true,
          'milestone' => 60
        ));

        MousageType::firstOrCreate(array(
          'key' => 'default_bands',
          'value' => "Customize Magic Bands.",
          'isChecklistTask' => true,
          'milestone' => 60
        ));

        MousageType::firstOrCreate(array(
          'key' => 'default_shuttle',
          'value' => "Provide your flight information to reserve the free Disney Magical Express shuttle from the Orlando airport to your resort.",
          'isChecklistTask' => true,
          'milestone' => 60
        ));

        MousageType::firstOrCreate(array(
          'key' => 'default_fastpass_30',
          'value' => "Make your FastPass+ reservations through your Disney account as early as 7am EST, 30 days from your check in date.",
          'isChecklistTask' => true,
          'milestone' => 30
        ));

        // Specifig mousages for a vacation
        MousageType::firstOrCreate(array(
          'key' => 'schedule1',
          'value' => "Scheduling a couple early mornings might be a good idea given all you want to do. If you arrive at the park by %time, you'll avoid the crowds.",
          'isChecklistTask' => false,
          'milestone' => 0
        ));

        MousageType::create(array(
          'key' => 'schedule2',
          'value' => "Since %park closes at %time we recommend to finish later on %day.",
          'isChecklistTask' => false,
          'milestone' => 0
        ));

        MousageType::firstOrCreate(array(
          'key' => 'schedule3',
          'value' => "%park closes at %time tonight – consider making this a later evening to enjoy the park at night when it’s cooler with fewer crowds.",
          'isChecklistTask' => false,
          'milestone' => 0
        ));

        MousageType::firstOrCreate(array(
          'key' => 'epcot1',
          'value' => "Epcot gets extra busy on Friday and Saturday nights so consider selecting a weeknight to enjoy an evening at Epcot.",
          'isChecklistTask' => false,
          'milestone' => 0
        ));

        MousageType::firstOrCreate(array(
          'key' => 'missingpark1',
          'value' => "Are you sure you don't want to include Hollywood Studios? There are Star Wars experiences there that you won't want to miss!",
          'isChecklistTask' => false,
          'milestone' => 0
        ));

        MousageType::firstOrCreate(array(
          'key' => 'missingpark2',
          'value' => "Don't miss out on seeing Disney princesses in Epcot -- you can visit Elsa and Anna in Norway, and dine with all the princesses at Akershus restaurant.",
          'isChecklistTask' => false,
          'milestone' => 0
        ));

        MousageType::firstOrCreate(array(
          'key' => 'missingpark3',
          'value' => "No visit to Disney World is complete without a stop in the Magic Kingdom. Are you sure you don't want to add this to your schedule?",
          'isChecklistTask' => false,
          'milestone' => 0
        ));

        MousageType::firstOrCreate(array(
          'key' => 'missingpark4',
          'value' => "Things get wild at Animal Kingdom -- a half day here is a must for all animal lovers.",
          'isChecklistTask' => false,
          'milestone' => 0
        ));

        MousageType::firstOrCreate(array(
          'key' => 'transit1',
          'value' => "The monorail is a great way to get to Magic Kingdom and Epcot, but did you know you can also take a boat to the Magic Kingdom?",
          'isChecklistTask' => false,
          'milestone' => 0
        ));

        MousageType::firstOrCreate(array(
          'key' => 'crowd1',
          'value' => "Expect heavy crowds today and be sure to use our touring strategies. Get Fast Pass+ and dining reservations as soon as possible, and then take it easy to enjoy your day in the park.",
          'isChecklistTask' => false,
          'milestone' => 0
        ));

        MousageType::firstOrCreate(array(
          'key' => 'fastpass1',
          'value' => "Remember to make your FastPass+ reservation for %experience at %park as soon as you are able to!",
          'isChecklistTask' => true,
          'milestone' => 60
        ));

        MousageType::firstOrCreate(array(
          'key' => 'missingpark2',
          'value' => "You're within 15 minutes of this park, are you sure you don't want to add it to your schedule?",
          'isChecklistTask' => false,
          'milestone' => 0
        ));

        MousageType::firstOrCreate(array(
          'key' => 'fantasmic',
          'value' => "Plan for the last Fantasmic show of the evening to beat the crowds and maximize your time in the park. You might want to purchase a Fantasmic Dinner Show Package to get priority seating at no extra cost! Make those reservations here. (URL = https://disneyworld.disney.go.com/dining/hollywood-studios/fantasmic-dining-package/) ",
          'isChecklistTask' => false,
          'milestone' => 0
        ));

        MousageType::firstOrCreate(array(
          'key' => 'crowd2',
          'value' => "Looks like crowd levels are gonna be high on this trip. Visit our planning tools for touring strategies to maximize your fun.",
          'isChecklistTask' => false,
          'milestone' => 0
        ));

        MousageType::firstOrCreate(array(
          'key' => 'epcot2',
          'value' => "Crowds will be heavy tonight and restaurants will book up quickly. Make dining reservations as early as possible.",
          'isChecklistTask' => false,
          'milestone' => 0
        ));

        MousageType::firstOrCreate(array(
          'key' => 'parkhopper',
          'value' => "Looks like you'll need to purchase a Park Hopper if you want to do as many parks as you have scheduled. Without one you can only enter one park a day.",
          'isChecklistTask' => true,
          'milestone' => 180
        ));

        MousageType::firstOrCreate(array(
          'key' => 'orphanFP+',
          'value' => "Looks like you have a FastPass+ reservation for %experience (%reservation) but you do not have %park scheduled.",
          'isChecklistTask' => false,
          'milestone' => 0
        ));

        MousageType::firstOrCreate(array(
          'key' => 'lowRankPark',
          'value' => "Are you sure you don't want to rate %park higher? There are experiences there that you won't want to miss!",
          'isChecklistTask' => false,
          'milestone' => 0
        ));

        MousageType::firstOrCreate(array(
          'key' => 'waterParkPass',
          'value' => "This MousePlan requires a Water Parks & More pass, remember to add it to your Park Pass or remove it from the schedule.",
          'isChecklistTask' => true,
          'milestone' => 180
        ));

        MousageType::firstOrCreate(array(
          'key' => 'epcot3',
          'value' => "Both Epcot and Hollywood Studios are within walking distance of your resort! Be sure and get a Park Hopper to enjoy dining at Epcot in the evenings after visiting other parks, or to board the monorail from Epcot to the Magic Kingdom.",
          'isChecklistTask' => true,
          'milestone' => 30
        ));

        MousageType::firstOrCreate(array(
          'key' => 'missingpark5',
          'value' => "Your resort is so close to %park. Are you sure you don't want to add it to your schedule?",
          'isChecklistTask' => false,
          'milestone' => 0
        ));

        MousageType::firstOrCreate(array(
          'key' => 'disneySpa',
          'value' => "Magically unwind at a Disney Senses spa. Available at some Disney Deluxe Resorts. Call 407-WDW-SPAS to book an appointment.",
          'isChecklistTask' => true,
          'milestone' => 180
        ));

        MousageType::firstOrCreate(array(
          'key' => 'disneyGolf',
          'value' => "Tee times are available at one of Disney's World-Class golf courses. Call (407) WDW-GOLF to reserve or visit http://golfwdw.com/ to learn more.",
          'isChecklistTask' => true,
          'milestone' => 180
        ));

        MousageType::firstOrCreate(array(
          'key' => 'pirateLeague',
          'value' => "If a pirate's life's for you check out the Pirate League in the Magic Kingdom where you can transform into a pirate, empress or mermaid. Call (407) WDW-CREW to book a reservation in advance.",
          'isChecklistTask' => true,
          'milestone' => 180
        ));

        MousageType::firstOrCreate(array(
          'key' => 'princessMakeover',
          'value' => "Indulge your little princess in a royal makeover at the Bibiddy Bobbity Botique in Magic Kingdom or Disney Springs. Appointments go fast so book as soon as 180 days before your trip by calling (407) WDW-STYLE.",
          'isChecklistTask' => true,
          'milestone' => 180
        ));

        MousageType::firstOrCreate(array(
          'key' => 'wildernessXplorer',
          'value' => "We think you'll really enjoy signing your kiddo up to be a Wilderness Explorer in Animal Kingdom! Complete challenges and earn adventure badges as you work your way around the Park. And it's free!",
          'isChecklistTask' => true,
          'milestone' => 60
        ));

        MousageType::firstOrCreate(array(
          'key' => 'parkhopper2',
          'value' => "Your Park Hopper will help you enjoy smaller crowds during Extra Magic Hours (EMH) but be sure and \"hop out\" when magic hours end so you don't get caught in swelling crowds.",
          'isChecklistTask' => false,
          'milestone' => 0
        ));

        MousageType::firstOrCreate(array(
          'key' => 'parkhopper3',
          'value' => "Make sure and avoid Parks on the days they have Extra Magic Hours, which are only available for Disney resort guests. Parks will likely fill up during the time you'll be visiting.",
          'isChecklistTask' => false,
          'milestone' => 0
        ));

        MousageType::firstOrCreate(array(
          'key' => 'candlelightSE',
          'value' => "Get reserved seating at Epcot's Candlelight Processional when you book a special candlelight dining package.",
          'isChecklistTask' => true,
          'milestone' => 180
        ));

        MousageType::firstOrCreate(array(
          'key' => 'purchaseSE',
          'value' => "To purchase tickets for %sename go to https://disneyworld.disney.go.com/tickets/events/",
          'isChecklistTask' => true,
          'milestone' => 60
        ));

        MousageType::firstOrCreate(array(
          'key' => 'foodSE',
          'value' => "Be sure and enjoy the Epcot Food & Wine Festival while you're there! Celebrity chefs, culinary treats and cocktails await! Plan lunch or dinner sampling global culinary creations in World Showcase. No reservations needed!",
          'isChecklistTask' => false,
          'milestone' => 0
        ));

        MousageType::firstOrCreate(array(
          'key' => 'flowerSE',
          'value' => "Be sure and enjoy the Epcot International Flower and Garden Festival while you're there! Plan lunch or dinner sampling global culinary creations in World Showcase. No reservations needed!",
          'isChecklistTask' => false,
          'milestone' => 0
        ));

        MousageType::firstOrCreate(array(
            'key' => 'advance',
            'value' => "Planning this far in advance of your trip is a great idea to ensure availability at Disney resorts. Just remember that park schedules won't be available until %x date. Please check back then to finalize your MousePlan before making dining and Fastpass+ reservations. Until then, you can explore MousePlanner, learn more about Disney vacations, and book your resort, if you haven't already.",
            'isChecklistTask' => false,
            'milestone' => 0
        ));
    }
}
