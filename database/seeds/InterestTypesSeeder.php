<?php

use Illuminate\Database\Seeder;

class InterestTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('interest_types')->delete();

      DB::table('interest_types')->insert(['value' => 5, 'name' => 'Animal Encounters']);
      DB::table('interest_types')->insert(['value' => 9, 'name' => 'Stage Shows']);
      DB::table('interest_types')->insert(['value' => 12, 'name' => 'Behind the scenes']);
      DB::table('interest_types')->insert(['value' => 2, 'name' => 'Character meet & greets']);
      DB::table('interest_types')->insert(['value' => 6, 'name' => 'Technology']);
      DB::table('interest_types')->insert(['value' => 7, 'name' => 'Travel & Culture']);
      DB::table('interest_types')->insert(['value' => 8, 'name' => 'Food & Drink']);
      DB::table('interest_types')->insert(['value' => 10, 'name' => 'Water Rides']);
      DB::table('interest_types')->insert(['value' => 11, 'name' => 'Educational']);
      DB::table('interest_types')->insert(['value' => 13, 'name' => 'Relaxation']);
      DB::table('interest_types')->insert(['value' => 14, 'name' => 'Golf']);
      DB::table('interest_types')->insert(['value' => 15, 'name' => 'Shopping']);
    }
}
