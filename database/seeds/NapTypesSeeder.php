<?php

use Illuminate\Database\Seeder;

class NapTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('nap_types')->insert(['value' => 1, 'name' => 'Love them! Everday', 'description' => '', 'img' => '']);
      DB::table('nap_types')->insert(['value' => 2, 'name' => 'Sure! if we can fit them in', 'description' => '', 'img' => '']);
      DB::table('nap_types')->insert(['value' => 3, 'name' => 'No naps!', 'description' => '', 'img' => '']);

    }
}
