<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //config data
        $this->call(ParkSeeder::class);
        $this->call(AccessibilityTypeSeeder::class);
        $this->call(AccessibilitiesChanges::class);
        $this->call(BookedPlaceTypesSeeder::class);
        $this->call(BudgetTypesSeeder::class);
        $this->call(CelebrationTypesSeeder::class);
        $this->call(ConfigSeeder::class);
        $this->call(ConfigFiltersUpdateSeeder::class);
        $this->call(ConfigBlockUpdateSeeder::class);
        $this->call(CustomFacetsSeeder::class);
        $this->call(DiningPlanTypesSeeder::class);
        $this->call(ModifyDiningAnswers::class);
        $this->call(DiningPreferenceTypesSeeder::class);
        $this->call(ExperienceExceptionsSeeder::class);
        $this->call(ExperienceDefaultsSeeder::class);
        $this->call(ParksExceptionsSeeder::class);
        $this->call(CharactersSeeder::class);
        $this->call(InterestTypesSeeder::class);
        $this->call(InterestVisitParkTypesSeeder::class);
        $this->call(InterestWaterParkTypesSeeder::class);
        $this->call(NapTypesSeeder::class);
        $this->call(PaceTypesSeeder::class);
        $this->call(PrefixTitleTypesSeeder::class);
        $this->call(ThrillLevelTypeSeeder::class);
        $this->call(TimeFlexibilityTypesSeeder::class);
        $this->call(TravelerHeightsSeeder::class);
        $this->call(BlockTypesTableSeeder::class);
        $this->call(BlockTypesImagesSeeder::class);
        $this->call(ResortsTableSeeder::class);
        $this->call(CrowdLevelsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(GeneralOperatingHoursTableSeeder::class);
        $this->call(MousageTypesTableSeeder::class);
        $this->call(AddMoreMousagesTypes::class);
        $this->call(RestaurantTableSeeder::class);
        $this->call(OrlandoHotelsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(MapAccessibilityTypesSeeder::class);
        $this->call(MapAgesSeeder::class);
        $this->call(MapFavoriteCharactersTypesSeeder::class);
        $this->call(MapTravelerHeightsSeeder::class);
        $this->call(MapThrillLevelsSeeder::class);
        $this->call(MapInterestsSeeder::class);
        $this->call(MapParksSeeder::class);
        $this->call(UpdatePaceSeeder::class);
        $this->call(ExperiencesTableSeeder::class);

        //Testing data
        if (App::environment('local')) {
          $this->call(TripsTableSeeder::class);
          $this->call(VacationSeeder::class);
          $this->call(MousePlansTableSeeder::class);
          $this->call(DaysTableSeeder::class);
          $this->call(BlocksTableSeeder::class);
          $this->call(VacationParkTableSeeder::class);
          $this->call(TravelerTableSeeder::class);
          $this->call(InterestsVacationTableSeeder::class);
          $this->call(ExperiencesVacationTableSeeder::class);
          $this->call(CharactersVacationTableSeeder::class);
          $this->call(TravelerVacationTableSeeder::class);
          $this->call(MousagesTableSeeder::class);
          $this->call(ChecklistTableSeeder::class);
        }
    }
}
