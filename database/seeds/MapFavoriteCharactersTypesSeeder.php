<?php

use Illuminate\Database\Seeder;

class MapFavoriteCharactersTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $MickeyPalsId = DB::table('characters')->where('name', 'Mickey & Pals')->first()->id;
      $DisneyClassicsId = DB::table('characters')->where('name', 'Disney Classics')->first()->id;
      $PrincessesId = DB::table('characters')->where('name', 'Princesses')->first()->id;
      $PixarPalsId = DB::table('characters')->where('name', 'Pixar Pals')->first()->id;
      $StarWarsId = DB::table('characters')->where('name', 'Star Wars')->first()->id;
      $DisneyJrId = DB::table('characters')->where('name', 'Disney Jr.')->first()->id;
      $PiratesId = DB::table('characters')->where('name', 'Pirates')->first()->id;

      DB::table('map_characters')->insert(['character_id' => $MickeyPalsId, 'disneyFavoriteCharactersTypeId' => 17043511, 'disneyFavoriteCharactersTypeValue' => 'Mickey & Friends', 'weight' => 1]);
      DB::table('map_characters')->insert(['character_id' => $DisneyClassicsId, 'disneyFavoriteCharactersTypeId' => 16726803, 'disneyFavoriteCharactersTypeValue' => 'Classic Characters', 'weight' => 1]);
      DB::table('map_characters')->insert(['character_id' => $DisneyClassicsId, 'disneyFavoriteCharactersTypeId' => 16669983, 'disneyFavoriteCharactersTypeValue' => 'Classics', 'weight' => 1]);
      DB::table('map_characters')->insert(['character_id' => $DisneyClassicsId, 'disneyFavoriteCharactersTypeId' => 16669983, 'disneyFavoriteCharactersTypeValue' => 'Disney Classics', 'weight' => 0]);
      DB::table('map_characters')->insert(['character_id' => $PrincessesId, 'disneyFavoriteCharactersTypeId' => 17043508, 'disneyFavoriteCharactersTypeValue' => 'Disney Princesses', 'weight' => 1]);
      DB::table('map_characters')->insert(['character_id' => $PrincessesId, 'disneyFavoriteCharactersTypeId' => 18389385, 'disneyFavoriteCharactersTypeValue' => 'Frozen', 'weight' => 1]);
      DB::table('map_characters')->insert(['character_id' => $PixarPalsId, 'disneyFavoriteCharactersTypeId' => 16726938, 'disneyFavoriteCharactersTypeValue' => 'Pixar Pals', 'weight' => 1]);
      DB::table('map_characters')->insert(['character_id' => $StarWarsId, 'disneyFavoriteCharactersTypeId' => 18375673, 'disneyFavoriteCharactersTypeValue' => 'Star Wars', 'weight' => 1]);
      DB::table('map_characters')->insert(['character_id' => $DisneyJrId, 'disneyFavoriteCharactersTypeId' => 16726810, 'disneyFavoriteCharactersTypeValue' => 'Disney Channel', 'weight' => 1]);
      DB::table('map_characters')->insert(['character_id' => $PiratesId, 'disneyFavoriteCharactersTypeId' => 0, 'disneyFavoriteCharactersTypeValue' => 'Pirates', 'weight' => 0]);

    }
}
