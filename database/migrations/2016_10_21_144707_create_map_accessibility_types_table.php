<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMapAccessibilityTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('map_accessibility_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('accessibility_type_id')->unsigned();
            $table->integer('disneyAccessibilityTypeId');
            $table->string('disneyAccessibilityTypeValue');
            $table->timestamps();

            $table->foreign('accessibility_type_id')
            ->references('id')
            ->on('accessibility_types')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('map_accessibility_types', function ($table) {
            $table->dropForeign('map_accessibility_types_accessibility_type_id_foreign');
        });
        Schema::dropIfExists('map_accessibility_types');
    }
}
