<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccessibilityTypeTravelerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accessibility_type_traveler', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('traveler_id')->unsigned();
            $table->integer('accessibility_type_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('user_id')
              ->references('id')
              ->on('users')
              ->onDelete('set null');

            $table->foreign('traveler_id')
              ->references('id')
              ->on('travelers')
              ->onDelete('cascade');

            $table->foreign('accessibility_type_id')
              ->references('id')
              ->on('accessibility_types')
              ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('accessibility_type_traveler', function ($table) {
        $table->dropForeign('accessibility_type_traveler_user_id_foreign');
        $table->dropForeign('accessibility_type_traveler_traveler_id_foreign');
        $table->dropForeign('accessibility_type_traveler_accessibility_type_id_foreign');
      });
      Schema::dropIfExists('accessibility_type_traveler');
    }
}
