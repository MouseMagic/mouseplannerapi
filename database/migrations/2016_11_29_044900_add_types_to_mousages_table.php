<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypesToMousagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mousages', function (Blueprint $table) {
            // associate with mousage type
            $table->integer('mousage_type_id')->unsigned()->nullable();
            $table->foreign('mousage_type_id')
              ->references('id')
              ->on('mousage_types')
              ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mousages', function (Blueprint $table) {
            $table->dropForeign('mousages_mousage_type_id_foreign');
        });
    }
}
