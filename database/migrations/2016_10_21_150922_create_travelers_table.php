<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTravelersTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('travelers', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('vacation_id')->unsigned();
      $table->integer('prefix_title_type_id')->unsigned()->nullable();
      $table->string('firstname');
      $table->string('lastname');
      $table->integer('monthOfBirth');
      $table->integer('yearOfBirth');
      $table->integer('traveler_height_id')->unsigned()->nullable();
      $table->timestamps();

      $table->foreign('vacation_id')
      ->references('id')
      ->on('vacations')
      ->onDelete('cascade');

      $table->foreign('prefix_title_type_id')
      ->references('id')
      ->on('prefix_title_types')
      ->onDelete('set null');

      $table->foreign('traveler_height_id')
      ->references('id')
      ->on('traveler_heights')
      ->onDelete('set null');
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::table('travelers', function ($table) {
      $table->dropForeign('travelers_vacation_id_foreign');
      $table->dropForeign('travelers_prefix_title_type_id_foreign');
      $table->dropForeign('travelers_traveler_height_id_foreign');
    });
    Schema::dropIfExists('travelers');
  }
}
