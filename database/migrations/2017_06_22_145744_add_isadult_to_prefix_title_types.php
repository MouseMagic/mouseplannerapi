<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsadultToPrefixTitleTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('prefix_title_types', function (Blueprint $table) {
            $table->boolean('is_adult')->nullable();
        });

        Schema::table('prefix_title_types', function ($table) {
            $table->integer('is_adult')->nullable(false)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('prefix_title_types', function (Blueprint $table) {
            $table->dropColumn('is_adult');
        });
    }
}
