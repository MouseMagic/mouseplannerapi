<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExperiencesToFastpassReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('fastpass_reservations', function (Blueprint $table) {
            $table->integer('experience_id')->unsigned()->nullable();
            $table->foreign('experience_id')
              ->references('id')
              ->on('experiences')
              ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('fastpass_reservations', function (Blueprint $table) {
            $table->dropForeign('fastpass_reservations_experience_id_foreign');
            $table->dropColumn('experience_id');
        });
    }
}
