<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCelebrationVacationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('celebration_vacation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vacation_id')->unsigned();
            $table->integer('celebration_type_id')->unsigned()->nullable();
            $table->timestamps();

          $table->foreign('vacation_id')
            ->references('id')
            ->on('vacations')
            ->onDelete('cascade');

          $table->foreign('celebration_type_id')
            ->references('id')
            ->on('celebration_types')
            ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('celebration_vacation', function (Blueprint $table) {
            $table->dropForeign('celebration_vacation_vacation_id_foreign');
            $table->dropForeign('celebration_vacation_celebration_type_id_foreign');
        });
        Schema::dropIfExists('celebration_vacation');
    }
}
