<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('blocks', function (Blueprint $table) {
         $table->increments('id');
         $table->integer('day_id')->unsigned();
         $table->integer('block_type_id')->unsigned()->nullable();
         $table->string('name');
         $table->string('location')->nullable();
         $table->datetime('startTime')->nullable();
         $table->datetime('endTime')->nullable();
         $table->boolean('anchored')->default(false);
         $table->integer('association_id')->unsigned()->nullable();
         $table->timestamps();

         $table->foreign('day_id')
         ->references('id')
         ->on('days')
         ->onDelete('cascade');

         $table->foreign('block_type_id')
         ->references('id')
         ->on('block_types')
         ->onDelete('set null');
       });
     }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('blocks', function ($table) {
          $table->dropForeign('blocks_day_id_foreign');
          $table->dropForeign('blocks_block_type_id_foreign');
      });

      Schema::dropIfExists('blocks');
    }
}
