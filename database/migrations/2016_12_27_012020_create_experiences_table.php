<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('experiences', function (Blueprint $table) {
            $table->increments('id');
            $table->string('external_id');
            $table->integer('park_id')->unsigned()->nullable();
            $table->string('external_name')->nullable();
            $table->json('external_description')->nullable();
            $table->string('custom_description')->nullable();
            $table->json('external_facets')->nullable();
            $table->timestamps();
            $table->foreign('park_id')
              ->references('id')
              ->on('parks')
              ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('experiences', function (Blueprint $table) {
            $table->dropForeign('experiences_park_id_foreign');
            $table->dropColumn('park_id');
        });
        Schema::dropIfExists('experiences');
    }
}
