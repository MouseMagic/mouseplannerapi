<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVacationExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacation_experiences', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vacation_id')->unsigned();
            $table->integer('hearts');
            $table->timestamps();

            $table->foreign('vacation_id')
            ->references('id')
            ->on('vacations')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('vacation_experiences', function (Blueprint $table) {
        $table->dropForeign('vacation_experiences_vacation_id_foreign');
      });
      Schema::dropIfExists('vacation_experiences');
    }
}
