<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTravelerVacationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('traveler_vacation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vacation_id')->unsigned();
            $table->integer('traveler_id')->unsigned();
            $table->timestamps();

            $table->foreign('vacation_id')
              ->references('id')
              ->on('vacations')
              ->onDelete('cascade');

            $table->foreign('traveler_id')
              ->references('id')
              ->on('travelers')
              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('traveler_vacation', function ($table) {
          $table->dropForeign('traveler_vacation_vacation_id_foreign');
          $table->dropForeign('traveler_vacation_traveler_id_foreign');
        });
        Schema::dropIfExists('traveler_vacation');
    }
}
