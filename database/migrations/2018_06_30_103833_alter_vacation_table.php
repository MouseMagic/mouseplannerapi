<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterVacationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('vacations')){
            Schema::table('vacations', function (Blueprint $table){
                $table->integer('adultsValue')->nullable();
                $table->integer('kidsTenAndUpValue')->nullable();
                $table->integer('kidsNineAndUnderValue')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('vacations', 'adultsValue') || 
            Schema::hasColumn('vacations', 'kidsTenAndUpValue') || 
            Schema::hasColumn('vacations', 'kidsNineAndUnderValue'))
        {
            Schema::table('vacations', function($table) {
                $table->dropColumn(['adultsValue', 'kidsTenAndUpValue', 'kidsNineAndUnderValue']);
            });
        }
    }
}
