<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMapFavoriteCharactersTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('map_characters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('character_id')->unsigned();
            $table->integer('disneyFavoriteCharactersTypeId');
            $table->string('disneyFavoriteCharactersTypeValue');
            $table->integer('weight');
            $table->timestamps();

            $table->foreign('character_id')
            ->references('id')
            ->on('characters')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('map_characters', function ($table) {
            $table->dropForeign('map_characters_character_id_foreign');
        });
        Schema::dropIfExists('map_characters');
    }
}
