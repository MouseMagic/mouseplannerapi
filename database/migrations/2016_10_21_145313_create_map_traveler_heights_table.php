<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMapTravelerHeightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('map_traveler_heights', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('traveler_height_id')->unsigned();
            $table->integer('disneyHeightId');
            $table->string('disneyHeightValue');
            $table->timestamps();

            $table->foreign('traveler_height_id')
            ->references('id')
            ->on('traveler_heights')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('map_traveler_heights', function ($table) {
            $table->dropForeign('map_traveler_heights_traveler_height_id_foreign');
        });
        Schema::dropIfExists('map_traveler_heights');
    }
}
