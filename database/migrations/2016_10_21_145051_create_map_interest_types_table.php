<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMapInterestTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('map_interest_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('interest_type_id')->unsigned();
            $table->integer('disneyInterestTypeId');
            $table->string('disneyInterestTypeValue');
            $table->integer('weight');
            $table->timestamps();

            $table->foreign('interest_type_id')
            ->references('id')
            ->on('interest_types')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('map_interest_types', function ($table) {
            $table->dropForeign('map_interest_types_interest_type_id_foreign');
        });
        Schema::dropIfExists('map_interest_types');
    }
}
