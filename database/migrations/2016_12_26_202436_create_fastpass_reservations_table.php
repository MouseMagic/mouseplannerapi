<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFastpassReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('fastpass_reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('day_id')->unsigned();
            $table->integer('block_id')->unsigned()->nullable();
            $table->datetime('reservation_time')->nullable();
            $table->timestamps();

            $table->foreign('day_id')
              ->references('id')
              ->on('days')
              ->onDelete('cascade');

              $table->foreign('block_id')
                ->references('id')
                ->on('blocks')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('fastpass_reservations', function (Blueprint $table) {
            $table->dropForeign('fastpass_reservations_day_id_foreign');
            $table->dropColumn('day_id');
        });
        Schema::table('fastpass_reservations', function (Blueprint $table) {
            $table->dropForeign('fastpass_reservations_block_id_foreign');
            $table->dropColumn('block_id');
        });
        Schema::dropIfExists('fastpass_reservations');
    }
}
