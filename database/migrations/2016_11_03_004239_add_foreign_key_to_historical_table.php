<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyToHistoricalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('general_operating_hours', function (Blueprint $table) {
          $table->integer('crowd_level_id')->unsigned()->nullable();

          $table->foreign('crowd_level_id')
            ->references('id')
            ->on('crowd_levels')
            ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('general_operating_hours', function (Blueprint $table) {
            $table->dropForeign('general_operating_hours_crowd_level_id_foreign');
            $table->dropColumn('crowd_level_Id');
        });
    }
}
