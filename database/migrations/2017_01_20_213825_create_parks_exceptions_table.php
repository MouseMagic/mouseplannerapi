<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParksExceptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parks_exceptions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('park_id')->unsigned();
            $table->timestamps();

            $table->foreign('park_id')
              ->references('id')
              ->on('parks')
              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('parks_exceptions', function (Blueprint $table) {
            $table->dropForeign('parks_exceptions_park_id_foreign');
            $table->dropColumn('park_id');
        });
        Schema::dropIfExists('parks_exceptions');
    }
}
