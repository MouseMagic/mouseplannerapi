<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVacationsTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('vacations', function (Blueprint $table) {
      $table->increments('id');
      $table->string('name');
      $table->integer('stepNumber')->default(1);
      $table->integer('highestStepCompleted')->default(1);
      $table->datetime('startTravelDate')->nullable();
      $table->datetime('endTravelDate')->nullable();
      $table->boolean('isFlying')->nullable();
      $table->datetime('departureDate')->nullable();
      $table->datetime('arrivalDate')->nullable();
      $table->integer('booked_place_type_id')->unsigned()->nullable();
      $table->integer('resort_id')->unsigned()->nullable();
      $table->boolean('haveDisneyPackage')->nullable();
      $table->integer('visitDays')->nullable();
      $table->integer('interest_visit_park_type_id')->unsigned()->nullable();
      $table->integer('interest_water_park_type_id')->unsigned()->nullable();
      $table->integer('dining_plan_type_id')->unsigned()->nullable();
      $table->string('hotelName')->nullable();
      $table->string('streetAddress')->nullable();
      $table->string('zipCode')->nullable();
      $table->boolean('haveCar')->nullable();
      $table->integer('havePurchasedParkTicket_id')->nullable();
      $table->string('phone')->nullable();
      $table->integer('thrill_level_type_id')->unsigned()->nullable();
      $table->integer('pace_type_id')->unsigned()->nullable();
      $table->integer('budget_type_id')->unsigned()->nullable();
      $table->datetime('beginDayTime')->nullable();
      $table->datetime('endDayTime')->nullable();
      $table->integer('time_flexibility_type_id')->unsigned()->nullable();
      $table->integer('nap_type_id')->unsigned()->nullable();
      $table->timestamps();
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('vacations');
  }
}
