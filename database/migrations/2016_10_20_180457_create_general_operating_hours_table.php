<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneralOperatingHoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general_operating_hours', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('weekYear')->unsigned();
            $table->string('dayWeek');
            $table->string('park');
            $table->time('openTime');
            $table->time('closeTime');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('general_operating_hours');
    }
}
