<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExperiencesToVacexpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('vacation_experiences', function (Blueprint $table) {
            $table->integer('experience_id')->unsigned()->nullable();
            $table->foreign('experience_id')
            ->references('id')
            ->on('experiences')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('vacation_experiences', function (Blueprint $table) {
            $table->dropForeign('vacation_experiences_experience_id_foreign');
            $table->dropColumn('experience_id');
        });
    }
}
