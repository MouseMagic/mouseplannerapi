<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVacationParkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacation_park', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vacation_id')->unsigned();
            $table->integer('park_id')->unsigned()->nullable();
            $table->integer('hearts');
            $table->timestamps();

            $table->foreign('vacation_id')
              ->references('id')
              ->on('vacations')
              ->onDelete('cascade');

            $table->foreign('park_id')
              ->references('id')
              ->on('parks')
              ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('vacation_park', function ($table) {
        $table->dropForeign('vacation_park_vacation_id_foreign');
        $table->dropForeign('vacation_park_park_id_foreign');
      });
      Schema::dropIfExists('vacation_park');
    }
}
