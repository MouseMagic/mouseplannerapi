<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMapParksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('map_parks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('park_id')->unsigned();
            $table->integer('disneyParkInterestId');
            $table->string('disneyParkInterestValue');
            $table->timestamps();

            $table->foreign('park_id')
            ->references('id')
            ->on('parks')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('map_parks', function ($table) {
            $table->dropForeign('map_parks_park_id_foreign');
        });
        Schema::dropIfExists('map_parks');
    }
}
