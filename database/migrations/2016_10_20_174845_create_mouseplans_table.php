<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMousePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mouse_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vacation_id')->unsigned();
            $table->integer('tripScore')->unsigned()->default(0);
            $table->string('dashboardImg')->nullable();
            $table->timestamps();

            $table->foreign('vacation_id')
              ->references('id')
              ->on('vacations')
              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mouse_plans', function ($table) {
            $table->dropForeign('mouse_plans_vacation_id_foreign');
        });

        Schema::dropIfExists('mouse_plans');
    }
}
