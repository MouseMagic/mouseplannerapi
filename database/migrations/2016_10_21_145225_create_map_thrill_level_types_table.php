<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMapThrillLevelTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('map_thrill_level_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('thrill_level_type_id')->unsigned();
            $table->integer('disneyThrillLevelTypeId');
            $table->string('disneyThrillLevelTypeValue');
            $table->timestamps();

            $table->foreign('thrill_level_type_id')
            ->references('id')
            ->on('thrill_level_types')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('map_thrill_level_types', function ($table) {
            $table->dropForeign('map_thrill_level_types_thrill_level_type_id_foreign');
        });
        Schema::dropIfExists('map_thrill_level_types');
    }
}
