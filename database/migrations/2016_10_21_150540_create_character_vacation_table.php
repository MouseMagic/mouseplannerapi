<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCharacterVacationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('character_vacation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('vacation_id')->unsigned();
            $table->integer('character_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('user_id')
              ->references('id')
              ->on('users')
              ->onDelete('cascade');

            $table->foreign('vacation_id')
              ->references('id')
              ->on('vacations')
              ->onDelete('cascade');

            $table->foreign('character_id')
              ->references('id')
              ->on('characters')
              ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('character_vacation', function ($table) {
          $table->dropForeign('character_vacation_user_id_foreign');
          $table->dropForeign('character_vacation_vacation_id_foreign');
          $table->dropForeign('character_vacation_character_id_foreign');
        });
        Schema::dropIfExists('character_vacation');
    }
}
