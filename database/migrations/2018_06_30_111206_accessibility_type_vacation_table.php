<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AccessibilityTypeVacationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('accessibility_type_vacation')){
            Schema::create('accessibility_type_vacation', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('vacation_id')->unsigned();
                $table->integer('accessibility_type_id')->unsigned()->nullable();

                $table->foreign('vacation_id')->references('id')->on('vacations')->onDelete('cascade');
                $table->foreign('accessibility_type_id')->references('id')->on('accessibility_types')
                        ->onDelete('set null');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accessibility_type_vacation', function ($table) {
            $table->dropForeign('accessibility_type_vacation_vacation_id_foreign');
            $table->dropForeign('accessibility_type_vacation_accessibility_type_id_foreign');
        });
        Schema::dropIfExists('accessibility_type_vacation');
    }
}
