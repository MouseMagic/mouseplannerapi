<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVacationDiningPreferenceTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacation_dining_preference_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userId')->unsigned();
            $table->integer('vacationId')->unsigned();
            $table->integer('diningPreferenceTypeId')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('userId')
              ->references('id')
              ->on('users')
              ->onDelete('cascade');

            $table->foreign('vacationId')
              ->references('id')
              ->on('vacations')
              ->onDelete('cascade');

            $table->foreign('diningPreferenceTypeId')
              ->references('id')
              ->on('dining_preference_types')
              ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vacation_dining_preference_types');
    }
}
