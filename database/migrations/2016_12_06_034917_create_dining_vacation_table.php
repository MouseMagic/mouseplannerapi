<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiningVacationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dining_vacation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vacation_id')->unsigned();
            $table->integer('dining_preference_type_id')->unsigned()->nullable();
            $table->timestamps();

          $table->foreign('vacation_id')
            ->references('id')
            ->on('vacations')
            ->onDelete('cascade');

          $table->foreign('dining_preference_type_id')
            ->references('id')
            ->on('dining_preference_types')
            ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('dining_vacation', function (Blueprint $table) {
            $table->dropForeign('dining_vacation_vacation_id_foreign');
            $table->dropForeign('dining_vacation_dining_preference_type_id_foreign');
        });Schema::dropIfExists('dining_vacation');
    }
}
