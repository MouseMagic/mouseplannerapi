<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeingKeysToVacationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vacations', function (Blueprint $table) {
          $table->foreign('resort_id')
          ->references('id')
          ->on('resorts')
          ->onDelete('set null');

          $table->foreign('thrill_level_type_id')
          ->references('id')
          ->on('thrill_level_types')
          ->onDelete('set null');

          $table->foreign('booked_place_type_id')
          ->references('id')
          ->on('booked_place_types')
          ->onDelete('set null');

          $table->foreign('interest_visit_park_type_id')
          ->references('id')
          ->on('interest_visit_park_types')
          ->onDelete('set null');

          $table->foreign('interest_water_park_type_id')
          ->references('id')
          ->on('interest_water_park_types')
          ->onDelete('set null');

          $table->foreign('dining_plan_type_id')
          ->references('id')
          ->on('dining_plan_types')
          ->onDelete('set null');

          $table->foreign('pace_type_id')
          ->references('id')
          ->on('pace_types')
          ->onDelete('set null');

          $table->foreign('budget_type_id')
          ->references('id')
          ->on('budget_types')
          ->onDelete('set null');

          $table->foreign('time_flexibility_type_id')
          ->references('id')
          ->on('time_flexibility_types')
          ->onDelete('set null');

          $table->foreign('nap_type_id')
          ->references('id')
          ->on('nap_types')
          ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vacations', function (Blueprint $table) {
          $table->dropForeign('vacations_resort_id_foreign');
          $table->dropForeign('vacations_thrill_level_type_id_foreign');
          $table->dropForeign('vacations_booked_place_type_id_foreign');
          $table->dropForeign('vacations_interest_visit_park_type_id_foreign');
          $table->dropForeign('vacations_interest_water_park_type_id_foreign');
          $table->dropForeign('vacations_dining_plan_type_id_foreign');
          $table->dropForeign('vacations_pace_type_id_foreign');
          $table->dropForeign('vacations_budget_type_id_foreign');
          $table->dropForeign('vacations_time_flexibility_type_id_foreign');
          $table->dropForeign('vacations_nap_type_id_foreign');
        });
    }
}
