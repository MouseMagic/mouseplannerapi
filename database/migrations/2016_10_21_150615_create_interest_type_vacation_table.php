<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterestTypeVacationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interest_type_vacation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('vacation_id')->unsigned();
            $table->integer('interest_type_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('user_id')
              ->references('id')
              ->on('users')
              ->onDelete('cascade');

            $table->foreign('vacation_id')
              ->references('id')
              ->on('vacations')
              ->onDelete('cascade');

            $table->foreign('interest_type_id')
              ->references('id')
              ->on('interest_types')
              ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('interest_type_vacation', function ($table) {
        $table->dropForeign('interest_type_vacation_user_id_foreign');
        $table->dropForeign('interest_type_vacation_vacation_id_foreign');
        $table->dropForeign('interest_type_vacation_interest_type_id_foreign');
      });
      Schema::dropIfExists('interest_type_vacation');
    }
}
