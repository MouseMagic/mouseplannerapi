<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropAnchoredAtBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up() {
       Schema::table('blocks', function (Blueprint $table) {
           $table->dropColumn('anchored');
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down() {
       Schema::table('blocks', function (Blueprint $table) {
           $table->integer('anchored')->default(false);
       });
     }
}
