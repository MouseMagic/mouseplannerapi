<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTravelAgentsClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('travel_agents_clients', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('travelId')->unsigned();
            $table->integer('userId')->unsigned();
            $table->timestamps();

            $table->foreign('travelId')
              ->references('id')
              ->on('users')
              ->onDelete('cascade');

            $table->foreign('userId')
              ->references('id')
              ->on('users')
              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('travel_agents_clients', function ($table) {
          $table->dropForeign('travel_agents_clients_userid_foreign');
          $table->dropForeign('travel_agents_clients_travelid_foreign');
      });

      Schema::dropIfExists('travel_agents_clients');
    }
}
