<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVacationCelebrationTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacation_celebration_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('vacation_id')->unsigned();
            $table->integer('celebration_type_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('user_id')
              ->references('id')
              ->on('users')
              ->onDelete('cascade');

            $table->foreign('vacation_id')
              ->references('id')
              ->on('vacations')
              ->onDelete('cascade');

            $table->foreign('celebration_type_id')
              ->references('id')
              ->on('celebration_types')
              ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('vacation_celebration_types', function ($table) {
        $table->dropForeign('vacation_celebration_types_user_id_foreign');
        $table->dropForeign('vacation_celebration_types_vacation_id_foreign');
        $table->dropForeign('vacation_celebration_types_celebration_type_id_foreign');
      });
      Schema::dropIfExists('vacation_celebration_types');
    }
}
