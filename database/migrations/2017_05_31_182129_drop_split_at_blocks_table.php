<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropSplitAtBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
      Schema::table('blocks', function (Blueprint $table) {
          $table->dropColumn('split');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
      Schema::table('blocks', function (Blueprint $table) {
          $table->integer('split')->default(0);
      });
    }
}
