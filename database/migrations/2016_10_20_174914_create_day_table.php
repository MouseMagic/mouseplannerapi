<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('days', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mouse_plan_id')->unsigned();
            $table->datetime('date');
            $table->integer('day_number');
            $table->timestamps();

            $table->foreign('mouse_plan_id')
              ->references('id')
              ->on('mouse_plans')
              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('days', function ($table) {
          $table->dropForeign('days_mouse_plan_id_foreign');
      });

      Schema::dropIfExists('days');
    }
}
