<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Trip::class, function (Faker\Generator $faker) {
    return [
      'name' => $faker->words(3, true),
      'firstTrip' => false,
      'tripCount' => $faker->numberBetween(1, 10),
      'lastVisit' => $faker->dateTime,
    ];
});
