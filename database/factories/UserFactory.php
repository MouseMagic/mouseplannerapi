<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
      'role_id' => 1,
      'firstname' => $faker->firstName,
      'lastname' => $faker->lastName,
      'email' => $faker->unique()->safeEmail,
      'password' => bcrypt('secret'),
      'confirmed' => 1,
    ];
});
