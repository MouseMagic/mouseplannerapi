<?php

use Carbon\Carbon;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Vacation::class, function (Faker\Generator $faker) {
  $travelDate = $faker->dateTimeBetween('+1 days', '+6 months');
  return [
    'name' => $faker->sentence($nbWords = 6, $variableNbWords = true),
    'stepNumber' => $faker->numberBetween($min = 1, $max = 6),
    'highestStepCompleted' => 6,
    'startTravelDate' => $travelDate,
    'endTravelDate' => Carbon::instance($travelDate)->addDays(6),
    'isFlying' => 1,
    'departureDate' => $faker->dateTime,
    'arrivalDate' => $faker->dateTime,
    'booked_place_type_id' => $faker->numberBetween($min = 1, $max = 3),
    'resort_id' => $faker->numberBetween($min = 1, $max = 31),
    'visitDays' => $faker->numberBetween($min = 2, $max = 6),
    'interest_visit_park_type_id' => $faker->numberBetween($min = 1, $max = 3),
    'interest_water_park_type_id' => $faker->numberBetween($min = 1, $max = 3),
    'dining_plan_type_id' => $faker->numberBetween($min = 1, $max = 3),
    'thrill_level_type_id' => $faker->numberBetween($min = 1, $max = 3),
    'pace_type_id' => $faker->numberBetween($min = 1, $max = 3),
    'budget_type_id' => $faker->numberBetween($min = 1, $max = 3),
    'beginDayTime' => Carbon::today()->addHours(13),
    'endDayTime' => Carbon::tomorrow()->addHours(8),
    'time_flexibility_type_id' => $faker->numberBetween($min = 1, $max = 3),
    'nap_type_id' => $faker->numberBetween($min = 1, $max = 3),
  ];
});
